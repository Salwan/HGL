#pragma once

#include <iostream>
#include <sstream>
#include <string>
#include <exception>
#include <vector>
#include <list>
#include <memory>

#define _USE_MATH_DEFINES
#include <math.h>
#include <glm/glm.hpp>

#define GL_GLEXT_PROTOTYPES
#include <GLES2/gl2.h>
#include <EGL/egl.h>

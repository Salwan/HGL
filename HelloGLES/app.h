// App Module
// Handles native windows initialization, creation, and updating.
// Handles EGL initialization and context creation.

#pragma once

class CApp {
public:
	explicit CApp(const wchar_t* app_title, unsigned window_width = 1920, unsigned window_height = 1080);
	virtual ~CApp();

	void Run();
	void Quit() { bAppQuit = true; }

private:
	void _Init();
	void _InitWindow();
	void _InitEGL();

	// Windows
	void* _CreateWindow();

	// EGL

	std::wstring strAppTitle;
	unsigned uWinWidth;
	unsigned uWinHeight;
	bool bAppQuit;

	struct PIMPL;
	PIMPL* pNative;

// Statics
public:
	static CApp* GetInstance() { return pInstance; }

private:
	static CApp* pInstance;

};

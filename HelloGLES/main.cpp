#include "stdafx.h"

#include <Windows.h>
#include "utils.h"
#include "app.h"

static bool		bQuit		= false;
static unsigned uAppWidth	= 1920;
static unsigned uAppHeight	= 1080;
static LONGLONG qpcFrequency;
static double	dTimeDelta = 1.0;

// Window messages handles
LRESULT CALLBACK wndProc(HWND hwnd, unsigned int msg, WPARAM wParam, LPARAM lParam) {
	switch(msg) {
		case WM_SYSCOMMAND: {
			switch(wParam) {
				case SC_SCREENSAVE:
				case SC_MONITORPOWER:
					return 0;
					break;
			}
			break;
		}
		case WM_CLOSE: {
			bQuit = true;
			DestroyWindow(hwnd);
			PostQuitMessage(0);
			return 0;
			break;
		}
		case WM_SIZE: {
			uAppWidth = LOWORD(lParam);
			uAppHeight = HIWORD(lParam);
			break;
		}
		case WM_KEYUP:
			if(wParam == VK_ESCAPE) {
				bQuit = true;
				return 0;
			}
			break;
		default:
			break;
	}
	return DefWindowProc(hwnd, msg, wParam, lParam);
}

// Window creation
// returns: window handle
HWND createWindow(unsigned width, unsigned height) {
	HINSTANCE hInstance = GetModuleHandle(NULL);
	WNDCLASSEX wc;
	{
		wc.cbSize = sizeof(WNDCLASSEX);
		wc.style = CS_OWNDC;
		wc.lpfnWndProc = &DefWindowProc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = 0;
		wc.hInstance = hInstance;
		wc.hIcon = NULL;
		wc.hCursor = LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground = static_cast<HBRUSH>(GetStockObject(BLACK_BRUSH));
		wc.lpszMenuName = NULL;
		wc.lpszClassName = L"HelloGLES";
		wc.hIconSm = NULL;
		wc.lpfnWndProc = wndProc;
	}
	HRESULT hr = RegisterClassEx(&wc);
	if(SUCCEEDED(hr)) {
		std::cout << "Window registration -> success" << std::endl;
	} else {
		std::cout << "Window registration -> failure" << std::endl;
		throw new std::exception("RegisterClassEx() failed");
	}
	RECT rect = {0, 0, static_cast<long>(width), static_cast<long>(height)};
	int style = WS_BORDER | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME;
	AdjustWindowRect(&rect, style, false);
	
	HWND hwnd = CreateWindow(L"HelloGLES", L"EGL and OpenGL ES 2.0: BunnyMark", style, CW_USEDEFAULT, CW_USEDEFAULT, 
		rect.right - rect.left, rect.bottom - rect.top, NULL, NULL, GetModuleHandle(NULL), NULL);
	if(!hwnd) {
		std::cout << "Window creation -> failure" << std::endl;
		throw new std::exception("CreateWindow() failed");
	} else {
		std::cout << "Window creation -> success" << std::endl;
	}
	ShowWindow(hwnd, SW_SHOW);
	// MessageBox(window, content, title, MB_OK | MB_ICONEXCMLAMATION);
	return hwnd;
}

// Initialize EGL
// returns: true for success
bool createEGLDisplay(EGLDisplay &egl_display, HDC hdc, EGLint &ver_major, EGLint &ver_minor) {
	// EGL Get Display
	egl_display = eglGetDisplay(hdc);
	if(egl_display == EGL_NO_DISPLAY) {
		egl_display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
		if(egl_display == EGL_NO_DISPLAY) {
			std::cout << "eglGetDisplay() returned no display" << std::endl;
			return false;
		}
	}
	// EGL Initialize
	EGLBoolean result = eglInitialize(egl_display, &ver_major, &ver_minor);
	if(result == EGL_FALSE) {
		std::cout << "eglInitialize() failed" << std::endl;
		return false;
	} else {
		std::cout << "EGL initialized" << std::endl;
	}
	return true;
}

bool chooseEGLConfig(EGLDisplay &egl_display, EGLConfig &egl_config) {
	const EGLint config_attribs[] = {
		EGL_SURFACE_TYPE,		EGL_WINDOW_BIT,
		EGL_RENDERABLE_TYPE,	EGL_OPENGL_ES2_BIT,
		EGL_NONE
	};
	EGLint configs_count = 0;
	EGLBoolean result = eglChooseConfig(egl_display, config_attribs, &egl_config, 1, &configs_count);
	if(result == EGL_FALSE) {
		std::cout << "eglChooseConfig() failed" << std::endl;
		return false;
	} else {
		std::cout << "EGL config ready" << std::endl;
	}
	return true;
}

bool createEGLSurface(HWND hwnd, EGLDisplay &egl_display, EGLConfig &egl_config, EGLSurface &egl_surface) {
	egl_surface = eglCreateWindowSurface(egl_display, egl_config, hwnd, NULL);
	if(egl_surface == EGL_NO_SURFACE) {
		std::cout << "Failed to create surface" << std::endl;
		// attempt creating a non-window surface
		eglGetError();
		egl_surface = eglCreateWindowSurface(egl_display, egl_config, NULL, NULL);
		if(egl_surface == EGL_NO_SURFACE) {
			std::cout << "Could not create EGL surface: " << eglGetError() << std::endl;
			return false;
		} else {
			std::cout << "Non-window surface created (native window passed as null)" << std::endl;
		}
	} else {
		std::cout << "EGL surface ready" << std::endl;
	}
	EGLint last_err = eglGetError();
	if(last_err != EGL_SUCCESS) {
		std::cout << "Something went wrong while creating window surface: " << last_err << std::endl;
		return false;
	}
	return true;
}

bool setupEGLContext(EGLDisplay &egl_display, EGLConfig &egl_config, EGLSurface &egl_surface, EGLContext &egl_context) {
	EGLBoolean result;

	result = eglBindAPI(EGL_OPENGL_ES_API);
	if(result == EGL_FALSE) {
		std::cout << "eglBindAPI() failed, error code = " << eglGetError() << std::endl;
		return false;
	} else if(result == EGL_BAD_PARAMETER) {
		std::cout << "eglBindAPI() returned BAD_PARAMETER" << std::endl;
		return false;
	} else {
		std::cout << "OpenGL ES API bound" << std::endl;
	}

	EGLint context_attribs[] = {
		EGL_CONTEXT_CLIENT_VERSION, 2,
		EGL_NONE
	};
	egl_context = eglCreateContext(egl_display, egl_config, NULL, context_attribs);
	if(egl_context == EGL_NO_CONTEXT) {
		std::cout << "eglCreateContext() failed" << std::endl;
		return false;
	}
	std::cout << "EGL context ready" << std::endl;

	result = eglMakeCurrent(egl_display, egl_surface, egl_surface, egl_context);
	if(result != EGL_TRUE) {
		std::cout << "eglMakeCurrent() failed, EGL error: " << eglGetError() << std::endl;
		return false;
	}

	std::cout << "EGL Version   = " << eglQueryString(egl_display, EGL_VERSION) << std::endl;
	std::cout << "EGL Vendor    = " << eglQueryString(egl_display, EGL_VENDOR) << std::endl;
	std::cout << "EGL Client APIs: " << std::endl << "  " << eglQueryString(egl_display, EGL_CLIENT_APIS) << std::endl;
	std::vector<std::string> v_extensions = Utils::SplitExtensions(eglQueryString(egl_display, EGL_EXTENSIONS));
	std::cout << "EGL Extensions: " << std::endl;
	for(unsigned i = 0; i < v_extensions.size(); ++i) {
		std::cout << "  " << v_extensions[i] << std::endl;
	}
	return true;
}

bool initEGL(HWND hwnd, HDC hdc, EGLDisplay &egl_display, EGLSurface &egl_surface) {
	EGLint ver_major, ver_minor;
	EGLConfig egl_config;
	EGLContext egl_context;
	bool result = true;

	result = createEGLDisplay(egl_display, hdc, ver_major, ver_minor);
	if(!result) {
		std::cout << "createEGLDisplay() failed." << std::endl;
		return false;
	}

	result = chooseEGLConfig(egl_display, egl_config);
	if(!result) {
		std::cout << "chooseEGLConfig() failed" << std::endl;
		return false;
	}

	result = createEGLSurface(hwnd, egl_display, egl_config, egl_surface);
	if(!result) {
		std::cout << "createEGLSurface() failed" << std::endl;
		return false;
	}

	result = setupEGLContext(egl_display, egl_config, egl_surface, egl_context);
	if(!result) {
		std::cout << "setupEGLContext() failed" << std::endl;
		return false;
	}

	return result;
}

void logGLESInfo() {
	std::cout << "OpenGL ES Info: " << std::endl;
	std::cout << "  Vendor = " << glGetString(GL_VENDOR) << std::endl;
	std::cout << "  Renderer = " << glGetString(GL_RENDERER) << std::endl;
	std::cout << "  Version = " << glGetString(GL_VERSION) << std::endl;
	std::cout << "  Shader Language Version = " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
	std::cout << "Extensions: " << std::endl;
	std::string ext;
	std::stringstream ext_stream;
	ext_stream << glGetString(GL_EXTENSIONS);
	while(ext_stream >> ext) {
		std::cout << "  " << ext << std::endl;
	}
}

void initScene() {
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.0f, 1.0f, 0.5f, 1.0f);
	glViewport(0, 0, uAppWidth, uAppHeight);
}

void runScene(double) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void endScene() {
	glFlush();
}

// HelloGLES: OpenGL ES 2.0 and EGL BunnyMark!
int main(int, const TCHAR* []) 
{
	std::shared_ptr<CApp> app(new CApp(L"EGL & OpenGL ES 2.0 BunnyMark"));
	app->Run();

	/*
	EGLDisplay eglDisplay;
	EGLSurface eglSurface;

	std::cout << "Initializing EGL..." << std::endl;
	{
		bool result = initEGL(hwnd, hdc, eglDisplay, eglSurface);
		if(!result) {
			std::cout << "Failed to initialize EGL" << std::endl;
			std::cout << std::endl << "<Enter> to end application ";
			std::cin.get();
			return 1;
		} else {
			std::cout << "EGL context activation success!" << std::endl << std::endl;
		}
	}

	logGLESInfo();
	std::cout << "OpenGL ES 2.0 is ready." << std::endl;

	LONGLONG qpc_start, qpc_end;
	QueryPerformanceCounter((LARGE_INTEGER*)&qpcFrequency);

	initScene();

	MSG msg;
	PeekMessage(&msg, NULL, 0, 0, PM_REMOVE);

	while(!bQuit) {
		QueryPerformanceCounter((LARGE_INTEGER*)&qpc_start);
		runScene(dTimeDelta);
		while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) > 0) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		eglSwapBuffers(eglDisplay, eglSurface);
		QueryPerformanceCounter((LARGE_INTEGER*)&qpc_end);
		dTimeDelta = static_cast<double>(qpc_end - qpc_start) / static_cast<double>(qpcFrequency);
	}

	endScene();
	*/

	return 0;
}

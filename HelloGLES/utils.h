#pragma once

#include "stdafx.h"

namespace Utils {
	static std::vector<std::string> SplitExtensions(const char* extensions_list) {
		std::vector<std::string> out;
		unsigned s = 0;
		unsigned i = 0;
		while(extensions_list[i]) {
			if(i > 0 && extensions_list[i] == ' ') {
				out.push_back(std::string(&extensions_list[s], i - s));
				s = i+1;
			}
			++i;
		}
		return out;
	}
}

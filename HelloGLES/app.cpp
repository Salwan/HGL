// App Module
// Handles native windows initialization, creation, and updating.
// Handles EGL initialization and context creation.

#include "stdafx.h"
#include "app.h"
#include <Windows.h>

using namespace std;

// Globals
static const wchar_t* AppClassName = L"HelloGLES2";

// PIMPL
struct CApp::PIMPL {
	PIMPL() : hwnd(NULL), hdc(NULL) {}
	HWND hwnd;
	HDC hdc;
};

// Statics
CApp* CApp::pInstance = nullptr;

// CSystem constructor
CApp::CApp(const wchar_t* app_title, unsigned window_width, unsigned window_height) 
	: strAppTitle(app_title), uWinWidth(window_width), uWinHeight(window_height), 
	  bAppQuit(false), pNative(new PIMPL)
{
	CApp::pInstance = this;
	_Init();
}

CApp::~CApp() 
{
	CApp::pInstance = nullptr;

	// OpenGL ES Cleanup

	// EGL Cleanup

	// Window cleanup
	if(pNative->hdc) {
		ReleaseDC(pNative->hwnd, pNative->hdc);
	}
	if(pNative->hwnd) {
		DestroyWindow(pNative->hwnd);
	}
	
	delete pNative;
}

// Initialize all
void CApp::_Init()
{
	clog << "Starting Application..." << endl;
	clog << "Initializing Window..." << endl;
	try {
		_InitWindow();
	}
	catch(std::exception &e) {
		DWORD last_windows_err = GetLastError();
		ostringstream oss;
		oss << "CApp::_Init() Exception Thrown.\nMessage:" << e.what() << "\nWindows Error = " << last_windows_err;
		MessageBoxA(NULL, oss.str().c_str(), "Windows Initialization Failure", MB_OK | MB_ICONERROR);
	}
}

// Initializes native window system and creates window
void CApp::_InitWindow()
{
	pNative->hwnd = static_cast<HWND>(_CreateWindow());
	pNative->hdc = GetDC(pNative->hwnd);
}

// Native window messages handler
static unsigned Proc_uWinWidth;
static unsigned Proc_uWinHeight;
LRESULT CALLBACK AppWndProc(HWND hwnd, unsigned int msg, WPARAM wParam, LPARAM lParam) {
	switch(msg) {
		case WM_SYSCOMMAND: {
			switch(wParam) {
				case SC_SCREENSAVE:
				case SC_MONITORPOWER:
					return 0;
			}
			break;
		}
		case WM_CLOSE: {
			CApp::GetInstance()->Quit();
			DestroyWindow(hwnd);
			PostQuitMessage(0);
			return 0;
		}
		case WM_SIZE: {
			Proc_uWinWidth = LOWORD(lParam);
			Proc_uWinHeight = HIWORD(lParam);
			break;
		}
		case WM_KEYUP:
			if(wParam == VK_ESCAPE) {
				CApp::GetInstance()->Quit();
				return 0;
			}
			break;
		default:
			break;
	}
	return DefWindowProc(hwnd, msg, wParam, lParam);
}

// Create native window (init Windows)
void* CApp::_CreateWindow()
{
	Proc_uWinWidth = uWinWidth;
	Proc_uWinHeight = uWinHeight;
	HINSTANCE hInstance = GetModuleHandle(NULL);
	WNDCLASSEX wc;
	{
		wc.cbSize = sizeof(WNDCLASSEX);
		wc.style = CS_OWNDC;
		wc.lpfnWndProc = &DefWindowProc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = 0;
		wc.hInstance = hInstance;
		wc.hIcon = NULL;
		wc.hCursor = LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground = CreateSolidBrush(0x808080);
		wc.lpszMenuName = NULL;
		wc.lpszClassName = AppClassName;
		wc.hIconSm = NULL;
		wc.lpfnWndProc = AppWndProc;
	}
	HRESULT hr = RegisterClassEx(&wc);
	if(SUCCEEDED(hr)) {
		clog << "Window registration -> success" << endl;
	} else {
		clog << "Window registration -> failure" << endl;
		throw exception("RegisterClassEx() failed");
	}
	RECT rect = {0, 0, static_cast<long>(uWinWidth), static_cast<long>(uWinHeight)};
	int style = WS_BORDER | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME;
	AdjustWindowRect(&rect, style, false);

	HWND hwnd = CreateWindow(AppClassName, strAppTitle.c_str(), style, CW_USEDEFAULT, CW_USEDEFAULT,
		rect.right - rect.left, rect.bottom - rect.top, NULL, NULL, GetModuleHandle(NULL), NULL);
	if(!hwnd) {
		clog << "Window creation -> failure" << endl;
		throw exception("CreateWindow() failed");
	} else {
		clog << "Window creation -> success" << endl;
	}
	ShowWindow(hwnd, SW_SHOW);
	return static_cast<void*>(hwnd);
}

void CApp::Run()
{
	MSG msg;
	PeekMessage(&msg, NULL, 0, 0, PM_REMOVE);

	while(!bAppQuit) {
		while(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) > 0) {
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}
}
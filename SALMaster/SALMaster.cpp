// SALMaster.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#define CACHE_LINE  32  
#define CACHE_ALIGN __declspec(align(CACHE_LINE))  


class Z80
{
public:
	explicit Z80() {
		F = A = 0;
		BC = DE = HL = IX = IY = 0;
		SP = 0xffff;
		PC = 0;
		RAM = new uint8_t [65536];
	}

	virtual ~Z80() {
		delete [] RAM;
	}

	bool tick() {
		return true;
	}

	void dumpRegisters() {
		std::cout << "Status Flags = " << static_cast<unsigned>(F) << std::endl;
		std::cout << "Accumulator =  " << static_cast<unsigned>(A) << std::endl;
		std::cout << "BC = " << BC << " DE = " << DE << " HL = " << HL << std::endl;
		std::cout << "IX = " << IX << " IY = " << IY << std::endl;
		std::cout << "SP = " << SP << " PC = " << PC << std::endl;
	}

protected:
	uint8_t		F;			// Status flags:
							//		b0:C->Carry			b1:N->Add/Subtract  b2:P/V->Parity/Overflow	b3:X->NotUsed  
							//		b4:N->Add/Subtract  b5:X->NotUsed		b6:Z->Zero				b7:S->Sign
	uint8_t		A;			// Accumulator
	uint16_t	BC, DE, HL;	// General purpose registers
	uint16_t	IX, IY;		// Index registers
	uint16_t	SP;			// Stack pointer
	uint16_t	PC;			// Program pointer
	uint8_t		*RAM;		// Test 64KB RAM
};


int main(int, const char* [])
{
	std::cout << "Hello World!" << std::endl;

	Z80 z80;
	while(z80.tick()) {
		z80.dumpRegisters();
		break;
	}

	std::cout << "<Enter> to exit ";
	std::cin.get();
    return 0;
}


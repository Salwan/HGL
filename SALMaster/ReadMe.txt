========================================================================
    SALMaster Project Overview
========================================================================

SALMaster is my own SEGA Master emulator.
I chose a system I'm unfamliar with that has many great but simple games and
is based on Z80 so should be straight forward.

Targets: Windows, OSX, Linux
Backend: EGL, OpenGL ES 2

Stages of Implementation:
-------------------------

App: HGLES
- EGL initialization
- OpenGL ES 2.0 working perfectly
- Implement straight forward quad rendering
- Implement Font rendering based on HGEFont
- BunnyMark working
- HGE - stage 1: 
	Interface: all
	System: all except System_Launch and System_Snapshot
	Random: all
	Timer: all
	Input: Input_KeyDown, Input_KeyUp
	Graphics: Gfx_BeginScene, Gfx_EndScene, Gfx_Clear, Gfx_RenderQuad, Gfx_SetTransform
- HGE - stage 2: TODO
- ...
- Port build to CMake
- Port HGLES to OSX
- Port HGLES to Linux 64-bit
- Port HGLES to Linux 32-bit 

Z80:
- Implementing a straight forward Z80 emulator
- Optimizing it a little to run fast, use sample Z80 code for benchmarking
- Implement any undocumented features (stage 1)
- Implement Sega Master memory map (stage 2)
- Implement Sega Master I/O (stage 3)
- ...

VDP: requires HGLES stage 1 and Z80 stage 3
- VDP I/O ports access
- VDP controls and register write
- VDP status flags
- VDP Colors and palettes
- VDP display modes
- VDP patterns
- VDP background and scrolling
- VDP sprites
- Display timing
- VDP interrupts
- ...

Master's Memory Systems
- ...

Master's Sound
- ...

/////////////////////////////////////////////////////////////////////////////
Other notes:

AppWizard uses "TODO:" comments to indicate parts of the source code you
should add to or customize.

/////////////////////////////////////////////////////////////////////////////

#pragma once

// This is a dll linked to from dlllib internally. (a dependency)

#ifdef DLL_EXPORT
#define LIBDECL __declspec(dllexport)
#else
#define LIBDECL __declspec(dllimport)
#endif

class MyObject
{
public:
	virtual const char* getName() const = 0;
	virtual int getAge() const = 0;
};

extern "C"
{
	LIBDECL MyObject* createObject(const char* name, int age);
}
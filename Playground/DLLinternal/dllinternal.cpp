#define DLL_EXPORT

#include "dllinternal.h"

class MyObjectImpl : public MyObject
{
public:
	MyObjectImpl(const char* _name, int _age) : name(_name), age(_age) {}

	virtual const char* getName() const { return name; };
	virtual int getAge() const { return age; }

private:
	const char* name;
	int age;
};

extern "C" {
	LIBDECL MyObject* createObject(const char* name, int age) {
		return new MyObjectImpl(name, age);
	}
}

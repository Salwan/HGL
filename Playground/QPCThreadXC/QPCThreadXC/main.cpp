// Main thread reading time intervals at random.
// Timer thread tied to core 0 for QPC time.
// Timer thread is idle except when flagged to update time.
// Reading from timer thread is direct no need to worry about locks.

#include <iostream>
#include <string>
#include <thread>
#include <chrono>
#include <mutex>

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <Windows.h>
#endif

// Self-made Windows QueryPerformanceCounter based C++11 API compatible clock
// Fallback to std::chrono::high_performance_clock on Apple
struct qpc_clock {
	typedef std::chrono::nanoseconds                       duration;      // nanoseconds resolution
	typedef duration::rep                                  rep;
	typedef duration::period                               period;
	typedef std::chrono::time_point<qpc_clock, duration>   time_point;
	
	bool is_steady;                                                // = true
#ifdef _WIN32
	time_point now()
#elif defined(__APPLE__)
    std::chrono::high_resolution_clock::time_point now()
#endif
	{
		if(!is_inited) {
			init();
			is_inited = true;
		}
#ifdef _WIN32
		LARGE_INTEGER counter;
		QueryPerformanceCounter(&counter);
		return time_point(duration(static_cast<rep>((double)counter.QuadPart / frequency.QuadPart *
			period::den / period::num)));
#elif defined(__APPLE__)
        std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
        return t1;
#endif
	}

	static qpc_clock instance;

private:
	bool is_inited;                                                // = false
#ifdef _WIN32
	LARGE_INTEGER frequency;
#endif
	void init()
	{
#ifdef _WIN32
		if(QueryPerformanceFrequency(&frequency) == 0)
			throw std::logic_error("QueryPerformanceCounter not supported: " + std::to_string(GetLastError()));
#elif defined(__APPLE__)
        // nothing
#endif
	}
};

qpc_clock qpc_clock::instance;



std::mutex mux_1, mux_2;
std::condition_variable cvar1, cvar2;
bool ready1 = false, ready2 = false;

// THREAD RACING
void y_print_id(int id) {
	std::unique_lock<std::mutex> lockz(mux_1);
	while(!ready1) cvar1.wait(lockz);
	// ...
	std::cout << "thread " << id << '\n';
}

void y_go() {
	std::unique_lock<std::mutex> lockz(mux_1);
	ready1 = true;
	cvar1.notify_all();
}

void y_DoTest() {
	std::thread threads[10];
	for(unsigned i = 0; i < 10; ++i) {
		threads[i] = std::thread(y_print_id, i);
	}
	std::cout << "10 threads ready to race..\n";
	y_go();
	for(auto& th : threads) th.join();
}

// TIMER THREAD
void timer_thread_test() {
	//std::cout << "I am timer_thread." << std::endl;
	std::unique_lock<std::mutex> lockz(mux_2);
	//cv.wait(lk, []{return ttready;});
	while(!ready2) {
		//std::cout << "w..";
		cvar2.wait(lockz);
	}

	std::cout << "I am start!" << std::endl;
	/*qpc_clock thread_clock;
	thread_clock.now(); // init
	int c = 8;
	qpc_clock::time_point start = qpc_clock::instance.now();
	qpc_clock::time_point end;
	while(c--) {
		Sleep(100);
		std::cout << "I AM BOSS! = ";
		end = thread_clock.now();
		long long duration = std::chrono::duration_cast<qpc_clock::duration>(end - start).count();
		std::cout << (duration/1000000.0) << "ms\n";
		start = end;
	}
	std::cout << "I am done." << std::endl;

	t_lk.unlock();
	t_cv.notify_one();
	*/
}

void t_LaunchTimer() {
	std::thread tt = std::thread(timer_thread_test);
    std::unique_lock<std::mutex> lockz(mux_2);
    ready2 = true;
    cvar2.notify_one();
    lockz.unlock();         // This must be done for notify_one to work!
                            // alternatively just lock/unlock mux_2 yourself directly (no need for unique_lock).
	tt.join();
}

// MAIN
int main(int, char**) {
	std::cout << "QPC timer thread implementation" << std::endl;

	std::cout << "Testing QPC read.. " << std::endl;
	auto start = qpc_clock::instance.now();
    std::this_thread::sleep_for(std::chrono::milliseconds(16));
	auto end = qpc_clock::instance.now();
	long long duration = std::chrono::duration_cast<qpc_clock::duration>(end - start).count();
	std::cout << "Time it took for sleeping 100ms = " << duration << "us\n";
	std::cout << "    or = " << (duration/1000000.0) << "ms\n";

	std::cout << "Racing some threads.." << std::endl;
	y_DoTest();

	std::cout << "\nTimer Thread Launch..\n";
	t_LaunchTimer();

	std::cout << "\n[ENTER] to exit.. ";
	std::cin.get();
	return 0;
}

#include <iostream>

#include <dlllib.h>

int main(int, char**) {
	std::cout << "DLL Testing.. " << std::endl;
	std::cout << "Whats your name = " << WhatsMyName() << std::endl;
	std::cout << "AddMul 3 and 5 = " << AddMul(3, 5) << std::endl;
	std::cout << "Get some delicious pi = " << GetPI() << std::endl;

	std::cout << "Deep dependency, agent's name: " << GetNameOfAgent() << std::endl;

	std::cout << "\n[ENTER] to exit.. " << std::endl;
	std::cin.get();
	return 0;
}
#pragma once

#ifdef DLL_EXPORT
#define LIBDECL __declspec(dllexport)
#else
#define LIBDECL __declspec(dllimport)
#endif

extern "C"
{
	LIBDECL const char* WhatsMyName();
	LIBDECL int AddMul(int a, int b);
	LIBDECL float GetPI();
	LIBDECL const char* GetNameOfAgent();
}
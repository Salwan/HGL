#define DLL_EXPORT

#include "dlllib.h"
#include <dllinternal.h>

MyObject* myobject = nullptr;

extern "C" {

LIBDECL const char* WhatsMyName() {
	return "Ryzenberg!";
}

LIBDECL int AddMul(int a, int b) {
	return a * (a + b);
}

LIBDECL float GetPI() {
	static const float pi = 22.0f/7.0f;
	return pi;
}

LIBDECL const char* GetNameOfAgent() {
	if(!myobject) {
		myobject = createObject("MR ANDERSON", 35);
	}
	return myobject->getName();
}

}
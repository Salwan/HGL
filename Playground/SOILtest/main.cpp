#include <iostream>
#include <SOIL.h>

#pragma comment(lib, "SOIL.lib")

int main(int, char**) {
	std::cout << "Testing SOIL by reading a small 32-bit PNG..." << std::endl;

	const unsigned fn = 7;
	const char* png_files[] = {
		"rgba_4x4.fw.png",		// Direct fireworks png
		"rgba_4x4_8.png",		// 8-bit with alpha
		"rgba_4x4_32.png",		// 32-bit with alpha
		"rgba_4x4_24.png",		// 24-bit with no alpha (won't pass alpha test!)
		"rgba_4x4.bmp",
		"rgba_4x4.jpg",
		"rgba_4x4.dds",
	};
	int width[fn], height[fn], channels[fn];
	unsigned char* png_data[fn];
	memset(png_data, 0, sizeof(unsigned char*) * fn);
	for(unsigned i = 0; i < fn; ++i) {
		std::cout << "\nLoading " << png_files[i] << " ... ";
		png_data[i] = SOIL_load_image(png_files[i], &width[i], &height[i], &channels[i], SOIL_LOAD_AUTO);
		if(!png_data[i]) {
			std::cout << "FAILED!" << std::endl;
		} else {
			std::cout << "OK" << std::endl;
			std::cout << "  Width       = " << width[i] << std::endl;
			std::cout << "  Height      = " << height[i] << std::endl;
			std::cout << "  Channels    = " << channels[i] << std::endl;
			std::cout << "ARGB Data: " << std::endl;
			for(unsigned p = 0; p < 16; ++p) {
				unsigned rgb = (png_data[i][p * channels[i] + 0] << 16) + (png_data[i][p * channels[i] + 1] << 8) + png_data[i][p * channels[i] + 2];
				if(channels[i] == 4) {
					rgb += (png_data[i][p * channels[i] + 3] << 24);
				}
				std::cout << "0x" << std::hex << rgb  << "  ";
			}
		}
	}

	for(unsigned i = 0; i < fn; ++i) {
		if(png_data[i]) {
			SOIL_free_image_data(png_data[i]);
		}
	}

	std::cout << "\n[ENTER] to end.. ";
	std::cin.get();
	return 0;
}

// CONCLUSIONS:
//=====================================================
// - Can load PNG-32 and Fireworks PNG correctly with alpha.
// - Cannot load PNG-8 and PNG-24.
// - JPG and BMP are 3 channels RGB
// - DDS is ARGB (4 channels)
// - Data starts top left and goes row by row.
// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright(C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge 
#pragma once

namespace HGL_BuiltinShaders {

	const char* defaultVertexShaderSource = R"VSDEFAULT(
		#version 100

		attribute vec2			aPosition;
		attribute vec2			aTexCoords;
		varying vec2			vTexCoords;
		uniform mediump mat4	uTransformationMatrix;
		uniform highp float		uZ;

		void main() {
			gl_Position = uTransformationMatrix * vec4(aPosition.xy, uZ, 1.0);
			vTexCoords = aTexCoords;
		}
	)VSDEFAULT";

	const char* defaultFragmentShaderSource = R"FSDEFAULT(
		#version 100
		precision mediump float;
	
		varying vec2			vTexCoords;
		uniform mediump vec4	uTintColor;

		void main() {
			gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
		}
	)FSDEFAULT";

};
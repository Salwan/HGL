#include <windows.h>
#include <cstdio>
#include <tchar.h>



#define GL_GLEXT_PROTOTYPES
#include <EGL/egl.h>
#include <GLES2/gl2.h>
#include <vector>

#define	WINDOW_CLASS_NAME _T("PVRShellClass")
#define APPLICATION_NAME _T("HelloAPI")
#define ERROR_TITLE _T("Error")

const unsigned int WindowWidth = 800;
const unsigned int WindowHeight = 600;
const unsigned int VertexArray = 0;
bool HasUserQuit = false;

LRESULT CALLBACK handleWindowMessages(HWND nativeWindow, UINT message, WPARAM windowParameters, LPARAM longWindowParameters)
{
	switch(message) {
		case WM_SYSCOMMAND:{
			switch(windowParameters) {
				case SC_SCREENSAVE:
				case SC_MONITORPOWER:{
					return 0;
				}
			}
			break;
		}
		case WM_CLOSE:{
			HasUserQuit = true;
			PostQuitMessage(0);
			return 1;
		}
	}
	return DefWindowProc(nativeWindow, message, windowParameters, longWindowParameters);
}

bool testEGLError(HWND nativeWindow, const char* functionLastCalled){
	EGLint lastError = eglGetError();
	if(lastError != EGL_SUCCESS) {
		TCHAR stringBuffer[256];
		_stprintf(stringBuffer, _T("%s failed (%x).\n"), functionLastCalled, lastError);
		MessageBox(nativeWindow, stringBuffer, ERROR_TITLE, MB_OK | MB_ICONEXCLAMATION);
		return false;
	}

	return true;
}
bool testGLError(HWND nativeWindow, const char* functionLastCalled){
	GLenum lastError = glGetError();
	if(lastError != GL_NO_ERROR) {
		TCHAR stringBuffer[256];
		_stprintf(stringBuffer, _T("%s failed (%x).\n"), functionLastCalled, lastError);
		MessageBox(nativeWindow, stringBuffer, ERROR_TITLE, MB_OK | MB_ICONEXCLAMATION);
		return false;
	}
	return true;
}





































int WINAPI WinMain(HINSTANCE applicationInstance, HINSTANCE previousInstance, TCHAR* /*commandLineString*/, int /*showCommand*/)
{
	HWND				nativeWindow = NULL;
	HDC					deviceContext = NULL;
	EGLDisplay			eglDisplay = NULL;
	EGLConfig			eglConfig = NULL;
	EGLSurface			eglSurface = NULL;
	EGLContext			eglContext = NULL;
	GLuint fragmentShader = 0, vertexShader = 0;
	GLuint shaderProgram = 0;
	GLuint	vertexBuffer = 0;

	// Window init
	WNDCLASS nativeWindowDescription;
	nativeWindowDescription.style = CS_HREDRAW | CS_VREDRAW;
	nativeWindowDescription.lpfnWndProc = handleWindowMessages;
	nativeWindowDescription.cbClsExtra = 0;
	nativeWindowDescription.cbWndExtra = 0;
	nativeWindowDescription.hInstance = applicationInstance;
	nativeWindowDescription.hIcon = 0;
	nativeWindowDescription.hCursor = 0;
	nativeWindowDescription.lpszMenuName = 0;
	nativeWindowDescription.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	nativeWindowDescription.lpszClassName = WINDOW_CLASS_NAME;

	ATOM registerClass = RegisterClass(&nativeWindowDescription);
	if(!registerClass) {
		MessageBox(0, _T("Failed to register the window class"), ERROR_TITLE, MB_OK | MB_ICONEXCLAMATION);
	}

	RECT windowRectangle;
	SetRect(&windowRectangle, 0, 0, WindowWidth, WindowHeight);
	AdjustWindowRectEx(&windowRectangle, WS_CAPTION | WS_SYSMENU, false, 0);

	nativeWindow = CreateWindow(WINDOW_CLASS_NAME, APPLICATION_NAME, WS_VISIBLE | WS_SYSMENU, CW_USEDEFAULT, CW_USEDEFAULT,
		windowRectangle.right - windowRectangle.left, windowRectangle.bottom - windowRectangle.top,
		NULL, NULL, applicationInstance, NULL);
	if(!nativeWindow) {
		MessageBox(0, _T("Failed to create the window"), ERROR_TITLE, MB_OK | MB_ICONEXCLAMATION);
		return 1;
	}

	deviceContext = GetDC(nativeWindow);
	if(!deviceContext) {
		MessageBox(nativeWindow, _T("Failed to create the device context"), ERROR_TITLE, MB_OK | MB_ICONEXCLAMATION);
		return 1;
	}

	// EGL display
	eglDisplay = eglGetDisplay(deviceContext);
	if(eglDisplay == EGL_NO_DISPLAY) {
		eglDisplay = eglGetDisplay((EGLNativeDisplayType)EGL_DEFAULT_DISPLAY);
	}
	if(eglDisplay == EGL_NO_DISPLAY) {
		MessageBox(0, _T("Failed to get an EGLDisplay"), ERROR_TITLE, MB_OK | MB_ICONEXCLAMATION);
		return 1;
	}

	EGLint eglMajorVersion, eglMinorVersion;
	if(!eglInitialize(eglDisplay, &eglMajorVersion, &eglMinorVersion)) {
		MessageBox(0, _T("Failed to initialize the EGLDisplay"), ERROR_TITLE, MB_OK | MB_ICONEXCLAMATION);
		return 1;
	}

	// EGL config
	const EGLint configurationAttributes[] = {
		EGL_SURFACE_TYPE,		EGL_WINDOW_BIT,
		EGL_RENDERABLE_TYPE,	EGL_OPENGL_ES2_BIT,
		EGL_NONE
	};
	EGLint configsReturned;
	if(!eglChooseConfig(eglDisplay, configurationAttributes, &eglConfig, 1, &configsReturned) || (configsReturned != 1)) {
		MessageBox(0, _T("eglChooseConfig() failed."), ERROR_TITLE, MB_OK | MB_ICONEXCLAMATION);
		return 1;
	}

	// EGL surface
	eglSurface = eglCreateWindowSurface(eglDisplay, eglConfig, nativeWindow, NULL);
	if(eglSurface == EGL_NO_SURFACE) {
		eglGetError();
		eglSurface = eglCreateWindowSurface(eglDisplay, eglConfig, NULL, NULL);
	}
	if(!testEGLError(nativeWindow, "eglCreateWindowSurface")) { return 1; }

	// EGL context
	eglBindAPI(EGL_OPENGL_ES_API);
	if(!testEGLError(nativeWindow, "eglBindAPI")) { return 1; }
	EGLint contextAttributes[] = {
		EGL_CONTEXT_CLIENT_VERSION, 2,
		EGL_NONE
	};
	eglContext = eglCreateContext(eglDisplay, eglConfig, NULL, contextAttributes);
	if(!testEGLError(nativeWindow, "eglCreateContext")) { return 1; }
	eglMakeCurrent(eglDisplay, eglSurface, eglSurface, eglContext);
	if(!testEGLError(nativeWindow, "eglMakeCurrent")) { return 1; }

	// Init buffers
	GLfloat vertexData[] =
	{
		-0.4f, -0.4f, 0.0f,
		0.4f, -0.4f, 0.0f,
		0.0f, 0.4f, 0.0f
	};
	glGenBuffers(1, &vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW);
	if(!testGLError(nativeWindow, "glBufferData")) { return 1; }

	// Init shaders
	// -- Fragment shader
	const char* const fragmentShaderSource = "\
											void main (void)\
											{\
											gl_FragColor = vec4(1.0, 1.0, 0.66 ,1.0);\
											}";
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, (const char**)&fragmentShaderSource, NULL);
	glCompileShader(fragmentShader);

	GLint isShaderCompiled;
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &isShaderCompiled);
	if(!isShaderCompiled) {
		int infoLogLength, charactersWritten;
		glGetShaderiv(fragmentShader, GL_INFO_LOG_LENGTH, &infoLogLength);
		std::vector<char> infoLog; infoLog.resize(infoLogLength);
		glGetShaderInfoLog(fragmentShader, infoLogLength, &charactersWritten, infoLog.data());
		MessageBox(nativeWindow, infoLogLength > 1 ? infoLog.data() : _T("Failed to compile fragment shader. (No information)"),
			ERROR_TITLE, MB_OK | MB_ICONEXCLAMATION);
		return false;
	}

	// -- Vertex Shader
	const char* const vertexShaderSource = "\
										attribute highp vec4	myVertex;\
										uniform mediump mat4	transformationMatrix;\
										void main(void)\
										{\
										gl_Position = transformationMatrix * myVertex;\
										}";
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, (const char**)&vertexShaderSource, NULL);
	glCompileShader(vertexShader);
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &isShaderCompiled);
	if(!isShaderCompiled) {
		int infoLogLength, charactersWritten;
		glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &infoLogLength);
		char* infoLog = new char[infoLogLength];
		glGetShaderInfoLog(vertexShader, infoLogLength, &charactersWritten, infoLog);
		MessageBox(nativeWindow, infoLogLength > 1 ? infoLog : _T("Failed to compile vertex shader. (No information)"),
			ERROR_TITLE, MB_OK | MB_ICONEXCLAMATION);
		delete[] infoLog;
		return false;
	}

	// -- Init Program
	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, fragmentShader);
	glAttachShader(shaderProgram, vertexShader);
	glBindAttribLocation(shaderProgram, VertexArray, "myVertex");
	glLinkProgram(shaderProgram);
	GLint isLinked;
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &isLinked);
	if(!isLinked) {
		int infoLogLength, charactersWritten;
		glGetProgramiv(shaderProgram, GL_INFO_LOG_LENGTH, &infoLogLength);
		char* infoLog = new char[infoLogLength];
		glGetProgramInfoLog(shaderProgram, infoLogLength, &charactersWritten, infoLog);
		MessageBox(nativeWindow, infoLogLength > 1 ? infoLog : _T("Failed to link GL program object. (No information)"),
			ERROR_TITLE, MB_OK | MB_ICONEXCLAMATION);

		delete[] infoLog;
		return false;
	}
	glUseProgram(shaderProgram);

	if(!testGLError(nativeWindow, "glUseProgram")) { return false; }

	// Rendering 800 frames
	for(int i = 0; i < 800; ++i) {
		if(HasUserQuit) { return false; }
		glClearColor(0.00f, 0.70f, 0.67f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);
		int matrixLocation = glGetUniformLocation(shaderProgram, "transformationMatrix");
		const float transformationMatrix[] =
		{
			1.0f, 0.0f, 0.0f, 0.0f,
			0.0f, 1.0f, 0.0f, 0.0f,
			0.0f, 0.0f, 1.0f, 0.0f,
			0.0f, 0.0f, 0.0f, 1.0f
		};
		glUniformMatrix4fv(matrixLocation, 1, GL_FALSE, transformationMatrix);
		if(!testGLError(nativeWindow, "glUniformMatrix4fv")) {
			return false;
		}
		glEnableVertexAttribArray(VertexArray);
		glVertexAttribPointer(VertexArray, 3, GL_FLOAT, GL_FALSE, 0, 0);
		if(!testGLError(nativeWindow, "glVertexAttribPointer")) { return 1; }
		glDrawArrays(GL_TRIANGLES, 0, 3);
		if(!testGLError(nativeWindow, "glDrawArrays")) { return 1; }
		
		
		
		
		if(!eglSwapBuffers(eglDisplay, eglSurface)) {
			testEGLError(nativeWindow, "eglSwapBuffers");
			return 1;
		}
		MSG eventMessage;
		PeekMessage(&eventMessage, nativeWindow, NULL, NULL, PM_REMOVE);
		TranslateMessage(&eventMessage);
		DispatchMessage(&eventMessage);
	}
	return 0;
}

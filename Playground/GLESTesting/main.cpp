#include <windows.h>
#include <stdio.h>
#include <iostream>
#include <tchar.h>
#include <cassert>

#define GL_GLEXT_PROTOTYPES
#include <EGL/egl.h>
#include <GLES2/gl2.h>
#include <vector>

#include "linetest.h"

#define	WINDOW_CLASS_NAME _T("PVRShellClass")
#define APPLICATION_NAME _T("HelloAPI")
#define ERROR_TITLE _T("Error")

const unsigned int WindowWidth = 800;
const unsigned int WindowHeight = 600;
const unsigned VertexArray = 0;
bool HasUserQuit = false;

LRESULT CALLBACK handleWindowMessages(HWND nativeWindow, UINT message, WPARAM windowParameters, LPARAM longWindowParameters)
{
	switch(message) {
		case WM_SYSCOMMAND: {
			switch(windowParameters) {
				case SC_SCREENSAVE:
				case SC_MONITORPOWER: {
					return 0;
				}
			}
			break;
		}
		case WM_CLOSE: {
			HasUserQuit = true;
			PostQuitMessage(0);
			return 1;
		}
		case WM_KEYUP: {
			if (windowParameters == VK_ESCAPE) {
				HasUserQuit = true;
				return 1;
			}
		}
	}
	return DefWindowProc(nativeWindow, message, windowParameters, longWindowParameters);
}

bool testEGLError(HWND nativeWindow, const char* functionLastCalled) {
	EGLint lastError = eglGetError();
	if(lastError != EGL_SUCCESS) {
		TCHAR stringBuffer[256];
		_stprintf(stringBuffer, _T("%s failed (%x).\n"), functionLastCalled, lastError);
		MessageBox(nativeWindow, stringBuffer, ERROR_TITLE, MB_OK | MB_ICONEXCLAMATION);
		return false;
	}

	return true;
}
bool testGLError(HWND nativeWindow, const char* functionLastCalled) {
	GLenum lastError = glGetError();
	if(lastError != GL_NO_ERROR) {
		TCHAR stringBuffer[256];
		_stprintf(stringBuffer, _T("%s failed (%x).\n"), functionLastCalled, lastError);
		MessageBox(nativeWindow, stringBuffer, ERROR_TITLE, MB_OK | MB_ICONEXCLAMATION);
		return false;
	}
	return true;
}

static bool first = true;
static float vertexData[] = {
	0.0f,  0.5f, 0.0f, 	// Vertex 1 (X, Y, Z)
	-0.5f, -0.5f, 0.0f,	// Vertex 2 (X, Y, Z)
	0.5f, -0.5f, 0.0f, // Vertex 3 (X, Y, Z)

	-0.5f,  0.5f, 0.0f, 	// Vertex 1 (X, Y, Z)
	0.0f, -0.5f, 0.0f,	// Vertex 2 (X, Y, Z)
	0.5f, 0.5f, 0.0f, // Vertex 3 (X, Y, Z)
};
static const float identityMatrix[] =
{
	1.0f, 0.0f, 0.0f, 0.0f,
	0.0f, 1.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 1.0f
};
static GLuint vertexBuffer;







static GLuint vertexShader;






static GLuint fragmentShader;
static GLuint shaderProgram;


int WINAPI _tWinMain(HINSTANCE applicationInstance, HINSTANCE previousInstance, LPTSTR , int )
{
	HWND				nativeWindow = NULL;
	HDC					deviceContext = NULL;
	EGLDisplay			eglDisplay = NULL;
	EGLConfig			eglConfig = NULL;
	EGLSurface			eglSurface = NULL;
	EGLContext			eglContext = NULL;
	GLuint fragmentShader = 0, vertexShader = 0;
	GLuint shaderProgram = 0;
	GLuint	vertexBuffer = 0;

	// Window init
	WNDCLASS nativeWindowDescription;
	nativeWindowDescription.style = CS_HREDRAW | CS_VREDRAW;
	nativeWindowDescription.lpfnWndProc = handleWindowMessages;
	nativeWindowDescription.cbClsExtra = 0;
	nativeWindowDescription.cbWndExtra = 0;
	nativeWindowDescription.hInstance = applicationInstance;
	nativeWindowDescription.hIcon = 0;
	nativeWindowDescription.hCursor = 0;
	nativeWindowDescription.lpszMenuName = 0;
	nativeWindowDescription.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	nativeWindowDescription.lpszClassName = WINDOW_CLASS_NAME;

	ATOM registerClass = RegisterClass(&nativeWindowDescription);
	if(!registerClass) {
		MessageBox(0, _T("Failed to register the window class"), ERROR_TITLE, MB_OK | MB_ICONEXCLAMATION);
	}

	RECT windowRectangle;
	SetRect(&windowRectangle, 0, 0, WindowWidth, WindowHeight);
	AdjustWindowRectEx(&windowRectangle, WS_CAPTION | WS_SYSMENU, false, 0);

	nativeWindow = CreateWindow(WINDOW_CLASS_NAME, APPLICATION_NAME, WS_VISIBLE | WS_SYSMENU, CW_USEDEFAULT, CW_USEDEFAULT,
		windowRectangle.right - windowRectangle.left, windowRectangle.bottom - windowRectangle.top,
		NULL, NULL, applicationInstance, NULL);
	if(!nativeWindow) {
		MessageBox(0, _T("Failed to create the window"), ERROR_TITLE, MB_OK | MB_ICONEXCLAMATION);
		return 1;
	}

	deviceContext = GetDC(nativeWindow);
	if(!deviceContext) {
		MessageBox(nativeWindow, _T("Failed to create the device context"), ERROR_TITLE, MB_OK | MB_ICONEXCLAMATION);
		return 1;
	}

	// EGL display
	eglDisplay = eglGetDisplay(deviceContext);
	assert(eglDisplay != EGL_NO_DISPLAY);

	
	
	
	
	
	int iEGLVerMaj, iEGLVerMin;
	EGLBoolean result = eglInitialize(eglDisplay, &iEGLVerMaj, &iEGLVerMin);
	assert(result);

	
	
	
	// EGL config
	constexpr EGLint configurationAttributes[] = {
		EGL_SURFACE_TYPE,		EGL_WINDOW_BIT,
		EGL_RENDERABLE_TYPE,	EGL_OPENGL_ES2_BIT,
		EGL_NONE
	};
	EGLint configs_count = 0;
	result = eglChooseConfig(eglDisplay, configurationAttributes, &eglConfig, 1, &configs_count);
	assert(configs_count == 1 && result);



	// EGL surface
	eglSurface = eglCreateWindowSurface(eglDisplay, eglConfig, nativeWindow, NULL);
	assert(eglSurface != EGL_NO_SURFACE);
	EGLint last_err = eglGetError();
	assert(last_err == EGL_SUCCESS);
	


	// EGL context		
	result = eglBindAPI(EGL_OPENGL_ES_API);
	assert(result);
	constexpr EGLint contextAttributes[] = {
		EGL_CONTEXT_CLIENT_VERSION, 2,
		EGL_NONE
	};
	eglContext = eglCreateContext(eglDisplay, eglConfig, NULL, contextAttributes);
	assert(eglContext != EGL_NO_CONTEXT);
	result = eglMakeCurrent(eglDisplay, eglSurface, eglSurface, eglContext);
	assert(result);

	// Init buffers
	
	
	
	
	
	
	glGenBuffers(1, &vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW);
	assert(sizeof(vertexData) == 3 * 6 * sizeof(float));

	// Init shaders
	// -- Fragment shader
	const char *fragmentSource = "precision mediump float; \n\
		void main() \n\
		{ \n\
			gl_FragColor = vec4(1.0, 0.0, 0.66, 1.0); \n\
		}\n";
	fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragmentShader, 1, &fragmentSource, NULL);
	glCompileShader(fragmentShader);
	
	GLint status;
	glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &status);
	assert(status == GL_TRUE);

	
	
	
	
	
	
	
	
	// -- Vertex Shader
	const char *vertexSource = "\
	attribute highp vec4 myVertex; \n\
	uniform mediump mat4 transformationMatrix; \n\
	void main() \n\
	{ \n\
		gl_Position = transformationMatrix * vec4(myVertex.xyz, 1.0); \n\
	} \n";
	vertexShader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertexShader, 1, &vertexSource, NULL);
	glCompileShader(vertexShader);
	glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &status);
	assert(status == GL_TRUE);

	
	
	
	
	
	
	
	
	
	// -- Init program
	shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glBindAttribLocation(shaderProgram, VertexArray, "myVertex");
	glLinkProgram(shaderProgram);

	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &status);
	assert(status == GL_TRUE);










	//glUseProgram(shaderProgram);
	//glEnableVertexAttribArray(VertexArray);
	//glVertexAttribPointer(VertexArray, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glClearColor(0.0f, 0.66f, 0.33f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	// Init line test
	LineTest::init();

	// Rendering 800 frames
	while(!HasUserQuit) {
		glClearColor(0.0f, 0.66f, 0.33f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);
		int matloc = glGetUniformLocation(shaderProgram, "transformationMatrix");
		assert(matloc >= 0); // Make sure we found transformationMatrix
		
		glUseProgram(shaderProgram);
		glUniformMatrix4fv(matloc, 1, GL_FALSE, identityMatrix);
		glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
		glEnableVertexAttribArray(VertexArray);
		glVertexAttribPointer(VertexArray, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glDrawArrays(GL_TRIANGLES, 0, 6);
		GLenum glerr = glGetError();
		if(glerr != GL_NO_ERROR) {
			std::cerr << glGetString(glerr);
		}
		glDisableVertexAttribArray(VertexArray);

		LineTest::render();

		EGLBoolean result = eglSwapBuffers(eglDisplay, eglSurface);
		assert(result);


		MSG eventMessage;
		PeekMessage(&eventMessage, nativeWindow, NULL, NULL, PM_REMOVE);
		TranslateMessage(&eventMessage);
		DispatchMessage(&eventMessage);
	}
	return 0;
}

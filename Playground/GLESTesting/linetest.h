#pragma once

#define GL_GLEXT_PROTOTYPES
#include <EGL/egl.h>
#include <EGL/eglext.h>

#include <cassert>

#include "hgl_builtin_shaders.h"

// Render line
namespace LineTest {
	static float lineData [] = {
		-0.7f, 0.7f,	0.0f, 0.0f,
		0.7f, 0.7f,		0.0f, 0.0f,

		0.7f, 0.7f,		0.0f, 0.0f,
		0.6f, -0.6f,	0.0f, 0.0f,

		0.6f, -0.6f,	0.0f, 0.0f,
		-0.6f, -0.6f,	0.0f, 0.0f,

		-0.6f, -0.6f,	0.0f, 0.0f,
		-0.7f, 0.7f,	0.0f, 0.0f,
	};
	static const float identityMatrix[] =
	{
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	};
	GLuint vb;
	GLuint fshader, vshader, sprog;
	const unsigned va_pos = 4;
	const unsigned va_tex = 5;
	GLuint uniTransform, uniZ, uniColor;

	void init() {
		glGenBuffers(1, &vb);
		glBindBuffer(GL_ARRAY_BUFFER, vb);
		glBufferData(GL_ARRAY_BUFFER, sizeof(lineData), nullptr, GL_DYNAMIC_DRAW);

		// Init shaders
		GLint status;
		// -- Vertex Shader
		vshader = glCreateShader(GL_VERTEX_SHADER);
		glShaderSource(vshader, 1, &HGL_BuiltinShaders::defaultVertexShaderSource, NULL);
		glCompileShader(vshader);
		glGetShaderiv(vshader, GL_COMPILE_STATUS, &status);
		assert(status == GL_TRUE);
		// -- Fragment shader
		fshader = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(fshader, 1, &HGL_BuiltinShaders::defaultFragmentShaderSource, NULL);
		glCompileShader(fshader);
		glGetShaderiv(fshader, GL_COMPILE_STATUS, &status);
		assert(status == GL_TRUE);
		// -- Program
		sprog = glCreateProgram();
		glAttachShader(sprog, vshader);
		glAttachShader(sprog, fshader);
		glBindAttribLocation(sprog, va_pos, "aPosition");
		glBindAttribLocation(sprog, va_tex, "aTexCoords");
		glLinkProgram(sprog);
		glGetProgramiv(sprog, GL_LINK_STATUS, &status);
		assert(status == GL_TRUE);
		// -- Setting up uniforms
		glUseProgram(sprog);
		uniTransform = glGetUniformLocation(sprog, "uTransformationMatrix");
		glUniformMatrix4fv(uniTransform, 1, GL_FALSE, identityMatrix);
		uniZ = glGetUniformLocation(sprog, "uZ");
		glUniform1f(uniZ, 0.5f);
		//uniColor = glGetUniformLocation(sprog, "uTintColor");
		//glUniform4f(uniColor, 1.0f, 1.0f, 1.0f, 1.0f);

		// glEnableVertexAttribArray and glVertexAttribPointer have to be
		// called before rendering. Not required during initialization except
		// if using VAOs which I'm not.
		// Also glDisableVertexAttribArray have to be called to avoid crashes when
		// using different shaders (attrib array will leak!)
	}

	void render() {
		glBindBuffer(GL_ARRAY_BUFFER, vb);

		glUseProgram(sprog);

		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(float) * 4 * 8, lineData);
		glEnableVertexAttribArray(va_pos);
		glVertexAttribPointer(va_pos, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), 0);
		glEnableVertexAttribArray(va_tex);
		glVertexAttribPointer(va_tex, 2, GL_FLOAT, GL_FALSE, 4 * sizeof(float), 0);
		glDrawArrays(GL_LINES, 0, 8);
		glDisableVertexAttribArray(va_tex);
		glDisableVertexAttribArray(va_pos);
	}

};
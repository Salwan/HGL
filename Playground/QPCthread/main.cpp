// Main thread reading time intervals at random.
// Timer thread tied to core 0 for QPC time.
// Timer thread is idle except when flagged to update time.
// Reading from timer thread is direct no need to worry about locks.

#include <iostream>
#include <string>
#include <thread>
#include <chrono>
#include <mutex>

#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <Windows.h>

// Self-made Windows QueryPerformanceCounter based C++11 API compatible clock
struct qpc_clock {
	typedef std::chrono::nanoseconds                       duration;      // nanoseconds resolution
	typedef duration::rep                                  rep;
	typedef duration::period                               period;
	typedef std::chrono::time_point<qpc_clock, duration>   time_point;
	
	bool is_steady;                                                // = true
	time_point now()
	{
		if(!is_inited) {
			init();
			is_inited = true;
		}
		LARGE_INTEGER counter;
		QueryPerformanceCounter(&counter);
		return time_point(duration(static_cast<rep>((double)counter.QuadPart / frequency.QuadPart *
			period::den / period::num)));
	}

	static qpc_clock instance;

private:
	bool is_inited;                                                // = false
	LARGE_INTEGER frequency;
	void init()
	{
		if(QueryPerformanceFrequency(&frequency) == 0)
			throw std::logic_error("QueryPerformanceCounter not supported: " + std::to_string(GetLastError()));
	}
};

qpc_clock qpc_clock::instance;



std::mutex y_mtx;
std::condition_variable y_cv;
bool y_ready = false;

void y_print_id(int id) {
	std::unique_lock<std::mutex> y_lck(y_mtx);
	while(!y_ready) y_cv.wait(y_lck);
	// ...
	std::cout << "thread " << id << '\n';
}

void y_go() {
	std::unique_lock<std::mutex> y_lck(y_mtx);
	y_ready = true;
	y_cv.notify_all();
}

void y_DoTest() {
	std::thread threads[10];
	for(unsigned i = 0; i < 10; ++i) {
		threads[i] = std::thread(y_print_id, i);
	}
	std::cout << "10 threads ready to race..\n";
	y_go();
	for(auto& th : threads) th.join();
}


std::mutex t_mtx;
std::condition_variable t_cv;
bool t_ready = false;

void timer_thread() {
	//std::cout << "I am timer_thread." << std::endl;
	std::unique_lock<std::mutex> t_lk(t_mtx);
	//cv.wait(lk, []{return ttready;});
	while(!t_ready) {
		//std::cout << "w..";
		t_cv.wait(t_lk);
	}

	std::cout << "I am start!" << std::endl;
	/*qpc_clock thread_clock;
	thread_clock.now(); // init
	int c = 8;
	qpc_clock::time_point start = qpc_clock::instance.now();
	qpc_clock::time_point end;
	while(c--) {
		Sleep(100);
		std::cout << "I AM BOSS! = ";
		end = thread_clock.now();
		long long duration = std::chrono::duration_cast<qpc_clock::duration>(end - start).count();
		std::cout << (duration/1000000.0) << "ms\n";
		start = end;
	}
	std::cout << "I am done." << std::endl;

	t_lk.unlock();
	t_cv.notify_one();
	*/
}

void t_LaunchTimer() {
	std::thread tt = std::thread(timer_thread);

	//std::lock_guard<std::mutex> lk(m);
	std::unique_lock<std::mutex> t_lk(t_mtx);
	t_ready = true;
	t_cv.notify_one();

	tt.join();
}

int main(int, char**) {
	std::cout << "QPC timer thread implementation" << std::endl;

	std::cout << "Testing QPC read.. " << std::endl;
	qpc_clock::time_point start = qpc_clock::instance.now();
	Sleep(16);
	qpc_clock::time_point end = qpc_clock::instance.now();
	long long duration = std::chrono::duration_cast<qpc_clock::duration>(end - start).count();
	std::cout << "Time it took for Sleep(100) = " << duration << "us\n";
	std::cout << "    or = " << (duration/1000000.0) << "ms\n";

	std::cout << "Racing some threads.." << std::endl;
	y_DoTest();

	std::cout << "\nTimer Thread Launch..\n";
	t_LaunchTimer();

	std::cout << "\n[ENTER] to exit.. ";
	std::cin.get();
	return 0;
}
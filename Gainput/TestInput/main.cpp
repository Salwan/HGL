#include <iostream>
#include <windows.h>

#include <gainput/gainput.h>

bool HasUserQuit = false;
#define WINDOW_CLASS_NAME L"Gainput TestInput"
int WindowWidth = 800;
int WindowHeight = 500;
HWND nativeWindow;

LRESULT CALLBACK handleWindowMessages(HWND nativeWindow, UINT message, WPARAM windowParameters, LPARAM longWindowParameters)
{
	switch (message) {
	case WM_SYSCOMMAND:
	{
		switch (windowParameters) {
		case SC_SCREENSAVE:
		case SC_MONITORPOWER:
		{
			return 0;
		}
		}
		break;
	}
	case WM_CLOSE:
	{
		HasUserQuit = true;
		PostQuitMessage(0);
		return 1;
	}
	case WM_KEYUP:
	{
		if (windowParameters == VK_ESCAPE) {
			HasUserQuit = true;
			return 1;
		}
	}
	}
	return DefWindowProc(nativeWindow, message, windowParameters, longWindowParameters);
}

enum Button
{
	ButtonConfirm,
	ButtonAnalog,
	ButtonTest,
};


int main(int argc, char** argv) {
	HINSTANCE applicationInstance = GetModuleHandle(NULL);
	// Window init
	WNDCLASS nativeWindowDescription;
	nativeWindowDescription.style = CS_HREDRAW | CS_VREDRAW;
	nativeWindowDescription.lpfnWndProc = handleWindowMessages;
	nativeWindowDescription.cbClsExtra = 0;
	nativeWindowDescription.cbWndExtra = 0;
	nativeWindowDescription.hInstance = applicationInstance;
	nativeWindowDescription.hIcon = 0;
	nativeWindowDescription.hCursor = 0;
	nativeWindowDescription.lpszMenuName = 0;
	nativeWindowDescription.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	nativeWindowDescription.lpszClassName = WINDOW_CLASS_NAME;

	ATOM registerClass = RegisterClass(&nativeWindowDescription);
	if (!registerClass) {
		MessageBoxA(0, "Failed to register the window class", "ERROR", MB_OK | MB_ICONEXCLAMATION);
	}

	RECT windowRectangle;
	SetRect(&windowRectangle, 0, 0, WindowWidth, WindowHeight);
	AdjustWindowRectEx(&windowRectangle, WS_CAPTION | WS_SYSMENU, false, 0);

	nativeWindow = CreateWindow(WINDOW_CLASS_NAME, L"TestInput", WS_VISIBLE | WS_SYSMENU, 
		CW_USEDEFAULT, CW_USEDEFAULT,
		windowRectangle.right - windowRectangle.left, windowRectangle.bottom - windowRectangle.top,
		NULL, NULL, applicationInstance, NULL);

	if (!nativeWindow) {
		MessageBoxA(0, "Failed to create the window", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return 1;
	}

	gainput::InputManager manager;
	const gainput::DeviceId kbId = manager.CreateDevice<gainput::InputDeviceKeyboard>();
	const gainput::DeviceId mouseId = manager.CreateDevice<gainput::InputDeviceMouse>();
	const gainput::DeviceId padId = manager.CreateDevice<gainput::InputDevicePad>(2);
	gainput::InputDevicePad* pad = (gainput::InputDevicePad*)manager.GetDevice(padId);

	manager.SetDisplaySize(800, 500);

	gainput::InputMap map(manager);
	map.MapBool(ButtonConfirm, padId, gainput::PadButton::PadButtonA);
	map.MapBool(ButtonConfirm, kbId, gainput::Key::KeyQ);
	map.MapBool(ButtonConfirm, mouseId, gainput::MouseButton::MouseButtonLeft);  
	map.MapFloat(ButtonAnalog, padId, gainput::PadButton::PadButtonLeftStickY);
	map.MapFloat(ButtonAnalog, padId, gainput::PadButton::PadButtonRightStickY);
	map.MapFloat(ButtonAnalog, padId, gainput::PadButton::PadButtonR2);
	map.MapBool(ButtonAnalog, kbId, gainput::Key::KeySpace);
	map.MapFloat(ButtonTest, padId, gainput::PadButton::PadButtonX);
	
	int c = 0;
	 
	while (/*!map.GetBoolWasDown(ButtonConfirm) &&*/ !HasUserQuit) {
		MSG eventMessage;
		PeekMessage(&eventMessage, nativeWindow, NULL, NULL, PM_REMOVE);
		TranslateMessage(&eventMessage);
		DispatchMessage(&eventMessage);

		if(map.GetBool(ButtonConfirm)) {
			std::cout << "Dude " << c++ << " " << map.GetFloat(ButtonAnalog) << std::endl;
		}
		float v = map.GetFloat(ButtonAnalog);
		if(v != 0.0f) {
			std::cout << "Analog" << c++ << " " << v << std::endl;
		}
		if(map.GetBool(ButtonTest)) {
			std::cout << "Test " << c++ << " " << map.GetFloat(ButtonTest) << std::endl;
		}

		manager.Update();
		manager.HandleMessage(eventMessage);
	}

	return 0;         
}


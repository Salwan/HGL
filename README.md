HGL
=========================

A multiplatform 2D game engine powered by OpenGL ES 2.0  
License: zlib/libpng

HGL's API is based on Haaf's Game Engine 1.8  
https://github.com/kvakvs/hge

##### Current Dependencies:

- EGL: context management
- OpenGL ES 2.0: everything visual
- libsndfile: decoding audio files, streaming
- OpenAL Soft: audio playback engine
- SOIL: loading images from multiple formats
- gainput: cross-platform input handling library
- zlib: cross-platform gzip compression for resource packs
- glm: header-only math library

##### Planned Target Platforms:

- Win32 API (32-bit and 64-bit) -- Main Development Platform
- OSX (64-bit) -- Tier 1 Porting
- Linux (32-bit and 64-bit) -- Tier 1 Porting
- Android TV (NVIDIA Shield TV) -- Tier 2 Porting
- SoC armv7 Debian (C.H.I.P. and Raspberry Pi) -- Tier 2 Porting
- iOS Phone and Tablet -- Tier 3 Porting
- Android Phone and Tablet -- Tier 3 Porting

##### Features Wishlist:

- Multi-threading and fibers everywhere
- Visual-feel shaders to give HGL a unique feel (HDR, tone-mapping, bokeh, blur/bloom, pbr, indirect lighting, etc)
- Vulkan renderer on Android/iOS for less power consumption and lower overhead
- Tile maps full support based on Tiled
- Box2D full integration
- Tweening support
- Camera system
- Scene graph implementation
- Support for networking UDP/TCP possibly via a library
- Video playback support for at least one modern format (theora?)
- Multi-monitor full support
- Basic 3D support: geometry rendering, shaders, post-processing, mesh parsing
- Python bindings for HGL
- Emscripten javascript port for WebGL browsers
- D lang interface (can directly access HGL's "C" libraries so should be easy :D)
- Console port: Switch maybe?

HGL Engine  
Copyright (C) 2017, Cloud Mill Games

Haaf's Game Engine  
Copyright(C) 2003 - 2007, Relish Games

//// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// https://github.com/kvakvs/hge 

// Tutorial 2: Using input, sound and rendering

// Copy the files "particles.png" and "menu.wav"
// from the folder "precompiled" to the folder with
// executable file. Also copy hge.dll and bass.dll
// to the same folder.


#include <hgl.h>
#include <iostream>

HGL *hgl = 0;

// Quad is the basic primitive in HGL
// used for rendering graphics.
// Quad contains 4 vertices, numbered
// 0 to 3 clockwise.
hglQuad quad;

// Handle for a sound effect
HEFFECT snd;

// Some "gameplay" variables and constants
float x = 100.0f, y = 100.0f;
float dx = 0.0f, dy = 0.0f;

const float speed = 90;
const float friction = 0.98f;

// This function plays collision sound with
// parameters based on sprite position and speed
void boom() {
	int pan = int((x - 400) / 4);
	float pitch = (dx*dx + dy*dy)*0.0005f + 0.2f;
	hgl->Effect_PlayEx(snd, 100, pan, pitch);
}

bool FrameFunc()
{
	// Get the time elapsed since last call of FrameFunc().
	// This will help us to synchronize on different
	// machines and video modes.
	float dt = hgl->Timer_GetDelta();

	// Process keys
	if(hgl->Input_GetKeyState(HGLK_ESCAPE)) return true;
	if(hgl->Input_GetKeyState(HGLK_LEFT)) dx -= speed*dt;
	if(hgl->Input_GetKeyState(HGLK_RIGHT)) dx += speed*dt;
	if(hgl->Input_GetKeyState(HGLK_UP)) dy -= speed*dt;
	if(hgl->Input_GetKeyState(HGLK_DOWN)) dy += speed*dt;

	// Do some movement calculations and collision detection	
	dx *= friction; dy *= friction; x += dx; y += dy;
	if(x>784) { x = 784 - (x - 784); dx = -dx; boom(); }
	if(x<16) { x = 16 + 16 - x; dx = -dx; boom(); }
	if(y>584) { y = 584 - (y - 584); dy = -dy; boom(); }
	if(y<16) { y = 16 + 16 - y; dy = -dy; boom(); }

	// Set up quad's screen coordinates
	quad.v[0].x = x - 16; quad.v[0].y = y - 16;
	quad.v[1].x = x + 16; quad.v[1].y = y - 16;
	quad.v[2].x = x + 16; quad.v[2].y = y + 16;
	quad.v[3].x = x - 16; quad.v[3].y = y + 16;

	// Continue execution
	return false;
}

// This function will be called by HGL when
// the application window should be redrawn.
// Put your rendering code here.
bool RenderFunc()
{
	// Begin rendering quads.
	// This function must be called
	// before any actual rendering.
	hgl->Gfx_BeginScene();
	
	// Clear screen with black color
	hgl->Gfx_Clear(0);
	
	// Render quads here. This time just
	// one of them will serve our needs.
	hgl->Gfx_RenderQuad(&quad);

	static const hglTriple tri = {
		{	
			hglVertex(550.0, 350.0f, 0.5f,  0xfff00f00),
			hglVertex(750.0f, 350.0f, 0.5f, 0xff0ff0ff),
			hglVertex(650.0f, 500.0f, 0.5f, 0xff0f0f0f),
		},
		0,	// HTEXTURE
		0,	// BLEND
	};
	hgl->Gfx_RenderTriple(&tri);
	
	static const HTEXTURE tqx = hgl->Texture_Load("assets/picard.png");
	static const hglQuad tq [2] = {
		{
			{	
				hglVertex(5.0f, 350.0f, 0.5f, 0xffff0000, 0.0f, 0.0f),
				hglVertex(220.0f, 350.0f, 0.5f, 0xff00ff00, 1.0f, 0.0f),
				hglVertex(220.0f, 580.0f, 0.5f, 0xff0000ff, 1.0f, 1.0f),
				hglVertex(5.0f, 580.0f, 0.5f, 0xffffffff, 0.0f, 1.0f),
			},
			0,	// HTEXTURE
			BLEND_COLORMUL,
		},
		{
			{
				hglVertex(105.0f, 480.0f, 0.5f, 0x00000000, 0.0f, 1.0f),
				hglVertex(105.0f, 250.0f, 0.5f, 0x00000000, 0.0f, 0.0f),
				hglVertex(320.0f, 250.0f, 0.5f, 0x00000000, 1.0f, 0.0f),
				hglVertex(320.0f, 480.0f, 0.5f, 0x00000000, 1.0f, 1.0f),
			},
			tqx,
			BLEND_COLORADD | BLEND_ALPHABLEND | BLEND_NOZWRITE,
		},
	};
	hgl->Gfx_RenderQuad(&tq[0]);
	hgl->Gfx_RenderQuad(&tq[1]);
	
	// End rendering and update the screen
	hgl->Gfx_EndScene();

	// RenderFunc should always return false
	return false;
}


int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	// Get HGE interface
	hgl = hglCreate(HGL_VERSION);

	// Set up log file, frame function, render function and window title
	hgl->System_SetState(HGL_LOGFILE, "hge_tut02.log");
	hgl->System_SetState(HGL_FRAMEFUNC, FrameFunc);
	hgl->System_SetState(HGL_RENDERFUNC, RenderFunc);
	hgl->System_SetState(HGL_TITLE, "HGE Tutorial 02 - Using input, sound and rendering");

	// Set up video mode
	hgl->System_SetState(HGL_WINDOWED, true);
	hgl->System_SetState(HGL_SCREENWIDTH, 800);
	hgl->System_SetState(HGL_SCREENHEIGHT, 600);
	hgl->System_SetState(HGL_SCREENBPP, 32);

	if(hgl->System_Initiate()) {
		// Load sound and texture
		snd = hgl->Effect_Load("assets/menu.wav");
		hgl->Effect_PlayEx(snd);
		quad.tex = hgl->Texture_Load("assets/particles.png");

		if(!snd || !quad.tex) {
			// If one of the data files is not found, display
			// an error message and shutdown.
			MessageBoxA(NULL, "Can't load MENU.WAV or PARTICLES.PNG", "Error", MB_OK | MB_ICONERROR | MB_APPLMODAL);
			hgl->System_Shutdown();
			hgl->Release();
			return 0;
		}

		// Set up quad which we will use for rendering sprite
		quad.blend = BLEND_ALPHAADD | BLEND_COLORMUL | BLEND_ZWRITE;
		
		for(int i = 0; i<4; i++) {
			// Set up z-coordinate of vertices
			quad.v[i].z = 0.5f;
			// Set up color. The format of DWORD col is 0xAARRGGBB
			quad.v[i].col = 0xFFFFA000;
		}

		// Set up quad's texture coordinates.
		// 0,0 means top left corner and 1,1 -
		// bottom right corner of the texture.
		quad.v[0].tx = 96.0 / 128.0; quad.v[0].ty = 64.0 / 128.0;
		quad.v[1].tx = 128.0 / 128.0; quad.v[1].ty = 64.0 / 128.0;
		quad.v[2].tx = 128.0 / 128.0; quad.v[2].ty = 96.0 / 128.0;
		quad.v[3].tx = 96.0 / 128.0; quad.v[3].ty = 96.0 / 128.0;

		// Let's rock now!
		hgl->System_Start();

		// Free loaded texture and sound
		hgl->Texture_Free(quad.tex);
		hgl->Effect_Free(snd);
		
	} else MessageBoxA(NULL, hgl->System_GetErrorMessage(), "Error", MB_OK | MB_ICONERROR | MB_SYSTEMMODAL);

	// Clean up and shutdown
	hgl->System_Shutdown();
	hgl->Release();
	return 0;
}
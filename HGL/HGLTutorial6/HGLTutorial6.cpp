//// Sal's HGL-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge 

#include <hgl.h>
#include <hgl_font.h>
#include <hgl_sprite.h>
#include <hgl_gui.h>

#include "menuitem.h"

#include <math.h>


// Pointer to the HGL interface.
// Helper classes require this to work.
HGL *hgl = 0;

constexpr unsigned WIDTH = 1280;
constexpr unsigned HEIGHT = 720;
constexpr unsigned HWIDTH = WIDTH / 2;
constexpr unsigned HHEIGHT = HEIGHT / 2;

// Some resource handles
HEFFECT				snd;
HEFFECT				music;
HTEXTURE			tex;
hglQuad				quad;

// Pointers to the HGL objects we will use
hglGUI				*gui;
hglFont				*fnt;
hglSprite			*spr;


bool FrameFunc()
{
	float dt = hgl->Timer_GetDelta();
	static float t = 0.0f;
	float tx, ty;
	int id;
	static int lastid = 0;

	// If ESCAPE was pressed, tell the GUI to finish
	if (hgl->Input_GetKeyState(HGLK_ESCAPE)) { 
		lastid = 5; 
		gui->Leave(); 
	}

	// We update the GUI and take an action if
	// one of the menu items was selected
	id = gui->Update(dt);
	if (id == -1) {
		switch (lastid) {
		case 1:
		case 2:
		case 3:
		case 4:
			gui->SetFocus(1);
			gui->Enter();
			break;

		case 5: return true;
		}
	} else if (id) { lastid = id; gui->Leave(); }

	// Here we update our background animation
	t += dt;
	tx = 50 * cosf(t / 60);
	ty = 50 * sinf(t / 60);

	quad.v[0].tx = tx;        quad.v[0].ty = ty;
	quad.v[1].tx = tx + WIDTH / 64; quad.v[1].ty = ty;
	quad.v[2].tx = tx + WIDTH / 64; quad.v[2].ty = ty + HEIGHT / 64;
	quad.v[3].tx = tx;        quad.v[3].ty = ty + HEIGHT / 64;

	return false;
}


bool RenderFunc()
{
	// Render graphics
	hgl->Gfx_BeginScene();
	hgl->Gfx_RenderQuad(&quad);
	gui->Render();
	fnt->SetColor(0xFFFFFFFF);
	fnt->printf(5, 5, HGLTEXT_LEFT, "dt:%.4f\nFPS:%d\nPad 1: %d", hgl->Timer_GetDelta(), hgl->Timer_GetFPS(),
		hgl->Input_IsPadAvailable(GAMEPAD_1));
	hgl->Gfx_EndScene();

	return false;
}


int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	hgl = hglCreate(HGL_VERSION);

	hgl->System_SetState(HGL_LOGFILE, "hgl_tut06.log");
	hgl->System_SetState(HGL_FRAMEFUNC, FrameFunc);
	hgl->System_SetState(HGL_RENDERFUNC, RenderFunc);
	hgl->System_SetState(HGL_TITLE, "HGL Tutorial 06 - Creating menus");
	hgl->System_SetState(HGL_WINDOWED, true);
	hgl->System_SetState(HGL_SCREENWIDTH, WIDTH);
	hgl->System_SetState(HGL_SCREENHEIGHT, HEIGHT);
	hgl->System_SetState(HGL_SCREENBPP, 32);

	if (hgl->System_Initiate()) {

		// Load sound and textures
		quad.tex = hgl->Texture_Load("assets/bg.png");
		tex = hgl->Texture_Load("assets/cursor.png");
		snd = hgl->Effect_Load("assets/menu.wav");
		music = hgl->Effect_Load("assets/doxent_-_Flower.ogg");
		if (!quad.tex || !tex || !snd || !music) {
			// If one of the data files is not found, display
			// an error message and shutdown.
			MessageBoxA(NULL, "Can't load BG.PNG, CURSOR.PNG or MENU.WAV", "Error", MB_OK | MB_ICONERROR | MB_APPLMODAL);
			hgl->System_Shutdown();
			hgl->Release();
			return 0;
		}

		// Set up the quad we will use for background animation
		quad.blend = BLEND_ALPHABLEND | BLEND_COLORMUL | BLEND_NOZWRITE;

		for (int i = 0;i<4;i++) {
			// Set up z-coordinate of vertices
			quad.v[i].z = 0.5f;
			// Set up color. The format of DWORD col is 0xAARRGGBB
			quad.v[i].col = 0xFFFFFFFF;
		}

		quad.v[0].x = 0; 
		quad.v[0].y = 0;
		quad.v[1].x = WIDTH; 
		quad.v[1].y = 0;
		quad.v[2].x = WIDTH; 
		quad.v[2].y = HEIGHT;
		quad.v[3].x = 0; 
		quad.v[3].y = HEIGHT;


		// Load the font, create the cursor sprite
		fnt = new hglFont("assets/font1.fnt");
		spr = new hglSprite(tex, 0, 0, 32, 32);

		// Create and initialize the GUI
		gui = new hglGUI();

		gui->AddCtrl(new hglGUIMenuItem(1, fnt, snd, (float)HWIDTH, 350.0f, 0.0f, "Play"));
		gui->AddCtrl(new hglGUIMenuItem(2, fnt, snd, (float)HWIDTH, 390.0f, 0.1f, "Options"));
		gui->AddCtrl(new hglGUIMenuItem(3, fnt, snd, (float)HWIDTH, 430.0f, 0.2f, "Instructions"));
		gui->AddCtrl(new hglGUIMenuItem(4, fnt, snd, (float)HWIDTH, 470.0f, 0.3f, "Credits"));
		gui->AddCtrl(new hglGUIMenuItem(5, fnt, snd, (float)HWIDTH, 510.0f, 0.4f, "Exit"));

		gui->SetNavMode(HGLGUI_UPDOWN | HGLGUI_CYCLED);
		gui->SetCursor(spr);
		gui->SetFocus(1);
		gui->Enter();

		hgl->Effect_Play(music);

		// Let's rock now!
		hgl->System_Start();

		// Delete created objects and free loaded resources
		//delete gui;
		delete fnt;
		delete spr;
		hgl->Effect_Free(snd);
		hgl->Texture_Free(tex);
		hgl->Texture_Free(quad.tex);
		hgl->Action_FreeAll();
	}

	// Clean up and shutdown
	hgl->System_Shutdown();
	hgl->Release();
	return 0;
}

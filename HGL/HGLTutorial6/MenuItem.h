//// Sal's HGL-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hgl 
#pragma once

// behaviour of our custom GUI control

#include <hgl.h>
#include <hgl_gui.h>
#include <hgl_font.h>
#include <hgl_color.h>


class hglGUIMenuItem : public hglGUIObject
{
public:
	hglGUIMenuItem(int id, hglFont *fnt, HEFFECT snd, float x, float y, float delay, char *title);

	virtual void	Render();
	virtual void	Update(float dt);

	virtual void	Enter();
	virtual void	Leave();
	virtual bool	IsDone();
	virtual void	Focus(bool bFocused);
	virtual void	MouseOver(bool bOver);

	virtual bool	MouseLButton(bool bDown);
	virtual bool	KeyClick(int key, int chr);
	virtual bool    PadClick(hglGUIPadKey_t pad_gui_key);

private:
	hglFont		*fnt;
	HEFFECT		snd;
	float		delay;
	char		*title;

	hglColor	scolor, dcolor, scolor2, dcolor2, sshadow, dshadow;
	hglColor	color, shadow;
	float		soffset, doffset, offset;
	float		timer, timer2;
};

//// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// https://github.com/kvakvs/hge 

// Tutorial 1: Minimal HGL application

#include <hgl.h>

#include <Windows.h>

HGL *hgl = 0;

// This function will be called by HGL once per frame.
// Put your game loop code here. In this example we
// just check whether ESC key has been pressed.
bool FrameFunc()
{
	// By returning "true" we tell HGE
	// to stop running the application.
	//if(hgl->Input_GetKeyState(HGLK_ESCAPE)) return true;

	// Continue execution
	return false;
}

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{	
	// Here we use global pointer to HGL interface.
	// Instead you may use hglCreate() every
	// time you need access to HGL. Just be sure to
	// have a corresponding hgl->Release()
	// for each call to hglCreate()
	hgl = hglCreate(HGL_VERSION);
	
	// Set our frame function
	hgl->System_SetState(HGL_FRAMEFUNC, FrameFunc);

	// Set logging for debug
	hgl->System_SetState(HGL_LOGFILE, "debug.log");
	
	// Set the window title
	hgl->System_SetState(HGL_TITLE, "HGL Tutorial 01 - Minimal HGL application");
	
	// Run in windowed mode
	// Default window size is 800x600
	hgl->System_SetState(HGL_WINDOWED, true);

	// Don't use BASS for sound
	hgl->System_SetState(HGL_USESOUND, false);

	// Tries to initiate HGE with the states set.
	// If something goes wrong, "false" is returned
	// and more specific description of what have
	// happened can be read with System_GetErrorMessage().
	if(hgl->System_Initiate()) {
		// Starts running FrameFunc().
		// Note that the execution "stops" here
		// until "true" is returned from FrameFunc().
		hgl->System_Start();
	} else {
		// If HGE initialization failed show error message
		MessageBoxA(NULL, hgl->System_GetErrorMessage(), "Error", MB_OK | MB_ICONERROR | MB_APPLMODAL);
	}

	// Now ESC has been pressed or the user
	// has closed the window by other means.

	// Restore video mode and free
	// all allocated resources
	hgl->System_Shutdown();

	// Release the HGE interface.
	// If there are no more references,
	// the HGE object will be deleted.
	hgl->Release();

	return 0;
}

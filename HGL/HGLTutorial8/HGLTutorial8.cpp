//// Sal's HGL-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge 

#include <hgl.h>
#include <hgl_color.h>
#include <hgl_sprite.h>
#include <hgl_distort.h>
#include <hgl_font.h>
#include <hgl_stringtable.h>

// Copy the files "font2.fnt", "font2.png",
// and "objects.png" from the folder "precompiled" to
// the folder with executable file. Also copy hgl.dll
// to the same folder.


// smaller sun & moon, underwater
// moon shape, hide stars behind the moon

#include <math.h>
#include <ctime>
#include <iostream>

// Pointer to the HGL interface (helper classes require this to work)

HGL			*hgl = 0;
hglFont		*fnt = 0;

// Simulation constants

#define SCREEN_WIDTH	1280
#define SCREEN_HEIGHT	720
#define NUM_STARS		100
#define SEA_SUBDIVISION 16

#define SKY_HEIGHT		(SCREEN_HEIGHT*0.6f)
#define STARS_HEIGHT	(SKY_HEIGHT*0.9f)
#define ORBITS_RADIUS	(SCREEN_WIDTH*0.43f)

unsigned skyTopColors[] = { 0xFF15092A, 0xFF6C6480, 0xFF89B9D0 };
unsigned skyBtmColors[] = { 0xFF303E57, 0xFFAC7963, 0xFFCAD7DB };
unsigned seaTopColors[] = { 0xFF3D546B, 0xFF927E76, 0xFF86A2AD };
unsigned seaBtmColors[] = { 0xFF1E394C, 0xFF2F4E64, 0xFF2F4E64 };

int seq[] = { 0, 0, 1, 2, 2, 2, 1, 0, 0 };

// Simulation resource handles

HTEXTURE	texObjects;

hglSprite	*sky;
hglSprite	*sun;
hglSprite	*moon;
hglSprite	*glow;
hglSprite	*seaglow;
hglSprite	*star;

hglDistortionMesh *sea;

hglColor	colWhite;

// Simulation state variables

float current_time;		// 0-24 hrs
float speed;	// hrs per second

int	  seq_id;
float seq_residue;

float starX[NUM_STARS];  // x
float starY[NUM_STARS];  // y
float starS[NUM_STARS];  // scale
float starA[NUM_STARS];  // alpha

float seaP[SEA_SUBDIVISION]; // phase shift array

hglColor colSkyTop;
hglColor colSkyBtm;
hglColor colSeaTop;
hglColor colSeaBtm;

hglColor colSun;
hglColor colSunGlow;
hglColor colMoon;
hglColor colMoonGlow;
hglColor colSeaGlow;

float sunX, sunY, sunS, sunGlowS;
float moonX, moonY, moonS, moonGlowS;
float seaGlowX, seaGlowSX, seaGlowSY;

// Simulation methods

bool InitSimulation();
void DoneSimulation();
void UpdateSimulation();
void RenderSimulation();


///////////////////////// Implementation ///////////////////////////


bool FrameFunc()
{
	// Process keys

	switch (hgl->Input_GetKey()) {
		case HGLK_0:		speed = 0.0f; break;
		case HGLK_1:		speed = 0.1f; break;
		case HGLK_2:		speed = 0.2f; break;
		case HGLK_3:		speed = 0.4f; break;
		case HGLK_4:		speed = 0.8f; break;
		case HGLK_5:		speed = 1.6f; break;
		case HGLK_6:		speed = 3.2f; break;
		case HGLK_7:		speed = 6.4f; break;
		case HGLK_8:		speed = 12.8f; break;
		case HGLK_9:		speed = 25.6f; break;
		case HGLK_ESCAPE:	return true;
	}

	// Update scene

	UpdateSimulation();

	return false;
}


bool RenderFunc()
{
	int hrs, mins, secs;
	float tmp;

	// Calculate display time

	hrs = (int)floorf(current_time);
	tmp = (current_time - hrs)*60.0f;
	mins = (int)floorf(tmp);
	secs = (int)floorf((tmp - mins)*60.0f);

	// Render scene

	hgl->Gfx_BeginScene();
	RenderSimulation();
	fnt->printf(7, 7, HGLTEXT_LEFT, "Keys 1-9 to adjust simulation speed, 0 - real time\nFPS: %d", hgl->Timer_GetFPS());
	fnt->printf(SCREEN_WIDTH - 50, 7, HGLTEXT_LEFT, "%02d:%02d:%02d", hrs, mins, secs);
	hgl->Gfx_EndScene();

	return false;
}


int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	hgl = hglCreate(HGL_VERSION);

	// Set desired system states and initialize HGL

	hgl->System_SetState(HGL_LOGFILE, "hgl_tut08.log");
	hgl->System_SetState(HGL_FRAMEFUNC, FrameFunc);
	hgl->System_SetState(HGL_RENDERFUNC, RenderFunc);
	hgl->System_SetState(HGL_TITLE, "HGL Tutorial 08 - The Big Calm");
	hgl->System_SetState(HGL_USESOUND, false);
	hgl->System_SetState(HGL_WINDOWED, true);
	hgl->System_SetState(HGL_HIDEMOUSE, false);
	hgl->System_SetState(HGL_SCREENWIDTH, SCREEN_WIDTH);
	hgl->System_SetState(HGL_SCREENHEIGHT, SCREEN_HEIGHT);
	hgl->System_SetState(HGL_SCREENBPP, 32);

	if (hgl->System_Initiate()) {
		fnt = new hglFont("assets/font2.fnt");

		if (!InitSimulation()) {
			// If one of the data files is not found, display an error message and shutdown
			MessageBoxA(NULL, "Can't load resources. See log for details.", "Error", MB_OK | MB_ICONERROR | MB_APPLMODAL);
			hgl->System_Shutdown();
			hgl->Release();
			return 0;
		}

		// Test resource system
		{
			const char *given[] = {
				"assets/fonts",
				"&SAVEDATA&/HGL Tutorial 8/",
				"&HOME&",
				""
			};
			const char *path[4];
			for(auto i = 0; i < 4; ++i) {
				path[i] = hgl->Resource_MakePath(given[i]);
				hgl->System_Log("PATH %d", i);
				hgl->System_Log("    GIVEN: \"%s\"", given[i]);
				hgl->System_Log("    RESULT: \"%s\"", path[i]);
			}
			path[0] = hgl->Resource_MakePath(given[2]);
			hgl->System_Log("EnumFolders called on:\n  %s", path[0]);
			std::string pstr = path[0];
			pstr += "*";
			const char *tmp = hgl->Resource_EnumFolders(pstr.c_str());
			do {
				hgl->System_Log("    Folder: \"%s\"", tmp);
			} while(tmp = hgl->Resource_EnumFolders());
			tmp = hgl->Resource_EnumFiles(pstr.c_str());
			do {
				hgl->System_Log("    File: \"%s\"", tmp);
			} while(tmp = hgl->Resource_EnumFiles());

			hgl->System_Log("Testing resource packs: ");
			const char *pack = "assets/resource_pack.zip";
			bool success = hgl->Resource_AttachPack(pack);
			if(!success) {
				hgl->System_Log("- Failed to attach pack: %s", pack);
			} else {
				hgl->System_Log("- Pack attached successfully.");
			}
			//hgl->Resource_RemovePack(pack);
			hgl->System_Log("Test loading stringtable from resource pack:");
			const char *st_table = "strings.txt";
			unsigned st_size;
			const char* data = static_cast<const char*>(hgl->Resource_Load(st_table, &st_size));
			if(data) {
				hgl->System_Log("- String table loaded successfully. Size = %d", st_size);
				std::string sdata;
				sdata.resize(st_size + 1);
				for(unsigned i = 0; i < st_size; ++i) {
					sdata[i] = data[i];
				}
				sdata[st_size] = 0;
				hgl->System_Log("- Content:\n%s", sdata.c_str());
			} else {
				hgl->System_Log("- Failed to load stringtable from resource pack");
			}
			hgl->System_Log("Test directly loading string table: ");
			
			hglStringTable* string_table = new hglStringTable(st_table);
			if(string_table) {
				hgl->System_Log("  GameName = %s", string_table->GetString("GameName"));
			}
		}

		hgl->System_Start();

		DoneSimulation();
		delete fnt;
	}

	hgl->System_Shutdown();
	hgl->Release();
	return 0;
}


float GetTime()
{
	time_t time = std::time(nullptr);
	struct tm time_now;
	localtime_s(&time_now, &time);
	float tmp;

	tmp = static_cast<float>(time_now.tm_sec);
	tmp = time_now.tm_min + tmp / 60.0f;
	tmp = time_now.tm_hour + tmp / 60.0f;

	return tmp;
}


bool InitSimulation()
{
	// Load texture

	texObjects = hgl->Texture_Load("assets/objects.png");
	if (!texObjects) return false;

	// Create sprites

	sky = new hglSprite(0, 0, 0, SCREEN_WIDTH, SKY_HEIGHT);
	sea = new hglDistortionMesh(SEA_SUBDIVISION, SEA_SUBDIVISION);
	sea->SetTextureRect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - SKY_HEIGHT);

	sun = new hglSprite(texObjects, 81, 0, 114, 114);
	sun->SetHotSpot(57, 57);
	moon = new hglSprite(texObjects, 0, 0, 81, 81);
	moon->SetHotSpot(40, 40);
	star = new hglSprite(texObjects, 72, 81, 9, 9);
	star->SetHotSpot(5, 5);

	glow = new hglSprite(texObjects, 128, 128, 128, 128);
	glow->SetHotSpot(64, 64);
	glow->SetBlendMode(BLEND_COLORADD | BLEND_ALPHABLEND | BLEND_NOZWRITE);
	seaglow = new hglSprite(texObjects, 128, 224, 128, 32);
	seaglow->SetHotSpot(64, 0);
	seaglow->SetBlendMode(BLEND_COLORADD | BLEND_ALPHAADD | BLEND_NOZWRITE);

	// Initialize simulation state

	colWhite.SetHWColor(0xFFFFFFFF);
	current_time = GetTime();
	speed = 0.0f;

	for (int i = 0;i<NUM_STARS;i++) // star positions
	{
		starX[i] = hgl->Random_Float(0, SCREEN_WIDTH);
		starY[i] = hgl->Random_Float(0, STARS_HEIGHT);
		starS[i] = hgl->Random_Float(0.1f, 0.7f);
	}

	for (int i = 0;i<SEA_SUBDIVISION;i++) // sea waves phase shifts
	{
		seaP[i] = i + hgl->Random_Float(-15.0f, 15.0f);
	}

	// Systems are ready now!

	return true;
}


void DoneSimulation()
{
	// Delete sprites

	delete seaglow;
	delete glow;
	delete star;
	delete moon;
	delete sun;

	delete sky;
	delete sea;

	// Free texture

	hgl->Texture_Free(texObjects);
}


void UpdateSimulation()
{
	int i, j, k;
	float zenith, a, dy, fTime;
	float posX, s1, s2;
	const float cellw = SCREEN_WIDTH / (SEA_SUBDIVISION - 1);
	hglColor col1, col2;
	unsigned dwCol1, dwCol2;

	// Update time of day

	if (speed == 0.0f) current_time = GetTime();
	else {
		current_time += hgl->Timer_GetDelta()*speed;
		if (current_time >= 24.0f) current_time -= 24.0f;
	}

	seq_id = (int)(current_time / 3);
	seq_residue = current_time / 3 - seq_id;
	zenith = -(current_time / 12.0f*M_PI - M_PI_2);

	// Interpolate sea and sky colors

	col1.SetHWColor(skyTopColors[seq[seq_id]]);
	col2.SetHWColor(skyTopColors[seq[seq_id + 1]]);
	colSkyTop = col2*seq_residue + col1*(1.0f - seq_residue);

	col1.SetHWColor(skyBtmColors[seq[seq_id]]);
	col2.SetHWColor(skyBtmColors[seq[seq_id + 1]]);
	colSkyBtm = col2*seq_residue + col1*(1.0f - seq_residue);

	col1.SetHWColor(seaTopColors[seq[seq_id]]);
	col2.SetHWColor(seaTopColors[seq[seq_id + 1]]);
	colSeaTop = col2*seq_residue + col1*(1.0f - seq_residue);

	col1.SetHWColor(seaBtmColors[seq[seq_id]]);
	col2.SetHWColor(seaBtmColors[seq[seq_id + 1]]);
	colSeaBtm = col2*seq_residue + col1*(1.0f - seq_residue);

	// Update stars

	if (seq_id >= 6 || seq_id<2)
		for (int i = 0; i<NUM_STARS; i++) {
			a = 1.0f - starY[i] / STARS_HEIGHT;
			a *= hgl->Random_Float(0.6f, 1.0f);
			if (seq_id >= 6) a *= sinf((current_time - 18.0f) / 6.0f*M_PI_2);
			else a *= sinf((1.0f - current_time / 6.0f)*M_PI_2);
			starA[i] = a;
		}

	// Calculate sun position, scale and colors

	if (seq_id == 2) a = sinf(seq_residue*M_PI_2);
	else if (seq_id == 5) a = cosf(seq_residue*M_PI_2);
	else if (seq_id>2 && seq_id<5) a = 1.0f;
	else a = 0.0f;

	colSun.SetHWColor(0xFFEAE1BE);
	colSun = colSun*(1 - a) + colWhite*a;

	a = (cosf(current_time / 6.0f*M_PI) + 1.0f) / 2.0f;
	if (seq_id >= 2 && seq_id <= 6) {
		colSunGlow = colWhite*a;
		colSunGlow.a = 1.0f;
	} else colSunGlow.SetHWColor(0xFF000000);

	sunX = SCREEN_WIDTH*0.5f + cosf(zenith)*ORBITS_RADIUS;
	sunY = SKY_HEIGHT*1.2f + sinf(zenith)*ORBITS_RADIUS;
	sunS = 1.0f - 0.3f*sinf((current_time - 6.0f) / 12.0f*M_PI);
	sunGlowS = 3.0f*(1.0f - a) + 3.0f;

	// Calculate moon position, scale and colors

	if (seq_id >= 6) a = sinf((current_time - 18.0f) / 6.0f*M_PI_2);
	else a = sinf((1.0f - current_time / 6.0f)*M_PI_2);
	colMoon.SetHWColor(0x20FFFFFF);
	colMoon = colMoon*(1 - a) + colWhite*a;

	colMoonGlow = colWhite;
	colMoonGlow.a = 0.5f*a;

	moonX = SCREEN_WIDTH*0.5f + cosf(zenith - M_PI)*ORBITS_RADIUS;
	moonY = SKY_HEIGHT*1.2f + sinf(zenith - M_PI)*ORBITS_RADIUS;
	moonS = 1.0f - 0.3f*sinf((current_time + 6.0f) / 12.0f*M_PI);
	moonGlowS = a*0.4f + 0.5f;

	// Calculate sea glow

	if (current_time>19.0f || current_time<4.5f) // moon
	{
		a = 0.2f; // intensity
		if (current_time>19.0f && current_time<20.0f) a *= (current_time - 19.0f);
		else if (current_time>3.5f && current_time<4.5f) a *= 1.0f - (current_time - 3.5f);

		colSeaGlow = colMoonGlow;
		colSeaGlow.a = a;
		seaGlowX = moonX;
		seaGlowSX = moonGlowS*3.0f;
		seaGlowSY = moonGlowS*2.0f;
	} else if (current_time>6.5f && current_time<19.0f) // sun
	{
		a = 0.3f; // intensity
		if (current_time<7.5f) a *= (current_time - 6.5f);
		else if (current_time>18.0f) a *= 1.0f - (current_time - 18.0f);

		colSeaGlow = colSunGlow;
		colSeaGlow.a = a;
		seaGlowX = sunX;
		seaGlowSX = sunGlowS;
		seaGlowSY = sunGlowS*0.6f;
	} else colSeaGlow.a = 0.0f;

	// Move waves and update sea color

	for (i = 1; i<SEA_SUBDIVISION - 1; i++) {
		a = float(i) / (SEA_SUBDIVISION - 1);
		col1 = colSeaTop*(1 - a) + colSeaBtm*a;
		dwCol1 = col1.GetHWColor();
		fTime = 2.0f*hgl->Timer_GetTime();
		a *= 20;

		for (j = 0; j<SEA_SUBDIVISION; j++) {
			sea->SetColor(j, i, dwCol1);

			dy = a*sinf(seaP[i] + (float(j) / (SEA_SUBDIVISION - 1) - 0.5f)*M_PI*16.0f - fTime);
			sea->SetDisplacement(j, i, 0.0f, dy, HGLDISP_NODE);
		}
	}

	dwCol1 = colSeaTop.GetHWColor();
	dwCol2 = colSeaBtm.GetHWColor();

	for (j = 0; j<SEA_SUBDIVISION; j++) {
		sea->SetColor(j, 0, dwCol1);
		sea->SetColor(j, SEA_SUBDIVISION - 1, dwCol2);
	}

	// Calculate light path

	if (current_time>19.0f || current_time<5.0f) // moon
	{
		a = 0.12f; // intensity
		if (current_time>19.0f && current_time<20.0f) a *= (current_time - 19.0f);
		else if (current_time>4.0f && current_time<5.0f) a *= 1.0f - (current_time - 4.0f);
		posX = moonX;
	} else if (current_time>7.0f && current_time<17.0f) // sun
	{
		a = 0.14f; // intensity
		if (current_time<8.0f) a *= (current_time - 7.0f);
		else if (current_time>16.0f) a *= 1.0f - (current_time - 16.0f);
		posX = sunX;
	} else a = 0.0f;

	if (a != 0.0f) {
		k = (int)floorf(posX / cellw);
		s1 = (1.0f - (posX - k*cellw) / cellw);
		s2 = (1.0f - ((k + 1)*cellw - posX) / cellw);

		if (s1>0.7f) s1 = 0.7f;
		if (s2>0.7f) s2 = 0.7f;

		s1 *= a;
		s2 *= a;

		for (i = 0; i<SEA_SUBDIVISION; i += 2) {
			a = sinf(float(i) / (SEA_SUBDIVISION - 1)*M_PI_2);

			col1.SetHWColor(sea->GetColor(k, i));
			col1 += colSun*s1*(1 - a);
			col1.Clamp();
			sea->SetColor(k, i, col1.GetHWColor());

			col1.SetHWColor(sea->GetColor(k + 1, i));
			col1 += colSun*s2*(1 - a);
			col1.Clamp();
			sea->SetColor(k + 1, i, col1.GetHWColor());
		}
	}
}


void RenderSimulation()
{
	// Render sky

	sky->SetColor(colSkyTop.GetHWColor(), 0);
	sky->SetColor(colSkyTop.GetHWColor(), 1);
	sky->SetColor(colSkyBtm.GetHWColor(), 2);
	sky->SetColor(colSkyBtm.GetHWColor(), 3);
	sky->Render(0, 0);

	// Render stars

	if (seq_id >= 6 || seq_id<2)
		for (int i = 0; i<NUM_STARS; i++) {
			star->SetColor((unsigned(starA[i] * 255.0f) << 24) | 0xFFFFFF);
			star->RenderEx(starX[i], starY[i], 0.0f, starS[i]);
		}

	// Render sun

	glow->SetColor(colSunGlow.GetHWColor());
	glow->RenderEx(sunX, sunY, 0.0f, sunGlowS);
	sun->SetColor(colSun.GetHWColor());
	sun->RenderEx(sunX, sunY, 0.0f, sunS);

	// Render moon

	glow->SetColor(colMoonGlow.GetHWColor());
	glow->RenderEx(moonX, moonY, 0.0f, moonGlowS);
	moon->SetColor(colMoon.GetHWColor());
	moon->RenderEx(moonX, moonY, 0.0f, moonS);

	// Render sea

	sea->Render(0, SKY_HEIGHT);
	seaglow->SetColor(colSeaGlow.GetHWColor());
	seaglow->RenderEx(seaGlowX, SKY_HEIGHT, 0.0f, seaGlowSX, seaGlowSY);
}
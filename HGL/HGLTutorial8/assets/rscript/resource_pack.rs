; This is a comment

; Include another resource script
Include resource_include.rs

; Raw data from a path
Resource level2 {
  filename=levels\level2.dat
  resgroup=1
}

; Textures
Texture background {
  filename=bg.png
  resgroup=1
}
Texture larkinTexture {
  filename=larkin.png
  resgroup=1
}
Texture sprParticle {
  filename=particle.png
  resgroup=1
}

; Sound effects
Sound testSFX {
  filename=test.wav
  resgroup=1
}

; Mod music
Music main_theme {
  filename=test.xm
  amplify=70
  resgroup=1
}

; Streamed music
Stream music_stream {
  filename=ost.ogg
  resgroup=1
}

; Render target
Target rtHUD {
  size=256,256
  zbuffer=false
  resgroup=1
}

; Larkin sprite
Sprite sprLarkin {
  texture=larkinTexture
  rect=0,0,170,170
  hotspot=85,169
  blendmode=COLORMUL,ALPHABLEND,NOZWRITE
  resgroup=1
}

; Animated larkin
Animation larkin.walk {
  texture=larkinTexture
  rect=0,0,510,510
  frames=8
  fps=6.0
  mode=FORWARD,LOOP
  hotspot=85,169
  blendmode=COLORMUL,ALPHABLEND,NOZWRITE
  resgroup=1
}

; Font description file
Font testFont {
  filename=testfont.fnt
  tracking=1.0
  resgroup=1
}

; Particle system preset
Particle testParticle {
  filename=test.psi
  sprite=sprParticle
  resgroup=1
}

; Distortion mesh
Distortion distortWaves {
  texture=bg2.png
  rect=0,0,400,400
  mesh=16,8
  color=FF2060F0
  resgroup=1
}

; Strings
StringTable testStrings {
  filename=strings.txt
  resgroup=1
}

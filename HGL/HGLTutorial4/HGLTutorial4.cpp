//// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge 

// Copy the files "particles.png", "menu.wav",
// "font1.fnt", "font1.png" and "trail.psi" from
// the folder "precompiled" to the folder with
// executable file. Also copy hge.dll and bass.dll
// to the same folder.


#include <hgl.h>
#include <hgl_sprite.h>
#include <hgl_font.h>
#include <hgl_particle.h>
#include <hgl_anim.h>


HGL *hgl = 0;

hglSprite*			spr;
hglSprite*			spt;
hglSprite*			tar;
hglFont*			fnt;
hglParticleSystem*	par;
hglAnimation*		burd;
hglAnimation*		macho;
hglAnimation*		testAnim;

HTEXTURE			tex;
HEFFECT				snd;

// HGL render target handle
HTARGET				target;

float x = 100.0f, y = 100.0f;
float dx = 0.0f, dy = 0.0f;

const float speed = 90;
const float friction = 0.98f;

void boom() {
	int pan = int((x - 256) / 2.56f);
	float pitch = (dx*dx + dy*dy)*0.0005f + 0.2f;
	hgl->Effect_PlayEx(snd, 100, pan, pitch);
}

// This function will be called by HGE when
// render targets were lost and have been just created
// again. We use it here to update the render
// target's texture handle that changes during recreation.
bool GfxRestoreFunc()
{
	if (tar && target) tar->SetTexture(hgl->Target_GetTexture(target));
	return false;
}

bool ResizeFunc() {
	return false;
}

bool FrameFunc()
{
	float dt = hgl->Timer_GetDelta();

	// Process keys
	if (hgl->Input_GetKeyState(HGLK_ESCAPE)) return true;
	if (hgl->Input_GetKeyState(HGLK_LEFT)) dx -= speed*dt;
	if (hgl->Input_GetKeyState(HGLK_RIGHT)) dx += speed*dt;
	if (hgl->Input_GetKeyState(HGLK_UP)) dy -= speed*dt;
	if (hgl->Input_GetKeyState(HGLK_DOWN)) dy += speed*dt;

	// Do some movement calculations and collision detection	
	dx *= friction; dy *= friction; x += dx; y += dy;
	if (x>496) { x = 496 - (x - 496);dx = -dx;boom(); }
	if (x<16) { x = 16 + 16 - x;dx = -dx;boom(); }
	if (y>496) { y = 496 - (y - 496);dy = -dy;boom(); }
	if (y<16) { y = 16 + 16 - y;dy = -dy;boom(); }

	// Update particle system
	par->info.nEmission = (int)(dx*dx + dy*dy);
	par->MoveTo(x, y);
	par->Update(dt);

	// Update animated sprites
	burd->Update(dt);
	macho->Update(dt);
	testAnim->Update(dt);

	return false;
}


bool RenderFunc()
{
	int i;

	// Render graphics to the texture
	hgl->Gfx_BeginScene(target);
	hgl->Gfx_Clear(0);
	par->Render();
	spr->Render(x, y);
	hgl->Gfx_EndScene();
	
	// Now put several instances of the rendered texture to the screen
	hgl->Gfx_BeginScene();
	hgl->Gfx_Clear(0);
	for (i = 0; i < 1; i++) {
		tar->SetColor(0xFFFFFF | (((5 - i) * 40 + 55) << 24));
		tar->RenderEx(i*100.0f, i*50.0f, i*M_PI / 8, 1.0f - i*0.1f);
	}

	burd->Render(0.0f, 200.0f);
	macho->Render(200.0f, 200.0f);
	testAnim->Render(400.0f, 300.0f);

	fnt->printf(5, 5, HGLTEXT_LEFT, "dt:%.3f\nFPS:%d (constant)", hgl->Timer_GetDelta(), hgl->Timer_GetFPS());
	hgl->Gfx_EndScene();
	return false;
}



int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	hgl = hglCreate(HGL_VERSION);

	hgl->System_SetState(HGL_LOGFILE, "hge_tut04.log");
	hgl->System_SetState(HGL_FRAMEFUNC, FrameFunc);
	hgl->System_SetState(HGL_RENDERFUNC, RenderFunc);
	hgl->System_SetState(HGL_GFXRESTOREFUNC, GfxRestoreFunc);
	hgl->System_SetState(HGL_RESIZEFUNC, ResizeFunc);
	hgl->System_SetState(HGL_TITLE, "HGE Tutorial 04 - Using render targets");
	hgl->System_SetState(HGL_FPS, 100);
	hgl->System_SetState(HGL_WINDOWED, true);
	hgl->System_SetState(HGL_SCREENWIDTH, 800);
	hgl->System_SetState(HGL_SCREENHEIGHT, 600);
	hgl->System_SetState(HGL_SCREENBPP, 32);
	hgl->System_SetState(HGL_HIDEMOUSE, false);

	tar = 0;
	target = 0;

	if (hgl->System_Initiate()) {
		snd = hgl->Effect_Load("assets/menu.wav");
		tex = hgl->Texture_Load("assets/particles.png");
		if (!snd || !tex) {
			// If one of the data files is not found, display
			// an error message and shutdown.
			MessageBoxA(NULL, 
				"Can't load one of the following files:\nMENU.WAV, PARTICLES.PNG, FONT1.FNT, FONT1.PNG, TRAIL.PSI", 
				"Error", MB_OK | MB_ICONERROR | MB_APPLMODAL);
			hgl->System_Shutdown();
			hgl->Release();
			return 0;
		}

		spr = new hglSprite(tex, 96, 64, 32, 32);
		spr->SetColor(0xFFFFA000);
		spr->SetHotSpot(16, 16);

		fnt = new hglFont("assets/font1.fnt");

		spt = new hglSprite(tex, 32, 32, 32, 32);
		spt->SetBlendMode(BLEND_COLORMUL | BLEND_ALPHAADD | BLEND_NOZWRITE);
		spt->SetHotSpot(16, 16);
		par = new hglParticleSystem("assets/trail.psi", spt);
		par->Fire();

		// Create a render target and a sprite for it
		target = hgl->Target_Create(512, 512, true, false);
		tar = new hglSprite(hgl->Target_GetTexture(target), 0, 0, 512, 512, true);
		tar->SetBlendMode(BLEND_COLORMUL | BLEND_ALPHAADD | BLEND_NOZWRITE);

		// Init animated sprites
		HTEXTURE burd_tex = hgl->Texture_Load("assets/burd.png");
		HTEXTURE macho_tex = hgl->Texture_Load("assets/macho.png");
		HTEXTURE test_tex = hgl->Texture_Load("assets/test_sheet.png");
		burd = new hglAnimation(burd_tex, 14, 32.0f, 0.0f, 0.0f, 183.0f, 168.0f);
		macho = new hglAnimation(macho_tex, 32, 8.0f, 0.0f, 0.0f, 46.0f, 50.0f);
		testAnim = new hglAnimation(test_tex, 4, 4.0f, 0.0f, 0.0f, 32.0f, 32.0f);
		burd->Play();
		macho->Play();
		testAnim->Play();

		// Let's rock now!
		hgl->System_Start();

		// Delete created objects and free loaded resources
		delete par;
		delete fnt;
		delete spt;
		delete spr;
		delete tar;
		hgl->Target_Free(target);
		hgl->Texture_Free(tex);
		hgl->Effect_Free(snd);
	}

	// Clean up and shutdown
	hgl->System_Shutdown();
	hgl->Release();
	return 0;
}

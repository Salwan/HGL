//// Sal's HGL-compatible mini engine using EGL, OpenGL ES 2
//// Copyright (C) 2017, Cloud Mill Games
// Demo: Legend of Larkin

#include <memory>
#include <sstream>
#include <hgl.h>
#include <hgl_font.h>
#include <hgl_sprite.h>
#include <hgl_gui.h>
#include "hgl_scenemanager.h"

HGL *hgl = 0;

constexpr unsigned WIDTH = 1280;
constexpr unsigned HEIGHT = 720;

hglSceneManager     *sceneMgr = nullptr;
hglFont				*devfont = nullptr;

#include "game.h"

/////////////////////////////////////////// App
LOLGame* game = nullptr;

bool FrameFunc()
{
	static float fps_update_timer = 0.0f;
	const float dt = hgl->Timer_GetDelta();
	fps_update_timer += dt;
	if(fps_update_timer >= 1.0f) {
		fps_update_timer -= 1.0f;
		std::ostringstream ss;
		ss << "Legend of Larkin (FPS: " << hgl->Timer_GetFPS() << ")";
		hgl->System_SetState(HGL_TITLE, ss.str().c_str());
	}
	return game->Update();
}


bool RenderFunc()
{
	hgl->Gfx_BeginScene();
	hgl->Gfx_Clear(0x404060);
	game->Render();
	// Stats
	devfont->printf(static_cast<float>(hgl->Gfx_GetWidth() - 25), 1.0f, HGLTEXT_LEFT, "%d", hgl->Timer_GetFPS());
	hgl->Gfx_EndScene();

	return false;
}


int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	hgl = hglCreate(HGL_VERSION);

	hgl->System_SetState(HGL_LOGFILE, "legend_of_larkin.log");
	hgl->System_SetState(HGL_FRAMEFUNC, FrameFunc);
	hgl->System_SetState(HGL_RENDERFUNC, RenderFunc);
	hgl->System_SetState(HGL_TITLE, "Legend of Larkin");
	hgl->System_SetState(HGL_WINDOWED, true);
	hgl->System_SetState(HGL_SCREENWIDTH, WIDTH);
	hgl->System_SetState(HGL_SCREENHEIGHT, HEIGHT);
	hgl->System_SetState(HGL_SCREENBPP, 32);
	hgl->System_SetState(HGL_HIDEMOUSE, false);

	if(hgl->System_Initiate()) {
		// Load the font, create the cursor sprite
		devfont = new hglFont("assets/fonts/devfont.fnt");

		// Do game initialization
		sceneMgr = new hglSceneManager();
		game = new LOLGame(sceneMgr);

		// Let's rock now!
		hgl->System_Start();

		// Delete created objects and free loaded resources
		delete game;
		delete devfont;
		hgl->Action_FreeAll();
	}

	// Clean up and shutdown
	hgl->System_Shutdown();
	hgl->Release();
	return 0;
}



//// Sal's HGL-compatible mini engine using EGL, OpenGL ES 2
//// Copyright (C) 2017, Cloud Mill Games
#pragma once

#include <hgl.h>
#include <cassert>
#include <map>
#include <vector>
#include <memory>

#define NOT_IMPL assert(false && "Not Implemented");

struct hglEvent {
	hglEvent() : id(0), data(nullptr) {}
	hglEvent(unsigned _id) : id(_id), data(nullptr) {}
	unsigned id;
	void* data;
};
using hglEvents = std::vector<hglEvent>;

class hglSceneManager;

class hglScene {
public:
	hglScene() {}
	hglScene(const hglScene &) = delete;
	virtual ~hglScene() {}

	virtual bool Init() { return true; }
	virtual bool Destroy() { return true; }
	virtual bool Enter() { return true; }
	virtual bool Frame(float dt, const hglEvents& events) { return false; }
	virtual bool Render() { return true; }
	virtual bool Exit() { return true; }

protected:
	friend class hglSceneManager;
	hglSceneManager* sceneMgr;
	HGL* hgl;
};

class hglSceneManager {
private:
	typedef std::map<unsigned, std::unique_ptr<hglScene>> TSceneMap;

public:
	hglSceneManager() : scCurrent(nullptr) {
		hgl = hglCreate(HGL_VERSION);
	}
	virtual ~hglSceneManager() {
		for(auto& s : sceneMap) {
			// unique_ptr handles the actualdeletion
			s.second->Destroy();
		}
		hgl->Release();
	}

	// Returns newly added scene
	hglScene* AddScene(unsigned id, hglScene* scene) {
		assert(scene);
		if(scene) {
			auto ifind = sceneMap.find(id);
			assert(ifind == sceneMap.end() && "Attempting to add scene that has already been added.");
			if(ifind == sceneMap.end()) {
				sceneMap[id].reset(scene);
				scene->hgl = hgl;
				scene->sceneMgr = this;
				bool result = scene->Init();
				assert(result && "Scene failed to initialize");
			} else {
				delete scene;
				scene = nullptr;
			}
		}
		return scene;
	}

	// Use initializer lists to create and init all scenes at once
	// Returns how many scenes were actually added
	int AddScenes(std::vector<unsigned> ids, std::vector<hglScene*> scenes) {
		assert(!ids.empty() && !scenes.empty() && ids.size() == scenes.size() && "Incorrect parameters given to AddScenes");
		int count = 0;
		for(unsigned i = 0; i < ids.size(); ++i) {
			if(AddScene(ids[i], scenes[i])) {
				count++;
			}
		}
		return count;
	}

	// Returns previous scene
	hglScene* ChangeScene(unsigned id) {
		auto ifind = sceneMap.find(id);
		assert(ifind != sceneMap.end() && "Attempting to change to an undefined scene");
		if(ifind != sceneMap.end()) {
			if(scCurrent) {
				scCurrent->Exit();
			}
			scCurrent = ifind->second.get();
			uCurrentScene = id;
			scCurrent->Enter();
		}
		return nullptr;
	}

	unsigned GetCurrentScene() {
		return uCurrentScene;
	}

	hglScene* GetScene(unsigned id) {
		auto ifind = sceneMap.find(id);
		assert(ifind != sceneMap.end() && "Attempting to retrieve scene with an undefined id");
		hglScene* sout = nullptr;
		if(ifind != sceneMap.end()) {
			sout = sceneMap[id].get();
		}
		return sout;
	}

	bool Frame(const hglEvents& events = hglEvents()) {
		const float dt = hgl->Timer_GetDelta();
		if(scCurrent) {
			return scCurrent->Frame(dt, events);
		}
		return false;
	}

	bool Render() {
		if(scCurrent) {
			scCurrent->Render();
		}
		return false;
	}

private:
	HGL* hgl;
	TSceneMap sceneMap;
	hglScene* scCurrent;
	unsigned uCurrentScene;
};

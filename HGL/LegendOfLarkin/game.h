//// Sal's HGL-compatible mini engine using EGL, OpenGL ES 2
//// Copyright (C) 2017, Cloud Mill Games
#pragma once

#include <hgl.h>
#include <hgl_sprite.h>
#include <cmath>

#define COPYRIGHT_TEXT "Copyright (c) 2017 Cloud Mill Games, Salwan & Abdullah Hilali"

/////////////////////////////////// Game
class LOLGame {
	/////////////////////////////// Scene definition
	enum EScene {
		SCENE_MENU,
		SCENE_GAME,
	};
	enum EEvents : unsigned {
		EDown,
		EUp,
		ELeft,
		ERight,
		EStart,
		EBack,
	};
	/////////////////////////////// Main Menu scene
	class LOLMenuScene : public hglScene {
	private:
		HTEXTURE texBG;
		HTEXTURE texBtns;
		HTEXTURE texLabels [2];
		HTEXTURE texTitle;
		HEFFECT musTeaser;
		HEFFECT sndMenu;
		std::unique_ptr<hglFont> font;
		std::unique_ptr<hglSprite> sprTitle;
		std::unique_ptr<hglQuad> quadBG;
		std::unique_ptr<hglSprite> sprBtnUp;
		std::unique_ptr<hglSprite> sprBtnDown;
		std::unique_ptr<hglSprite> sprLabels [2];
		int iCurrent;

	public:
		LOLMenuScene() : musTeaser(nullptr), sndMenu(nullptr) {}

		virtual bool Init() override {
			// File loading
			texBG = hgl->Texture_Load("assets/mainmenu/intro_bg_tile.png");
			texTitle = hgl->Texture_Load("assets/mainmenu/title_alter.png");
			font.reset(new hglFont("assets/fonts/font1.fnt"));
			// Background
			float width = (float)hgl->Gfx_GetWidth();
			float height = (float)hgl->Gfx_GetHeight();
			quadBG.reset(new hglQuad());
			float tw = width / (float)hgl->Texture_GetWidth(texBG);
			float th = height / (float)hgl->Texture_GetHeight(texBG);
			quadBG->fullscreen(width, height, tw, th);
			quadBG->tex = texBG;
			// Title
			sprTitle.reset(new hglSprite(texTitle));
			sprTitle->CentreHotSpot();
			// Buttons
			texBtns = hgl->Texture_Load("assets/mainmenu/title_btn.png");
			sprBtnUp.reset(new hglSprite(texBtns));
			sprBtnUp->SetTextureRect(0.0f, 0.0f, 248.0f, 51.0f);
			sprBtnUp->CentreHotSpot();
			sprBtnDown.reset(new hglSprite(texBtns));
			sprBtnDown->SetTextureRect(0.0f, 51.0f, 248.0f, 51.0f);
			sprBtnDown->CentreHotSpot();
			// Labels
			texLabels[0] = hgl->Texture_Load("assets/mainmenu/title_START.png");
			texLabels[1] = hgl->Texture_Load("assets/mainmenu/title_EXIT.png");
			sprLabels[0].reset(new hglSprite(texLabels[0]));
			sprLabels[1].reset(new hglSprite(texLabels[1]));
			sprLabels[0]->CentreHotSpot();
			sprLabels[1]->CentreHotSpot();
			//
			iCurrent = 0;
			return true;

		}

		virtual bool Enter() override {
			// Music
			if(!musTeaser) {
				musTeaser = hgl->Effect_Load("assets/mainmenu/teaser.ogg");
			}
			if(!sndMenu) {
				sndMenu = hgl->Effect_Load("assets/mainmenu/menu.wav");
			}
			hgl->Effect_PlayEx(musTeaser, 70, 0, 1.0f, true);
			return true;
		}

		virtual bool Frame(float dt, const hglEvents& events) override {
			quadBG->scroll(0.8f * -dt, 0.5f * dt);
			for(auto& e : events) {
				if(e.id == EUp) {
					if(iCurrent != 0) {
						hgl->Effect_Play(sndMenu);
					}
					iCurrent = 0;
				} else if(e.id == EDown) {
					if(iCurrent != 1) {
						hgl->Effect_Play(sndMenu);
					}
					iCurrent = 1;
				}
				if(e.id == EStart) {
					if(iCurrent == 0) {
						sceneMgr->ChangeScene(SCENE_GAME);
					} else {
						return true;
					}
				} else if(e.id == EBack) {
					return true;
				}
			}
			return false;
		}

		virtual bool Render() override {
			static float angle = 0.0f, angle2 = 0.0f;
			const float dt = hgl->Timer_GetDelta();
			angle += dt * 1.2f;
			angle2 += dt * 8.0f;
			float width = (float)hgl->Gfx_GetWidth();
			float height = (float)hgl->Gfx_GetHeight();
			// Background
			hgl->Gfx_RenderQuad(quadBG.get());
			// Title
			sprTitle->Render((width / 2.0f) + (std::sinf(angle) * 1.0f), 266.0f + (std::cosf(angle) * -4.0f));
			// Stretch
			const float stx = std::sinf(angle2);
			const float sty = std::cosf(angle2);
			// Buttons
			const auto mid = (width - 51.0f) / 2.0f;
			auto yb1 = 550.0f;
			auto yb2 = 605.0f;
			if(iCurrent == 0) {
				sprBtnDown->RenderEx(mid, yb1, 0.0f, 1.0f + (stx * 0.03f), 1.0f + (stx * 0.03f));
				sprBtnUp->Render(mid, yb2);
				sprLabels[0]->RenderEx(mid, yb1, 0.0f, 1.0f + (stx * 0.05f), 1.0f + (sty * 0.05f));
				sprLabels[1]->Render(mid, yb2);
			} else {
				sprBtnDown->RenderEx(mid, yb2, 0.0f, 1.0f + (stx * 0.03f), 1.0f + (stx * 0.03f));
				sprBtnUp->Render(mid, yb1);
				sprLabels[1]->RenderEx(mid, yb2, 0.0f, 1.0f + (stx * 0.05f), 1.0f + (sty * 0.05f));
				sprLabels[0]->Render(mid, yb1);
			}
			// Copyright
			float strw = font->GetStringWidth(COPYRIGHT_TEXT);
			font->Render(width / 2.0f, height - 32.0f, HGL_TextAlignH::HGLTEXT_CENTER, COPYRIGHT_TEXT);
			return false;
		}

		virtual bool Exit() override {
			hgl->Channel_StopAll();
			return true;
		}
		
	};
	//////////////////////////////////// Game Scene
	class LOLGameScene : public hglScene {
		virtual bool Render() override {
			hgl->Gfx_Clear(0xff000000);
			return false;
		}

		virtual bool Frame(float dt, const hglEvents& events) override {
			for (auto& e : events) {
				if (e.id == EEvents::EBack) {
					sceneMgr->ChangeScene(SCENE_MENU);
				}
			}
			return false;
		}
	};
public:
	//////////////////////////////////// Game
	LOLGame(hglSceneManager* scene_mgr) : sceneMgr(scene_mgr), scMenu(nullptr) {
		// Create all scenes
		sceneMgr->AddScenes(
			{SCENE_MENU,		SCENE_GAME}, 
			{new LOLMenuScene,	new LOLGameScene}
		);
		sceneMgr->ChangeScene(SCENE_MENU);
		// All scenes have access to sceneMgr directly (internally initialized)
		// Can a scene pass info to another scene on change?
		// init input
		acUp = hgl->Action_Create("Up");
		acDown = hgl->Action_Create("Down");
		acLeft = hgl->Action_Create("Left");
		acRight = hgl->Action_Create("Right");
		acStart = hgl->Action_Create("Start");
		acBack = hgl->Action_Create("Back");

		hgl->Action_AddKey(acUp, HGLK_UP);
		hgl->Action_AddKey(acUp, HGLK_W);
		hgl->Action_AddPadButton(acUp, HGLP_DPAD_UP);

		hgl->Action_AddKey(acDown, HGLK_DOWN);
		hgl->Action_AddKey(acDown, HGLK_S);
		hgl->Action_AddPadButton(acDown, HGLP_DPAD_DOWN);

		hgl->Action_AddKey(acLeft, HGLK_LEFT);
		hgl->Action_AddKey(acLeft, HGLK_A);
		hgl->Action_AddPadButton(acLeft, HGLP_DPAD_LEFT);

		hgl->Action_AddKey(acRight, HGLK_RIGHT);
		hgl->Action_AddKey(acRight, HGLK_D);
		hgl->Action_AddPadButton(acRight, HGLP_DPAD_RIGHT);

		hgl->Action_AddKey(acStart, HGLK_ENTER);
		hgl->Action_AddKey(acStart, HGLK_SPACE);
		hgl->Action_AddPadButton(acStart, HGLP_A);

		hgl->Action_AddKey(acBack, HGLK_ESCAPE);
		hgl->Action_AddPadButton(acBack, HGLP_B);

	}
	virtual ~LOLGame() {
		// No need to delete scenes, sceneMgr owns them
	}
public:
	///////////////// Game Events
	bool Update() {
		hglEvents events;
		bool up = hgl->Action_GetState(acUp) != HGLA_UP;
		bool down = hgl->Action_GetState(acDown) != HGLA_UP;
		bool left = hgl->Action_GetState(acLeft) != HGLA_UP;
		bool right = hgl->Action_GetState(acRight) != HGLA_UP;
		bool start = hgl->Action_GetState(acStart) != HGLA_UP;
		bool back = hgl->Action_GetState(acBack) == HGLA_RELEASED;
		if(up && !down) {
			events.push_back(EUp);
		}
		if(down && !up) {
			events.push_back(EDown);
		}
		if(left && !right) {
			events.push_back(ELeft);
		}
		if(right && !left) {
			events.push_back(ERight);
		}
		if(start && !back) {
			events.push_back(EStart);
		}
		if(back && !start) {
			events.push_back(EBack);
		}
		return sceneMgr->Frame(events);
	}

	void Render() {
		sceneMgr->Render();
	}

	void Test() {
		if(sceneMgr->GetCurrentScene() == SCENE_GAME) {
			sceneMgr->ChangeScene(SCENE_MENU);
		} else {
			sceneMgr->ChangeScene(SCENE_GAME);
		}
	}

private:
	hglSceneManager*	sceneMgr;
	hglScene*			scMenu;
	hglScene*			scGame;
	HACTION				acUp;
	HACTION				acDown;
	HACTION				acLeft;
	HACTION				acRight;
	HACTION				acStart;
	HACTION				acBack;
};
//// Sal's HGL-compatible mini engine using EGL, OpenGL ES 2
//// Copyright (C) 2017, Cloud Mill Games
#pragma once

#include <hgl.h>
#include <hgl_gui.h>
#include <hgl_color.h>
#include <hgl_sprite.h>

class lolMainMenuItem : public hglGUIObject {
public:
	lolMainMenuItem(int _id, hglSprite *_text, hglSprite *_btnUp, hglSprite *_btnDown, HEFFECT _snd, float _x, float _y, float _delay) :
		            sprText(_text), sprBtnUp(_btnUp), sprBtnDown(_btnDown), snd(_snd), delay(_delay) {
		id = _id;
		offset = 0.0f;
		timer = -1.0f;
		timer2 = -1.0f;
		bStatic = false;
		bVisible = true;
		bEnabled = true;
		const float w = _btnUp->GetWidth();
		rect.Set(_x - w / 2, _y, _x + w / 2, _y + _btnUp->GetHeight());
	}

	void Render() {
		sprBtnUp->Render(rect.x1 + offset + 3, rect.y1 + 3);
	}

	void Update(float dt) {
		if(timer2 != -1.0f) {
			timer2 += dt;
			if(timer2 >= delay + 0.1f) {
				offset = 0.0f;
				timer2 = -1.0f;
			}
		} else if(timer != -1.0f) {
			timer += dt;
			if(timer >= 0.2f) {
				offset = soffset + doffset;
				timer = -1.0f;
			} else {
				offset = soffset + doffset * timer * 5;
			}
		}
	}

	void Enter() {
		timer2 = 0.0f;
	}

	void Leave() {
		timer2 = 0.0f;
	}

	bool IsDone() {
		if(timer2 == -1.0f) {
			return true;
		} else {
			return false;
		}
	}

	void Focus(bool bFocused) {
		if(bFocused) {
			soffset = 0;
			doffset = 4;
		} else {
			soffset = 4;
			doffset = -4;
		}
		timer = 0.0f;
	}

	void MouseOver(bool bOver) {
		if(bOver) {
			gui->SetFocus(id);
		}
	}

	bool MouseLButton(bool bDown) {
		if(!bDown) {
			offset = 4;
			return true;
		} else {
			if(snd) hgl->Effect_Play(snd);
			offset = 0;
			return false;
		}
	}

	bool KeyClick(int key, int chr) {
		if(key == HGLK_ENTER || key == HGLK_SPACE) {
			MouseLButton(true);
			return MouseLButton(false);
		}
		return false;
	}

	bool PadClick(hglGUIPadKey_t pad_gui_key) {
		if(pad_gui_key == HGLPK_A) {
			MouseLButton(true);
			return MouseLButton(false);
		}
		return false;
	}

private:
	hglSprite	*sprText;
	hglSprite   *sprBtnUp;
	hglSprite   *sprBtnDown;
	HEFFECT		snd;
	float		delay;
	char		*title;

	hglColor	scolor, dcolor, scolor2, dcolor2, sshadow, dshadow;
	hglColor	color, shadow;
	float		soffset, doffset, offset;
	float		timer, timer2;
};
set Config=%1
set Arch=%2

echo Copying hgl.h to Build\Include..
mkdir ..\Build\Include
copy /A ..\HGL\hgl.h ..\Build\Include\

echo Configuration: %Config%
echo Architecture: %Arch%

echo Copying libsndfile-1.dll..
copy ..\External\Windows\Libs%Arch%\libsndfile-1.dll ..\Build\%Config%%Arch%\

echo Copying libGLESv2.dll..
copy ..\External\Windows\Libs%Arch%\libGLESv2.dll ..\Build\%Config%%Arch%\

echo Copying soft_oal.dll..
copy ..\External\Windows\Libs%Arch%\soft_oal.dll ..\Build\%Config%%Arch%\OpenAL32.dll

echo Copying SOIL.dll..
copy ..\External\Windows\Libs%Arch%\SOIL.dll ..\Build\%Config%%Arch%\

echo Copying libEGL.dll..
copy ..\External\Windows\Libs%Arch%\libEGL.dll ..\Build\%Config%%Arch%\

echo Copying gainput.dll
copy ..\External\Windows\Libs%Arch%\gainput.dll ..\Build\%Config%%Arch%\

echo Copying zlibwapi.dll
copy ..\External\Windows\Libs%Arch%\zlibwapi.dll ..\Build\%Config%%Arch%\

echo Generating Plan.html off Plan.md (this requires markdown installed: easy_install markdown)
python -m markdown Plan.md > Plan.html

//// Sal's HGL-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hgl 

#include <hgl.h>
#include <hgl_font.h>
#include <hgl_sprite.h>


#define SCREEN_WIDTH  1280
#define SCREEN_HEIGHT 720

#define MIN_OBJECTS	100
#define START_OBJECTS 25000
#define MAX_OBJECTS 200000
#define STEP_OBJECTS 1000

//#define WALLPAPER_MODE

struct sprObject
{
	float x, y;
	float dx, dy;
	float scale, rot;
	float dscale, drot;
	unsigned color;
};

sprObject*	pObjects;
int			nObjects;
int			nBlend;

// Pointer to the HGL interface (helper classes require this to work)

HGL *hgl = 0;

// Resource handles

HTEXTURE			tex, bgtex;
hglSprite			*spr, *bgspr;
hglFont				*fnt;
hglQuad				batchQuad;
hglQuad				rQuad;

// Set up blending mode for the scene

void SetBlend(int blend)
{
	static int sprBlend[5] =
	{
		BLEND_COLORMUL | BLEND_ALPHABLEND | BLEND_NOZWRITE,
		BLEND_COLORADD | BLEND_ALPHABLEND | BLEND_NOZWRITE,
		BLEND_COLORMUL | BLEND_ALPHABLEND | BLEND_NOZWRITE,
		BLEND_COLORMUL | BLEND_ALPHAADD | BLEND_NOZWRITE,
		BLEND_COLORMUL | BLEND_ALPHABLEND | BLEND_NOZWRITE
	};

	static unsigned fntColor[5] =
	{
		0xFFFFFFFF, 0xFF000000, 0xFFFFFFFF, 0xFF000000, 0xFFFFFFFF
	};

	static unsigned sprColors[5][5] =
	{
		{ 0xFFFFFFFF, 0xFFFFE080, 0xFF80A0FF, 0xFFA0FF80, 0xFFFF80A0 },
		{ 0xFF000000, 0xFF303000, 0xFF000060, 0xFF006000, 0xFF600000 },
		{ 0x80FFFFFF, 0x80FFE080, 0x8080A0FF, 0x80A0FF80, 0x80FF80A0 },
		{ 0x80FFFFFF, 0x80FFE080, 0x8080A0FF, 0x80A0FF80, 0x80FF80A0 },
		{ 0x40202020, 0x40302010, 0x40102030, 0x40203010, 0x40102030 }
	};

	if (blend>4) blend = 0;
	nBlend = blend;

	spr->SetBlendMode(sprBlend[blend]);
	fnt->SetColor(fntColor[blend]);
	for (int i = 0;i<MAX_OBJECTS;i++) pObjects[i].color = sprColors[blend][hgl->Random_Int(0, 4)];
}

bool FrameFunc()
{
	float dt = hgl->Timer_GetDelta();
	int i;

	// Process keys

	switch (hgl->Input_GetKey()) {
	case HGLK_UP:		if (nObjects<MAX_OBJECTS) nObjects += STEP_OBJECTS; break;
	case HGLK_DOWN:		if (nObjects>MIN_OBJECTS) nObjects -= STEP_OBJECTS; break;
	case HGLK_SPACE:	SetBlend(++nBlend); break;
	case HGLK_ESCAPE:	return true;
	}

	// Update the scene

	for (i = 0;i<nObjects;i++) {
		pObjects[i].x += pObjects[i].dx*dt;
		if (pObjects[i].x>SCREEN_WIDTH || pObjects[i].x<0) pObjects[i].dx = -pObjects[i].dx;
		pObjects[i].y += pObjects[i].dy*dt;
		if (pObjects[i].y>SCREEN_HEIGHT || pObjects[i].y<0) pObjects[i].dy = -pObjects[i].dy;
		pObjects[i].scale += pObjects[i].dscale*dt;
		if (pObjects[i].scale>2 || pObjects[i].scale<0.5) pObjects[i].dscale = -pObjects[i].dscale;
		pObjects[i].rot += pObjects[i].drot*dt;
	}

	return false;
}


bool RenderFunc()
{
	int i;

	// Render the scene
	hgl->Gfx_BeginScene();
	bgspr->Render(0, 0);

	for (i = 0;i<nObjects;i++) {
		spr->SetColor(pObjects[i].color);
		spr->RenderEx(pObjects[i].x, pObjects[i].y, pObjects[i].rot, pObjects[i].scale);
	}

	hgl->Gfx_RenderQuad(&rQuad);

	// Batch test
	{
		int mp;
		hglVertex* vb = hgl->Gfx_StartBatch(HGLPRIM_QUADS, 0, batchQuad.blend, &mp);
		if (mp > 0) {
			memcpy(vb, &batchQuad.v, sizeof(hglVertex) * 4);
		}
		hgl->Gfx_FinishBatch(1);
	}

#ifndef WALLPAPER_MODE
	fnt->printf(7, 7, HGLTEXT_LEFT, "UP and DOWN to adjust number of hares: %d\nSPACE to change blending mode: %d\nFPS: %d", nObjects, nBlend, hgl->Timer_GetFPS());
#endif
	hgl->Gfx_EndScene();

	return false;
}

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	int i;

	hgl = hglCreate(HGL_VERSION);

	// Set desired system states and initialize HGL

	hgl->System_SetState(HGL_LOGFILE, "hgl_tut07.log");
	hgl->System_SetState(HGL_FRAMEFUNC, FrameFunc);
	hgl->System_SetState(HGL_RENDERFUNC, RenderFunc);
	hgl->System_SetState(HGL_TITLE, "HGL Tutorial 07 - Thousand of Hares");
	hgl->System_SetState(HGL_USESOUND, false);
	hgl->System_SetState(HGL_WINDOWED, true);
	hgl->System_SetState(HGL_SCREENWIDTH, SCREEN_WIDTH);
	hgl->System_SetState(HGL_SCREENHEIGHT, SCREEN_HEIGHT);
	hgl->System_SetState(HGL_SCREENBPP, 32);

	if (hgl->System_Initiate()) {

		// Load textures

		bgtex = hgl->Texture_Load("assets/bg2.png");
		tex = hgl->Texture_Load("assets/zazaka.png");
		if (!bgtex || !tex) {
			// If one of the data files is not found,
			// display an error message and shutdown
			MessageBoxA(NULL, "Can't load BG2.PNG or ZAZAKA.PNG", "Error", MB_OK | MB_ICONERROR | MB_APPLMODAL);
			hgl->System_Shutdown();
			hgl->Release();
			return 0;
		}

		// Load font, create sprites

		fnt = new hglFont("assets/font2.fnt");
		spr = new hglSprite(tex, 0, 0, 64, 64);
		spr->SetHotSpot(32, 32);

		bgspr = new hglSprite(bgtex, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
		bgspr->SetBlendMode(BLEND_COLORADD | BLEND_ALPHABLEND | BLEND_NOZWRITE);
		bgspr->SetColor(0xFF000000, 0);
		bgspr->SetColor(0xFF000000, 1);
		bgspr->SetColor(0xFF000040, 2);
		bgspr->SetColor(0xFF000040, 3);

		// Initialize objects list

		pObjects = new sprObject[MAX_OBJECTS];
		nObjects = START_OBJECTS;

		for (i = 0;i<MAX_OBJECTS;i++) {
			pObjects[i].x = hgl->Random_Float(0, SCREEN_WIDTH);
			pObjects[i].y = hgl->Random_Float(0, SCREEN_HEIGHT);
			pObjects[i].dx = hgl->Random_Float(-200, 200);
			pObjects[i].dy = hgl->Random_Float(-200, 200);
			pObjects[i].scale = hgl->Random_Float(0.5f, 2.0f);
			pObjects[i].dscale = hgl->Random_Float(-1.0f, 1.0f);
			pObjects[i].rot = hgl->Random_Float(0, M_PI * 2);
			pObjects[i].drot = hgl->Random_Float(-1.0f, 1.0f);
		}

		SetBlend(0);

		// Batch rendering
		batchQuad = {
			{
				hglVertex(10.0f, 110.0f, 0.5f, 0x80ff0000, 0.0f, 0.0f),
				hglVertex(300.0f, 120.0f, 0.5f, 0xff00ff00, 1.0f, 0.0f),
				hglVertex(300.0f, 300.0f, 0.5f, 0xff0000ff, 1.0f, 1.0f),
				hglVertex(10.0f, 290.0f, 0.5f, 0x80ffffff, 0.0f, 1.0f),
			},
			0,	// HTEXTURE
			BLEND_COLORMUL | BLEND_ALPHABLEND,
		};
		memcpy(&rQuad, &batchQuad, sizeof(hglQuad));
		rQuad.v[0].x = 310.0f;
		rQuad.v[1].x = 610.0f;
		rQuad.v[2].x = 610.0f;
		rQuad.v[3].x = 310.0f;
		rQuad.v[0].z = rQuad.v[1].z = rQuad.v[2].z = rQuad.v[3].z = 1.0f;

		// Let's rock now!

		hgl->System_Start();

		// Delete created objects and free loaded resources

		delete[] pObjects;
		delete fnt;
		delete spr;
		delete bgspr;
		hgl->Texture_Free(tex);
		hgl->Texture_Free(bgtex);
	}

	// Clean up and shutdown

	hgl->System_Shutdown();
	hgl->Release();
	return 0;
}

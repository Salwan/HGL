// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// https://github.com/kvakvs/hge 

#include <vector>

#include <hgl.h>
#include <hgl_font.h>
#include <hgl_sprite.h>

constexpr unsigned STARTUP_BUNNIES = 10000;
constexpr unsigned SCREENW = 1280;
constexpr unsigned SCREENH = 720;

HGL							*hgl = nullptr;
hglFont						*debugFont = nullptr;
HTEXTURE					bunnyTex;
hglSprite					*rectBG = nullptr;

unsigned					bunnyW;
unsigned					bunnyH;
hglRect						rcBounds;
float						fGravity = 0.5f;
bool						bAddBunnies = false;

class Bunny : public hglSprite {
public:
	Bunny() : hglSprite(bunnyTex, 0.0f, 0.0f, (float)bunnyW, (float)bunnyH) {\
		x = 0.0f;
		y = 0.0f;
	}

	void Render() {
		hglSprite::Render(x, y);
	}

	float x;
	float y;
	float speedX;
	float speedY;
};

std::vector<Bunny*>		bunnies;

void addBunny() {
	Bunny* bunny = new Bunny();
	bunny->speedX = hgl->Random_Float() * 5.0f;
	bunny->speedY = (hgl->Random_Float() * 5.0f) - 2.5f;
	bunnies.push_back(bunny);
}

bool FrameFunc() {
	float dt = hgl->Timer_GetDelta();

	// Input
	if (hgl->Input_GetKeyState(HGLK_SPACE)) {
		// Add bunnies!
		bAddBunnies = true;
	} else {
		bAddBunnies = false;
	}
	if (hgl->Input_GetKeyState(HGLK_ESCAPE)) { // exit
		return true;
	}

	// Update all bunnies
	for (auto b : bunnies) {
		Bunny &bunny = *b;
		bunny.x += bunny.speedX;
		bunny.y += bunny.speedY;
		bunny.speedY += fGravity;

		if (bunny.x > rcBounds.x2) {

			bunny.speedX *= -1.0f;
			bunny.x = rcBounds.x2;

		} else if (bunny.x < rcBounds.x1) {

			bunny.speedX *= -1.0f;
			bunny.x = rcBounds.x1;

		}

		if (bunny.y > rcBounds.y2) {

			bunny.speedY *= -0.8f;
			bunny.y = rcBounds.y2;

			if (hgl->Random_Float() > 0.5f) {

				bunny.speedY -= 3.0f + hgl->Random_Float() * 4.0f;

			}

			// Change color
			uint8_t r = hgl->Random_Int(128, 255);
			uint8_t g = hgl->Random_Int(128, 255);
			uint8_t b = hgl->Random_Int(128
				, 255);
			bunny.SetColor(ARGB(0xff, r, g, b));

		} else if (bunny.y < rcBounds.y1) {

			bunny.speedY = 0.0f;
			bunny.y = rcBounds.y1;

		}
	}

	if (bAddBunnies) {
		for (auto i = 0; i < 100; ++i) {
			addBunny();
		}
	}

	return false;
}

bool RenderFunc() {
	hgl->Gfx_BeginScene();
	hgl->Gfx_Clear(0xffa0a0a0);

	// Render bunnies
	for (auto b : bunnies) {
		b->Render();
	}
	
	// Render text rect
	rectBG->Render(0.0f, 5.0f);

	// Render text
	debugFont->printf(5.0f, 6.0f, HGLTEXT_LEFT, "FPS:%d", hgl->Timer_GetFPS());
	debugFont->printf(SCREENW - 120.0f, 6.0f, HGLTEXT_LEFT, "%d BUNNIES", bunnies.size());

	hgl->Gfx_EndScene();

	return false;
}

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int) {
	hgl = hglCreate(HGL_VERSION);
	hgl->System_SetState(HGL_LOGFILE, "hgl_bunnymark.log");
	hgl->System_SetState(HGL_FRAMEFUNC, FrameFunc);
	hgl->System_SetState(HGL_RENDERFUNC, RenderFunc);
	hgl->System_SetState(HGL_TITLE, "HGL - BunnyMark");
	hgl->System_SetState(HGL_WINDOWED, true);
	hgl->System_SetState(HGL_SCREENWIDTH, SCREENW);
	hgl->System_SetState(HGL_SCREENHEIGHT, SCREENH);
	hgl->System_SetState(HGL_SCREENBPP, 32);
	hgl->System_SetState(HGL_PERFORMANCEMODE, PERFORMANCE_HIGH);

	if (hgl->System_Initiate()) {
		hgl->Random_Seed();

		bunnyTex = hgl->Texture_Load("assets/wabbit_alpha.png");
		if (!bunnyTex) {
			MessageBoxA(NULL, 
				"Couldn't load one of the resource files", "Error", MB_OK|MB_ICONERROR|MB_APPLMODAL);
			hgl->System_Shutdown();
			hgl->Release();
			return 0;
		}
		bunnyW = hgl->Texture_GetWidth(bunnyTex);
		bunnyH = hgl->Texture_GetHeight(bunnyTex);

		debugFont = new hglFont("assets/debug_font.fnt");
		debugFont->SetColor(0xff000000);

		rcBounds.x1 = 0.0f;
		rcBounds.x2 = SCREENW;
		rcBounds.y1 = 0.0f;
		rcBounds.y2 = SCREENH;

		rectBG = new hglSprite(0, 0.0f, 0.0f, (float)SCREENW, 20.0f);
		rectBG->SetColor(0x80ff80ff);
		rectBG->SetBlendMode(BLEND_COLORADD);

		for (auto i = 0; i < STARTUP_BUNNIES; ++i) {
			addBunny();
		}

		hgl->System_Start();

		delete debugFont;
		delete rectBG;
		hgl->Texture_Free(bunnyTex);
	}

	hgl->System_Shutdown();
	hgl->Release();
	return 0;
}
// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge 
#pragma once

#define hglColor hglColorRGB

inline void ColorClamp(float &x) { if (x<0.0f) x = 0.0f; if (x>1.0f) x = 1.0f; }


class hglColorRGB
{
public:
	float		r, g, b, a;

	hglColorRGB(float _r, float _g, float _b, float _a) { r = _r; g = _g; b = _b; a = _a; }
	hglColorRGB(unsigned col) { SetHWColor(col); }
	hglColorRGB() { r = g = b = a = 0; }

	hglColorRGB		operator-  (const hglColorRGB &c) const { return hglColorRGB(r - c.r, g - c.g, b - c.b, a - c.a); }
	hglColorRGB		operator+  (const hglColorRGB &c) const { return hglColorRGB(r + c.r, g + c.g, b + c.b, a + c.a); }
	hglColorRGB		operator*  (const hglColorRGB &c) const { return hglColorRGB(r*c.r, g*c.g, b*c.b, a*c.a); }
	hglColorRGB&	operator-= (const hglColorRGB &c) { r -= c.r; g -= c.g; b -= c.b; a -= c.a; return *this; }
	hglColorRGB&	operator+= (const hglColorRGB &c) { r += c.r; g += c.g; b += c.b; a += c.a; return *this; }
	bool			operator== (const hglColorRGB &c) const { return (r == c.r && g == c.g && b == c.b && a == c.a); }
	bool			operator!= (const hglColorRGB &c) const { return (r != c.r || g != c.g || b != c.b || a != c.a); }

	hglColorRGB		operator/  (const float scalar) const { return hglColorRGB(r / scalar, g / scalar, b / scalar, a / scalar); }
	hglColorRGB		operator*  (const float scalar) const { return hglColorRGB(r*scalar, g*scalar, b*scalar, a*scalar); }
	hglColorRGB&	operator*= (const float scalar) { r *= scalar; g *= scalar; b *= scalar; a *= scalar; return *this; }

	void			Clamp() { ColorClamp(r); ColorClamp(g); ColorClamp(b); ColorClamp(a); }
	void			SetHWColor(unsigned col) { a = (col >> 24) / 255.0f; r = ((col >> 16) & 0xFF) / 255.0f; g = ((col >> 8) & 0xFF) / 255.0f; b = (col & 0xFF) / 255.0f; }
	unsigned		GetHWColor() const { return (unsigned(a*255.0f) << 24) + (unsigned(r*255.0f) << 16) + (unsigned(g*255.0f) << 8) + unsigned(b*255.0f); }
};

inline hglColorRGB operator* (const float sc, const hglColorRGB &c) { return c*sc; }


class hglColorHSV
{
public:
	float		h, s, v, a;

	hglColorHSV(float _h, float _s, float _v, float _a) { h = _h; s = _s; v = _v; a = _a; }
	hglColorHSV(unsigned col) { SetHWColor(col); }
	hglColorHSV() { h = s = v = a = 0; }

	hglColorHSV		operator-  (const hglColorHSV &c) const { return hglColorHSV(h - c.h, s - c.s, v - c.v, a - c.a); }
	hglColorHSV		operator+  (const hglColorHSV &c) const { return hglColorHSV(h + c.h, s + c.s, v + c.v, a + c.a); }
	hglColorHSV		operator*  (const hglColorHSV &c) const { return hglColorHSV(h*c.h, s*c.s, v*c.v, a*c.a); }
	hglColorHSV&	operator-= (const hglColorHSV &c) { h -= c.h; s -= c.s; v -= c.v; a -= c.a; return *this; }
	hglColorHSV&	operator+= (const hglColorHSV &c) { h += c.h; s += c.s; v += c.v; a += c.a; return *this; }
	bool			operator== (const hglColorHSV &c) const { return (h == c.h && s == c.s && v == c.v && a == c.a); }
	bool			operator!= (const hglColorHSV &c) const { return (h != c.h || s != c.s || v != c.v || a != c.a); }

	hglColorHSV		operator/  (const float scalar) const { return hglColorHSV(h / scalar, s / scalar, v / scalar, a / scalar); }
	hglColorHSV		operator*  (const float scalar) const { return hglColorHSV(h*scalar, s*scalar, v*scalar, a*scalar); }
	hglColorHSV&	operator*= (const float scalar) { h *= scalar; s *= scalar; v *= scalar; a *= scalar; return *this; }

	void			Clamp() { ColorClamp(h); ColorClamp(s); ColorClamp(v); ColorClamp(a); }
	void			SetHWColor(unsigned col);
	unsigned		GetHWColor() const;
};

inline hglColorHSV operator* (const float sc, const hglColorHSV &c) { return c*sc; }


// Sal's HGL-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge
#pragma once

#include "hgl_resource_manager.h"

class RScriptParser;

#define RES_SCRIPT		0

#define RES_RESOURCE	1
#define RES_TEXTURE		2
#define RES_EFFECT		3
#define RES_MUSIC		4
#define RES_STREAM		5
#define RES_TARGET		6
#define RES_SPRITE		7
#define RES_ANIMATION	8
#define RES_FONT		9
#define RES_PARTICLE	10
#define RES_DISTORT		11
#define RES_STRTABLE	12


void		AddRes(hglResourceManager *rm, int type, ResDesc *resource);
ResDesc*	FindRes(hglResourceManager *rm, int type, const char *name);


struct RScript : public ResDesc {
	static  void  Parse(hglResourceManager *rm, RScriptParser *sp, const char *name,
		const char *basename);
	virtual unsigned Get(hglResourceManager *rm) {
		return 0;
	}
	virtual void  Free() {}
};

struct RResource : public ResDesc {
	char		  filename[MAXRESCHARS];

	static  void  Parse(hglResourceManager *rm, RScriptParser *sp, const char *name,
		const char *basename);
	virtual unsigned Get(hglResourceManager *rm);
	virtual void  Free();
};

struct RTexture : public ResDesc {
	char		  filename[MAXRESCHARS];
	bool		  mipmap;

	static  void  Parse(hglResourceManager *rm, RScriptParser *sp, const char *name,
		const char *basename);
	virtual unsigned Get(hglResourceManager *rm);
	virtual void  Free();
};

struct REffect : public ResDesc {
	char		  filename[MAXRESCHARS];

	static  void  Parse(hglResourceManager *rm, RScriptParser *sp, const char *name,
		const char *basename);
	virtual unsigned Get(hglResourceManager *rm);
	virtual void  Free();
};

struct RMusic : public ResDesc {
	char		  filename[MAXRESCHARS];
	int			  amplify;

	static  void  Parse(hglResourceManager *rm, RScriptParser *sp, const char *name,
		const char *basename);
	virtual unsigned Get(hglResourceManager *rm);
	virtual void  Free();
};

struct RStream : public ResDesc {
	char		  filename[MAXRESCHARS];

	static  void  Parse(hglResourceManager *rm, RScriptParser *sp, const char *name,
		const char *basename);
	virtual unsigned Get(hglResourceManager *rm);
	virtual void  Free();
};

struct RTarget : public ResDesc {
	int			width;
	int			height;
	bool		zbuffer;

	static  void  Parse(hglResourceManager *rm, RScriptParser *sp, const char *name,
		const char *basename);
	virtual unsigned Get(hglResourceManager *rm);
	virtual void  Free();
};

struct RSprite : public ResDesc {
	char		texname[MAXRESCHARS];
	float		tx, ty, w, h;
	float		hotx, hoty;
	int			blend;
	unsigned		color;
	float		z;
	bool		bXFlip, bYFlip;
	//	float		x,y;
	//	float		scale;
	//	float		rotation;
	//	int			collision;

	static  void  Parse(hglResourceManager *rm, RScriptParser *sp, const char *name,
		const char *basename);
	virtual unsigned Get(hglResourceManager *rm);
	virtual void  Free();
};

struct RAnimation : public RSprite {
	int			frames;
	float		fps;
	int			mode;

	static  void  Parse(hglResourceManager *rm, RScriptParser *sp, const char *name,
		const char *basename);
	virtual unsigned Get(hglResourceManager *rm);
	virtual void  Free();
};

struct RFont : public ResDesc {
	char		filename[MAXRESCHARS];
	bool		mipmap;
	int			blend;
	unsigned		color;
	float		z;
	float		scale;
	float		proportion;
	float		tracking;
	float		spacing;
	float		rotation;

	static  void  Parse(hglResourceManager *rm, RScriptParser *sp, const char *name,
		const char *basename);
	virtual unsigned Get(hglResourceManager *rm);
	virtual void  Free();
};

struct RParticle : public ResDesc {
	char		filename[MAXRESCHARS];
	char		spritename[MAXRESCHARS];

	static  void  Parse(hglResourceManager *rm, RScriptParser *sp, const char *name,
		const char *basename);
	virtual unsigned Get(hglResourceManager *rm);
	virtual void  Free();
};

struct RDistort : public ResDesc {
	char		texname[MAXRESCHARS];
	float		tx, ty, w, h;
	int			cols, rows;
	int			blend;
	unsigned		color;
	float		z;

	static  void  Parse(hglResourceManager *rm, RScriptParser *sp, const char *name,
		const char *basename);
	virtual unsigned Get(hglResourceManager *rm);
	virtual void  Free();
};


struct RStringTable : public ResDesc {
	char		  filename[MAXRESCHARS];

	static  void  Parse(hglResourceManager *rm, RScriptParser *sp, const char *name,
		const char *basename);
	virtual unsigned Get(hglResourceManager *rm);
	virtual void  Free();
};


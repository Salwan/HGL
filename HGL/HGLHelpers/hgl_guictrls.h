// Sal's HGL-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge 
#pragma once

#include <hgl.h>
#include "hgl_sprite.h"
#include "hgl_font.h"
#include "hgl_rect.h"
#include "hgl_gui.h"


#define hglButtonGetState(gui,id)		((hglGUIButton*)gui->GetCtrl(id))->GetState()
#define hglButtonSetState(gui,id,b)		((hglGUIButton*)gui->GetCtrl(id))->SetState(b)
#define hglSliderGetValue(gui,id)		((hglGUISlider*)gui->GetCtrl(id))->GetValue()
#define hglSliderSetValue(gui,id,f)		((hglGUISlider*)gui->GetCtrl(id))->SetValue(f)
#define hglGetTextCtrl(gui,id)			((hglGUIText*)gui->GetCtrl(id))
#define hglGetListboxCtrl(gui,id)		((hglGUIListbox*)gui->GetCtrl(id))


/*
** hglGUIText
*/
class hglGUIText : public hglGUIObject
{
public:
	hglGUIText(int id, float x, float y, float w, float h, hglFont *fnt);

	void			SetMode(int _align);
	void			SetText(const char *_text);
	void			printf(const char *format, ...);

	virtual void	Render();

private:
	hglFont*		font;
	float			tx, ty;
	int				align;
	char			text[256];
};


/*
** hglGUIButton
*/
class hglGUIButton : public hglGUIObject
{
public:
	hglGUIButton(int id, float x, float y, float w, float h, HTEXTURE tex, float tx, float ty);
	virtual			~hglGUIButton();

	void			SetMode(bool _bTrigger) { bTrigger = _bTrigger; }
	void			SetState(bool _bPressed) { bPressed = _bPressed; }
	bool			GetState() const { return bPressed; }

	virtual void	Render();
	virtual bool	MouseLButton(bool bDown);

private:
	bool			bTrigger;
	bool			bPressed;
	bool			bOldState;
	hglSprite		*sprUp, *sprDown;
};


/*
** hglGUISlider
*/
#define HGLSLIDER_BAR			0
#define HGLSLIDER_BARRELATIVE	1
#define HGLSLIDER_SLIDER		2

class hglGUISlider : public hglGUIObject
{
public:
	hglGUISlider(int id, float x, float y, float w, float h, HTEXTURE tex, float tx, float ty, float sw, float sh, bool vertical = false);
	virtual			~hglGUISlider();

	void			SetMode(float _fMin, float _fMax, int _mode) { fMin = _fMin; fMax = _fMax; mode = _mode; }
	void			SetValue(float _fVal);
	float			GetValue() const { return fVal; }

	virtual void	Render();
	virtual bool	MouseMove(float x, float y);
	virtual bool	MouseLButton(bool bDown);

private:
	bool			bPressed;
	bool			bVertical;
	int				mode;
	float			fMin, fMax, fVal;
	float			sl_w, sl_h;
	hglSprite		*sprSlider;
};


/*
** hglGUIListbox
*/
struct hglGUIListboxItem
{
	char				text[64];
	hglGUIListboxItem	*next;
};

class hglGUIListbox : public hglGUIObject
{
public:
	hglGUIListbox(int id, float x, float y, float w, float h, hglFont *fnt, unsigned tColor, 
		          unsigned thColor, unsigned hColor);
	virtual			~hglGUIListbox();

	int				AddItem(char *item);
	void			DeleteItem(int n);
	int				GetSelectedItem() { return nSelectedItem; }
	void			SetSelectedItem(int n) { if (n >= 0 && n<GetNumItems()) nSelectedItem = n; }
	int				GetTopItem() { return nTopItem; }
	void			SetTopItem(int n) { if (n >= 0 && n <= GetNumItems() - GetNumRows()) nTopItem = n; }

	char			*GetItemText(int n);
	int				GetNumItems() { return nItems; }
	int				GetNumRows() { return int((rect.y2 - rect.y1) / font->GetHeight()); }
	void			Clear();

	virtual void	Render();
	virtual bool	MouseMove(float x, float y) { mx = x; my = y; return false; }
	virtual bool	MouseLButton(bool bDown);
	virtual bool	MouseWheel(int nNotches);
	virtual bool	KeyClick(int key, int chr);
	virtual bool	PadClick(hglGUIPadKey_t pad_gui_key);

private:
	hglSprite		*sprHighlight;
	hglFont			*font;
	unsigned		textColor, texthilColor;

	int					nItems, nSelectedItem, nTopItem;
	float				mx, my;
	hglGUIListboxItem	*pItems;
};


// Sal's HGL-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge
#include "precomp.h"
#include "hgl_guictrls.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#if defined(_MSC_VER)
#pragma warning(disable : 4996)
#endif

/*
** hglGUIText
*/

hglGUIText::hglGUIText(int _id, float x, float y, float w, float h, hglFont *fnt)
{
	id = _id;
	bStatic = true;
	bVisible = true;
	bEnabled = true;
	rect.Set(x, y, x + w, y + h);

	font = fnt;
	tx = x;
	ty = y + (h - fnt->GetHeight()) / 2.0f;

	text[0] = 0;
}

void hglGUIText::SetMode(int _align)
{
	align = _align;
	if (align == HGLTEXT_RIGHT) {
		tx = rect.x2;
	} else if (align == HGLTEXT_CENTER) {
		tx = (rect.x1 + rect.x2) / 2.0f;
	} else {
		tx = rect.x1;
	}
}

void hglGUIText::SetText(const char *_text)
{
	strcpy_s(text, _TRUNCATE, _text);
}

void hglGUIText::printf(const char *format, ...)
{
	vsprintf(text, format, (char *)&format + sizeof(format));
}

void hglGUIText::Render()
{
	font->SetColor(color);
	font->Render(tx, ty, align, text);
}

/*
** hglGUIButton
*/

hglGUIButton::hglGUIButton(int _id, float x, float y, float w, float h, HTEXTURE tex, float tx,
	float ty)
{
	id = _id;
	bStatic = false;
	bVisible = true;
	bEnabled = true;
	rect.Set(x, y, x + w, y + h);

	bPressed = false;
	bTrigger = false;

	sprUp = new hglSprite(tex, tx, ty, w, h);
	sprDown = new hglSprite(tex, tx + w, ty, w, h);
}

hglGUIButton::~hglGUIButton()
{
	if (sprUp) {
		delete sprUp;
	}
	if (sprDown) {
		delete sprDown;
	}
}

void hglGUIButton::Render()
{
	if (bPressed) {
		sprDown->Render(rect.x1, rect.y1);
	} else {
		sprUp->Render(rect.x1, rect.y1);
	}
}

bool hglGUIButton::MouseLButton(bool bDown)
{
	if (bDown) {
		bOldState = bPressed;
		bPressed = true;
		return false;
	} else {
		if (bTrigger) {
			bPressed = !bOldState;
		} else {
			bPressed = false;
		}
		return true;
	}
}

/*
** hglGUISlider
*/

hglGUISlider::hglGUISlider(int _id, float x, float y, float w, float h, HTEXTURE tex, float tx,
	float ty, float sw, float sh, bool vertical)
{
	id = _id;
	bStatic = false;
	bVisible = true;
	bEnabled = true;
	bPressed = false;
	bVertical = vertical;
	rect.Set(x, y, x + w, y + h);

	mode = HGLSLIDER_BAR;
	fMin = 0;
	fMax = 100;
	fVal = 50;
	sl_w = sw;
	sl_h = sh;

	sprSlider = new hglSprite(tex, tx, ty, sw, sh);
}

hglGUISlider::~hglGUISlider()
{
	if (sprSlider) {
		delete sprSlider;
	}
}

void hglGUISlider::SetValue(float _fVal)
{
	if (_fVal<fMin) {
		fVal = fMin;
	} else if (_fVal>fMax) {
		fVal = fMax;
	} else {
		fVal = _fVal;
	}
}

void hglGUISlider::Render()
{
	float xx, yy;
	float x1, y1, x2, y2;

	xx = rect.x1 + (rect.x2 - rect.x1)*(fVal - fMin) / (fMax - fMin);
	yy = rect.y1 + (rect.y2 - rect.y1)*(fVal - fMin) / (fMax - fMin);

	if (bVertical)
		switch (mode) {
		case HGLSLIDER_BAR:
			x1 = rect.x1;
			y1 = rect.y1;
			x2 = rect.x2;
			y2 = yy;
			break;
		case HGLSLIDER_BARRELATIVE:
			x1 = rect.x1;
			y1 = (rect.y1 + rect.y2) / 2;
			x2 = rect.x2;
			y2 = yy;
			break;
		case HGLSLIDER_SLIDER:
			x1 = (rect.x1 + rect.x2 - sl_w) / 2;
			y1 = yy - sl_h / 2;
			x2 = (rect.x1 + rect.x2 + sl_w) / 2;
			y2 = yy + sl_h / 2;
			break;
		} else
			switch (mode) {
			case HGLSLIDER_BAR:
				x1 = rect.x1;
				y1 = rect.y1;
				x2 = xx;
				y2 = rect.y2;
				break;
			case HGLSLIDER_BARRELATIVE:
				x1 = (rect.x1 + rect.x2) / 2;
				y1 = rect.y1;
				x2 = xx;
				y2 = rect.y2;
				break;
			case HGLSLIDER_SLIDER:
				x1 = xx - sl_w / 2;
				y1 = (rect.y1 + rect.y2 - sl_h) / 2;
				x2 = xx + sl_w / 2;
				y2 = (rect.y1 + rect.y2 + sl_h) / 2;
				break;
			}

		sprSlider->RenderStretch(x1, y1, x2, y2);
}

bool hglGUISlider::MouseLButton(bool bDown)
{
	bPressed = bDown;
	return false;
}

bool hglGUISlider::MouseMove(float x, float y)
{
	if (bPressed) {
		if (bVertical) {
			if (y>rect.y2 - rect.y1) {
				y = rect.y2 - rect.y1;
			}
			if (y<0) {
				y = 0;
			}
			fVal = fMin + (fMax - fMin)*y / (rect.y2 - rect.y1);
		} else {
			if (x>rect.x2 - rect.x1) {
				x = rect.x2 - rect.x1;
			}
			if (x<0) {
				x = 0;
			}
			fVal = fMin + (fMax - fMin)*x / (rect.x2 - rect.x1);
		}
		return true;
	}

	return false;
}


/*
** hglGUIListbox
*/

hglGUIListbox::hglGUIListbox(int _id, float x, float y, float w, float h, hglFont *fnt,
	unsigned tColor, unsigned thColor, unsigned hColor)
{
	id = _id;
	bStatic = false;
	bVisible = true;
	bEnabled = true;
	rect.Set(x, y, x + w, y + h);
	font = fnt;
	sprHighlight = new hglSprite(0, 0, 0, w, fnt->GetHeight());
	sprHighlight->SetColor(hColor);
	textColor = tColor;
	texthilColor = thColor;
	pItems = 0;
	nItems = 0;

	nSelectedItem = 0;
	nTopItem = 0;
	mx = 0;
	my = 0;
}

hglGUIListbox::~hglGUIListbox()
{
	Clear();
	if (sprHighlight) {
		delete sprHighlight;
	}
}


int hglGUIListbox::AddItem(char *item)
{
	hglGUIListboxItem *pItem = pItems, *pPrev = 0, *pNew;

	pNew = new hglGUIListboxItem;
	memcpy(pNew->text, item, min(sizeof(pNew->text), strlen(item) + 1));
	pNew->text[sizeof(pNew->text) - 1] = '\0';
	pNew->next = 0;

	while (pItem) {
		pPrev = pItem;
		pItem = pItem->next;
	}

	if (pPrev) {
		pPrev->next = pNew;
	} else {
		pItems = pNew;
	}
	nItems++;

	return nItems - 1;
}

void hglGUIListbox::DeleteItem(int n)
{
	int i;
	hglGUIListboxItem *pItem = pItems, *pPrev = 0;

	if (n<0 || n >= GetNumItems()) {
		return;
	}

	for (i = 0; i<n; i++) {
		pPrev = pItem;
		pItem = pItem->next;
	}

	if (pPrev) {
		pPrev->next = pItem->next;
	} else {
		pItems = pItem->next;
	}

	delete pItem;
	nItems--;
}

char *hglGUIListbox::GetItemText(int n)
{
	int i;
	hglGUIListboxItem *pItem = pItems;

	if (n<0 || n >= GetNumItems()) {
		return 0;
	}

	for (i = 0; i<n; i++) {
		pItem = pItem->next;
	}

	return pItem->text;
}

void hglGUIListbox::Clear()
{
	hglGUIListboxItem *pItem = pItems, *pNext;

	while (pItem) {
		pNext = pItem->next;
		delete pItem;
		pItem = pNext;
	}

	pItems = 0;
	nItems = 0;
}

void hglGUIListbox::Render()
{
	int i;
	hglGUIListboxItem *pItem = pItems;

	for (i = 0; i<nTopItem; i++) {
		pItem = pItem->next;
	}
	for (i = 0; i<GetNumRows(); i++) {
		if (i >= nItems) {
			return;
		}

		if (nTopItem + i == nSelectedItem) {
			sprHighlight->Render(rect.x1, rect.y1 + i*font->GetHeight());
			font->SetColor(texthilColor);
		} else {
			font->SetColor(textColor);
		}

		font->Render(rect.x1 + 3, rect.y1 + i*font->GetHeight(), HGLTEXT_LEFT, pItem->text);
		pItem = pItem->next;
	}
}

bool hglGUIListbox::MouseLButton(bool bDown)
{
	int nItem;

	if (bDown) {
		nItem = nTopItem + int(my) / int(font->GetHeight());
		if (nItem<nItems) {
			nSelectedItem = nItem;
			return true;
		}
	}
	return false;
}


bool hglGUIListbox::MouseWheel(int nNotches)
{
	nTopItem -= nNotches;
	if (nTopItem<0) {
		nTopItem = 0;
	}
	if (nTopItem>GetNumItems() - GetNumRows()) {
		nTopItem = GetNumItems() - GetNumRows();
	}

	return true;
}

bool hglGUIListbox::KeyClick(int key, int chr)
{
	switch (key) {
	case HGLK_DOWN:
		if (nSelectedItem < nItems - 1) {
			nSelectedItem++;
			if (nSelectedItem > nTopItem + GetNumRows() - 1) {
				nTopItem = nSelectedItem - GetNumRows() + 1;
			}
			return true;
		}
		break;

	case HGLK_UP:
		if (nSelectedItem > 0) {
			nSelectedItem--;
			if (nSelectedItem < nTopItem) {
				nTopItem = nSelectedItem;
			}
			return true;
		}
		break;
	}
	return false;
}

bool hglGUIListbox::PadClick(hglGUIPadKey_t padkey) {
	switch(padkey) {
		case HGLPK_DOWN:
			if(nSelectedItem < nItems - 1) {
				nSelectedItem++;
				if(nSelectedItem > nTopItem + GetNumRows() - 1) {
					nTopItem = nSelectedItem - GetNumRows() + 1;
				}
				return true;
			}
			break;

		case HGLPK_UP:
			if(nSelectedItem > 0) {
				nSelectedItem--;
				if(nSelectedItem < nTopItem) {
					nTopItem = nSelectedItem;
				}
				return true;
			}
			break;
	}
	return false;
}

#if defined(_MSC_VER)
#pragma warning(default : 4996)
#endif

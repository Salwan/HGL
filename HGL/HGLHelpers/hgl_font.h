// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge 
#pragma once

#include "hgl.h"

enum HGL_TextAlignH : unsigned short {
	HGLTEXT_LEFT		= 0,
	HGLTEXT_RIGHT		= 1,
	HGLTEXT_CENTER		= 2,
	HGLTEXT_HORZMASK	= 0x03,
};

enum HGL_TextAlignV : unsigned short {
	HGLTEXT_TOP			= 0,
	HGLTEXT_BOTTOM		= 4,
	HGLTEXT_MIDDLE		= 8,
	HGLTEXT_VERTMASK	= 0x0C,
};

// Forward def
class hglSprite;

/*
** HGL Font class
*/
class hglFont
{
public:
	hglFont(const char *filename, bool bMipmap = false);
	~hglFont();

	void		Render(float x, float y, int align, const char *string);
	void		printf(float x, float y, int align, const char *format, ...);
	void		printfb(float x, float y, float w, float h, int align, const char *format, ...);

	void		SetColor(unsigned col);
	void		SetZ(float z);
	void		SetBlendMode(int blend);
	void		SetScale(float scale) { fScale = scale; }
	void		SetProportion(float prop) { fProportion = prop; }
	void		SetRotation(float rot) { fRot = rot; }
	void		SetTracking(float tracking) { fTracking = tracking; }
	void		SetSpacing(float spacing) { fSpacing = spacing; }

	unsigned	GetColor() const { return dwCol; }
	float		GetZ() const { return fZ; }
	int			GetBlendMode() const { return nBlend; }
	float		GetScale() const { return fScale; }
	float		GetProportion() const { return fProportion; }
	float		GetRotation() const { return fRot; }
	float		GetTracking() const { return fTracking; }
	float		GetSpacing() const { return fSpacing; }

	hglSprite*	GetSprite(char chr) const { return letters[(unsigned char)chr]; }
	float		GetPreWidth(char chr) const { return pre[(unsigned char)chr]; }
	float		GetPostWidth(char chr) const { return post[(unsigned char)chr]; }
	float		GetHeight() const { return fHeight; }
	float		GetStringWidth(const char *string, bool bMultiline = true) const;

private:
	hglFont();
	hglFont(const hglFont &fnt);
	hglFont&	operator= (const hglFont &fnt);

	char*		_get_line(char *file, char *line);

	static HGL	*hgl;

	static char	buffer[1024];

	HTEXTURE	hTexture;
	hglSprite*	letters[256];
	float		pre[256];
	float		post[256];
	float		fHeight;
	float		fScale;
	float		fProportion;
	float		fRot;
	float		fTracking;
	float		fSpacing;

	unsigned	dwCol;
	float		fZ;
	int			nBlend;
};


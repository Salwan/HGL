set Config=%1
set Arch=%2

echo Copying helper header files to Build\Include..
mkdir ..\Build\Include
copy /A ..\HGLHelpers\*.h ..\Build\Include\

echo Configuration: %Config%
echo Architecture: %Arch%

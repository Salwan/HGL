// Sal's HGL-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge
#include "precomp.h"
#include "hgl_gui.h"

HGL *hglGUI::hgl = 0;
HGL *hglGUIObject::hgl = 0;


hglGUI::hglGUI()
{
	hgl = hglCreate(HGL_VERSION);

	ctrls = 0;
	ctrlLock = 0;
	ctrlFocus = 0;
	ctrlOver = 0;
	navmode = HGLGUI_NONAVKEYS;
	bLPressed = bLReleased = false;
	bRPressed = bRReleased = false;
	nWheel = 0;
	mx = my = 0.0f;
	nEnterLeave = 0;
	sprCursor = 0;

	actionPadDown = hgl->Action_Create("_actionPadDown");
	actionPadUp = hgl->Action_Create("_actionPadUp");
	actionPadLeft = hgl->Action_Create("_actionPadLeft");
	actionPadRight = hgl->Action_Create("_actionPadRight");
	actionPadBtn1 = hgl->Action_Create("_actionPadBtn1");
	actionPadBtn2 = hgl->Action_Create("_actionPadBtn2");

	// Add bindings
	hgl->Action_AddPadButton(actionPadDown, HGLP_DPAD_DOWN);
	//
	hgl->Action_AddPadButton(actionPadUp, HGLP_DPAD_UP);
	//
	hgl->Action_AddPadButton(actionPadLeft, HGLP_DPAD_LEFT);
	//
	hgl->Action_AddPadButton(actionPadLeft, HGLP_DPAD_RIGHT);
	//
	hgl->Action_AddPadButton(actionPadBtn1, HGLP_A);
	hgl->Action_AddPadButton(actionPadBtn2, HGLP_B);
}

hglGUI::~hglGUI()
{
	hglGUIObject *ctrl = ctrls, *nextctrl;

	while (ctrl) {
		nextctrl = ctrl->next;
		delete ctrl;
		ctrl = nextctrl;
	}

	hgl->Action_Free(actionPadDown);
	hgl->Action_Free(actionPadUp);
	hgl->Action_Free(actionPadLeft);
	hgl->Action_Free(actionPadRight);
	hgl->Action_Free(actionPadBtn1);
	hgl->Action_Free(actionPadBtn2);

	hgl->Release();
}

void hglGUI::AddCtrl(hglGUIObject *ctrl)
{
	hglGUIObject *last = ctrls;

	ctrl->gui = this;

	if (!ctrls) {
		ctrls = ctrl;
		ctrl->prev = 0;
		ctrl->next = 0;
	} else {
		while (last->next) {
			last = last->next;
		}
		last->next = ctrl;
		ctrl->prev = last;
		ctrl->next = 0;
	}
}

void hglGUI::DelCtrl(int id)
{
	hglGUIObject *ctrl = ctrls;

	while (ctrl) {
		if (ctrl->id == id) {
			if (ctrl->prev) {
				ctrl->prev->next = ctrl->next;
			} else {
				ctrls = ctrl->next;
			}
			if (ctrl->next) {
				ctrl->next->prev = ctrl->prev;
			}
			delete ctrl;
			return;
		}
		ctrl = ctrl->next;
	}
}

hglGUIObject* hglGUI::GetCtrl(int id) const
{
	hglGUIObject *ctrl = ctrls;

	while (ctrl) {
		if (ctrl->id == id) {
			return ctrl;
		}
		ctrl = ctrl->next;
	}

	return NULL;
}

void hglGUI::MoveCtrl(int id, float x, float y)
{
	hglGUIObject *ctrl = GetCtrl(id);
	ctrl->rect.x2 = x + (ctrl->rect.x2 - ctrl->rect.x1);
	ctrl->rect.y2 = y + (ctrl->rect.y2 - ctrl->rect.y1);
	ctrl->rect.x1 = x;
	ctrl->rect.y1 = y;
}

void hglGUI::ShowCtrl(int id, bool bVisible)
{
	GetCtrl(id)->bVisible = bVisible;
}

void hglGUI::EnableCtrl(int id, bool bEnabled)
{
	GetCtrl(id)->bEnabled = bEnabled;
}

void hglGUI::SetNavMode(int mode)
{
	navmode = mode;
}

void hglGUI::SetCursor(hglSprite *spr)
{
	sprCursor = spr;
}


void hglGUI::SetColor(unsigned color)
{
	hglGUIObject *ctrl = ctrls;

	while (ctrl) {
		ctrl->SetColor(color);
		ctrl = ctrl->next;
	}
}


void hglGUI::Reset()
{
	hglGUIObject *ctrl = ctrls;

	while (ctrl) {
		ctrl->Reset();
		ctrl = ctrl->next;
	}

	ctrlLock = 0;
	ctrlOver = 0;
	ctrlFocus = 0;
}


void hglGUI::Move(float dx, float dy)
{
	hglGUIObject *ctrl = ctrls;

	while (ctrl) {
		ctrl->rect.x1 += dx;
		ctrl->rect.y1 += dy;
		ctrl->rect.x2 += dx;
		ctrl->rect.y2 += dy;

		ctrl = ctrl->next;
	}
}


void hglGUI::SetFocus(int id)
{
	hglGUIObject *ctrlNewFocus = GetCtrl(id);

	if (ctrlNewFocus == ctrlFocus) {
		return;
	}
	if (!ctrlNewFocus) {
		if (ctrlFocus) {
			ctrlFocus->Focus(false);
		}
		ctrlFocus = 0;
	} else if (!ctrlNewFocus->bStatic && ctrlNewFocus->bVisible && ctrlNewFocus->bEnabled) {
		if (ctrlFocus) {
			ctrlFocus->Focus(false);
		}
		if (ctrlNewFocus) {
			ctrlNewFocus->Focus(true);
		}
		ctrlFocus = ctrlNewFocus;
	}
}

int hglGUI::GetFocus() const
{
	if (ctrlFocus) {
		return ctrlFocus->id;
	} else {
		return 0;
	}
}

void hglGUI::Enter()
{
	hglGUIObject *ctrl = ctrls;

	while (ctrl) {
		ctrl->Enter();
		ctrl = ctrl->next;
	}

	nEnterLeave = 2;
}

void hglGUI::Leave()
{
	hglGUIObject *ctrl = ctrls;

	while (ctrl) {
		ctrl->Leave();
		ctrl = ctrl->next;
	}

	ctrlFocus = 0;
	ctrlOver = 0;
	ctrlLock = 0;
	nEnterLeave = 1;
}

void hglGUI::Render()
{
	hglGUIObject *ctrl = ctrls;

	while (ctrl) {
		if (ctrl->bVisible) {
			ctrl->Render();
		}
		ctrl = ctrl->next;
	}

	if (hgl->Input_IsMouseOver() && sprCursor) {
		sprCursor->Render(mx, my);
	}
}

int hglGUI::Update(float dt)
{
	bool bDone;
	int key;
	hglGUIObject *ctrl;

	// Update the mouse variables

	hglAction_t a_btn1	= hgl->Action_GetState(actionPadBtn1);
	hglAction_t a_btn2	= hgl->Action_GetState(actionPadBtn2);
	hglAction_t a_up	= hgl->Action_GetState(actionPadUp);
	hglAction_t a_down	= hgl->Action_GetState(actionPadDown);
	hglAction_t a_right = hgl->Action_GetState(actionPadRight);
	hglAction_t a_left = hgl->Action_GetState(actionPadLeft);

	hgl->Input_GetMousePos(&mx, &my);
	bLPressed = hgl->Input_KeyDown(HGLM_LBUTTON) || a_btn1 == HGLA_HIT;
	bLReleased = hgl->Input_KeyUp(HGLM_LBUTTON) || a_btn1 == HGLA_RELEASED;
	bRPressed = hgl->Input_KeyDown(HGLM_RBUTTON) || a_btn2 == HGLA_HIT;
	bRReleased = hgl->Input_KeyUp(HGLM_RBUTTON) || a_btn2 == HGLA_RELEASED;
	nWheel = hgl->Input_GetMouseWheel();

	// Update all controls

	ctrl = ctrls;
	while (ctrl) {
		ctrl->Update(dt);
		ctrl = ctrl->next;
	}

	// Handle Enter/Leave

	if (nEnterLeave) {
		ctrl = ctrls;
		bDone = true;
		while (ctrl) {
			if (!ctrl->IsDone()) {
				bDone = false;
				break;
			}
			ctrl = ctrl->next;
		}
		if (!bDone) {
			return 0;
		} else {
			if (nEnterLeave == 1) {
				return -1;
			} else {
				nEnterLeave = 0;
			}
		}
	}

	// Handle keys

	key = hgl->Input_GetKey();
	if (((navmode & HGLGUI_LEFTRIGHT) && (key == HGLK_LEFT || a_left == HGLA_HIT)) ||
		((navmode & HGLGUI_UPDOWN) && (key == HGLK_UP || a_up == HGLA_HIT))) {
		ctrl = ctrlFocus;
		if (!ctrl) {
			ctrl = ctrls;
			if (!ctrl) {
				return 0;
			}
		}
		do {
			ctrl = ctrl->prev;
			if (!ctrl && ((navmode & HGLGUI_CYCLED) || !ctrlFocus)) {
				ctrl = ctrls;
				while (ctrl->next) {
					ctrl = ctrl->next;
				}
			}
			if (!ctrl || ctrl == ctrlFocus) {
				break;
			}
		} while (ctrl->bStatic == true || ctrl->bVisible == false || ctrl->bEnabled == false);

		if (ctrl && ctrl != ctrlFocus) {
			if (ctrlFocus) {
				ctrlFocus->Focus(false);
			}
			if (ctrl) {
				ctrl->Focus(true);
			}
			ctrlFocus = ctrl;
		}
	} else if (((navmode & HGLGUI_LEFTRIGHT) && (key == HGLK_RIGHT || a_right == HGLA_HIT)) ||
		((navmode & HGLGUI_UPDOWN) && (key == HGLK_DOWN || a_down == HGLA_HIT))) {
		ctrl = ctrlFocus;
		if (!ctrl) {
			ctrl = ctrls;
			if (!ctrl) {
				return 0;
			}
			while (ctrl->next) {
				ctrl = ctrl->next;
			}
		}
		do {
			ctrl = ctrl->next;
			if (!ctrl && ((navmode & HGLGUI_CYCLED) || !ctrlFocus)) {
				ctrl = ctrls;
			}
			if (!ctrl || ctrl == ctrlFocus) {
				break;
			}
		} while (ctrl->bStatic == true || ctrl->bVisible == false || ctrl->bEnabled == false);

		if (ctrl && ctrl != ctrlFocus) {
			if (ctrlFocus) {
				ctrlFocus->Focus(false);
			}
			if (ctrl) {
				ctrl->Focus(true);
			}
			ctrlFocus = ctrl;
		}
	} else if (ctrlFocus && key && key != HGLM_LBUTTON && key != HGLM_RBUTTON) {
		if (ctrlFocus->KeyClick(key, hgl->Input_GetChar())) {
			return ctrlFocus->id;
		}
	} else if(a_btn1 == HGLA_HIT) {
		if(ctrlFocus->PadClick(HGLPK_A)) {
			return ctrlFocus->id;
		}
	}

	// Handle mouse

	bool bLDown = hgl->Input_GetKeyState(HGLM_LBUTTON) || hgl->Action_GetState(actionPadBtn1) == HGLA_PRESSED;
	bool bRDown = hgl->Input_GetKeyState(HGLM_RBUTTON) || hgl->Action_GetState(actionPadBtn2) == HGLA_PRESSED;

	if (ctrlLock) {
		ctrl = ctrlLock;
		if (!bLDown && !bRDown) {
			ctrlLock = 0;
		}
		if (ProcessCtrl(ctrl)) {
			return ctrl->id;
		}
	} else {
		// Find last (topmost) control

		ctrl = ctrls;
		if (ctrl)
			while (ctrl->next) {
				ctrl = ctrl->next;
			}

		while (ctrl) {
			if (ctrl->rect.TestPoint(mx, my) && ctrl->bEnabled) {
				if (ctrlOver != ctrl) {
					if (ctrlOver) {
						ctrlOver->MouseOver(false);
					}
					ctrl->MouseOver(true);
					ctrlOver = ctrl;
				}

				if (ProcessCtrl(ctrl)) {
					return ctrl->id;
				} else {
					return 0;
				}
			}
			ctrl = ctrl->prev;
		}

		if (ctrlOver) {
			ctrlOver->MouseOver(false);
			ctrlOver = 0;
		}

	}

	return 0;
}

bool hglGUI::ProcessCtrl(hglGUIObject *ctrl)
{
	bool bResult = false;

	if (bLPressed) {
		ctrlLock = ctrl;
		SetFocus(ctrl->id);
		bResult = bResult || ctrl->MouseLButton(true);
	}
	if (bRPressed) {
		ctrlLock = ctrl;
		SetFocus(ctrl->id);
		bResult = bResult || ctrl->MouseRButton(true);
	}
	if (bLReleased) {
		bResult = bResult || ctrl->MouseLButton(false);
	}
	if (bRReleased) {
		bResult = bResult || ctrl->MouseRButton(false);
	}
	if (nWheel) {
		bResult = bResult || ctrl->MouseWheel(nWheel);
	}
	bResult = bResult || ctrl->MouseMove(mx - ctrl->rect.x1, my - ctrl->rect.y1);

	return bResult;
}


// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hgl 
#pragma once

#include "hgl_sprite.h"

#define HGLANIM_FWD			0
#define HGLANIM_REV			1
#define HGLANIM_PINGPONG	2
#define HGLANIM_NOPINGPONG	0
#define HGLANIM_LOOP		4
#define HGLANIM_NOLOOP		0


// HGL Animation Class
class hglAnimation : public hglSprite
{
public:
	hglAnimation(HTEXTURE tex, int nframes, float FPS, float x, float y, float w, float h);
	hglAnimation(const hglAnimation &anim);

	void		Play();
	void		Stop() { bPlaying = false; }
	void		Resume() { bPlaying = true; }
	void		Update(float fDeltaTime);
	bool		IsPlaying() const { return bPlaying; }

	void		SetTexture(HTEXTURE tex) { hglSprite::SetTexture(tex); orig_width = hgl->Texture_GetWidth(tex, true); }
	void		SetTextureRect(float x1, float y1, float x2, float y2) { hglSprite::SetTextureRect(x1, y1, x2, y2); SetFrame(nCurFrame); }
	void		SetMode(int mode);
	void		SetSpeed(float FPS) { fSpeed = 1.0f / FPS; }
	void		SetFrame(int n);
	void		SetFrames(int n) { nFrames = n; }

	int			GetMode() const { return Mode; }
	float		GetSpeed() const { return 1.0f / fSpeed; }
	int			GetFrame() const { return nCurFrame; }
	int			GetFrames() const { return nFrames; }

private:
	hglAnimation();

	int			orig_width;

	bool		bPlaying;

	float		fSpeed;
	float		fSinceLastFrame;

	int			Mode;
	int			nDelta;
	int			nFrames;
	int			nCurFrame;
};


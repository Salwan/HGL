// Sal's HGL-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge 
#include "precomp.h"
#include "hgl_distort.h"


HGL *hglDistortionMesh::hgl = 0;


hglDistortionMesh::hglDistortionMesh(int cols, int rows)
{
	int i;

	hgl = hglCreate(HGL_VERSION);

	nRows = rows;
	nCols = cols;
	cellw = cellh = 0;
	quad.tex = 0;
	quad.blend = BLEND_COLORMUL | BLEND_ALPHABLEND | BLEND_ZWRITE;
	disp_array = new hglVertex[rows*cols];

	for (i = 0; i<rows*cols; i++) {
		disp_array[i].x = 0.0f;
		disp_array[i].y = 0.0f;
		disp_array[i].tx = 0.0f;
		disp_array[i].ty = 0.0f;

		disp_array[i].z = 0.5f;
		disp_array[i].col = 0xFFFFFFFF;
	}
}

hglDistortionMesh::hglDistortionMesh(const hglDistortionMesh &dm)
{
	hgl = hglCreate(HGL_VERSION);

	nRows = dm.nRows;
	nCols = dm.nCols;
	cellw = dm.cellw;
	cellh = dm.cellh;
	tx = dm.tx;
	ty = dm.ty;
	width = dm.width;
	height = dm.height;
	quad = dm.quad;

	disp_array = new hglVertex[nRows*nCols];
	memcpy(disp_array, dm.disp_array, sizeof(hglVertex)*nRows*nCols);
}

hglDistortionMesh::~hglDistortionMesh()
{
	delete[] disp_array;
	hgl->Release();
}

hglDistortionMesh& hglDistortionMesh::operator= (const hglDistortionMesh &dm)
{
	if (this != &dm) {
		nRows = dm.nRows;
		nCols = dm.nCols;
		cellw = dm.cellw;
		cellh = dm.cellh;
		tx = dm.tx;
		ty = dm.ty;
		width = dm.width;
		height = dm.height;
		quad = dm.quad;

		delete[] disp_array;
		disp_array = new hglVertex[nRows*nCols];
		memcpy(disp_array, dm.disp_array, sizeof(hglVertex)*nRows*nCols);
	}

	return *this;

}

void hglDistortionMesh::SetTexture(HTEXTURE tex)
{
	quad.tex = tex;
}

void hglDistortionMesh::SetTextureRect(float x, float y, float w, float h)
{
	int i, j;
	float tw, th;

	tx = x;
	ty = y;
	width = w;
	height = h;

	if (quad.tex) {
		tw = (float)hgl->Texture_GetWidth(quad.tex);
		th = (float)hgl->Texture_GetHeight(quad.tex);
	} else {
		tw = w;
		th = h;
	}

	cellw = w / (nCols - 1);
	cellh = h / (nRows - 1);

	for (j = 0; j<nRows; j++)
		for (i = 0; i<nCols; i++) {
			disp_array[j*nCols + i].tx = (x + i*cellw) / tw;
			disp_array[j*nCols + i].ty = (y + j*cellh) / th;

			disp_array[j*nCols + i].x = i*cellw;
			disp_array[j*nCols + i].y = j*cellh;
		}
}

void hglDistortionMesh::SetBlendMode(int blend)
{
	quad.blend = blend;
}

void hglDistortionMesh::Clear(unsigned col, float z)
{
	int i, j;

	for (j = 0; j<nRows; j++)
		for (i = 0; i<nCols; i++) {
			disp_array[j*nCols + i].x = i*cellw;
			disp_array[j*nCols + i].y = j*cellh;
			disp_array[j*nCols + i].col = col;
			disp_array[j*nCols + i].z = z;
		}
}

void hglDistortionMesh::Render(float x, float y)
{
	int i, j, idx;

	for (j = 0; j<nRows - 1; j++)
		for (i = 0; i<nCols - 1; i++) {
			idx = j*nCols + i;

			quad.v[0].tx = disp_array[idx].tx;
			quad.v[0].ty = disp_array[idx].ty;
			quad.v[0].x = x + disp_array[idx].x;
			quad.v[0].y = y + disp_array[idx].y;
			quad.v[0].z = disp_array[idx].z;
			quad.v[0].col = disp_array[idx].col;

			quad.v[1].tx = disp_array[idx + 1].tx;
			quad.v[1].ty = disp_array[idx + 1].ty;
			quad.v[1].x = x + disp_array[idx + 1].x;
			quad.v[1].y = y + disp_array[idx + 1].y;
			quad.v[1].z = disp_array[idx + 1].z;
			quad.v[1].col = disp_array[idx + 1].col;

			quad.v[2].tx = disp_array[idx + nCols + 1].tx;
			quad.v[2].ty = disp_array[idx + nCols + 1].ty;
			quad.v[2].x = x + disp_array[idx + nCols + 1].x;
			quad.v[2].y = y + disp_array[idx + nCols + 1].y;
			quad.v[2].z = disp_array[idx + nCols + 1].z;
			quad.v[2].col = disp_array[idx + nCols + 1].col;

			quad.v[3].tx = disp_array[idx + nCols].tx;
			quad.v[3].ty = disp_array[idx + nCols].ty;
			quad.v[3].x = x + disp_array[idx + nCols].x;
			quad.v[3].y = y + disp_array[idx + nCols].y;
			quad.v[3].z = disp_array[idx + nCols].z;
			quad.v[3].col = disp_array[idx + nCols].col;

			hgl->Gfx_RenderQuad(&quad);
		}
}

void hglDistortionMesh::SetZ(int col, int row, float z)
{
	if (row<nRows && col<nCols) {
		disp_array[row*nCols + col].z = z;
	}
}

void hglDistortionMesh::SetColor(int col, int row, unsigned color)
{
	if (row<nRows && col<nCols) {
		disp_array[row*nCols + col].col = color;
	}
}

void hglDistortionMesh::SetDisplacement(int col, int row, float dx, float dy, int ref)
{
	if (row<nRows && col<nCols) {
		switch (ref) {
		case HGLDISP_NODE:
			dx += col*cellw;
			dy += row*cellh;
			break;
		case HGLDISP_CENTER:
			dx += cellw*(nCols - 1) / 2;
			dy += cellh*(nRows - 1) / 2;
			break;
		case HGLDISP_TOPLEFT:
			break;
		}

		disp_array[row*nCols + col].x = dx;
		disp_array[row*nCols + col].y = dy;
	}
}

float hglDistortionMesh::GetZ(int col, int row) const
{
	if (row<nRows && col<nCols) {
		return disp_array[row*nCols + col].z;
	} else {
		return 0.0f;
	}
}

unsigned hglDistortionMesh::GetColor(int col, int row) const
{
	if (row<nRows && col<nCols) {
		return disp_array[row*nCols + col].col;
	} else {
		return 0;
	}
}

void hglDistortionMesh::GetDisplacement(int col, int row, float *dx, float *dy, int ref) const
{
	if (row<nRows && col<nCols) {
		switch (ref) {
		case HGLDISP_NODE:
			*dx = disp_array[row*nCols + col].x - col*cellw;
			*dy = disp_array[row*nCols + col].y - row*cellh;
			break;

		case HGLDISP_CENTER:
			*dx = disp_array[row*nCols + col].x - cellw*(nCols - 1) / 2;
			*dy = disp_array[row*nCols + col].x - cellh*(nRows - 1) / 2;
			break;

		case HGLDISP_TOPLEFT:
			*dx = disp_array[row*nCols + col].x;
			*dy = disp_array[row*nCols + col].y;
			break;
		}
	}
}

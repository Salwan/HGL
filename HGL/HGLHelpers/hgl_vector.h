// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge 
#pragma once

/*
** Fast 1.0/sqrtf(float) routine
*/
float InvSqrt(float x);

class hglVector
{
public:
	float	x, y;

	hglVector(float _x, float _y) { x = _x; y = _y; }
	hglVector() { x = 0; y = 0; }

	hglVector	operator-  ()					const { return hglVector(-x, -y); }
	hglVector	operator-  (const hglVector &v) const { return hglVector(x - v.x, y - v.y); }
	hglVector	operator+  (const hglVector &v) const { return hglVector(x + v.x, y + v.y); }
	hglVector&	operator-= (const hglVector &v) { x -= v.x; y -= v.y; return *this; }
	hglVector&	operator+= (const hglVector &v) { x += v.x; y += v.y; return *this; }
	bool		operator== (const hglVector &v)	const { return (x == v.x && y == v.y); }
	bool		operator!= (const hglVector &v)	const { return (x != v.x || y != v.y); }

	hglVector	operator/  (const float scalar)	const { return hglVector(x / scalar, y / scalar); }
	hglVector	operator*  (const float scalar) const { return hglVector(x*scalar, y*scalar); }
	hglVector&	operator*= (const float scalar) { x *= scalar; y *= scalar; return *this; }

	float		Dot(const hglVector *v) const { return x*v->x + y*v->y; }
	float		Length() const;
	float		Angle(const hglVector *v = 0) const;

	void		Clamp(const float max) { if (Length() > max) { Normalize(); x *= max; y *= max; } }
	hglVector*	Normalize() { float rc = InvSqrt(Dot(this)); x *= rc; y *= rc; return this; }
	hglVector*	Rotate(float a);
};

inline hglVector operator* (const float s, const hglVector &v) { return v*s; }
inline float	 operator^ (const hglVector &v, const hglVector &u) { return v.Angle(&u); }
inline float	 operator% (const hglVector &v, const hglVector &u) { return v.Dot(&u); }


// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge 
#pragma once

#include "hgl.h"
#include "hgl_rect.h"

class hglSprite
{
public:
	hglSprite(HTEXTURE tex, float x = 0.0f, float y = 0.0f, float w = 0.0f, float h = 0.0f, bool upside_down = false);
	hglSprite(const hglSprite &spr);
	~hglSprite() { hgl->Release(); }


	void		Render(float x, float y);
	void		RenderEx(float x, float y, float rot, float hscale = 1.0f, float vscale = 0.0f);
	void		RenderStretch(float x1, float y1, float x2, float y2);
	void		Render4V(float x0, float y0, float x1, float y1, float x2, float y2, float x3, float y3);

	void		SetTexture(HTEXTURE tex);
	void		SetTextureRect(float x, float y, float w, float h, bool adjSize = true);
	void		SetColor(unsigned col, int i = -1);
	void		SetZ(float z, int i = -1);
	void		SetBlendMode(int blend) { quad.blend = blend; }
	void		SetHotSpot(float x, float y) { hotX = x; hotY = y; }
	void		CentreHotSpot() { hotX = width / 2.0f; hotY = height / 2.0f; }
	void		SetFlip(bool bX, bool bY, bool bHotSpot = false);

	HTEXTURE	GetTexture() const { return quad.tex; }
	void		GetTextureRect(float *x, float *y, float *w, float *h) const { *x = tx; *y = ty; *w = width; *h = height; }
	unsigned	GetColor(int i = 0) const { return quad.v[i].col; }
	float		GetZ(int i = 0) const { return quad.v[i].z; }
	int			GetBlendMode() const { return quad.blend; }
	void		GetHotSpot(float *x, float *y) const { *x = hotX; *y = hotY; }
	void		GetFlip(bool *bX, bool *bY) const { *bX = bXFlip; *bY = bYFlip; }

	float		GetWidth() const { return width; }
	float		GetHeight() const { return height; }
	hglRect*	GetBoundingBox(float x, float y, hglRect *rect) const { 
		rect->Set(x - hotX, y - hotY, x - hotX + width, y - hotY + height); return rect; }
	hglRect*	GetBoundingBoxEx(float x, float y, float rot, float hscale, 
		float vscale, hglRect *rect) const;

protected:
	hglSprite();
	static HGL	*hgl;

	hglQuad		quad;
	float		tx, ty, width, height;
	float		tex_width, tex_height;
	float		hotX, hotY;
	bool		bXFlip, bYFlip, bHSFlip;
};
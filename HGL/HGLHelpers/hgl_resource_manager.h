// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge 
#pragma once

#define RESTYPES 13
#define MAXRESCHARS 128


class hglResourceManager;
class hglStringTable;

struct ResDesc
{
	char		name[MAXRESCHARS];
	int			resgroup;
	unsigned	handle;
	ResDesc*	next;

	ResDesc() { hgl = hglCreate(HGL_VERSION); }
	virtual ~ResDesc() { hgl->Release(); }

	virtual unsigned Get(hglResourceManager *rm) = 0;
	virtual void  Free() = 0;

protected:
	static HGL	*hgl;
};

/*
** HGL Resource manager class
*/
class hglResourceManager
{
public:
	hglResourceManager(const char *scriptname = 0);
	~hglResourceManager();

	void				ChangeScript(const char *scriptname = 0);
	bool				Precache(int groupid = 0);
	void				Purge(int groupid = 0);

	void*				GetResource(const char *name, int resgroup = 0);
	HTEXTURE			GetTexture(const char *name, int resgroup = 0);
	HEFFECT				GetEffect(const char *name, int resgroup = 0);
	HMUSIC				GetMusic(const char *name, int resgroup = 0);
	HSTREAM				GetStream(const char *name, int resgroup = 0);
	HTARGET				GetTarget(const char *name);

	// TODO: This will require linking to helpers for HGL too. Potential duplicate symbols case!
	//       When I decided to put helpers in a static lib I was assuming no dependency in HGL.
	//hglSprite*			GetSprite(const char *name);
	//hglAnimation*		GetAnimation(const char *name);
	//hglFont*			GetFont(const char *name);
	//hglParticleSystem*	GetParticleSystem(const char *name);
	//hglDistortionMesh*	GetDistortionMesh(const char *name);
	hglStringTable*		GetStringTable(const char *name, int resgroup = 0);

	ResDesc*			res[RESTYPES];

private:
	hglResourceManager(const hglResourceManager &);
	hglResourceManager&	operator= (const hglResourceManager&);
	void				_remove_all();
	void				_parse_script(const char *scriptname = 0);

	static HGL			*hgl;
};

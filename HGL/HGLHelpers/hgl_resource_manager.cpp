// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge 
#include "precomp.h"
#include "hgl_resource_manager.h"
#include <hgl.h>
#include <hgl_stringtable.h>
#include "hgl_rm_resources.h"
#include "hgl_rm_parser.h"

HGL *hglResourceManager::hgl = 0;


hglResourceManager::hglResourceManager(const char *scriptname)
{
	hgl = hglCreate(HGL_VERSION);

	for (int i = 0; i < RESTYPES; i++) {
		res[i] = 0;
	}
	_parse_script(scriptname);
}

hglResourceManager::~hglResourceManager()
{
	_remove_all();
	hgl->Release();
}

void hglResourceManager::_parse_script(const char *scriptname)
{
	ResDesc *rc, *rcnext;

	if (scriptname) {
		/*RScript::Parse(this, NULL, scriptname, NULL);

		rc = res[RES_SCRIPT];
		while (rc) {
			rc->Free();
			rcnext = rc->next;
			delete rc;
			rc = rcnext;
		}
		res[RES_SCRIPT] = 0;*/
	}
}

void hglResourceManager::_remove_all()
{
	/*
	int i;
	ResDesc *rc, *rcnext;

	for (i = 0; i<RESTYPES; i++) {
		rc = res[i];
		while (rc) {
			rc->Free();
			rcnext = rc->next;
			delete rc;
			rc = rcnext;
		}
		res[i] = 0;
	}
	*/
}

void hglResourceManager::ChangeScript(const char *scriptname)
{
	/*
	_remove_all();
	_parse_script(scriptname);
	*/
}

bool hglResourceManager::Precache(int groupid)
{
	/*
	int i;
	ResDesc *rc;
	bool bResult = true;

	for (i = 0; i<RESTYPES; i++) {
		rc = res[i];
		while (rc) {
			if (!groupid || groupid == rc->resgroup) {
				bResult = bResult && (rc->Get(this) != 0);
			}
			rc = rc->next;
		}
	}

	return bResult;
	*/
	return true;
}

void hglResourceManager::Purge(int groupid)
{
	/*
	int i;
	ResDesc *rc;

	for (i = 0; i<RESTYPES; i++) {
		rc = res[i];
		while (rc) {
			if (!groupid || groupid == rc->resgroup) {
				rc->Free();
			}
			rc = rc->next;
		}
	}
	*/
}

void* hglResourceManager::GetResource(const char *name, int resgroup)
{
	/*
	void *reshandle;
	RResource *resource;
	ResDesc *Res = FindRes(this, RES_RESOURCE, name);

	if (Res) {
		return (void *)Res->Get(this);
	} else {
		reshandle = hgl->Resource_Load(name);
		if (reshandle) {
			resource = new RResource();
			resource->handle = (unsigned)reshandle;
			resource->resgroup = resgroup;
			strcpy(resource->name, name);
			strcpy(resource->filename, name);
			AddRes(this, RES_RESOURCE, resource);

			return reshandle;
		}
	}
	*/
	return 0;
}

HTEXTURE hglResourceManager::GetTexture(const char *name, int resgroup)
{
	/*
	HTEXTURE reshandle;
	RTexture *resource;
	ResDesc *Res = FindRes(this, RES_TEXTURE, name);
	if (Res) {
		return (HTEXTURE)Res->Get(this);
	} else {
		reshandle = hgl->Texture_Load(name);
		if (reshandle) {
			resource = new RTexture();
			resource->handle = reshandle;
			resource->resgroup = resgroup;
			resource->mipmap = false;
			strcpy(resource->name, name);
			strcpy(resource->filename, name);
			AddRes(this, RES_TEXTURE, resource);

			return reshandle;
		}
	}
	*/
	return 0;
}

HEFFECT hglResourceManager::GetEffect(const char *name, int resgroup)
{
	/*
	HEFFECT reshandle;
	REffect *resource;
	ResDesc *Res = FindRes(this, RES_EFFECT, name);
	if (Res) {
		return (HEFFECT)Res->Get(this);
	} else {
		reshandle = hgl->Effect_Load(name);
		if (reshandle) {
			resource = new REffect();
			resource->handle = reshandle;
			resource->resgroup = resgroup;
			strcpy(resource->name, name);
			strcpy(resource->filename, name);
			AddRes(this, RES_EFFECT, resource);

			return reshandle;
		}
	}
	*/
	return 0;
}

HMUSIC hglResourceManager::GetMusic(const char *name, int resgroup)
{
	/*
	HMUSIC reshandle;
	RMusic *resource;
	ResDesc *Res = FindRes(this, RES_MUSIC, name);
	if (Res) {
		return (HMUSIC)Res->Get(this);
	} else {
		reshandle = hgl->Music_Load(name);
		if (reshandle) {
			resource = new RMusic();
			resource->handle = reshandle;
			resource->resgroup = resgroup;
			strcpy(resource->name, name);
			strcpy(resource->filename, name);
			AddRes(this, RES_MUSIC, resource);

			return reshandle;
		}
	}
	*/
	return 0;
}

HSTREAM hglResourceManager::GetStream(const char *name, int resgroup)
{
	/*HSTREAM reshandle;
	RStream *resource;
	ResDesc *Res = FindRes(this, RES_STREAM, name);
	if (Res) {
		return (HSTREAM)Res->Get(this);
	} else {
		reshandle = hgl->Stream_Load(name);
		if (reshandle) {
			resource = new RStream();
			resource->handle = reshandle;
			resource->resgroup = resgroup;
			strcpy(resource->name, name);
			strcpy(resource->filename, name);
			AddRes(this, RES_STREAM, resource);

			return reshandle;
		}
	}
*/
	return 0;
}

HTARGET hglResourceManager::GetTarget(const char *name)
{
	/*ResDesc *Res = FindRes(this, RES_TARGET, name);
	if (Res) {
		return (HTARGET)Res->Get(this);
	} else {
		return 0;
	}*/
	return 0;
}

/*
hglSprite* hglResourceManager::GetSprite(const char *name)
{
	ResDesc *Res = FindRes(this, RES_SPRITE, name);
	if (Res) {
		return (hglSprite *)Res->Get(this);
	} else {
		return 0;
	}
}

hglAnimation* hglResourceManager::GetAnimation(const char *name)
{
	ResDesc *Res = FindRes(this, RES_ANIMATION, name);
	if (Res) {
		return (hglAnimation *)Res->Get(this);
	} else {
		return 0;
	}
}

hglFont* hglResourceManager::GetFont(const char *name)
{
	ResDesc *Res = FindRes(this, RES_FONT, name);
	if (Res) {
		return (hglFont *)Res->Get(this);
	} else {
		return 0;
	}
}

hglParticleSystem* hglResourceManager::GetParticleSystem(const char *name)
{
	ResDesc *Res = FindRes(this, RES_PARTICLE, name);
	if (Res) {
		return (hglParticleSystem *)Res->Get(this);
	} else {
		return 0;
	}
}

hglDistortionMesh* hglResourceManager::GetDistortionMesh(const char *name)
{
	ResDesc *Res = FindRes(this, RES_DISTORT, name);
	if (Res) {
		return (hglDistortionMesh *)Res->Get(this);
	} else {
		return 0;
	}
}
*/

hglStringTable* hglResourceManager::GetStringTable(const char *name, int resgroup)
{
	hglStringTable *strtable;
	RStringTable *resource;
	ResDesc *Res = FindRes(this, RES_STRTABLE, name);
	if (Res) {
		return (hglStringTable*)Res->Get(this);
	} else {
		strtable = new hglStringTable(name);
		resource = new RStringTable();
		resource->handle = (unsigned)strtable;
		resource->resgroup = resgroup;
		strcpy(resource->name, name);
		strcpy(resource->filename, name);
		AddRes(this, RES_STRTABLE, resource);

		return strtable;
	}
	return 0;
}
// Sal's HGL-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge
#include "precomp.h"
#include "hgl_particle.h"

hglParticleManager::hglParticleManager()
{
	nPS = 0;
	tX = tY = 0.0f;
}

hglParticleManager::~hglParticleManager()
{
	int i;
	for (i = 0; i<nPS; i++) {
		delete psList[i];
	}
}

void hglParticleManager::Update(float dt)
{
	int i;
	for (i = 0; i<nPS; i++) {
		psList[i]->Update(dt);
		if (psList[i]->GetAge() == -2.0f && psList[i]->GetParticlesAlive() == 0) {
			delete psList[i];
			psList[i] = psList[nPS - 1];
			nPS--;
			i--;
		}
	}
}

void hglParticleManager::Render()
{
	int i;
	for (i = 0; i<nPS; i++) {
		psList[i]->Render();
	}
}

hglParticleSystem* hglParticleManager::SpawnPS(hglParticleSystemInfo *psi, float x, float y)
{
	if (nPS == MAX_PSYSTEMS) {
		return 0;
	}
	psList[nPS] = new hglParticleSystem(psi);
	psList[nPS]->FireAt(x, y);
	psList[nPS]->Transpose(tX, tY);
	nPS++;
	return psList[nPS - 1];
}

bool hglParticleManager::IsPSAlive(hglParticleSystem *ps) const
{
	int i;
	for (i = 0; i<nPS; i++) if (psList[i] == ps) {
		return true;
	}
	return false;
}

void hglParticleManager::Transpose(float x, float y)
{
	int i;
	for (i = 0; i<nPS; i++) {
		psList[i]->Transpose(x, y);
	}
	tX = x;
	tY = y;
}

void hglParticleManager::KillPS(hglParticleSystem *ps)
{
	int i;
	for (i = 0; i<nPS; i++) {
		if (psList[i] == ps) {
			delete psList[i];
			psList[i] = psList[nPS - 1];
			nPS--;
			return;
		}
	}
}

void hglParticleManager::KillAll()
{
	int i;
	for (i = 0; i<nPS; i++) {
		delete psList[i];
	}
	nPS = 0;
}

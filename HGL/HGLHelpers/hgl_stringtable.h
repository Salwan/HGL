// Sal's HGL-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge
#pragma once

#include <hgl.h>


#define MAXSTRNAMELENGTH 64


struct NamedString
{
	char			name[MAXSTRNAMELENGTH];
	char			*string;
	NamedString		*next;
};

/*
** HGL String table class
*/
class hglStringTable
{
public:
	hglStringTable(const char *filename);
	~hglStringTable();

	char			*GetString(const char *name);

private:
	hglStringTable(const hglStringTable &);
	hglStringTable&	operator= (const hglStringTable &);

	NamedString		*strings;

	static HGL		*hgl;
};


/* Example string table file:
string names are limited to 64 chars.
you can use escape '\' with: \n, \\, \"
you can use %s %d etc and the string can be used in printf like functions directly.

	[HGLSTRINGTABLE]
	; A string name is a comment
	StringName="The string is strong with this one"
	Ch1Story =
	 "Once upon a time bla bla bla
	  bla bla bla bla bla bla
	  and then blabla all of a sudden."
	SpecialStuff = "Dude 1\nDude 2\nDude %s"
*/

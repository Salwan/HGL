// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge 
#include "precomp.h"
#include "hgl_vector.h"

float InvSqrt(float x)
{
	union {
		int intPart;
		float floatPart;
	} convertor;

	convertor.floatPart = x;
	convertor.intPart = 0x5f3759df - (convertor.intPart >> 1);
	return convertor.floatPart*(1.5f - 0.4999f*x*convertor.floatPart*convertor.floatPart);
}

float hglVector::Length() const {
	return sqrtf(Dot(this));
}

float hglVector::Angle(const hglVector *v) const
{
	if (v) {
		hglVector s = *this, t = *v;

		s.Normalize();
		t.Normalize();
		return acosf(s.Dot(&t));
	} else {
		return atan2f(y, x);
	}
}

hglVector *hglVector::Rotate(float a)
{
	hglVector v;

	v.x = x*cosf(a) - y*sinf(a);
	v.y = x*sinf(a) + y*cosf(a);

	x = v.x;
	y = v.y;

	return this;
}




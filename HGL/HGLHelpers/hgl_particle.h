// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge 
#pragma once

#include "hgl.h"
#include "hgl_sprite.h"
#include "hgl_vector.h"
#include "hgl_color.h"
#include "hgl_rect.h"


#define MAX_PARTICLES	500
#define MAX_PSYSTEMS	100

struct hglParticle
{
	hglVector	vecLocation;
	hglVector	vecVelocity;

	float		fGravity;
	float		fRadialAccel;
	float		fTangentialAccel;

	float		fSpin;
	float		fSpinDelta;

	float		fSize;
	float		fSizeDelta;

	hglColor	colColor;		// + alpha
	hglColor	colColorDelta;

	float		fAge;
	float		fTerminalAge;
};

struct hglParticleSystemInfo
{
	hglSprite*	sprite;    // texture + blend mode
	int			nEmission; // particles per sec
	float		fLifetime;

	float		fParticleLifeMin;
	float		fParticleLifeMax;

	float		fDirection;
	float		fSpread;
	bool		bRelative;

	float		fSpeedMin;
	float		fSpeedMax;

	float		fGravityMin;
	float		fGravityMax;

	float		fRadialAccelMin;
	float		fRadialAccelMax;

	float		fTangentialAccelMin;
	float		fTangentialAccelMax;

	float		fSizeStart;
	float		fSizeEnd;
	float		fSizeVar;

	float		fSpinStart;
	float		fSpinEnd;
	float		fSpinVar;

	hglColor	colColorStart; // + alpha
	hglColor	colColorEnd;
	float		fColorVar;
	float		fAlphaVar;
};

class hglParticleSystem
{
public:
	hglParticleSystemInfo info;

	hglParticleSystem(const char *filename, hglSprite *sprite);
	hglParticleSystem(hglParticleSystemInfo *psi);
	hglParticleSystem(const hglParticleSystem &ps);
	~hglParticleSystem() { hgl->Release(); }

	hglParticleSystem&	operator= (const hglParticleSystem &ps);


	void				Render();
	void				FireAt(float x, float y);
	void				Fire();
	void				Stop(bool bKillParticles = false);
	void				Update(float fDeltaTime);
	void				MoveTo(float x, float y, bool bMoveParticles = false);
	void				Transpose(float x, float y) { fTx = x; fTy = y; }
	void				SetScale(float scale) { fScale = scale; }
	void				TrackBoundingBox(bool bTrack) { bUpdateBoundingBox = bTrack; }

	int					GetParticlesAlive() const { return nParticlesAlive; }
	float				GetAge() const { return fAge; }
	void				GetPosition(float *x, float *y) const { *x = vecLocation.x; *y = vecLocation.y; }
	void				GetTransposition(float *x, float *y) const { *x = fTx; *y = fTy; }
	float				GetScale() { return fScale; }
	hglRect*			GetBoundingBox(hglRect *rect) const;

private:
	hglParticleSystem();

	static HGL			*hgl;

	float				fAge;
	float				fEmissionResidue;

	hglVector			vecPrevLocation;
	hglVector			vecLocation;
	float				fTx, fTy;
	float				fScale;

	int					nParticlesAlive;
	hglRect				rectBoundingBox;
	bool				bUpdateBoundingBox;

	hglParticle			particles[MAX_PARTICLES];
};

class hglParticleManager
{
public:
	hglParticleManager();
	~hglParticleManager();

	void				Update(float dt);
	void				Render();

	hglParticleSystem*	SpawnPS(hglParticleSystemInfo *psi, float x, float y);
	bool				IsPSAlive(hglParticleSystem *ps) const;
	void				Transpose(float x, float y);
	void				GetTransposition(float *dx, float *dy) const { *dx = tX; *dy = tY; }
	void				KillPS(hglParticleSystem *ps);
	void				KillAll();

private:
	hglParticleManager(const hglParticleManager &);
	hglParticleManager&	operator= (const hglParticleManager &);

	int					nPS;
	float				tX;
	float				tY;
	hglParticleSystem*	psList[MAX_PSYSTEMS];
};


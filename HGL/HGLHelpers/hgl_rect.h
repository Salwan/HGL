// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge 
#pragma once

class hglRect
{
public:
	float	x1, y1, x2, y2;

	hglRect(float _x1, float _y1, float _x2, float _y2) { x1 = _x1; y1 = _y1; x2 = _x2; y2 = _y2; bClean = false; }
	hglRect() { bClean = true; }

	void    Clear() { bClean = true; }
	bool    IsClean() const { return bClean; }
	void	Set(float _x1, float _y1, float _x2, float _y2) { 
		x1 = _x1; x2 = _x2; y1 = _y1; y2 = _y2; bClean = false; }
	void	SetRadius(float x, float y, float r) { 
		x1 = x - r; x2 = x + r; y1 = y - r; y2 = y + r; bClean = false; }
	void	Encapsulate(float x, float y);
	bool	TestPoint(float x, float y) const;
	bool	Intersect(const hglRect *rect) const;

private:
	bool	bClean;
};


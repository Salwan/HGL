// Sal's HGL-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge 
#pragma once

#include "hgl.h"

#define HGLDISP_NODE		0
#define HGLDISP_TOPLEFT		1
#define HGLDISP_CENTER		2

/*
** HGL Distortion mesh class
*/
class hglDistortionMesh
{
public:
	hglDistortionMesh(int cols, int rows);
	hglDistortionMesh(const hglDistortionMesh &dm);
	~hglDistortionMesh();

	hglDistortionMesh&	operator= (const hglDistortionMesh &dm);

	void		Render(float x, float y);
	void		Clear(unsigned col = 0xFFFFFFFF, float z = 0.5f);

	void		SetTexture(HTEXTURE tex);
	void		SetTextureRect(float x, float y, float w, float h);
	void		SetBlendMode(int blend);
	void		SetZ(int col, int row, float z);
	void		SetColor(int col, int row, unsigned color);
	void		SetDisplacement(int col, int row, float dx, float dy, int ref);

	HTEXTURE	GetTexture() const { return quad.tex; }
	void		GetTextureRect(float *x, float *y, float *w, float *h) const { *x = tx; *y = ty; *w = width; *h = height; }
	int		GetBlendMode() const { return quad.blend; }
	float		GetZ(int col, int row) const;
	unsigned		GetColor(int col, int row) const;
	void		GetDisplacement(int col, int row, float *dx, float *dy, int ref) const;

	int		GetRows() { return nRows; }
	int		GetCols() { return nCols; }

private:
	hglDistortionMesh();

	static HGL	*hgl;

	hglVertex	*disp_array;
	int			nRows, nCols;
	float		cellw, cellh;
	float		tx, ty, width, height;
	hglQuad		quad;
};


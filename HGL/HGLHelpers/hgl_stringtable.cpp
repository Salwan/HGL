// Sal's HGL-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge
#include "precomp.h"
#include "hgl_stringtable.h"
#include <ctype.h>

// Disable unsafe warning, strcpy is guaranteed in standard but strcpy_s is optional
#pragma warning(disable : 4996)

const char STRHEADERTAG[] = "[HGLSTRINGTABLE]";
const char STRFORMATERROR[] = "String table %s has incorrect format.";

HGL *hglStringTable::hgl = 0;


hglStringTable::hglStringTable(const char *filename)
{
	int i;
	void *data;
	unsigned size;
	char *desc, *pdesc;
	NamedString *str;
	char str_name[MAXSTRNAMELENGTH];
	char *str_value, *pvalue;

	hgl = hglCreate(HGL_VERSION);
	strings = 0;

	// load string table file
	data = hgl->Resource_Load(filename, &size);
	if(!data) {
		return;
	}

	desc = new char[size + 1];
	memcpy(desc, data, size);
	desc[size] = 0;
	hgl->Resource_Free(data);

	// check header
	if(memcmp(desc, STRHEADERTAG, sizeof(STRHEADERTAG) - 1)) {
		hgl->System_Log(STRFORMATERROR, filename);
		delete[] desc;
		return;
	}

	pdesc = desc + sizeof(STRHEADERTAG);
	str_value = new char[8192];

	for(;;) {
		// skip whitespaces
		while(isspace(*pdesc)) {
			pdesc++;
		}
		if(!*pdesc) {
			break;
		}

		// skip comments
		if(*pdesc == ';') {
			while(*pdesc && *pdesc != '\n') {
				pdesc++;
			}
			pdesc++;
			continue;
		}

		// get string name -> str_name
		i = 0;
		while(pdesc[i] && pdesc[i] != '=' && !isspace(pdesc[i]) && i<MAXSTRNAMELENGTH) {
			str_name[i] = pdesc[i];
			i++;
		}
		str_name[i] = 0;
		pdesc += i;

		// skip string name overflow characters
		while(*pdesc && *pdesc != '=' && !isspace(*pdesc)) {
			pdesc++;
		}
		if(!*pdesc) {
			break;
		}

		// skip whitespaces to '='
		while(isspace(*pdesc)) {
			pdesc++;
		}
		if(*pdesc != '=') {
			hgl->System_Log(STRFORMATERROR, filename);
			break;
		}
		pdesc++;

		// skip whitespaces to '"'
		while(isspace(*pdesc)) {
			pdesc++;
		}
		if(*pdesc != '"') {
			hgl->System_Log(STRFORMATERROR, filename);
			break;
		}
		pdesc++;

		// parse string value till the closing '"' -> str_value
		// consider: \", \n, \\, LF, CR, whitespaces at line begin/end
		pvalue = str_value;

		while(*pdesc && *pdesc != '"') {
			if(*pdesc == '\n' || *pdesc == '\r') {
				while(isspace(*pdesc)) {
					pdesc++;
				}

				pvalue--;
				while(pvalue >= str_value && isspace(*pvalue)) {
					pvalue--;
				}
				pvalue++;
				*pvalue = ' ';
				pvalue++;

				continue;
			}

			if(*pdesc == '\\') {
				pdesc++;
				if(!*pdesc) {
					continue;
				}
				if(*pdesc == 'n') {
					*pvalue = '\n';
				} else {
					*pvalue = *pdesc;
				}
				pvalue++;
				pdesc++;
				continue;
			}

			*pvalue = *pdesc;
			pvalue++;
			pdesc++;
		}

		*pvalue = 0;

		// add the parsed string to the list
		str = new NamedString;
		strcpy(str->name, str_name);
		str->string = new char[strlen(str_value) + 1];
		strcpy(str->string, str_value);
		str->next = strings;
		strings = str;

		if(!*pdesc) {
			break;
		}
		pdesc++;
	}

	delete[] str_value;
	delete[] desc;
}

hglStringTable::~hglStringTable()
{
	NamedString *str, *strnext;

	str = strings;
	while(str) {
		strnext = str->next;
		delete[] str->string;
		delete str;
		str = strnext;
	}

	hgl->Release();
}

char *hglStringTable::GetString(const char *name)
{
	NamedString *str = strings;

	while(str) {
		if(!strcmp(name, str->name)) {
			return str->string;
		}
		str = str->next;
	}

	return 0;
}

#pragma warning(default : 4996)
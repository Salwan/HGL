// Sal's HGL-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge 
#pragma once

#include <hgl.h>
#include "hgl_sprite.h"
#include "hgl_rect.h"


#define HGLGUI_NONAVKEYS		0
#define HGLGUI_LEFTRIGHT		1
#define HGLGUI_UPDOWN			2
#define HGLGUI_CYCLED			4

enum hglGUIPadKey_t {
	HGLPK_UP = 0,
	HGLPK_DOWN,
	HGLPK_LEFT,
	HGLPK_RIGHT,
	HGLPK_A,
	HGLPK_B,
};

class hglGUI;

/*
** hglGUIObject
*/
class hglGUIObject
{
public:
	hglGUIObject() { hgl = hglCreate(HGL_VERSION); color = 0xFFFFFFFF; }
	virtual			~hglGUIObject() { hgl->Release(); }

	virtual void	Render() = 0;
	virtual void	Update(float dt) {}

	virtual void	Enter() {}
	virtual void	Leave() {}
	virtual void	Reset() {}
	virtual bool	IsDone() { return true; }
	virtual void	Focus(bool bFocused) {}
	virtual void	MouseOver(bool bOver) {}

	virtual bool	MouseMove(float x, float y) { return false; }
	virtual bool	MouseLButton(bool bDown) { return false; }
	virtual bool	MouseRButton(bool bDown) { return false; }
	virtual bool	MouseWheel(int nNotches) { return false; }
	virtual bool	KeyClick(int key, int chr) { return false; }
	/*
	** \param pad_gui_key is a GUI key value for controls: DPAD_UP/DOWN/LEFT/RIGHT and A/B.
	*/
	virtual bool	PadClick(hglGUIPadKey_t pad_gui_key) { return false; }

	virtual void	SetColor(unsigned _color) { color = _color; }

	int				id;
	bool			bStatic;
	bool			bVisible;
	bool			bEnabled;
	hglRect			rect;
	unsigned		color;

	hglGUI			*gui;
	hglGUIObject	*next;
	hglGUIObject	*prev;

protected:
	hglGUIObject(const hglGUIObject &go);
	hglGUIObject&	operator= (const hglGUIObject &go);

	static HGL		*hgl;
};


/*
** hglGUI
*/
class hglGUI
{
public:
	hglGUI();
	~hglGUI();

	void			AddCtrl(hglGUIObject *ctrl);
	void			DelCtrl(int id);
	hglGUIObject*	GetCtrl(int id) const;

	void			MoveCtrl(int id, float x, float y);
	void			ShowCtrl(int id, bool bVisible);
	void			EnableCtrl(int id, bool bEnabled);

	void			SetNavMode(int mode);
	void			SetCursor(hglSprite *spr);
	void			SetColor(unsigned color);
	void			SetFocus(int id);
	int				GetFocus() const;

	void			Enter();
	void			Leave();
	void			Reset();
	void			Move(float dx, float dy);

	int				Update(float dt);
	void			Render();

private:
	hglGUI(const hglGUI &);
	hglGUI&			operator= (const hglGUI&);
	bool			ProcessCtrl(hglGUIObject *ctrl);

	static HGL		*hgl;

	hglGUIObject	*ctrls;
	hglGUIObject	*ctrlLock;
	hglGUIObject	*ctrlFocus;
	hglGUIObject	*ctrlOver;

	int				navmode;
	int				nEnterLeave;
	hglSprite		*sprCursor;

	float			mx, my;
	int				nWheel;
	bool			bLPressed, bLReleased;
	bool			bRPressed, bRReleased;

	HACTION			actionPadDown;
	HACTION			actionPadUp;
	HACTION			actionPadLeft;
	HACTION			actionPadRight;
	HACTION			actionPadBtn1;
	HACTION			actionPadBtn2;
};



// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge 
#include "precomp.h"
#include "hgl_rect.h"


void hglRect::Encapsulate(float x, float y)
{
	if(bClean) {
		x1 = x2 = x;
		y1 = y2 = y;
		bClean = false;
	} else {
		if(x<x1) {
			x1 = x;
		}
		if(x>x2) {
			x2 = x;
		}
		if(y<y1) {
			y1 = y;
		}
		if(y>y2) {
			y2 = y;
		}
	}
}

bool hglRect::TestPoint(float x, float y) const
{
	if(x >= x1 && x<x2 && y >= y1 && y<y2) {
		return true;
	}

	return false;
}

bool hglRect::Intersect(const hglRect *rect) const
{
	if(fabs(x1 + x2 - rect->x1 - rect->x2) < (x2 - x1 + rect->x2 - rect->x1))
		if(fabs(y1 + y2 - rect->y1 - rect->y2) < (y2 - y1 + rect->y2 - rect->y1)) {
			return true;
		}

	return false;
}

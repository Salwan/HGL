// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge 
#pragma once

#include "hgl.h"

// OpenGL ES 2.0 errors layed out in: erro value -> message
//typedef const char* GLErrorString;
typedef std::vector<const char*> GLErrorStrings;

// Even indices = error code
// Odd indices (even index + 1) = message
#define CC(v) (const char*)(v)
const GLErrorStrings GLErrors = {
	CC(GL_NO_ERROR),									"GL_NO_ERROR",
	CC(GL_INVALID_ENUM),								"GL_INVALID_ENUM",
	CC(GL_INVALID_VALUE),								"GL_INVALID_VALUE",
	CC(GL_INVALID_OPERATION),							"GL_INVALID_OPERATION",
	CC(GL_OUT_OF_MEMORY),								"GL_OUT_OF_MEMORY",
	CC(GL_FRAMEBUFFER_COMPLETE),						"GL_FRAMEBUFFER_COMPLETE",
	CC(GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT),			"GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT",
	CC(GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT),	"GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT",
	CC(GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS),			"GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS",
	CC(GL_FRAMEBUFFER_UNSUPPORTED),						"GL_FRAMEBUFFER_UNSUPPORTED",
	CC(GL_INVALID_FRAMEBUFFER_OPERATION),				"GL_INVALID_FRAMEBUFFER_OPERATION",
};
#undef CC

/////////////////////////////////////////////////////////// Input Lookup tables

////////////////////////////////////////////////////////// Keyboard & Mouse

// TODO: handle add or substitute extra keyboard events like mobile keys (call/end-call) and others.
static std::map<unsigned, unsigned> gainputKeyToHGL = {
	{gainput::Key::KeyEscape,				  hglKeyCode_t::HGLK_ESCAPE},
	{gainput::Key::KeyF1,                    hglKeyCode_t::HGLK_F1},
	{gainput::Key::KeyF2,                    hglKeyCode_t::HGLK_F2},
	{gainput::Key::KeyF3,                    hglKeyCode_t::HGLK_F3},
	{gainput::Key::KeyF4,                    hglKeyCode_t::HGLK_F4},
	{gainput::Key::KeyF5,                    hglKeyCode_t::HGLK_F5},
	{gainput::Key::KeyF6,                    hglKeyCode_t::HGLK_F6},
	{gainput::Key::KeyF7,                    hglKeyCode_t::HGLK_F7},
	{gainput::Key::KeyF8,                    hglKeyCode_t::HGLK_F8},
	{gainput::Key::KeyF9,                    hglKeyCode_t::HGLK_F9},
	{gainput::Key::KeyF10,                   hglKeyCode_t::HGLK_F10},
	{gainput::Key::KeyF11,                   hglKeyCode_t::HGLK_F11},
	{gainput::Key::KeyF12,                   hglKeyCode_t::HGLK_F12},
	{gainput::Key::KeyF13,                   hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyF14,                   hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyF15,                   hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyF16,                   hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyF17,                   hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyF18,                   hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyF19,                   hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyPrint,                 hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyScrollLock,            hglKeyCode_t::HGLK_SCROLLLOCK},
	{gainput::Key::KeyBreak,                 hglKeyCode_t::HGLK_PAUSE},
	{gainput::Key::KeySpace,                 hglKeyCode_t::HGLK_SPACE},
	{gainput::Key::KeyApostrophe,            hglKeyCode_t::HGLK_APOSTROPHE},
	{gainput::Key::KeyComma,                 hglKeyCode_t::HGLK_COMMA},
	{gainput::Key::KeyMinus,                 hglKeyCode_t::HGLK_MINUS},
	{gainput::Key::KeyPeriod,                hglKeyCode_t::HGLK_PERIOD},
	{gainput::Key::KeySlash,                 hglKeyCode_t::HGLK_SLASH},
	{gainput::Key::Key0,                     hglKeyCode_t::HGLK_0},
	{gainput::Key::Key1,                     hglKeyCode_t::HGLK_1},
	{gainput::Key::Key2,                     hglKeyCode_t::HGLK_2},
	{gainput::Key::Key3,                     hglKeyCode_t::HGLK_3},
	{gainput::Key::Key4,                     hglKeyCode_t::HGLK_4},
	{gainput::Key::Key5,                     hglKeyCode_t::HGLK_5},
	{gainput::Key::Key6,                     hglKeyCode_t::HGLK_6},
	{gainput::Key::Key7,                     hglKeyCode_t::HGLK_7},
	{gainput::Key::Key8,                     hglKeyCode_t::HGLK_8},
	{gainput::Key::Key9,                     hglKeyCode_t::HGLK_9},
	{gainput::Key::KeySemicolon,             hglKeyCode_t::HGLK_SEMICOLON},
	{gainput::Key::KeyLess,                  hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyEqual,                 hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyA,                     hglKeyCode_t::HGLK_A},
	{gainput::Key::KeyB,                     hglKeyCode_t::HGLK_B},
	{gainput::Key::KeyC,                     hglKeyCode_t::HGLK_C},
	{gainput::Key::KeyD,                     hglKeyCode_t::HGLK_D},
	{gainput::Key::KeyE,                     hglKeyCode_t::HGLK_E},
	{gainput::Key::KeyF,                     hglKeyCode_t::HGLK_F},
	{gainput::Key::KeyG,                     hglKeyCode_t::HGLK_G},
	{gainput::Key::KeyH,                     hglKeyCode_t::HGLK_H},
	{gainput::Key::KeyI,                     hglKeyCode_t::HGLK_I},
	{gainput::Key::KeyJ,                     hglKeyCode_t::HGLK_J},
	{gainput::Key::KeyK,                     hglKeyCode_t::HGLK_K},
	{gainput::Key::KeyL,                     hglKeyCode_t::HGLK_L},
	{gainput::Key::KeyM,                     hglKeyCode_t::HGLK_M},
	{gainput::Key::KeyN,                     hglKeyCode_t::HGLK_N},
	{gainput::Key::KeyO,                     hglKeyCode_t::HGLK_O},
	{gainput::Key::KeyP,                     hglKeyCode_t::HGLK_P},
	{gainput::Key::KeyQ,                     hglKeyCode_t::HGLK_Q},
	{gainput::Key::KeyR,                     hglKeyCode_t::HGLK_R},
	{gainput::Key::KeyS,                     hglKeyCode_t::HGLK_S},
	{gainput::Key::KeyT,                     hglKeyCode_t::HGLK_T},
	{gainput::Key::KeyU,                     hglKeyCode_t::HGLK_U},
	{gainput::Key::KeyV,                     hglKeyCode_t::HGLK_V},
	{gainput::Key::KeyW,                     hglKeyCode_t::HGLK_W},
	{gainput::Key::KeyX,                     hglKeyCode_t::HGLK_X},
	{gainput::Key::KeyY,                     hglKeyCode_t::HGLK_Y},
	{gainput::Key::KeyZ,                     hglKeyCode_t::HGLK_Z},
	{gainput::Key::KeyBracketLeft,           hglKeyCode_t::HGLK_LBRACKET},
	{gainput::Key::KeyBackslash,             hglKeyCode_t::HGLK_BACKSLASH},
	{gainput::Key::KeyBracketRight,          hglKeyCode_t::HGLK_RBRACKET},
	{gainput::Key::KeyGrave,                 hglKeyCode_t::HGLK_GRAVE},
	{gainput::Key::KeyLeft,                  hglKeyCode_t::HGLK_LEFT},
	{gainput::Key::KeyRight,                 hglKeyCode_t::HGLK_RIGHT},
	{gainput::Key::KeyUp,                    hglKeyCode_t::HGLK_UP},
	{gainput::Key::KeyDown,                  hglKeyCode_t::HGLK_DOWN},
	{gainput::Key::KeyInsert,                hglKeyCode_t::HGLK_INSERT},
	{gainput::Key::KeyHome,                  hglKeyCode_t::HGLK_HOME},
	{gainput::Key::KeyDelete,                hglKeyCode_t::HGLK_DELETE},
	{gainput::Key::KeyEnd,                   hglKeyCode_t::HGLK_END},
	{gainput::Key::KeyPageUp,                hglKeyCode_t::HGLK_PGUP},
	{gainput::Key::KeyPageDown,              hglKeyCode_t::HGLK_PGDN},
	{gainput::Key::KeyNumLock,               hglKeyCode_t::HGLK_NUMLOCK},
	{gainput::Key::KeyKpEqual,               hglKeyCode_t::HGLK_EQUALS},
	{gainput::Key::KeyKpDivide,              hglKeyCode_t::HGLK_DIVIDE},
	{gainput::Key::KeyKpMultiply,            hglKeyCode_t::HGLK_MULTIPLY},
	{gainput::Key::KeyKpSubtract,            hglKeyCode_t::HGLK_SUBTRACT},
	{gainput::Key::KeyKpAdd,                 hglKeyCode_t::HGLK_ADD},
	{gainput::Key::KeyKpEnter,               hglKeyCode_t::HGLK_ENTER},
	{gainput::Key::KeyKpInsert,              hglKeyCode_t::HGLK_INSERT},
	{gainput::Key::KeyKpEnd,                 hglKeyCode_t::HGLK_END},
	{gainput::Key::KeyKpDown,                hglKeyCode_t::HGLK_DOWN},
	{gainput::Key::KeyKpPageDown,            hglKeyCode_t::HGLK_PGDN},
	{gainput::Key::KeyKpLeft,                hglKeyCode_t::HGLK_LEFT},
	{gainput::Key::KeyKpBegin,               hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyKpRight,               hglKeyCode_t::HGLK_RIGHT},
	{gainput::Key::KeyKpHome,                hglKeyCode_t::HGLK_HOME},
	{gainput::Key::KeyKpUp,                  hglKeyCode_t::HGLK_UP},
	{gainput::Key::KeyKpPageUp,              hglKeyCode_t::HGLK_PGUP},
	{gainput::Key::KeyKpDelete,              hglKeyCode_t::HGLK_DELETE},
	{gainput::Key::KeyBackSpace,             hglKeyCode_t::HGLK_BACKSPACE},
	{gainput::Key::KeyTab,                   hglKeyCode_t::HGLK_TAB},
	{gainput::Key::KeyReturn,                hglKeyCode_t::HGLK_ENTER},
	{gainput::Key::KeyCapsLock,              hglKeyCode_t::HGLK_CAPSLOCK},
	{gainput::Key::KeyShiftL,                hglKeyCode_t::HGLK_SHIFT},
	{gainput::Key::KeyCtrlL,                 hglKeyCode_t::HGLK_CTRL},
	{gainput::Key::KeySuperL,                hglKeyCode_t::HGLK_LWIN},
	{gainput::Key::KeyAltL,                  hglKeyCode_t::HGLK_ALT},
	{gainput::Key::KeyAltR,                  hglKeyCode_t::HGLK_ALT},
	{gainput::Key::KeySuperR,                hglKeyCode_t::HGLK_RWIN},
	{gainput::Key::KeyMenu,                  hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyCtrlR,                 hglKeyCode_t::HGLK_CTRL},
	{gainput::Key::KeyShiftR,                hglKeyCode_t::HGLK_SHIFT},
	{gainput::Key::KeyBack,                  hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeySoftLeft,              hglKeyCode_t::HGLK_LEFT},
	{gainput::Key::KeySoftRight,             hglKeyCode_t::HGLK_RIGHT},
	{gainput::Key::KeyCall,                  hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyEndcall,               hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyStar,                  hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyPound,                 hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyDpadCenter,            hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyVolumeUp,              hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyVolumeDown,            hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyPower,                 hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyCamera,                hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyClear,                 hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeySymbol,                hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyExplorer,              hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyEnvelope,              hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyEquals,                hglKeyCode_t::HGLK_EQUALS},
	{gainput::Key::KeyAt,                    hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyHeadsethook,           hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyFocus,                 hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyPlus,                  hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyNotification,          hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeySearch,                hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyMediaPlayPause,        hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyMediaStop,             hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyMediaNext,             hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyMediaPrevious,         hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyMediaRewind,           hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyMediaFastForward,      hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyMute,                  hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyPictsymbols,           hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeySwitchCharset,         hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyForward,               hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyExtra1,                hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyExtra2,                hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyExtra3,                hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyExtra4,                hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyExtra5,                hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyExtra6,                hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyFn,                    hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyCircumflex,            hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeySsharp,                hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyAcute,                 hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyAltGr,                 hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyNumbersign,            hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyUdiaeresis,            hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyAdiaeresis,            hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyOdiaeresis,            hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeySection,               hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyAring,                 hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyDiaeresis,             hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyTwosuperior,           hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyRightParenthesis,      hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyDollar,                hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyUgrave,                hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyAsterisk,              hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyColon,                 hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyExclam,                hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyBraceLeft,             hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeyBraceRight,            hglKeyCode_t::HGLK_NO_KEY},
	{gainput::Key::KeySysRq,                 hglKeyCode_t::HGLK_NO_KEY},
	{200,									  hglKeyCode_t::HGLM_LBUTTON},
	{201,									  hglKeyCode_t::HGLM_RBUTTON},
	{202,									  hglKeyCode_t::HGLM_MBUTTON},
};

// Caution: map allows multiple key-value defs but only takes the first one
static std::map<unsigned, unsigned> HGLKeyToGainput = {
	{hglKeyCode_t::HGLK_ESCAPE,		gainput::Key::KeyEscape},
	{hglKeyCode_t::HGLK_F1,			gainput::Key::KeyF1},
	{hglKeyCode_t::HGLK_F2,			gainput::Key::KeyF2},
	{hglKeyCode_t::HGLK_F3,			gainput::Key::KeyF3},
	{hglKeyCode_t::HGLK_F4,			gainput::Key::KeyF4},
	{hglKeyCode_t::HGLK_F5,			gainput::Key::KeyF5},
	{hglKeyCode_t::HGLK_F6,			gainput::Key::KeyF6},
	{hglKeyCode_t::HGLK_F7,			gainput::Key::KeyF7},
	{hglKeyCode_t::HGLK_F8,			gainput::Key::KeyF8},
	{hglKeyCode_t::HGLK_F9,			gainput::Key::KeyF9},
	{hglKeyCode_t::HGLK_F10,			gainput::Key::KeyF10},
	{hglKeyCode_t::HGLK_F11,			gainput::Key::KeyF11},
	{hglKeyCode_t::HGLK_F12,			gainput::Key::KeyF12},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyF13},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyF14},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyF15},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyF16},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyF17},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyF18},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyF19},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyPrint},
	{hglKeyCode_t::HGLK_SCROLLLOCK,	gainput::Key::KeyScrollLock},
	{hglKeyCode_t::HGLK_PAUSE,			gainput::Key::KeyBreak},
	{hglKeyCode_t::HGLK_SPACE,			gainput::Key::KeySpace},
	{hglKeyCode_t::HGLK_APOSTROPHE,	gainput::Key::KeyApostrophe},
	{hglKeyCode_t::HGLK_COMMA,			gainput::Key::KeyComma},
	{hglKeyCode_t::HGLK_MINUS,			gainput::Key::KeyMinus},
	{hglKeyCode_t::HGLK_PERIOD,		gainput::Key::KeyPeriod},
	{hglKeyCode_t::HGLK_SLASH,			gainput::Key::KeySlash},
	{hglKeyCode_t::HGLK_0,				gainput::Key::Key0},
	{hglKeyCode_t::HGLK_1,				gainput::Key::Key1},
	{hglKeyCode_t::HGLK_2,				gainput::Key::Key2},
	{hglKeyCode_t::HGLK_3,				gainput::Key::Key3},
	{hglKeyCode_t::HGLK_4,				gainput::Key::Key4},
	{hglKeyCode_t::HGLK_5,				gainput::Key::Key5},
	{hglKeyCode_t::HGLK_6,				gainput::Key::Key6},
	{hglKeyCode_t::HGLK_7,				gainput::Key::Key7},
	{hglKeyCode_t::HGLK_8,				gainput::Key::Key8},
	{hglKeyCode_t::HGLK_9,				gainput::Key::Key9},
	{hglKeyCode_t::HGLK_SEMICOLON,		gainput::Key::KeySemicolon},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyLess},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyEqual},
	{hglKeyCode_t::HGLK_A,				gainput::Key::KeyA},
	{hglKeyCode_t::HGLK_B,				gainput::Key::KeyB},
	{hglKeyCode_t::HGLK_C,				gainput::Key::KeyC},
	{hglKeyCode_t::HGLK_D,				gainput::Key::KeyD},
	{hglKeyCode_t::HGLK_E,				gainput::Key::KeyE},
	{hglKeyCode_t::HGLK_F,				gainput::Key::KeyF},
	{hglKeyCode_t::HGLK_G,				gainput::Key::KeyG},
	{hglKeyCode_t::HGLK_H,				gainput::Key::KeyH},
	{hglKeyCode_t::HGLK_I,				gainput::Key::KeyI},
	{hglKeyCode_t::HGLK_J,				gainput::Key::KeyJ},
	{hglKeyCode_t::HGLK_K,				gainput::Key::KeyK},
	{hglKeyCode_t::HGLK_L,				gainput::Key::KeyL},
	{hglKeyCode_t::HGLK_M,				gainput::Key::KeyM},
	{hglKeyCode_t::HGLK_N,				gainput::Key::KeyN},
	{hglKeyCode_t::HGLK_O,				gainput::Key::KeyO},
	{hglKeyCode_t::HGLK_P,				gainput::Key::KeyP},
	{hglKeyCode_t::HGLK_Q,				gainput::Key::KeyQ},
	{hglKeyCode_t::HGLK_R,				gainput::Key::KeyR},
	{hglKeyCode_t::HGLK_S,				gainput::Key::KeyS},
	{hglKeyCode_t::HGLK_T,				gainput::Key::KeyT},
	{hglKeyCode_t::HGLK_U,				gainput::Key::KeyU},
	{hglKeyCode_t::HGLK_V,				gainput::Key::KeyV},
	{hglKeyCode_t::HGLK_W,				gainput::Key::KeyW},
	{hglKeyCode_t::HGLK_X,				gainput::Key::KeyX},
	{hglKeyCode_t::HGLK_Y,				gainput::Key::KeyY},
	{hglKeyCode_t::HGLK_Z,				gainput::Key::KeyZ},
	{hglKeyCode_t::HGLK_LBRACKET,		gainput::Key::KeyBracketLeft},
	{hglKeyCode_t::HGLK_BACKSLASH,		gainput::Key::KeyBackslash},
	{hglKeyCode_t::HGLK_RBRACKET,		gainput::Key::KeyBracketRight},
	{hglKeyCode_t::HGLK_GRAVE,			gainput::Key::KeyGrave},
	{hglKeyCode_t::HGLK_LEFT,			gainput::Key::KeyLeft},
	{hglKeyCode_t::HGLK_RIGHT,			gainput::Key::KeyRight},
	{hglKeyCode_t::HGLK_UP,			gainput::Key::KeyUp},
	{hglKeyCode_t::HGLK_DOWN,			gainput::Key::KeyDown},
	{hglKeyCode_t::HGLK_INSERT,		gainput::Key::KeyInsert},
	{hglKeyCode_t::HGLK_HOME,			gainput::Key::KeyHome},
	{hglKeyCode_t::HGLK_DELETE,		gainput::Key::KeyDelete},
	{hglKeyCode_t::HGLK_END,			gainput::Key::KeyEnd},
	{hglKeyCode_t::HGLK_PGUP,			gainput::Key::KeyPageUp},
	{hglKeyCode_t::HGLK_PGDN,			gainput::Key::KeyPageDown},
	{hglKeyCode_t::HGLK_NUMLOCK,		gainput::Key::KeyNumLock},
	{hglKeyCode_t::HGLK_EQUALS,		gainput::Key::KeyKpEqual},
	{hglKeyCode_t::HGLK_DIVIDE,		gainput::Key::KeyKpDivide},
	{hglKeyCode_t::HGLK_MULTIPLY,		gainput::Key::KeyKpMultiply},
	{hglKeyCode_t::HGLK_SUBTRACT,		gainput::Key::KeyKpSubtract},
	{hglKeyCode_t::HGLK_ADD,			gainput::Key::KeyKpAdd},
	{hglKeyCode_t::HGLK_INSERT,		gainput::Key::KeyKpInsert},
	{hglKeyCode_t::HGLK_END,			gainput::Key::KeyKpEnd},
	{hglKeyCode_t::HGLK_DOWN,			gainput::Key::KeyKpDown},
	{hglKeyCode_t::HGLK_PGDN,			gainput::Key::KeyKpPageDown},
	{hglKeyCode_t::HGLK_LEFT,			gainput::Key::KeyKpLeft},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyKpBegin},
	{hglKeyCode_t::HGLK_RIGHT,			gainput::Key::KeyKpRight},
	{hglKeyCode_t::HGLK_HOME,			gainput::Key::KeyKpHome},
	{hglKeyCode_t::HGLK_UP,			gainput::Key::KeyKpUp},
	{hglKeyCode_t::HGLK_PGUP,			gainput::Key::KeyKpPageUp},
	{hglKeyCode_t::HGLK_DELETE,		gainput::Key::KeyKpDelete},
	{hglKeyCode_t::HGLK_BACKSPACE,		gainput::Key::KeyBackSpace},
	{hglKeyCode_t::HGLK_TAB,			gainput::Key::KeyTab},
	{hglKeyCode_t::HGLK_ENTER,			gainput::Key::KeyReturn},
	{hglKeyCode_t::HGLK_CAPSLOCK,		gainput::Key::KeyCapsLock},
	{hglKeyCode_t::HGLK_SHIFT,			gainput::Key::KeyShiftL},
	{hglKeyCode_t::HGLK_CTRL,			gainput::Key::KeyCtrlL},
	{hglKeyCode_t::HGLK_LWIN,			gainput::Key::KeySuperL},
	{hglKeyCode_t::HGLK_ALT,			gainput::Key::KeyAltL},
	{hglKeyCode_t::HGLK_ALT,			gainput::Key::KeyAltR},
	{hglKeyCode_t::HGLK_RWIN,			gainput::Key::KeySuperR},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyMenu},
	{hglKeyCode_t::HGLK_CTRL,			gainput::Key::KeyCtrlR},
	{hglKeyCode_t::HGLK_SHIFT,			gainput::Key::KeyShiftR},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyBack},
	{hglKeyCode_t::HGLK_LEFT,			gainput::Key::KeySoftLeft},
	{hglKeyCode_t::HGLK_RIGHT,			gainput::Key::KeySoftRight},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyCall},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyEndcall},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyStar},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyPound},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyDpadCenter},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyVolumeUp},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyVolumeDown},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyPower},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyCamera},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyClear},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeySymbol},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyExplorer},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyEnvelope},
	{hglKeyCode_t::HGLK_EQUALS,		gainput::Key::KeyEquals},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyAt},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyHeadsethook},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyFocus},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyPlus},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyNotification},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeySearch},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyMediaPlayPause},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyMediaStop},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyMediaNext},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyMediaPrevious},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyMediaRewind},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyMediaFastForward},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyMute},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyPictsymbols},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeySwitchCharset},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyForward},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyExtra1},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyExtra2},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyExtra3},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyExtra4},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyExtra5},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyExtra6},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyFn},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyCircumflex},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeySsharp},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyAcute},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyAltGr},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyNumbersign},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyUdiaeresis},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyAdiaeresis},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyOdiaeresis},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeySection},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyAring},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyDiaeresis},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyTwosuperior},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyRightParenthesis},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyDollar},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyUgrave},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyAsterisk},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyColon},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyExclam},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyBraceLeft},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeyBraceRight},
	{hglKeyCode_t::HGLK_NO_KEY,		gainput::Key::KeySysRq},
	{hglKeyCode_t::HGLM_LBUTTON,	gainput::MouseButton::MouseButtonLeft},
	{hglKeyCode_t::HGLM_RBUTTON,	gainput::MouseButton::MouseButtonRight},
	{hglKeyCode_t::HGLM_MBUTTON,	gainput::MouseButton::MouseButtonMiddle},
};

// TODO: remove this when input events are fully implemented with special text entry mode.
static std::map<unsigned, char> gainputKeyToChar = {
	{gainput::Key::KeyEscape,				  '\0'},
	{gainput::Key::KeyF1,                    '\0'},
	{gainput::Key::KeyF2,                    '\0'},
	{gainput::Key::KeyF3,                    '\0'},
	{gainput::Key::KeyF4,                    '\0'},
	{gainput::Key::KeyF5,                    '\0'},
	{gainput::Key::KeyF6,                    '\0'},
	{gainput::Key::KeyF7,                    '\0'},
	{gainput::Key::KeyF8,                    '\0'},
	{gainput::Key::KeyF9,                    '\0'},
	{gainput::Key::KeyF10,                   '\0'},
	{gainput::Key::KeyF11,                   '\0'},
	{gainput::Key::KeyF12,                   '\0'},
	{gainput::Key::KeyF13,                   '\0'},
	{gainput::Key::KeyF14,                   '\0'},
	{gainput::Key::KeyF15,                   '\0'},
	{gainput::Key::KeyF16,                   '\0'},
	{gainput::Key::KeyF17,                   '\0'},
	{gainput::Key::KeyF18,                   '\0'},
	{gainput::Key::KeyF19,                   '\0'},
	{gainput::Key::KeyPrint,                 '\0'},
	{gainput::Key::KeyScrollLock,            '\0'},
	{gainput::Key::KeyBreak,                 '\0'},
	{gainput::Key::KeySpace,                 ' '},
	{gainput::Key::KeyApostrophe,            '\''},
	{gainput::Key::KeyComma,                 ','},
	{gainput::Key::KeyMinus,                 '-'},
	{gainput::Key::KeyPeriod,                '.'},
	{gainput::Key::KeySlash,                 '/'},
	{gainput::Key::Key0,                     '0'},
	{gainput::Key::Key1,                     '1'},
	{gainput::Key::Key2,                     '2'},
	{gainput::Key::Key3,                     '3'},
	{gainput::Key::Key4,                     '4'},
	{gainput::Key::Key5,                     '5'},
	{gainput::Key::Key6,                     '6'},
	{gainput::Key::Key7,                     '7'},
	{gainput::Key::Key8,                     '8'},
	{gainput::Key::Key9,                     '9'},
	{gainput::Key::KeySemicolon,             ';'},
	{gainput::Key::KeyLess,                  '<'},
	{gainput::Key::KeyEqual,                 '='},
	{gainput::Key::KeyA,                     'a'},
	{gainput::Key::KeyB,                     'b'},
	{gainput::Key::KeyC,                     'c'},
	{gainput::Key::KeyD,                     'd'},
	{gainput::Key::KeyE,                     'e'},
	{gainput::Key::KeyF,                     'f'},
	{gainput::Key::KeyG,                     'g'},
	{gainput::Key::KeyH,                     'h'},
	{gainput::Key::KeyI,                     'i'},
	{gainput::Key::KeyJ,                     'j'},
	{gainput::Key::KeyK,                     'k'},
	{gainput::Key::KeyL,                     'l'},
	{gainput::Key::KeyM,                     'm'},
	{gainput::Key::KeyN,                     'n'},
	{gainput::Key::KeyO,                     'o'},
	{gainput::Key::KeyP,                     'p'},
	{gainput::Key::KeyQ,                     'q'},
	{gainput::Key::KeyR,                     'r'},
	{gainput::Key::KeyS,                     's'},
	{gainput::Key::KeyT,                     't'},
	{gainput::Key::KeyU,                     'u'},
	{gainput::Key::KeyV,                     'v'},
	{gainput::Key::KeyW,                     'w'},
	{gainput::Key::KeyX,                     'x'},
	{gainput::Key::KeyY,                     'y'},
	{gainput::Key::KeyZ,                     'z'},
	{gainput::Key::KeyBracketLeft,           '['},
	{gainput::Key::KeyBackslash,             '\\'},
	{gainput::Key::KeyBracketRight,          ']'},
	{gainput::Key::KeyGrave,                 '\0'},
	{gainput::Key::KeyLeft,                  '\0'},
	{gainput::Key::KeyRight,                 '\0'},
	{gainput::Key::KeyUp,                    '\0'},
	{gainput::Key::KeyDown,                  '\0'},
	{gainput::Key::KeyInsert,                '\0'},
	{gainput::Key::KeyHome,                  '\0'},
	{gainput::Key::KeyDelete,                '\0'},
	{gainput::Key::KeyEnd,                   '\0'},
	{gainput::Key::KeyPageUp,                '\0'},
	{gainput::Key::KeyPageDown,              '\0'},
	{gainput::Key::KeyNumLock,               '\0'},
	{gainput::Key::KeyKpEqual,               '='},
	{gainput::Key::KeyKpDivide,              '/'},
	{gainput::Key::KeyKpMultiply,            '*'},
	{gainput::Key::KeyKpSubtract,            '-'},
	{gainput::Key::KeyKpAdd,                 '+'},
	{gainput::Key::KeyKpEnter,               '\0'},
	{gainput::Key::KeyKpInsert,              '\0'},
	{gainput::Key::KeyKpEnd,                 '\0'},
	{gainput::Key::KeyKpDown,                '\0'},
	{gainput::Key::KeyKpPageDown,            '\0'},
	{gainput::Key::KeyKpLeft,                '\0'},
	{gainput::Key::KeyKpBegin,               '\0'},
	{gainput::Key::KeyKpRight,               '\0'},
	{gainput::Key::KeyKpHome,                '\0'},
	{gainput::Key::KeyKpUp,                  '\0'},
	{gainput::Key::KeyKpPageUp,              '\0'},
	{gainput::Key::KeyKpDelete,              '\0'},
	{gainput::Key::KeyBackSpace,             '\0'},
	{gainput::Key::KeyTab,                   '\0'},
	{gainput::Key::KeyReturn,                '\0'},
	{gainput::Key::KeyCapsLock,              '\0'},
	{gainput::Key::KeyShiftL,                '\0'},
	{gainput::Key::KeyCtrlL,                 '\0'},
	{gainput::Key::KeySuperL,                '\0'},
	{gainput::Key::KeyAltL,                  '\0'},
	{gainput::Key::KeyAltR,                  '\0'},
	{gainput::Key::KeySuperR,                '\0'},
	{gainput::Key::KeyMenu,                  '\0'},
	{gainput::Key::KeyCtrlR,                 '\0'},
	{gainput::Key::KeyShiftR,                '\0'},
	{gainput::Key::KeyBack,                  '\0'},
	{gainput::Key::KeySoftLeft,              '\0'},
	{gainput::Key::KeySoftRight,             '\0'},
	{gainput::Key::KeyCall,                  '\0'},
	{gainput::Key::KeyEndcall,               '\0'},
	{gainput::Key::KeyStar,                  '*'},
	{gainput::Key::KeyPound,                 '#'},
	{gainput::Key::KeyDpadCenter,            '\0'},
	{gainput::Key::KeyVolumeUp,              '\0'},
	{gainput::Key::KeyVolumeDown,            '\0'},
	{gainput::Key::KeyPower,                 '\0'},
	{gainput::Key::KeyCamera,                '\0'},
	{gainput::Key::KeyClear,                 '\0'},
	{gainput::Key::KeySymbol,                '\0'},
	{gainput::Key::KeyExplorer,              '\0'},
	{gainput::Key::KeyEnvelope,              '\0'},
	{gainput::Key::KeyEquals,                '='},
	{gainput::Key::KeyAt,                    '@'},
	{gainput::Key::KeyHeadsethook,           '\0'},
	{gainput::Key::KeyFocus,                 '\0'},
	{gainput::Key::KeyPlus,                  '+'},
	{gainput::Key::KeyNotification,          '\0'},
	{gainput::Key::KeySearch,                '\0'},
	{gainput::Key::KeyMediaPlayPause,        '\0'},
	{gainput::Key::KeyMediaStop,             '\0'},
	{gainput::Key::KeyMediaNext,             '\0'},
	{gainput::Key::KeyMediaPrevious,         '\0'},
	{gainput::Key::KeyMediaRewind,           '\0'},
	{gainput::Key::KeyMediaFastForward,      '\0'},
	{gainput::Key::KeyMute,                  '\0'},
	{gainput::Key::KeyPictsymbols,           '\0'},
	{gainput::Key::KeySwitchCharset,         '\0'},
	{gainput::Key::KeyForward,               '\0'},
	{gainput::Key::KeyExtra1,                '\0'},
	{gainput::Key::KeyExtra2,                '\0'},
	{gainput::Key::KeyExtra3,                '\0'},
	{gainput::Key::KeyExtra4,                '\0'},
	{gainput::Key::KeyExtra5,                '\0'},
	{gainput::Key::KeyExtra6,                '\0'},
	{gainput::Key::KeyFn,                    '\0'},
	{gainput::Key::KeyCircumflex,            '\0'},
	{gainput::Key::KeySsharp,                '\0'},
	{gainput::Key::KeyAcute,                 '\0'},
	{gainput::Key::KeyAltGr,                 '\0'},
	{gainput::Key::KeyNumbersign,            '#'},
	{gainput::Key::KeyUdiaeresis,            '\0'},
	{gainput::Key::KeyAdiaeresis,            '\0'},
	{gainput::Key::KeyOdiaeresis,            '\0'},
	{gainput::Key::KeySection,               '\0'},
	{gainput::Key::KeyAring,                 '\0'},
	{gainput::Key::KeyDiaeresis,             '\0'},
	{gainput::Key::KeyTwosuperior,           '\0'},
	{gainput::Key::KeyRightParenthesis,      ')'},
	{gainput::Key::KeyDollar,                '$'},
	{gainput::Key::KeyUgrave,                '\0'},
	{gainput::Key::KeyAsterisk,              '*'},
	{gainput::Key::KeyColon,                 ':'},
	{gainput::Key::KeyExclam,                '!'},
	{gainput::Key::KeyBraceLeft,             '('},
	{gainput::Key::KeyBraceRight,            ')'},
	{gainput::Key::KeySysRq,                 '\0'},
};

const char *keyToName[] = {
	"?",
	"Left Mouse Button", "Right Mouse Button", "?", "Middle Mouse Button",
	"?", "?", "?", "Backspace", "Tab", "?", "?", "?", "Enter", "?", "?",
	"Shift", "Ctrl", "Alt", "Pause", "Caps Lock", "?", "?", "?", "?", "?", "?",
	"Escape", "?", "?", "?", "?",
	"Space", "Page Up", "Page Down", "End", "Home",
	"Left Arrow", "Up Arrow", "Right Arrow", "Down Arrow",
	"?", "?", "?", "?", "Insert", "Delete", "?",
	"0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
	"?", "?", "?", "?", "?", "?", "?",
	"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M",
	"N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
	"Left Win", "Right Win", "Application", "?", "?",
	"NumPad 0", "NumPad 1", "NumPad 2", "NumPad 3", "NumPad 4",
	"NumPad 5", "NumPad 6", "NumPad 7", "NumPad 8", "NumPad 9",
	"Multiply", "Add", "?", "Subtract", "Decimal", "Divide",
	"F1", "F2", "F3", "F4", "F5", "F6", "F7", "F8", "F9", "F10", "F11", "F12",
	"?", "?", "?", "?", "?", "?", "?", "?", "?", "?",
	"?", "?", "?", "?", "?", "?", "?", "?", "?", "?",
	"Num Lock", "Scroll Lock",
	"?", "?", "?", "?", "?", "?", "?", "?", "?", "?",
	"?", "?", "?", "?", "?", "?", "?", "?", "?", "?",
	"?", "?", "?", "?", "?", "?", "?", "?", "?", "?",
	"?", "?", "?", "?", "?", "?", "?", "?", "?", "?",
	"Semicolon", "Equals", "Comma", "Minus", "Period", "Slash", "Grave",
	"?", "?", "?", "?", "?", "?", "?", "?", "?", "?",
	"?", "?", "?", "?", "?", "?", "?", "?", "?", "?",
	"?", "?", "?", "?", "?", "?",
	"Left bracket", "Backslash", "Right bracket", "Apostrophe",
	"?", "?", "?", "?", "?", "?", "?", "?", "?", "?",
	"?", "?", "?", "?", "?", "?", "?", "?", "?", "?",
	"?", "?", "?", "?", "?", "?", "?", "?", "?", "?",
	"?", "?", "?"
};

//////////////////////////////////////////////////////////////////// Pad

static std::map<unsigned, unsigned> gainputPadToHGL = {
	// D-pad
	{gainput::PadButton::PadButtonUp,			hglPadCode_t::HGLP_DPAD_UP},
	{gainput::PadButton::PadButtonDown,		hglPadCode_t::HGLP_DPAD_DOWN},
	{gainput::PadButton::PadButtonLeft,		hglPadCode_t::HGLP_DPAD_LEFT},
	{gainput::PadButton::PadButtonRight,		hglPadCode_t::HGLP_DPAD_RIGHT},
	// Left stick
	{gainput::PadButton::PadButtonLeftStickX,	hglPadCode_t::HGLP_X_AXIS},
	{gainput::PadButton::PadButtonLeftStickY,	hglPadCode_t::HGLP_Y_AXIS},
	// Right stick
	{gainput::PadButton::PadButtonRightStickX,	hglPadCode_t::HGLP_XR_AXIS},
	{gainput::PadButton::PadButtonRightStickY,	hglPadCode_t::HGLP_YR_AXIS},
	// Face buttons
	{gainput::PadButton::PadButtonA,			hglPadCode_t::HGLP_A},
	{gainput::PadButton::PadButtonB,			hglPadCode_t::HGLP_B},
	{gainput::PadButton::PadButtonX,			hglPadCode_t::HGLP_X},
	{gainput::PadButton::PadButtonY,			hglPadCode_t::HGLP_Y},
	// Bumpers and triggers
	{gainput::PadButton::PadButtonL1,			hglPadCode_t::HGLP_LB},
	{gainput::PadButton::PadButtonL2,			hglPadCode_t::HGLP_LT},
	{gainput::PadButton::PadButtonL3,			hglPadCode_t::HGLP_LS},
	{gainput::PadButton::PadButtonR1,			hglPadCode_t::HGLP_RB},
	{gainput::PadButton::PadButtonR2,			hglPadCode_t::HGLP_RT},
	{gainput::PadButton::PadButtonR3,			hglPadCode_t::HGLP_RS},
	// Centre
	{gainput::PadButton::PadButtonStart,		hglPadCode_t::HGLP_START},
	{gainput::PadButton::PadButtonSelect,		hglPadCode_t::HGLP_BACK},
	{gainput::PadButton::PadButtonHome,		hglPadCode_t::HGLP_HOME},
};

static std::map<unsigned, unsigned> HGLPadToGainput = {
	// D-pad
	{hglPadCode_t::HGLP_DPAD_UP,		gainput::PadButton::PadButtonUp},
	{hglPadCode_t::HGLP_DPAD_DOWN,		gainput::PadButton::PadButtonDown},
	{hglPadCode_t::HGLP_DPAD_LEFT,		gainput::PadButton::PadButtonLeft},
	{hglPadCode_t::HGLP_DPAD_RIGHT,		gainput::PadButton::PadButtonRight},
	// Left stick                       
	{hglPadCode_t::HGLP_X_AXIS,			gainput::PadButton::PadButtonLeftStickX},
	{hglPadCode_t::HGLP_Y_AXIS,			gainput::PadButton::PadButtonLeftStickY},
	// Right stick                      
	{hglPadCode_t::HGLP_XR_AXIS,		gainput::PadButton::PadButtonRightStickX},
	{hglPadCode_t::HGLP_YR_AXIS,		gainput::PadButton::PadButtonRightStickY},
	// Face buttons                     
	{hglPadCode_t::HGLP_A,				gainput::PadButton::PadButtonA},
	{hglPadCode_t::HGLP_B,				gainput::PadButton::PadButtonB},
	{hglPadCode_t::HGLP_X,				gainput::PadButton::PadButtonX},
	{hglPadCode_t::HGLP_Y,				gainput::PadButton::PadButtonY},
	// Bumpers and triggers	            
	{hglPadCode_t::HGLP_LB,				gainput::PadButton::PadButtonL1},
	{hglPadCode_t::HGLP_LT,				gainput::PadButton::PadButtonL2},
	{hglPadCode_t::HGLP_LS,				gainput::PadButton::PadButtonL3},
	{hglPadCode_t::HGLP_RB,				gainput::PadButton::PadButtonR1},
	{hglPadCode_t::HGLP_RT,				gainput::PadButton::PadButtonR2},
	{hglPadCode_t::HGLP_RS,				gainput::PadButton::PadButtonR3},
	// Centre                           
	{hglPadCode_t::HGLP_START,			gainput::PadButton::PadButtonStart},
	{hglPadCode_t::HGLP_BACK,			gainput::PadButton::PadButtonSelect},
	{hglPadCode_t::HGLP_HOME,			gainput::PadButton::PadButtonHome},
};

static const char *padToName[] = {
	"?",
	"D-Pad Up", "D-Pad Down", "D-Pad Left", "D-Pad Right",
	"Left Stick X", "Left Stick Y", "LS",
	"Right Stick X", "Right Stick Y", "RS",
	"A", "B", "X", "Y",
	"LB", "RB", "LT", "RT",
	"Back", "Start", "Home",
};
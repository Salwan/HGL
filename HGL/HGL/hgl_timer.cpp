// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// https://github.com/kvakvs/hge 
#include "precomp.h"
#include "hgl_impl.h"

#ifdef HGLPLATFORM_WIN32
#include "hgl_system_win32.h"
#endif

constexpr double NANO_TO_SEC = 1.0/1000000000.0;
constexpr double MAX_DELTA_TIME_S = 0.1;

//////////////////////////////////////////////// Timer -> Interface

// Returns time elapsed since System_Initiate function call in seconds.
float HGL_Impl::Timer_GetTime() {
	auto now = std::chrono::steady_clock::now();
	return
		static_cast<float>(
			std::chrono::duration_cast<std::chrono::nanoseconds>(now - tpInitTime).count()
			* NANO_TO_SEC);
}

// Returns time elapsed since last frame function call in seconds. Single precison. (ms)
float HGL_Impl::Timer_GetDelta() {
	return static_cast<float>(sActiveTimer.duration);
}

// Returns time elapsed since last frame function call in seconds. Double precision. (ns)
double HGL_Impl::Timer_GetDeltaNS() {
	return sActiveTimer.duration;
}

// Returns number of frame function calls during last second.
int HGL_Impl::Timer_GetFPS() {
	return sActiveTimer.FPS;
}

//////////////////////////////////////////////// Timer -> Internal

// Main timer thread entry point
// The benefits of having a timer thread:
// - Can be locked to one core for more accuracy (OS dependent)
// - Timer tick can be initiated for last frame allowing it to run independently
//   Tick call takes place somewhere but doesn't directly update active timer
//   Instead update takes place at a predefined point later (after swap is done for example)
void HGL_Impl::_mainTimer(HGL_Impl* hgl_impl) {
	auto now = hgl_impl->tpInitTime;
	HGL_TimerThread &timer_thread = hgl_impl->sTimerThread;

	std::unique_lock<std::mutex> timer_lock(timer_thread.mutex);
	std::chrono::steady_clock::time_point start = now;
	std::chrono::steady_clock::time_point end;
	//long long duration = 0;
	while (!timer_thread.bEndTimerThread) {
		while (!timer_thread.bTickTimerThread) {
			timer_thread.cvar.wait(timer_lock);
		}
		end = std::chrono::steady_clock::now();
		auto d = end - start;
		//duration = std::chrono::duration_cast<std::chrono::steady_clock::duration>(d).count();
		timer_thread.duration_ns.store(
			std::chrono::duration_cast<std::chrono::nanoseconds>(d).count());
		start = end;
		timer_thread.bTickTimerThread = false;
	}
}

// Setup synchronization and start timer thread, lock it to one core (OS specific)
bool HGL_Impl::_initTimers() {
	tpInitTime = std::chrono::steady_clock::now();
	std::thread tt(HGL_Impl::_mainTimer, this);
	sTimerThread.thread.swap(tt);
	// Set timer thread to run exclusive on first core (+predictable time)
	HGLPlatform::SetThreadAffinity(sTimerThread.thread, 1);
	// Test run
	std::this_thread::sleep_for(std::chrono::milliseconds(16));
	std::unique_lock<std::mutex> timer_lock(sTimerThread.mutex);
	sTimerThread.bTickTimerThread = true;
	sTimerThread.cvar.notify_one();
	timer_lock.unlock();

	// This is first tick test, should result in successful run (16ms duration)
	while (sTimerThread.bTickTimerThread) {
		// Without this a deadlock may happen
		std::this_thread::sleep_for(std::chrono::milliseconds(0));
	}

	sActiveTimer.duration = 
		static_cast<double>(sTimerThread.duration_ns.load()) * NANO_TO_SEC;
	System_Log("Timer thread first tick (16ms test) = %.3fs", sActiveTimer.duration);

	if (sActiveTimer.duration > 0.0) {
		return true;
	} else {
		System_Log("[ERROR] duration returned was not valid! (duration <= 0.0)");
		return false;
	}
}

// This is called by hgl_system once every frame to read time from timer thread
// This updates internal timer using a mutex lock.
void HGL_Impl::_tickTimer() {
	std::unique_lock<std::mutex> timer_lock(sTimerThread.mutex);
	sTimerThread.bTickTimerThread = true;
	sTimerThread.cvar.notify_one();
	timer_lock.unlock();
}

// Update time delta and fps, this is called by hgl_system once every frame
void HGL_Impl::_updateTimer() {
	// Get latest internal time to active
	sActiveTimer.duration = 
		static_cast<double>(sTimerThread.duration_ns.load()) * NANO_TO_SEC;
	// Force timer to be 0.1 second maximum positive (in case of interruption of runtime or long loading time)
	sActiveTimer.duration = hglUtils::clamp(sActiveTimer.duration, 0.0, 0.1);
	// Update timer stuff
	sActiveTimer.frameCounter += 1;
	sActiveTimer.timeAccum += sActiveTimer.duration;
	while (sActiveTimer.timeAccum >= 1.0) {
		sActiveTimer.FPS = sActiveTimer.frameCounter;
		sActiveTimer.frameCounter = 0;
		sActiveTimer.timeAccum -= 1.0;
	}
}

// End timer thread
void HGL_Impl::_destroyTimers() {
	sTimerThread.bEndTimerThread = true;
	std::unique_lock<std::mutex> timer_lock(sTimerThread.mutex);
	sTimerThread.bTickTimerThread = true;
	sTimerThread.cvar.notify_one();
	timer_lock.unlock();
	sTimerThread.thread.join();
	System_Log("Timer thread ended");
}
// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// https://github.com/kvakvs/hge 
#include "precomp.h"
#include "hgl_impl.h"

#ifdef HGLPLATFORM_WIN32
#include "hgl_system_win32.h"
#endif

/////////////////////////////////////// Audio -> Sound Effects
#pragma region "Sound -> Effects"

// Audio Destructor
void HGL_Impl::_destroyAudio() {
	System_Log("Shutting down Audio..");
	if(spSoundEffectsList && spSoundEffectsList->size() > 0) {
		for(auto se : *spSoundEffectsList.get()) {
			for(unsigned i = 0; i < 4; ++i) {
				if(se->alSources[i] && alIsSource(se->alSources[i])) {
					alDeleteSources(1, &se->alSources[i]);
				}
			}
			if(alIsBuffer(se->alBuffer)) {
				alDeleteBuffers(1, &se->alBuffer);
			}
		}
		System_Log("  Deleted %d sound effects", spSoundEffectsList->size());
	}
	if(bAudioAvailable) {
		alcMakeContextCurrent(NULL);
		if(alContext) {
			alcDestroyContext(alContext);
		}
		if(alDevice) {
			alcCloseDevice(alDevice);
		}
	}
}

/*
** TODO: Loads a sound effect from memory, resource pack or disk file.
** TODO: Supports WAV, MP3, MP2, MP1 and OGG audio file formats.
*  filename: Sound effect file name.
*  size: If this parameter isn't 0, it is the size of memory block containing
*  the sound effect in one of the known formats and the parameter filename is
*  treated as a pointer to this block.
*  If successful, returns the sound effect handle to be used with Effect_Play
*  and Effect_PlayEx functions. Otherwise returns 0.
*/
HEFFECT	HGL_Impl::Effect_Load(const char *filename, size_t size) {
	// TODO: implement sound effects caching, so a sound can be reloaded immediately from memory if it's 
	//       already created. Might require std::map and storing by filename/path? or hash of filename/path.
	//		HGL_SoundEffect should store the filename/path internally for secondary confirmation.
	//      Problem is: how to do this for in-memory sounds? so sounds that aren't loaded from a file.
	HEFFECT heffect_out = nullptr;
	if(size > 0) {
		// Memory loading a file
		assert(true && "Effect_Load() memory Not implemented yet");
	} else {
		// Open file
		SF_INFO sf_info;
		memset(&sf_info, 0, sizeof(SF_INFO));
		SNDFILE *sndfile = sf_open(filename, SFM_READ, &sf_info);
		if(!sndfile || sf_info.frames == 0) {
			std::ostringstream ss;
			ss << "Effect_Load() couldn't open file: " << filename;
			std::cerr << ss.str() << std::endl;
			System_Log(ss.str().c_str());
			HGLPlatform::ShowErrorMessage(ss.str().c_str());
			return nullptr;
		}
		assert(sf_info.frames > 0 && "Frames count for sound effect are Zero!");

		if(sf_info.format & SF_FORMAT_OGG) {
			// Set float to int conversion correctly to avoid cracks
			// IMPORTANT: this might be a performance sink, it's a flaw in libsndfile that required this
			// workaround after-thought solution.
			// See: https://github.com/erikd/libsndfile/issues/16 
			//      https://github.com/erikd/libsndfile/issues/194
			// Eventually, I'm going to move all this into threads/fibers so maybe it
			// won't matter at the end.
			sf_command(sndfile, SFC_SET_SCALE_FLOAT_INT_READ, NULL, true);
		}

		ALenum sndformat = AL_FALSE;
		if(sf_info.format & SF_FORMAT_WAV || sf_info.format & SF_FORMAT_OGG) {
			if(sf_info.format & SF_FORMAT_PCM_16 || sf_info.format & SF_FORMAT_OGG) {
				if(sf_info.channels == 2) {
					sndformat = AL_FORMAT_STEREO16;
				} else if(sf_info.channels == 1) {
					sndformat = AL_FORMAT_MONO16;
				}
			}
		}
		if(sndformat == AL_FALSE) {
			System_Log("Sound effect \"%s\" could not be loaded because HGL can only play: WAV/OGG 16-bits Stereo or Mono sounds", filename);
			return nullptr;
		}

		short *data = new short[static_cast<unsigned>(sf_info.frames * sf_info.channels)];
		sf_count_t size_read = sf_read_short(sndfile, data, sf_info.frames * sf_info.channels);
		assert(size_read == sf_info.frames * sf_info.channels && "Frames read don't match actual sound effect frame count");

		float sndtime = sf_info.frames / static_cast<float>(sf_info.samplerate);
		System_Log("[IO] Sound effect \"%s\" loaded: %f seconds duration (size: %d)", 
			filename, sndtime, size_read);

		// Create OpenAL buffer
		ALuint buffer = 0;
		alGenBuffers(1, &buffer);
		alBufferData(buffer, sndformat, data, static_cast<ALsizei>(sf_info.frames) * sizeof(short) * sf_info.channels, sf_info.samplerate);
		ALenum err = alGetError();
		if(err != AL_NO_ERROR) {
			System_Log("OpenALSoft error while creating AL Buffer for \"%s\": %s", filename, alGetString(err));
			if(alIsBuffer(buffer)) {
				alDeleteBuffers(1, &buffer);
			}
			return nullptr;
		}

		// Create OpenAL source (1 source per buffer for now)
		ALuint sources [4] {0, 0, 0, 0};
		alGenSources(4, sources);
		for(unsigned i = 0; i < 4; ++i) {
			alSourcei(sources[i], AL_BUFFER, buffer);
			err = alGetError();
			if(err != AL_NO_ERROR) {
				System_Log("OpenALSoft error while creating AL Source %d for \"%s\": %s", i, filename, 
					alGetString(err));
				if(alIsBuffer(buffer)) {
					alDeleteBuffers(1, &buffer);
				}
				for(unsigned ii = 0; ii < 4; ++ii) {
					if(sources[ii] && alIsSource(sources[ii])) {
						alDeleteSources(1, &sources[ii]);
					}
				}
				return nullptr;
			}
		}

		// Create and add HGL sound effect
		HGL_SoundEffect* se = new HGL_SoundEffect(buffer, sources);
		spSoundEffectsList->push_back(se);
		heffect_out = static_cast<void*>(se);

		// Done with data, clear stuff
		delete[] data;
		sf_close(sndfile);
	}
	return heffect_out;
}

// Deletes loaded sound effect and frees associated resources.
void HGL_Impl::Effect_Free(HEFFECT eff) {
	if(eff) {
		auto iter = std::find(spSoundEffectsList->begin(), spSoundEffectsList->end(), eff);
		if(iter != spSoundEffectsList->end()) {
			HGL_SoundEffect* se = static_cast<HGL_SoundEffect*>(eff);
			for(unsigned i = 0; i < 4; ++i) {
				if(se->alSources[i] && alIsSource(se->alSources[i])) {
					alDeleteSources(1, &se->alSources[i]);
				}
			}
			if(alIsBuffer(se->alBuffer)) {
				alDeleteBuffers(1, &se->alBuffer);
			}
			spSoundEffectsList->erase(iter);
		}
	}
}

HCHANNEL HGL_Impl::Effect_Play(HEFFECT eff) {
	// TODO: for now, alSource is being returned as the 1 channel. 
	//       in HGE, each effect can be played up to 4 times simulatenously by having multiple channels.
	//       when 4 is exceeded, the instance with the lowest volume is restarted with default parameters.
	//       all I need to do this is have 4+ alsources per SoundEffect to emulate this.
	//       So basically, create a source whenever an effect should be played and remove it whenever it
	//       finishes playing.
	return Effect_PlayEx(eff);
}

HCHANNEL HGL_Impl::Effect_PlayEx(HEFFECT eff, int volume, int pan, float pitch, bool loop) {
	// See TODO: Effect_Play add support for multiple channels.
	if(eff) {
		HGL_SoundEffect* se = static_cast<HGL_SoundEffect*>(eff);
		const float gain = hglUtils::clamp<float>(static_cast<float>(volume/100.0f), 0.0f, 100.0f);
		// For some reason: pan -1.0 to 1.0 is from right to left, -1.0 for right
		const float pospan [] = {static_cast<float>(-pan) / 100.0f, 0.0f, 0.0f};
		float lowest_gain = 1.0f;
		int lowest_gain_source = 0;
		int selected_source = -1;
		for(unsigned i = 0; i < 4; ++i) { // Look for a source that isn't playing
			int state = 0;
			alGetSourcei(se->alSources[i], AL_SOURCE_STATE, &state);
			if(state != AL_PLAYING) {
				selected_source = i;
				break;
			}
			if(se->fGain[i] < lowest_gain) {
				lowest_gain = se->fGain[i];
				lowest_gain_source = i;
			}
		}
		if(selected_source == -1) { // All sources are playing? use lowest gain otherwise Source Walk
			if(lowest_gain < 1.0f) {
				selected_source = lowest_gain_source;
			} else {
				selected_source = se->iSourceWalker;
				se->iSourceWalker = ++se->iSourceWalker >= 4? 0 : se->iSourceWalker;
			}
		}

		alSourcef(se->alSources[selected_source], AL_GAIN, gain);
		alSourcefv(se->alSources[selected_source], AL_POSITION, pospan);
		alSourcef(se->alSources[selected_source], AL_PITCH, pitch);
		alSourcei(se->alSources[selected_source], AL_LOOPING, loop? AL_TRUE : AL_FALSE);
		alSourcePlay(se->alSources[selected_source]);	// This will restart sound if it's already playing.
		se->fGain[selected_source] = gain;
		// HCHANNEL for now is: *ALuint representing source.
		return static_cast<HCHANNEL>(&se->alSources[selected_source]);
	}
	return nullptr;
}
// Audio -> Sound Effects
#pragma endregion 

////////////////////////////////////////////////// Audio -> Channels
#pragma region "Audio Channels"

// Panning value is from -100 to 100. Center is 0
void  HGL_CALL HGL_Impl::Channel_SetPanning(HCHANNEL chn, int pan) {
	assert(chn && "Channel_SetPanning given invalid channel (null)");
	if (chn) {
		const float pospan[] = { static_cast<float>(-pan) / 100.0f, 0.0f, 0.0f };
		ALuint* psrc = static_cast<ALuint*>(chn);
		alSourcefv(*psrc, AL_POSITION, pospan);
	}
}

// Volume value is from [0, 100]
void  HGL_CALL HGL_Impl::Channel_SetVolume(HCHANNEL chn, int volume) {
	assert(chn && "Channel_SetVolume given invalid channel (null)");
	if (chn) {
		const float gain = hglUtils::clamp<float>(
			static_cast<float>(volume / 100.0f), 0.0f, 100.0f);
		ALuint* psrc = static_cast<ALuint*>(chn);
		alSourcef(*psrc, AL_GAIN, gain);
	}
}

// Pitch of 1.0 is default
void  HGL_CALL HGL_Impl::Channel_SetPitch(HCHANNEL chn, float pitch) {
	assert(chn && "Channel_SetPitch given invalid channel (null)");
	if (chn) {
		ALuint* psrc = static_cast<ALuint*>(chn);
		alSourcef(*psrc, AL_PITCH, pitch);
	}
}

void  HGL_CALL HGL_Impl::Channel_Pause(HCHANNEL chn) {
	assert(chn && "Channel_Pause given invalid channel (null)");
	if (chn) {
		ALuint* psrc = static_cast<ALuint*>(chn);
		alSourcePause(*psrc);
	}
}

void  HGL_CALL HGL_Impl::Channel_Resume(HCHANNEL chn) {
	assert(chn && "Channel_Resume given invalid channel (null)");
	if (chn) {
		ALuint* psrc = static_cast<ALuint*>(chn);
		alSourcePlay(*psrc);
	}
}

void  HGL_CALL HGL_Impl::Channel_Stop(HCHANNEL chn) {
	assert(chn && "Channel_Stop given invalid channel (null)");
	if (chn) {
		ALuint* psrc = static_cast<ALuint*>(chn);
		alSourceStop(*psrc);
	}
}

// _audio_Suspend/Resume/Stop should only be called by engine since it's tied to a special
// engine state rather than game. It's guaranteed to exactly restore the state of audio
// when application is resumed.

void  HGL_CALL HGL_Impl::Channel_PauseAll() {
	// Pause all playing channels
	if (spSoundEffectsList && spSoundEffectsList->size() > 0) {
		int state = 0;
		std::vector<ALuint> playing_sources;
		for (auto se : *spSoundEffectsList.get()) {
			for (unsigned i = 0; i < 4; ++i) {
				if (se->alSources[i]) {
					alGetSourcei(se->alSources[i], AL_SOURCE_STATE, &state);
					if (state == AL_PLAYING) {
						playing_sources.push_back(se->alSources[i]);
					}
				}
			}
		}
		alSourcePausev(static_cast<ALsizei>(playing_sources.size()), playing_sources.data());
	}
}

void  HGL_CALL HGL_Impl::Channel_ResumeAll() {
	// Resume all paused channels
	if (spSoundEffectsList && spSoundEffectsList->size() > 0) {
		int state = 0;
		std::vector<ALuint> paused_sources;
		for (auto se : *spSoundEffectsList.get()) {
			for (unsigned i = 0; i < 4; ++i) {
				if (se->alSources[i]) {
					alGetSourcei(se->alSources[i], AL_SOURCE_STATE, &state);
					if (state == AL_PAUSED) {
						paused_sources.push_back(se->alSources[i]);
					}
				}
			}
		}
		alSourcePlayv(static_cast<ALsizei>(paused_sources.size()), paused_sources.data());
	}
}

void  HGL_CALL HGL_Impl::Channel_StopAll() {
	// Stop all playing/paused channels
	if (spSoundEffectsList && spSoundEffectsList->size() > 0) {
		int state = 0;
		std::vector<ALuint> p_sources;
		for (auto se : *spSoundEffectsList.get()) {
			for (unsigned i = 0; i < 4; ++i) {
				if (se->alSources[i]) {
					alGetSourcei(se->alSources[i], AL_SOURCE_STATE, &state);
					if (state == AL_PAUSED || state == AL_PLAYING) {
						p_sources.push_back(se->alSources[i]);
					}
				}
			}
		}
		alSourceStopv(static_cast<ALsizei>(p_sources.size()), p_sources.data());
	}
}

bool  HGL_CALL HGL_Impl::Channel_IsPlaying(HCHANNEL chn) {
	assert(chn && "Channel_IsPlaying given invalid channel (null)");
	if(chn) {
		ALuint* psrc = static_cast<ALuint*>(chn);
		int state = 0;
		alGetSourcei(*psrc, AL_SOURCE_STATE, &state);
		return state == AL_PLAYING;
	}
	return false;
}

float HGL_CALL HGL_Impl::Channel_GetLength(HCHANNEL chn) {
	assert(false && "Not implemented");
	return 0.0f;
}

float HGL_CALL HGL_Impl::Channel_GetPos(HCHANNEL chn) {
	assert(false && "Not implemented");
	return 0.0f;
}

void  HGL_CALL HGL_Impl::Channel_SetPos(HCHANNEL chn, float fSeconds) {
	assert(false && "Not implemented");
	return;
}

void  HGL_CALL HGL_Impl::Channel_SlideTo(HCHANNEL channel, float time, int volume, 
									     int pan, float pitch) {
	assert(false && "Not implemented");
	return;
}

bool  HGL_CALL HGL_Impl::Channel_IsSliding(HCHANNEL channel) {
	assert(false && "Not implemented");
	return false;
}

#pragma endregion
// Audio -> Channels

////////////////////////////////////////////////// Audio operations
#pragma region "Audio operations"

void HGL_Impl::_audio_Suspend() {
	// Suspend all playing audio, keep track of which you suspended by channels.
	assert(spSuspendedAudioSources->empty() && "Found that suspended sources list is not empty upon trying to do a suspend");
	spSuspendedAudioSources->clear();
	int state = 0;
	if(spSoundEffectsList && spSoundEffectsList->size() > 0) {
		for(auto se : *spSoundEffectsList.get()) {
			for(unsigned i = 0; i < 4; ++i) {
				if(se->alSources[i] && alIsSource(se->alSources[i])) {
					//alDeleteSources(1, &se->alSources[i]);
					alGetSourcei(se->alSources[i], AL_SOURCE_STATE, &state);
					if(state == AL_PLAYING) {
						spSuspendedAudioSources->push_back(se->alSources[i]);
					}
				}
			}
		}
		// Pause all playing sources
		alSourcePausev(static_cast<ALsizei>(spSuspendedAudioSources->size()), spSuspendedAudioSources->data());
		System_Log("  Audio: Suspended %d channels.", spSuspendedAudioSources->size());
	}
}

void HGL_Impl::_audio_Resume() {
	// Resume all suspended channels
	if(!spSuspendedAudioSources->empty()) {
		// Resume all playing sources
		alSourcePlayv(static_cast<ALsizei>(spSuspendedAudioSources->size()), spSuspendedAudioSources->data());
		System_Log("  Audio: Resumed %d channels.", spSuspendedAudioSources->size());
		spSuspendedAudioSources->clear();
	}
}

void HGL_Impl::_audio_Stop() {
	// Stops all playing audio using channels
	spSuspendedAudioSources->clear();
	if(spSoundEffectsList && spSoundEffectsList->size() > 0) {
		std::vector<ALuint> all_sources;
		int state = 0;
		for (auto se : *spSoundEffectsList.get()) {
			for (unsigned i = 0; i < 4; ++i) {
				alGetSourcei(se->alSources[i], AL_SOURCE_STATE, &state);
				if (state != AL_STOPPED) {
					all_sources.push_back(se->alSources[i]);
				}
			}
		}
		if (!all_sources.empty()) {
			alSourceStopv(static_cast<ALsizei>(all_sources.size()), all_sources.data());
			System_Log("  Audio: Stopped %d channels.", all_sources.size());
		}
	}
}

#pragma endregion
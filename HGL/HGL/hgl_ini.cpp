// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// https://github.com/kvakvs/hge 
#include "precomp.h"
#include "hgl_impl.h"
#ifdef HGLPLATFORM_WIN32
#include "hgl_system_win32.h"
#endif

// Implementation Definitions

//////////////////////////////////////////////////////// INI parsing
// RULES:
// - Comments can be ; or #
// - No nested sections
// - INI in its entirty is stored as a map of sections/lines/keys and values.
// - TODO: Every key in map can have multiple values per section, normal set/get only manipulates the first value.
// - TODO: Add multi-value functionality in the form of arrays. So you can read/write arrays or members.
// - WriterThread writes from a copied buffer (non-blocking).
// - Reading first time is a blocking IO operation as it must happen immediately. (might find a way to optimize
//   this when working on resource pipeline, possibly write all configs to one file and load all when app starts)
// - Due to simplified parsing method, first character of every line MUST BE: a-zA-z or [ or ; or #
// - INI config strictly assumes that no line will be removed, new key=value pairs can be added though
// - If a get function is called on a section/name that don't exist, new will be added with default value

using namespace std;

void HGL_Impl::_malformedIniFile(unsigned line, const char *file, const char *desc) {
	ostringstream ss;
	ss << "[ERROR] Malformed INI file \"" << file << "\" line " << line << endl;
	if(desc) {
		ss << "        " << desc;
	}
	strErrorMessage = ss.str();
	System_Log("%s", strErrorMessage.c_str());
	assert(false && "Warning failure: fix your INI file");
}

bool HGL_Impl::_initIniFile(const char *filename) {
	if(!filename || strlen(filename) == 0) {
		return false;
	}
	// checks if file is there first, if not then creates a new ini.
	bool result = false;
	ifstream ifs(filename, ios_base::in);
	if(ifs.good() && ifs.is_open()) {
		// File exists, parse
		result = _parseIniFile(filename, ifs);
		assert(result && "Failed to parse ini file");
		ifs.close();
	} else {
		// File doesn't exist, create an empty file
		ofstream ofs(filename, ios_base::out);
		assert(ofs.good() && "Ini file doesn't exist (in release this would create a new empty file)");
		ofs.close();
	}
	return true;
}

bool HGL_Impl::_parseIniFile(const char *filename, ifstream& in_stream) {
	// Read whole file to data string
	stringstream ss;
	ss << in_stream.rdbuf();
	string data = ss.str();
	string line = "";
	unsigned lncount = 0;

	if (data.empty()) {
		ss.clear();
		ss << "Failed to parse Ini file, it is empty: " << filename;
		_malformedIniFile(0, filename, ss.str().c_str());
		return false;
	}

	pIniFile = make_shared<HGL_IniFile>();
	pIniFile->sections.insert(pair<string, HGL_IniSection>("", HGL_IniSection())); // Empty section ("")
	HGL_IniSection *current_section = &pIniFile->sections[""];

	//System_Log("### INI Parsing Begins");

	while(!ss.eof()) {
		getline(ss, line);
		lncount += 1;
		if(line.length() >= 2) {
			if(line[0] == ';' || line[0] == '#') {
				// Comment
				current_section->lines.push_back(HGL_IniLine(ILT_COMMENT, line));
			} else if(line[0] == '[') {
				// New section
				size_t end_of_name = line.find_first_of(']', 1);
				if(end_of_name - 1 < 1 || end_of_name == string::npos) {
					_malformedIniFile(lncount, filename, "invalid section name");
				} else {
					string section_name = line.substr(1, end_of_name - 1);
					pIniFile->sections.insert(pair<string, HGL_IniSection>(section_name, HGL_IniSection()));
					current_section = &pIniFile->sections[section_name];
					//System_Log("### INI Section: %s", section_name.c_str());
				}
			} else if((line[0] >= 'A' && line[0] <= 'Z') || (line[0] >= 'a' && line[0] <= 'z')) {
				// Key=Value: validate
				// Rules: It must start with char (already done)
				// It must have an equal char
				// following '=' there must be at least 1 none space character
				size_t eq_offset = line.find_first_of('=', 0);
				if(eq_offset == string::npos) {
					_malformedIniFile(lncount, filename, "invalid key value pair");
				} else {
					string key = hglUtils::trim(line.substr(0, eq_offset));
					string value = hglUtils::trim(line.substr(eq_offset + 1));
					if(value.empty()) {
						_malformedIniFile(lncount, filename, "empty value for key");
					} else {
						//System_Log("### INI KeyValue: %s = %s", key.c_str(), value.c_str());
						current_section->lines.push_back(HGL_IniLine(ILT_KEYVALUE, value));
						current_section->keyLines.insert(
							pair<string, size_t>(key, current_section->lines.size() - 1));
					}
				}
			} else {
				// Wtf is this?
				_malformedIniFile(lncount, filename, "incorrect line (doesn't start with ; # [ or a letter)");
			}
		} else {
			// empty line
			current_section->lines.push_back(HGL_IniLine());
		}
	}
	return true;
}

// This checks Ini initialized or not
// validates given key name
// converts null section to ""
// validates section name
// returns false on failure
inline bool HGL_Impl::_validateKey(const char **section, const char **name) {
	assert(pIniFile && "Invalid Ini File");
	if(!pIniFile) {
		System_Log("[WARNING] uninitialized ini file used: section=%s, key=%s", *section, *name);
		return false;
	}
	// Validate given key name
	if(!*name || strlen(*name) == 0) {
		System_Log("[WARNING] Ini_Get given invalid key name: section=%s, key=%s", *section, *name);
		ostringstream ss;
		ss << "[WARNING] Ini_Get given invalid key name: section=" << *section;
		ss << ", key=" << *name;
		strErrorMessage = ss.str();
		System_Log("%s", strErrorMessage.c_str());
		assert(false && "Warning failure: given key parameter is invalid");
		return false;
	}
	// This is set when section is null too
	if(!*section) {
		*section = "";
	}
	// Validate given section name
	auto len_section = strlen(*section);
	if(len_section > 0) {
		if(!isalpha(*section[0])) {
			strErrorMessage = "incorrect section name argument";
			System_Log("%s", strErrorMessage.c_str());
			assert(false && "Warning failure: given section parameter is invalid");
			return false;
		}
	}
	return true;
}



// Search for section, returns it if found or create new one and return it otherwise.
inline HGL_TIniSections::iterator HGL_Impl::_getOrCreateSection(const char *section) {
	auto ifsection = pIniFile->sections.find(section);
	if(ifsection == pIniFile->sections.end()) {
		// Create new section
		pIniFile->sections.insert(pair<string, HGL_IniSection>(section, HGL_IniSection()));
		ifsection = pIniFile->sections.find(section);
	}
	return ifsection;
}

// Search for key in section, returns it if found or creates a new one and return it otherwise
template<typename T>
inline HGL_TIniKeyLines::iterator HGL_Impl::_getOrCreateKey(HGL_TIniSections::iterator& ifsection,
	const char *name, T def_val, bool &added_new_key) {
	auto ifkey = ifsection->second.keyLines.find(name);
	if(ifkey == ifsection->second.keyLines.end()) {
		// Create new key
		ifsection->second.lines.push_back(HGL_IniLine(ILT_KEYVALUE, to_string(def_val)));
		ifsection->second.keyLines.insert(pair<string, size_t>(name, ifsection->second.lines.size() - 1));
		ifkey = ifsection->second.keyLines.find(name);
		added_new_key = true;
	} else {
		added_new_key = false;
	}
	return ifkey;
}
// Specialization for std::string (can't use to_string function)
template<>
inline HGL_TIniKeyLines::iterator HGL_Impl::_getOrCreateKey<string>(HGL_TIniSections::iterator& ifsection,
	const char *name, string def_val, bool &added_new_key) {
	auto ifkey = ifsection->second.keyLines.find(name);
	if(ifkey == ifsection->second.keyLines.end()) {
		// Create new key
		ifsection->second.lines.push_back(HGL_IniLine(ILT_KEYVALUE, def_val));
		ifsection->second.keyLines.insert(pair<string, size_t>(name, ifsection->second.lines.size() - 1));
		ifkey = ifsection->second.keyLines.find(name);
		added_new_key = true;
	} else {
		added_new_key = false;
	}
	return ifkey;
}

//////////////////////////////////////////////////////////////////// Ini_Methods

void HGL_Impl::Ini_SetInt(const char *section, const char *name, int value) {
	bool valid = _validateKey(&section, &name);
	if(!valid) {
		return;
	}
	// Section
	auto ifsection = _getOrCreateSection(section);
	// Key
	bool added;
	auto ifkey = _getOrCreateKey<int>(ifsection, name, value, added);
	ifsection->second.lines[ifkey->second].data = to_string(value);
}

int	HGL_Impl::Ini_GetInt(const char *section, const char *name, int def_val) {
	bool valid = _validateKey(&section, &name);
	if(!valid) {
		return def_val;
	}
	// Section
	int out_val;
	auto ifsection = _getOrCreateSection(section);
	// Key
	bool added_new_key = false;
	auto ifkey = _getOrCreateKey<int>(ifsection, name, def_val, added_new_key);
	if(added_new_key) {
		out_val = def_val; 
	} else {
		out_val = stoi(ifsection->second.lines[ifkey->second].data);
	}
	return out_val;
}

void HGL_Impl::Ini_SetFloat(const char *section, const char *name, float value) {
	bool valid = _validateKey(&section, &name);
	if(!valid) {
		return;
	}
	// Section
	auto ifsection = _getOrCreateSection(section);
	// Key
	bool added;
	auto ifkey = _getOrCreateKey<float>(ifsection, name, value, added);
	ifsection->second.lines[ifkey->second].data = to_string(value);
}

float HGL_Impl::Ini_GetFloat(const char *section, const char *name, float def_val) {
	bool valid = _validateKey(&section, &name);
	if(!valid) {
		return def_val;
	}
	// Section
	float out_val;
	auto ifsection = _getOrCreateSection(section);
	// Key
	bool added_new_key = false;
	auto ifkey = _getOrCreateKey<float>(ifsection, name, def_val, added_new_key);
	if(added_new_key) {
		out_val = def_val;
	} else {
		out_val = stof(ifsection->second.lines[ifkey->second].data);
	}
	return out_val;
}

void HGL_Impl::Ini_SetString(const char *section, const char *name, const char *value) {
	bool valid = _validateKey(&section, &name);
	if(!valid) {
		return;
	}
	// Section
	auto ifsection = _getOrCreateSection(section);
	// Key
	bool added;
	auto ifkey = _getOrCreateKey<string>(ifsection, name, value, added);
	ifsection->second.lines[ifkey->second].data = value;
}

const char* HGL_Impl::Ini_GetString(const char *section, const char *name, const char *def_val) {
	bool valid = _validateKey(&section, &name);
	if(!valid) {
		return def_val;
	}
	// Section
	const char *out_val;
	auto ifsection = _getOrCreateSection(section);
	// Key
	bool added_new_key = false;
	auto ifkey = _getOrCreateKey<string>(ifsection, name, def_val, added_new_key);
	if(added_new_key) {
		out_val = def_val;
	} else {
		out_val = ifsection->second.lines[ifkey->second].data.c_str();
	}
	return out_val;
}


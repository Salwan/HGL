// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// https://github.com/kvakvs/hge 
#pragma once

#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <exception>
#include <memory>
#include <algorithm>
#include <vector>
#include <list>
#include <map>
#include <deque>
#include <algorithm>
#include <thread>
#include <mutex>
#include <chrono>
#include <atomic>
#include <cstdarg>
#include <cassert>
#include <cctype>

#define _USE_MATH_DEFINES
#include <math.h>

// EGL and OpenGL ES 2 via PowerVR SDK on desktop
#define GL_GLEXT_PROTOTYPES
#include <EGL/egl.h>
#include <GLES2/gl2.h>

// OpenAL Soft (a port of OpenAL)
#include <AL/al.h>
#include <AL/alc.h>
#include <AL/alext.h>

// A sound decoding library
#include <libsndfile/sndfile.h>

// Simple OpenGL Image Library
#include <SOIL.h>

// Cross-platform input system
#include <gainput/gainput.h>

// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// https://github.com/kvakvs/hge 
#include "precomp.h"
#include "hgl_impl.h"
#include "hgl_system_win32.h"
#include <shlobj.h>
#include <locale.h>
#include <strsafe.h>

// Define statics and defaults
HGLNativeWindowHandle HGLPlatform::hWnd = 0;
HGLNativeDisplayContext HGLPlatform::hDC = 0;
const wchar_t* HGLPlatform::DefaultClassName = L"HGLApp";
HGL_Impl* HGLPlatform::hgl = nullptr;
bool HGLPlatform::bLaptopMode = false;
HANDLE HGLPlatform::hSearch = 0;
WIN32_FIND_DATAA HGLPlatform::w32SearchData;

/////////////////////////////////////////////////////// Platform
#pragma region "Platform"
bool HGLPlatform::InitPlatform(HGL_Impl *_hgl) {
	assert(!HGLPlatform::hgl && _hgl);
	if (HGLPlatform::hgl) {
		throw new std::runtime_error("InitPlatform called more than once");
	}
	if (!_hgl) {
		throw new std::runtime_error("InitPlatform passed invalid HGL object");
	}
	HGLPlatform::hgl = _hgl;
	return true;
}

void HGLPlatform::DestroyPlatform() {
	if(hSearch) {
		FindClose(hSearch);
		hSearch = 0;
	}
}
#pragma endregion

/////////////////////////////////////////////////////// Threading
void HGLPlatform::SetThreadAffinity(std::thread& thread, int64_t mask) {
	HANDLE hthread = thread.native_handle();
	SetThreadAffinityMask(hthread, static_cast<DWORD_PTR>(mask));
}

//////////////////////////////////////////////////////// FileSystem
std::string HGLPlatform::_getAppPath() {
	HMODULE hm = GetModuleHandleA(NULL);
	char path[MAX_RESOURCE_PATH];
	GetModuleFileNameA(hm, path, MAX_RESOURCE_PATH);
	if(GetLastError() == ERROR_INSUFFICIENT_BUFFER) {
		assert(false && "Not enough buffer size for application path");
		hgl->_postError("App path is too long, failed to retrieve path");
		ShowErrorMessage(hgl->strErrorMessage.c_str());
		throw std::runtime_error(hgl->strErrorMessage);
	}
	// Remove: "app.exe" from returned path
	std::string strpath{path};
	size_t pidx = strpath.find_last_of('\\');
	if(pidx != std::string::npos && pidx > 2) {
		strpath = strpath.substr(0, pidx + 1);
	}
	return strpath;
}

void HGLPlatform::_processPath(std::string& path) {
	for(auto i = 0; i < path.length(); ++i) {
		if(path[i] == '/') {
			path[i] = '\\';
		}
	}
	// Make path is being used for files as well in HGL :(
	// TODO: resolve this, there are contradictions in HGL resource system I can do this much
	//       cleaner: enumerate all files/folders using one call, allow user to specify in more
	//       detail what they want, etc.
	/*if(path[path.length() - 1] != '\\' && path[path.length() - 1] != '*') {
		path = path + "\\";
	}*/
}

std::string HGLPlatform::_getEnvironmentVariable(const char* name, size_t max_length) {
	assert(name && strlen(name) > 0);
	std::string out = "";
	if(name && strlen(name) > 0) {
		char *buffer = new char[max_length];
		auto result = GetEnvironmentVariableA(name, buffer, MAX_ENVVAR_DATA);
		if(result) {
			out = buffer;
			if(out.back() != '\\') {
				out += "\\";
			}
		} else {
			assert(false && "Failed to retrieve environment variable");
			hgl->System_Log("ERROR: Failed to retrieve environment variable: %s", name);
			hgl->strErrorMessage = "ERROR: Failed to retrieve environment variable";
		}
		if(buffer) {
			delete[] buffer;
		}
	}
	return out;
}

std::string HGLPlatform::_getSpecialSystemPath(HGL_SpecialSystemPath ssp) {
	std::string strout = ".\\";
	// No need to use %..% for env vars
	switch(ssp) {
		case HGL_SpecialSystemPath::SSP_TEMP:
			strout = _getEnvironmentVariable("TEMP");
			break;

		case HGL_SpecialSystemPath::SSP_SETTINGS:
			strout = _getEnvironmentVariable("APPDATA") + hgl->strTitle + "\\";
			break;

		case HGL_SpecialSystemPath::SSP_SAVEDATA:
			strout = _getEnvironmentVariable("USERPROFILE") + "Saved Games\\";
			break;

		case HGL_SpecialSystemPath::SSP_HOME:
		default:
			strout = _getEnvironmentVariable("USERPROFILE");
	}
	return strout;
}

void _enumError(LPTSTR lpszFunction)
{
	// Retrieve the system error message for the last-error code

	LPVOID lpMsgBuf;
	LPVOID lpDisplayBuf;
	DWORD dw = GetLastError();

	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		dw,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&lpMsgBuf,
		0, NULL);

	// Display the error message and clean up

	lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT,
		(lstrlen((LPCTSTR)lpMsgBuf) + lstrlen((LPCTSTR)lpszFunction) + 40)*sizeof(TCHAR));
	StringCchPrintf((LPTSTR)lpDisplayBuf,
		LocalSize(lpDisplayBuf) / sizeof(TCHAR),
		TEXT("%s failed with error %d: %s"),
		lpszFunction, dw, lpMsgBuf);
	MessageBox(NULL, (LPCTSTR)lpDisplayBuf, TEXT("Error"), MB_OK);

	LocalFree(lpMsgBuf);
	LocalFree(lpDisplayBuf);
}

const char* HGLPlatform::_enumNextFile(const char *wildcard) {
	if(wildcard) {
		if(hSearch) {
			FindClose(hSearch);
			hSearch = 0;
		}
		const char *path = hgl->Resource_MakePath(wildcard);
		hSearch = FindFirstFileA(path, &w32SearchData);
		if(hSearch == INVALID_HANDLE_VALUE) {
			hSearch = 0;
			return 0;
		}

		if(!(w32SearchData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {
			return w32SearchData.cFileName;
		} else {
			return hgl->Resource_EnumFiles();
		}
	} else {
		if(!hSearch) {
			return 0;
		}
		for(;;) {
			if(!FindNextFileA(hSearch, &w32SearchData)) {
				FindClose(hSearch);
				hSearch = 0;
				return 0;
			}
			if(!(w32SearchData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {
				return w32SearchData.cFileName;
			}
		}
	}
}

const char* HGLPlatform::_enumNextFolder(const char *wildcard) {
	if(wildcard) {
		if(hSearch) {
			FindClose(hSearch);
			hSearch = 0;
		}
		const char *path = hgl->Resource_MakePath(wildcard);
		hSearch = FindFirstFileA(path, &w32SearchData);
		if(hSearch == INVALID_HANDLE_VALUE) {
			_enumError(L"FindFirstFile");
			hSearch = 0;
			return 0;
		}

		if((w32SearchData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) &&
			strcmp(w32SearchData.cFileName, ".") && strcmp(w32SearchData.cFileName, "..")) {
			return w32SearchData.cFileName;
		} else {
			return hgl->Resource_EnumFolders();
		}
	} else {
		if(!hSearch) {
			return 0;
		}
		for(;;) {
			if(!FindNextFileA(hSearch, &w32SearchData)) {
				FindClose(hSearch);
				hSearch = 0;
				return 0;
			}
			if((w32SearchData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) &&
				strcmp(w32SearchData.cFileName, ".") && strcmp(w32SearchData.cFileName, "..")) {
				return w32SearchData.cFileName;
			}
		}
	}
}

//////////////////////////////////////////////////////// Mouse Cursor
void HGLPlatform::SetMouseCursorVisible(bool is_visible) {
	ShowCursor(is_visible);
}

void HGLPlatform::SetMousePosition(int x, int y) {
	SetCursorPos(x, y);
}

bool HGLPlatform::IsMouseInWindow() {
	POINT pt;
	GetCursorPos(&pt);
	if (ScreenToClient(hWnd, &pt)) {
		if(pt.x < 0 || pt.y < 0 || pt.x > hgl->iWidth || pt.y > hgl->iHeight) {
			return false;
		} else {
			return true;
		}
	}
	return false;
}

/////////////////////////////////////////////////////// Display
bool HGLPlatform::EnumDisplayModes() {
	assert(hgl->vDisplayModes.empty() && "EnumDisplayModes must only be called once!");
	if (!hgl->vDisplayModes.empty()) {
		hgl->_postError("EnumDisplayModes must only be called once");
		return false;
	}
	hgl->System_Log("Enumerating Display Modes..");
	DEVMODE devmode;
	devmode.dmSize = sizeof(DEVMODE);
	devmode.dmDriverExtra = 0;
	BOOL result;
	int i_mode = 0;
	unsigned mode_checksum = 0; // This is used to remove identical subsequent display modes
								// Same display mode has different: devmode.dmDefaultSource (0, 1, 2) and dmDisplayFixedOutput (0, 1, 2)
								// dmDisplayFixedOutput specifies scaling mode for smaller than native modes:
								// DMDFO_DEFAULT = 0 = whatever is default (HGL default)
								// DMDFO_STRETCH = 1 = low res stretches to native
								// DMDFO_CENTER = 2 = low res centred in native

								// System Display Mode (current for restoration)
	result = EnumDisplaySettings(NULL, ENUM_CURRENT_SETTINGS, &devmode);
	assert(result && "Failed to enumerate current display settings");
	if (!result) {
		hgl->_postError("Error: EnumDisplayModes() failed to get current display settings.");
		throw new std::runtime_error("Error: EnumDisplayModes() failed to get current display settings.");
	}
	{
		hgl->systemDisplayMode.width = devmode.dmPelsWidth;
		hgl->systemDisplayMode.height = devmode.dmPelsHeight;
		hgl->systemDisplayMode.refreshRate = devmode.dmDisplayFrequency;
		hgl->systemDisplayMode.aspectRatio = hglUtils::calcAspectRatio(devmode.dmPelsWidth, devmode.dmPelsHeight);
		DEVMODE *devmode_save = new DEVMODE;
		memcpy(devmode_save, &devmode, sizeof(DEVMODE));
		hgl->systemDisplayMode.platformMode = static_cast<void*>(devmode_save);
		hgl->System_Log("   System Display Mode: %dx%d (%d:%d) %d @ %d Hz",
			devmode.dmPelsWidth, devmode.dmPelsHeight,
			hgl->systemDisplayMode.aspectRatio.first, hgl->systemDisplayMode.aspectRatio.second,
			devmode.dmBitsPerPel, hgl->systemDisplayMode.refreshRate);
	}

	// Enum all available display modes
	result = EnumDisplaySettings(NULL, i_mode, &devmode);
	// TODO: Add support for dmDisplayFixedOutput 1 and 2 (forces CENTER or STRETCH) this is a good
	// decision to make for a game if you want auto-letterboxing go with CENTER: DMDFO_CENTER/STRETCH
	while (result) {
		if (devmode.dmBitsPerPel == 32 && devmode.dmDisplayFrequency > 30 &&
			devmode.dmDisplayFixedOutput == DMDFO_DEFAULT &&
			devmode.dmPelsWidth + devmode.dmPelsHeight + devmode.dmDisplayFrequency != mode_checksum) {
			// Copy devmode to heap allocated
			DEVMODE *devmode_save = new DEVMODE;
			memcpy(devmode_save, &devmode, sizeof(DEVMODE));
			// Create display mode
			HGL_DisplayMode *hdm = new HGL_DisplayMode();
			hdm->width = devmode.dmPelsWidth;
			hdm->height = devmode.dmPelsHeight;
			hdm->refreshRate = devmode.dmDisplayFrequency;
			hdm->platformMode = static_cast<void*>(devmode_save);
			hdm->aspectRatio = hglUtils::calcAspectRatio(hdm->width, hdm->height);
			// Log
			hgl->System_Log("   Display Mode %d: %dx%d (%d:%d) 32-bit @ %d Hz Source %d", i_mode,
				devmode.dmPelsWidth, devmode.dmPelsHeight, hdm->aspectRatio.first,
				hdm->aspectRatio.second, devmode.dmDisplayFrequency, devmode.dmDefaultSource);
			// Add it to list
			hgl->vDisplayModes.push_back(hdm);
			mode_checksum = hdm->width + hdm->height + hdm->refreshRate;
		}
		i_mode++;
		result = EnumDisplaySettings(NULL, i_mode, &devmode);
	}
	hgl->System_Log("Enumerated %d supported display modes.", hgl->vDisplayModes.size());
	return !hgl->vDisplayModes.empty();
}

bool HGLPlatform::GoFullscreen(HGL_DisplayMode *display_mode) {
	assert(display_mode && display_mode->platformMode);
	// has window been created already?
	// - Yes: resize and reposition to new screen
	// - No: do nothing, it'll be handled on window creation
	if (hgl->bWindowCreated) {
		// Store actual window position for use in switching back to windowed
		if (!hgl->bDisplayModeChanged) {
			RECT win_rect;
			GetWindowRect(HGLPlatform::hWnd, &win_rect);
			hgl->iWindowedPosX = win_rect.left;
			hgl->iWindowedPosY = win_rect.top;
		}
		// Change display mode if required (I have to pass CDS_FULLSCREEN for windowed fullscreen
		// even if mode matches?)
		if (display_mode->width != hgl->systemDisplayMode.width ||
			display_mode->height != hgl->systemDisplayMode.height) {
			DEVMODE *devmode = static_cast<DEVMODE*>(display_mode->platformMode);
			// Test first to find out if this change will succeed
			LONG lresult = ChangeDisplaySettings(devmode, CDS_TEST);
			if (lresult == DISP_CHANGE_SUCCESSFUL) {
				lresult = ChangeDisplaySettingsEx(NULL, devmode, NULL, CDS_FULLSCREEN, NULL);
				assert(lresult == DISP_CHANGE_SUCCESSFUL);
				if (lresult == DISP_CHANGE_SUCCESSFUL) {
					hgl->bDisplayModeChanged = true;
				} else {
					hgl->_postError("Error: failed to change display mode. Windowed mode maintained.");
				}
			}
		}
		// -- Set window style			
		SetWindowLongPtr(HGLPlatform::hWnd, GWL_EXSTYLE, HGLWINDOWS_FULLSCREEN_EXSTYLE);
		SetWindowLongPtr(HGLPlatform::hWnd, GWL_STYLE, HGLWINDOWS_FULLSCREEN_STYLE);
		// -- Reset window pos and set to topmost
		SetWindowPos(HGLPlatform::hWnd, HWND_TOPMOST, 0, 0, hgl->iWidth, hgl->iHeight,
			SWP_FRAMECHANGED | SWP_SHOWWINDOW);

		SetForegroundWindow(HGLPlatform::hWnd);
		SetFocus(HGLPlatform::hWnd);

		// -- Fire resize event
		hgl->_handleResize(hgl->iWidth, hgl->iHeight);
	}
	return true;
}

bool HGLPlatform::GoWindowed() {
	if (hgl->bWindowCreated) {
		// -- Set window style
		SetWindowLongPtr(HGLPlatform::hWnd, GWL_EXSTYLE, HGLWINDOWS_WINDOWED_EXSTYLE);
		SetWindowLongPtr(HGLPlatform::hWnd, GWL_STYLE, HGLWINDOWS_WINDOWED_STYLE);
		// -- Reset window pos
		RECT rect;
		rect.left = hgl->iWindowedPosX;
		rect.top = hgl->iWindowedPosY;
		rect.right = rect.left + hgl->iConfigWidth;
		rect.bottom = rect.top + hgl->iConfigHeight;
		AdjustWindowRectEx(&rect, HGLWINDOWS_WINDOWED_STYLE, false, HGLWINDOWS_WINDOWED_EXSTYLE);
		SetWindowPos(HGLPlatform::hWnd, HWND_NOTOPMOST, rect.left, rect.top + GetSystemMetrics(SM_CYSIZE),
			rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW);
		// Change display mode if required
		if (hgl->bDisplayModeChanged) {
			LONG lresult = ChangeDisplaySettingsEx(NULL, NULL, NULL, CDS_FULLSCREEN, NULL);
			assert(lresult == DISP_CHANGE_SUCCESSFUL); // I mean, I'm trying to switch back why would an error occur?
			if (lresult != DISP_CHANGE_SUCCESSFUL) {
				hgl->strErrorMessage = "Failed to change back to windowed mode";
			}
		}
		// -- Fire resize event
		hgl->System_Log("Going Windowed with resolution: %dx%d", hgl->iConfigWidth, hgl->iConfigHeight);
		hgl->_handleResize(hgl->iConfigWidth, hgl->iConfigHeight);
		//
		hgl->bDisplayModeChanged = false;
	}
	return true;
}

void HGLPlatform::SetWindowTitle(const std::string& title) {
	SetWindowTextA(hWnd, title.c_str());
}

/////////////////////////////////////////////////////// Logging
void HGLPlatform::InitStdLog() {
#ifdef _MSC_VER
	static OutputDebugStringBuf<char> charDebugOutput;
	std::cerr.rdbuf(&charDebugOutput);
	std::clog.rdbuf(&charDebugOutput);
	static OutputDebugStringBuf<wchar_t> wcharDebugOutput;
	std::wcerr.rdbuf(&wcharDebugOutput);
	std::wclog.rdbuf(&wcharDebugOutput);
#endif
}

void HGLPlatform::LogSystemInfo(HGL* _hgl, const char *title) {
	OSVERSIONINFO os_ver;
	SYSTEMTIME sys_time;
	MEMORYSTATUS mem_status;

	GetLocalTime(&sys_time);
	_hgl->System_Log("Date: %02d.%02d.%d, %02d:%02d:%02d\n", sys_time.wDay, sys_time.wMonth,
		sys_time.wYear, sys_time.wHour, sys_time.wMinute, sys_time.wSecond);
	_hgl->System_Log("Application: %s", title);
	os_ver.dwOSVersionInfoSize = sizeof(os_ver);
	GetVersionEx(&os_ver);
	_hgl->System_Log("OS: Windows %ld.%ld.%ld", os_ver.dwMajorVersion, os_ver.dwMinorVersion,
		os_ver.dwBuildNumber);
	GlobalMemoryStatus(&mem_status);
	_hgl->System_Log("Memory: %ldM total, %ldM free\n", mem_status.dwTotalPhys / 1024L / 1024L,
		mem_status.dwAvailPhys / 1024L / 1024L);
}

////////////////////////////////////////////////////// Window Procedure Handler
LRESULT CALLBACK HGLPlatform::HGLWndProc(HWND hwnd, unsigned int msg, WPARAM wParam, LPARAM lParam) {
	// TODO: Handle all wnd procedures required
	switch (msg) {
	case WM_CREATE:
		return FALSE;

	case WM_PAINT:
	{
		//if(hgl->bAppSuspended && hgl->bSystemInitiated && hgl->bWindowCreated && hgl->cbRenderFunc) {
		//	hgl->cbRenderFunc();
		//}
		_drawSuspensionRect();
	}
	break;

	case WM_SYSCOMMAND:
	{
		switch (wParam) {
		case SC_SCREENSAVE:
		case SC_MONITORPOWER:
			return 0;

		case SC_CLOSE:
			HGLPlatform::hgl->bAppQuit = true;
			break;
		}
		break;
	}

	case WM_CLOSE:
		DestroyWindow(hwnd);
		PostQuitMessage(0);
		HGLPlatform::hgl->bAppQuit = true;
		return 0;

	case WM_DESTROY:
		PostQuitMessage(0);
		HGLPlatform::hgl->bAppQuit = true;
		return 0;

	case WM_SIZE:
	{
		// wParam relevant events:
		// SIZE_MAXIMIZED: window has been maxed (resize)
		// SIZE_MINIMIZED: window has been minimized (lost focus?)
		// SIZE_RESTORED: window resize without minimized or maximized (resize)
		// iParam: LOWORD = new width, HIWORD = new height
		auto nw = LOWORD(lParam);
		auto nh = HIWORD(lParam);
		if (wParam == SIZE_MINIMIZED) {
			// TODO: Window minimized. Lost Focus perhaps?
		} else if (hgl->bSystemInitiated) {
			if (nw != HGLPlatform::hgl->iWidth || nh != HGLPlatform::hgl->iHeight) {
				HGLPlatform::hgl->_handleResize(nw, nh);
			}
			// Dispatch event: Resize
			if (HGLPlatform::hgl->cbResizeFunc) {
				HGLPlatform::hgl->cbResizeFunc();
			}
		}
	} break;

	case WM_ACTIVATE:
	{
		bool activating = (LOWORD(wParam) != WA_INACTIVE) && (HIWORD(wParam) == 0);
		if (hgl->bSystemInitiated && hgl->bWindowCreated && hgl->bAppSuspended == activating) {
			hgl->_handleFocusChange(activating);
		}
	}
	break;

	case WM_MOVING:
	case WM_SIZING:
		// Window is being moved around
		if (hgl->bSystemInitiated && hgl->bWindowCreated && !hgl->bAppSuspended) {
			hgl->_focusLost();
			InvalidateRect(hWnd, NULL, true); // this will trigger a paint to draw disabled rect
		}
		break;

	case WM_EXITSIZEMOVE:
		// Window movement is finished
		if (hgl->bSystemInitiated && hgl->bWindowCreated && hgl->bAppSuspended) {
			hgl->_focusGain();
		}
		break;

		
	case WM_MOUSEWHEEL:/*{
			MSG m;
			m.hwnd = hWnd;
			m.lParam = lParam;
			m.wParam = wParam;
			m.message = msg;
			hgl->spInputManager->HandleMessage(m);
			break;
		}*/
		{
			int wheel = GET_WHEEL_DELTA_WPARAM(wParam);
			if (wheel > 0) {
				hgl->iMouseWheel = 1;
			} else if(wheel < 0) {
				hgl->iMouseWheel = -1;
			} else {
				hgl->iMouseWheel = 0;
			}
		}
		break;
	default:
		break;
	}
	return DefWindowProc(hwnd, msg, wParam, lParam);
}

void HGLPlatform::_drawSuspensionRect() {
	static HBRUSH br = CreateSolidBrush(RGB(32, 32, 32));
	RECT wr;
	GetWindowRect(hWnd, &wr);
	SelectObject(hDC, br);
	Rectangle(hDC, 0, 0, wr.right - wr.left, wr.bottom - wr.top);
}

/////////////////////////////////////////////////////// Window Init
bool HGLPlatform::InitWindow() {
	assert(HGLPlatform::hgl);
	if (!HGLPlatform::hgl) {
		throw new std::runtime_error("InitWindow() called before InitPlatform.");
	}
	HINSTANCE hInstance = GetModuleHandle(NULL);
	WNDCLASSEX wc;
	{
		wc.cbSize = sizeof(WNDCLASSEX);
		wc.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW;
		wc.lpfnWndProc = &DefWindowProc;
		wc.cbClsExtra = 0;
		wc.cbWndExtra = 0;
		wc.hInstance = hInstance;
		wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
		wc.hCursor = LoadCursor(NULL, IDC_ARROW);
		wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
		wc.lpszMenuName = NULL;
		wc.lpszClassName = DefaultClassName;
		wc.hIconSm = NULL;
		wc.lpfnWndProc = HGLPlatform::HGLWndProc;
	}
	// TODO: add support for customizable icon via icon state in HGL
	HRESULT hr = RegisterClassEx(&wc);
	if (FAILED(hr)) {
		std::clog << "Window class registeration failed" << std::endl;
		HGLPlatform::hgl->strErrorMessage = "Window class registration failed";
		return false;
	}
	RECT rect = { 0, 0, static_cast<long>(HGLPlatform::hgl->iWidth),
		static_cast<long>(HGLPlatform::hgl->iHeight) };
	AdjustWindowRect(&rect, HGLWINDOWS_WINDOWED_STYLE, false);
	std::wostringstream wtitle;
	wtitle << hgl->strTitle.c_str();
	HWND hwnd = CreateWindowEx(
		HGLWINDOWS_WINDOWED_EXSTYLE,			// Extended style (different for fullscreen)
		DefaultClassName,						// Class name
		wtitle.str().c_str(),					// Window title
		HGLWINDOWS_WINDOWED_STYLE,				// Window style (different for fullscreen)
		CW_USEDEFAULT, CW_USEDEFAULT,			// Window position
		rect.right - rect.left,					// Window width
		rect.bottom - rect.top,					// Window height
		NULL,									// Parent window
		NULL,									// Menu
		hInstance,								// Instance
		NULL									// pass null to WM_CREATE
	);
	if (!hwnd) {
		std::clog << "Window creation -> failure" << std::endl;
		hgl->strErrorMessage = "Window creation failed";
		return false;
	}
	ShowWindow(hwnd, SW_SHOW);
	HDC hdc = GetDC(hwnd);
	hWnd = hwnd;
	hDC = hdc;
	SetMouseCursorVisible(hgl->bMouseCursor);
	hgl->bWindowCreated = true;
	// Store actual window rect for use in display mode switching
	{
		RECT win_rect;
		GetWindowRect(hWnd, &win_rect);
		hgl->iWindowedPosX = win_rect.left;
		hgl->iWindowedPosY = win_rect.top;
		hgl->iConfigWidth = HGLPlatform::hgl->iWidth;
		hgl->iConfigHeight = HGLPlatform::hgl->iHeight;
	}
	// Check whether this device is battery run or not (for power saving when possible)
	SYSTEM_POWER_STATUS sps;
	BOOL result = GetSystemPowerStatus(&sps);
	assert(result && "Failed to GetSystemPowerStatus()");
	if (result) {
		HGLPlatform::bLaptopMode = sps.ACLineStatus > 0 && sps.BatteryFlag >= 128 ? false : true;
		if (HGLPlatform::bLaptopMode) {
			hgl->System_Log("Detected running on a laptop with battery, power saving set to ALWAYS ON");
		} else {
			hgl->System_Log("Detected running on a Desktop or no battery, power saving ALWAYS OFF");
		}
	} else {
		hgl->System_Log("Failed to retrieve system power status from Windows. power saving ALWAYS off.");
		HGLPlatform::bLaptopMode = false;
	}
	// Go fullscreen if required
	if (!hgl->bWindowed) {
		assert(hgl->_goFullscreen());
	}
	return true;
}

void HGLPlatform::ShutdownWindow() {
	// Reset display mode if necessary
	if (hgl->bDisplayModeChanged) {
		GoWindowed();
	}
	if (HGLPlatform::hDC) {
		ReleaseDC(HGLPlatform::hWnd, HGLPlatform::hDC);
	}
	if (HGLPlatform::hWnd) {
		DestroyWindow(HGLPlatform::hWnd);
	}
}

/////////////////////////////////////////////////////////// App Loop
bool HGLPlatform::AppLoop() {
	MSG msg;
	while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE) > 0) {
		TranslateMessage(&msg);
		DispatchMessage(&msg);
		hgl->spInputManager->HandleMessage(msg);
	}
	return true;
}

////////////////////////////////////////////////////////// Error Messages
int HGLPlatform::ShowErrorMessage(const char *message) {
	std::ostringstream ss;
	ss << hgl->strTitle << ": Error";
	return MessageBoxA(NULL, message, ss.str().c_str(), MB_ICONERROR | MB_OK);
}

int HGLPlatform::ShowWarningMessage(const char *message) {
	std::ostringstream ss;
	ss << hgl->strTitle << ": Warning";
	return MessageBoxA(NULL, message, ss.str().c_str(), MB_ICONEXCLAMATION | MB_OK);
}

int HGLPlatform::ShowInfoMessage(const char *message) {
	std::ostringstream ss;
	ss << hgl->strTitle << ": Information";
	return MessageBoxA(NULL, message, ss.str().c_str(), MB_ICONINFORMATION | MB_OK);
}

////////////////////////////////////////////////////////// System Power
bool HGLPlatform::isPowerSavingMode() {
	return HGLPlatform::bLaptopMode;
}
// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// https://github.com/kvakvs/hge 
#include "precomp.h"
#include "hgl_impl.h"

#ifdef HGLPLATFORM_WIN32
#include "hgl_system_win32.h"
#endif

///////////////////////////////////////////////////////// Thread writer -> Internal
// Implementation details
// - Main goal is to remove file access IO blocking from the engine thread
// - Runs at max 10 writes/sec
// - Writing files happens immediately when request is made
// - Writer pauses for a while between queues, but a single queue will be executed in full first
// - Files and data are pushed to a write queue
// - Writer thread will write size bytes to filename even if data is less (padding using spaces after \0)
// TODO: make sure HGL class never destructs until thread writer is done. Otherwise write_queue could crash.
// TODO: can writer thread somehow have a timeout on write operations? just to avoid lockup in case of disk failure

void HGL_Impl::_mainWriterThread(HGL_Impl* hgl_impl) {
	assert(hgl_impl);
	HGL_WriterThread &thread = hgl_impl->sWriterThread;
	HGL_TFileWriteQueue &wrque = hgl_impl->deqFileWrite;
	std::unique_lock<std::mutex> queue_lock(thread.queueMutex);
	while (!thread.bEndThread) {
		while(!thread.bFlush && !thread.bEndThread) {
			thread.cvarRun.wait(queue_lock);
		}
		thread.bFlush = false;
		// Exit if close requested
		if (thread.bEndThread) {
			break;
		}
		// Write all queue
		while (!wrque.empty()) {
			std::ofstream output;
			output.open(wrque.front().filename, std::ios::binary | std::ios::out | std::ios::trunc);
			output.write((const char*)wrque.front().data, wrque.front().size);
			wrque.pop_front();
		}
		// Exit if close requested
		if (thread.bEndThread) {
			break;
		}
		// Sleep for a while just to go easy on IO
		std::this_thread::sleep_for(std::chrono::milliseconds(100));
	}
}

bool HGL_Impl::_initWriterThread() {
	std::thread wt(HGL_Impl::_mainWriterThread, this);
	sWriterThread.thread.swap(wt);
	System_Log("Writer thread ready");
	return true;
}

void HGL_Impl::_writeFile(const char *filename, void *data, size_t size) {
	assert(filename && data && size && "What in the actual frakk?");
	if (!filename || !data || size == 0) {
		return;
	}
	std::unique_lock<std::mutex> write_queue_lock(sWriterThread.queueMutex);
	deqFileWrite.push_back(HGL_FileRecord(filename, data, size));
	sWriterThread.bFlush = true;
	sWriterThread.cvarRun.notify_one();
	write_queue_lock.unlock();
}

void HGL_Impl::_destroyWriterThread() {
	std::unique_lock<std::mutex> lock(sWriterThread.queueMutex);
	sWriterThread.bEndThread = true;
	sWriterThread.cvarRun.notify_one();
	lock.unlock();
	sWriterThread.thread.join();
	System_Log("Writer thread ended");
}
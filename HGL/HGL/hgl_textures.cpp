// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// https://github.com/kvakvs/hge 
#include "precomp.h"
#include "hgl_impl.h"

#ifdef HGLPLATFORM_WIN32
#include "hgl_system_win32.h"
#endif

//////////////////////////////////////////// Graphics -> Textures
#pragma region "Graphics -> Textures"

bool HGL_Impl::_initTextures() {
	// White texture is used for null textures for non-textured primitive renders
	hWhiteTexture = Texture_Create(2, 2);
	const unsigned wtdata[4] = { 0xffffffff, 0xffffffff, 0xffffffff, 0xffffffff };
	glBindTexture(GL_TEXTURE_2D, _getGLTexture(hWhiteTexture));
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 2, 2, 0, GL_RGBA, GL_UNSIGNED_BYTE, wtdata);
	glBindTexture(GL_TEXTURE_2D, 0);
	return true;
}

void HGL_Impl::_destroyTextures() {
	System_Log("Destroying loaded textures..");
	glBindTexture(GL_TEXTURE_2D, 0);
	if(spTexturesList && spTexturesList->size() > 0) {
		std::vector<GLuint> to_destroy;
		for(auto t : *spTexturesList.get()) {
			if(t->texture && glIsTexture(t->texture)) {
				to_destroy.push_back(t->texture);
			}
			glDeleteTextures(static_cast<GLsizei>(to_destroy.size()), &to_destroy[0]);
		}
		System_Log("  Deleted %d textures", to_destroy.size());
	}
}

GLuint HGL_Impl::_getGLTexture(HTEXTURE tex, bool null_is_white) {
	if (null_is_white && !tex) {
		tex = hWhiteTexture;
	}
	if (tex) {
		return static_cast<HGL_Texture*>(tex)->texture;
	}
	return 0;
}

HTEXTURE HGL_Impl::Texture_Create(int width, int height) {
	// TODO: implement support for padding to power of two, currently not supported.
	assert(width == height);

	// HGL assumes 32-bit RGBA always
	GLuint gl_texture_id;
	glGenTextures(1, &gl_texture_id);
	GLenum glerr = glGetError();
	if(glerr == GL_NO_ERROR) {
		//glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
		glBindTexture(GL_TEXTURE_2D, gl_texture_id);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, 
			GL_UNSIGNED_BYTE, NULL);
		HGL_Texture *htexture = new HGL_Texture(gl_texture_id, "", width, height, 4);
		spTexturesList->push_back(htexture);
		glBindTexture(GL_TEXTURE_2D, 0);
		return static_cast<HTEXTURE>(htexture);
	} else {
		std::ostringstream ss;
		ss << "ERROR: Texture_Create() failed due to a GL error: " << std::hex << glerr;
		System_Log(ss.str().c_str());
		strErrorMessage = ss.str();
		return nullptr;
	}
}

HTEXTURE HGL_Impl::Texture_Load(const char *filename, unsigned size, bool bMipmap) {
	// TODO: Test Texture_Load and all its features.
	assert(filename);
	// TODO: implement data loading, disabled for now
	assert(size == 0);

	unsigned t_flags = SOIL_FLAG_POWER_OF_TWO | SOIL_FLAG_TEXTURE_REPEATS;
	if(bMipmap) {
		t_flags |= SOIL_FLAG_MIPMAPS;
	}
	GLuint gl_texture_id = 0;
	unsigned tex_width = 0, tex_height = 0, tex_channels = 0;
	unsigned gl_width = 0, gl_height = 0; // actual GL texture size in memory
	if(size == 0) {
		gl_texture_id = SOIL_load_OGL_texture(filename, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID,
			t_flags, &tex_width, &tex_height, &tex_channels, &gl_width, &gl_height);
	} else {
		gl_texture_id = SOIL_load_OGL_texture_from_memory(reinterpret_cast<const unsigned char*>(filename), 
			size, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, t_flags, &tex_width, &tex_height, &tex_channels,
			&gl_width, &gl_height);
	}
	if(gl_texture_id > 0) {
		const char *source_name = size == 0? filename : "";
		HGL_Texture *texture = new HGL_Texture(gl_texture_id, source_name, tex_width, tex_height, 
			tex_channels, gl_width, gl_height);
		spTexturesList->push_back(texture);
		if(size == 0) {
			System_Log("[IO] Image file \"%s\" loaded: %dx%d %d-bit", filename, tex_width, 
				tex_height, tex_channels * 8);
		} else {
			System_Log("[IO] Image file loaded from memory: %dx%d %d-bit", tex_width, tex_height, 
				tex_channels * 8);
		}
		return static_cast<HTEXTURE>(texture);
	} else {
		std::ostringstream ss;
		if(size == 0) {
			ss << "ERROR: Texture_Load failed to load texture file \"" << filename << "\"" << std::endl;
			ss << "  SOIL message: " << SOIL_last_result();
			System_Log(ss.str().c_str());
			strErrorMessage = ss.str();
		} else {
			ss << "ERROR: Texture_Load failed to load texture from memory" << std::endl;
			ss << "  SOIL message: " << SOIL_last_result();
			System_Log(ss.str().c_str());
			strErrorMessage = ss.str();
		}
		assert(false && "Failed to load texture file");
		return nullptr;
	}
}

void HGL_Impl::Texture_Free(HTEXTURE tex) {
	if(tex) {
		auto iter = std::find(spTexturesList->begin(), spTexturesList->end(), tex);
		if(iter != spTexturesList->end()) {
			HGL_Texture* htexture = static_cast<HGL_Texture*>(tex);
			if(glIsTexture(htexture->texture)) {
				glDeleteTextures(1, &htexture->texture);
			}
			spTexturesList->erase(iter);
		}
	}
}

int	HGL_Impl::Texture_GetWidth(HTEXTURE tex, bool bOriginal) {
	if(tex) {
		// Disabled original/actual width height as they seem to break hglAnimation
		if(bOriginal) {
			return static_cast<HGL_Texture*>(tex)->uWidth;
		} else {
			return static_cast<HGL_Texture*>(tex)->uWidth;
		}
	} else { 
		return 0;
	}
}
int	HGL_Impl::Texture_GetHeight(HTEXTURE tex, bool bOriginal) {
	if(tex) {
		// Disabled original/actual width height as they seem to break hglAnimation
		if (bOriginal) {
			return static_cast<HGL_Texture*>(tex)->uHeight;
		} else {
			return static_cast<HGL_Texture*>(tex)->uHeight;
		}
	} else {
		return 0;
	}
}
unsigned* HGL_Impl::Texture_Lock(HTEXTURE tex, bool bReadOnly, int left, int top, int width, int height) {
	// TODO: Test Texture_Lock
	assert(false && "Not implemented yet");
	// TODO: Texture_Locking
	return nullptr;
}
void HGL_Impl::Texture_Unlock(HTEXTURE tex) {
	// TODO: Test Texture_Unlock
	// TODO: Texture_Unlocking
	assert(false && "Not implemented yet");
}

#pragma endregion
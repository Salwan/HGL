Platforms
======================================================================

## Where was I last time?

Next Milestone:

For each of these features, a demo is added to Tutorial 8:
- Implement pack file support for..
	> INI: invalid, gamedata is immutable while INIs are writable
	> Texture: I'm here, I can load texture as data? otherwise this requires Texture_Lock/Unlock implementation
	> Sound effect: Loading sound from memory instead of file
	> Mod music: parsing mod music from memory instead of file
	> Streaming: this one might be challenging, I have to stream music from a zip file multi-threaded.
	> Particle Script: load particle script from memory instead of file
	> Font: load font script and texture from memory instead of file (depends on Texture)
- Change audio effect and channel gain to use AL_MIN_GAIN and AL_MAX_GAIN for correct hardware values
- Implement music channels support (Channel_*)
- Implement tracker music player support using chosen dependency
- Test playing all tracker media supported: XM, IT, S3M, and MOD

## Target platforms for HGL:

- Win32 API (32-bit and 64-bit)		-- Main Development Platform
- Linux (32-bit and 64-bit)			-- Tier 1 Porting
- OSX (64-bit)						-- Tier 1 Porting
- NVIDIA Shield TV (Android TV)		-- Tier 2 Porting
- C.H.I.P. (armv7 Debian)			-- Tier 2 Porting
- iOS								-- Tier 3 Porting
- Android Phone						-- Tier 3 Porting

Standard C++ Types used and assumed:

- int 8-bit: int8_t and uint8_t
- int 16-bit: short and uint16_t
- int 32-bit: int and unsigned
- int 64-bit: int64_t and uint64_t
- real 16-bit: not supported (there are libs to do this though)
- real 32-bit: float
- real 64-bit: double
- real 80-bit: long double

External Depedencies (so far):
- EGL						> context management
- OpenGL ES 2.0				> everything visual
- libsndfile				> decoding audio files, streaming
- OpenAL Soft				> audio playback engine
- SOIL						> loading images from multiple formats
- gainput					> cross-platform input handling library
- zlib						> cross-platform gzip compression for resource packs
- glm						> header-only math library

Some boost libraries that might be useful in the future:
- fiber: fibers support (one thread can run many tasks with yield cooperative multitasking)
- lockfree: lockfree data structures like queue
- signals2: signal slot with thread safety
- format: for formatting strings instead of using stringstream (just less verbose)
- serialization: yep
- python: far future maybe as a scripting system
- filesystem: portable paths and iteration of directories

Libs I might have to write:
- Tweener

Current Milestone Target
===================================================================
Next font iteration: render freetype font and attempt getting some arabic writing working.

Future: TUTORIAL 9: tools

- Expand on tools support.

Future: TUTORIAL 10 to 15+ attempt to cover all modules from HGE

(Somewhere here I'll hit v1.0 and public release.)

FAR FAR Future:

- Porting to Linux, OSX, Shield, CHIP/RPi, Vulkan
- Touch support for multi-touch devices
- Tile maps support targeting Tiled
- Box2D full integration
- Tweening support
- Camera system
- Scene graph implementation
- Support for networking UDP/TCP possibly via a library

Wishlist:

- Multi-threading and fibers everywhere
- Visual-feel shaders to give HGL a unique feel (HDR, tone-mapping, bokeh, blur/bloom, pbr, indirect lighting, etc)
- Android and iOS
- Console: Switch maybe?
- Video playback support for at least one modern format (theora? or mp4)
- Procedural music synthesizer and support for mod and it
- Multi-monitor support
- Basic 3D support: geometry rendering, shaders, post-processing
- Python binding
- Emscripten javascript port
- D lang interface (can directly access HGL's "C" libraries :D)

HGE Library:
===================================================================

##### Interface functions
    [X]	hgeCreate				Returns a pointer to HGE interface.
    [X]	Release					Releases obtained HGE interface.
##### System functions
	[X]	System_Initiate			Initializes hardware and software needed to run engine.
	[X]	System_Shutdown			Restores video mode and frees allocated resources.
	[X]	System_Start			Starts running user defined frame function.
	[X]	System_SetState			Sets internal system states.
	[X]	System_GetState			Returns internal system state values.
	[X]	System_GetErrorMessage	Returns last occured HGE error description.
	[X]	System_Log				Writes a formatted message to the log file.
	[ ]	System_Launch			Launches an URL or external executable/data file.
	[ ]	System_Snapshot			Saves current screen snapshot into a file.
##### Resource functions
	[X]	Resource_Load			Loads a resource into memory from disk.
	[X]	Resource_Free			Deletes a previously loaded resource from memory.
	[X]	Resource_AttachPack		Attaches a resource pack.
	[X]	Resource_RemovePack		Removes a resource pack.
	[X]	Resource_RemoveAllPacks	Removes all resource packs previously attached.
	[X]	Resource_MakePath		Builds absolute file path.
	[X]	Resource_EnumFiles		Enumerates files by given wildcard.
	[X]	Resource_EnumFolders	Enumerates folders by given wildcard.
##### Initialization file functions
	[X]	Ini_SetInt				Writes an integer value to initialization file.
	[X]	Ini_GetInt				Reads an integer value from initialization file.
	[X]	Ini_SetFloat			Writes a float value to initialization file.
	[X]	Ini_GetFloat			Reads a float value from initialization file.
	[X]	Ini_SetString			Writes a string to initialization file.
	[X]	Ini_GetString			Reads a string from initialization file.
##### Random number generation functions
	[X]	Random_Seed				Sets random number generator's seed.
	[X]	Random_Int				Generates int random number.
	[X]	Random_Float			Generates float random number.
##### Timer functions
	[X]	Timer_GetTime			Returns time elapsed since System_Initiate call.
	[X]	Timer_GetDelta			Returns time elapsed since last frame function call.
	[X]	Timer_GetFPS			Returns current frames-per-second rate.
##### Sound effect functions
	[X]	Effect_Load				Loads a sound effect from disk or memory.
	[X]	Effect_Free				Deletes loaded effect and associated resources.
	[X]	Effect_Play				Starts playing sound effect.
	[X]	Effect_PlayEx			Starts playing sound effect with additional parameters.
##### Music functions
	[ ]	Music_Load				Loads a music from disk or memory.
	[ ]	Music_Free				Deletes loaded music and associated resources.
	[ ]	Music_Play				Starts playing music.
	[ ]	Music_SetAmplification	Sets level of amplification for a music.
	[ ]	Music_GetAmplification	Returns level of amplification of a music.
	[ ]	Music_GetLength			Returns length of a music's patterns list.
	[ ]	Music_SetPos			Sets music position in patterns and rows.
	[ ]	Music_GetPos			Returns current position of a music in patterns and rows.
	[ ]	Music_SetInstrVolume	Sets volume of a specific instrument in a music.
	[ ]	Music_GetInstrVolume	Returns volume of a specific instrument in a music.
	[ ]	Music_SetChannelVolume	Sets volume of a specific channel in a music.
	[ ]	Music_GetChannelVolume	Returns volume of a specific channel in a music.
##### Compressed audio stream functions
	[ ]	Stream_Load				Loads a stream from disk or memory.
	[ ]	Stream_Free				Deletes loaded stream and associated resources.
	[ ]	Stream_Play				Starts playing stream.
##### Audio channel functions
	[X]	Channel_SetPanning		Changes an audio channel panning.
	[X]	Channel_SetVolume		Changes an audio channel volume.
	[X]	Channel_SetPitch		Changes an audio channel pitch.
	[X]	Channel_Pause			Pauses an audio channel.
	[X]	Channel_Resume			Resumes a paused audio channel.
	[X]	Channel_Stop			Stops an audio channel.
	[X]	Channel_PauseAll		Pauses all active audio channels.
	[X]	Channel_ResumeAll		Resumes all active audio channels.
	[X]	Channel_StopAll			Stops all active audio channels.
	[X]	Channel_IsPlaying		Tests if an audio channel is playing.
	[ ]	Channel_GetLength		Retrieves the total length of a playing channel.
	[ ]	Channel_SetPos			Skips forwards or backwards in a playing channel.
	[ ]	Channel_GetPos			Retrieves a playing channel's position.
	[ ]	Channel_SlideTo			Starts sliding a channel volume, panning or pitch.
	[ ]	Channel_IsSliding		Tests if a channel parameters are sliding.
##### Input functions
	[X]	Input_GetMousePos		Returns current mouse cursor position.
	[X]	Input_SetMousePos		Sets current mouse cursor position.
	[X]	Input_GetMouseWheel		Returns the mouse wheel shift since last frame.
	[X]	Input_IsMouseOver		Tests if mouse cursor is inside HGE window or not.
	[X]	Input_KeyDown			Tests if a key was pressed down during the last frame.
	[X]	Input_KeyUp				Tests if a key was released during the last frame.
	[X]	Input_GetKeyName		Returns a key or mouse button name from it's code.
	[X]	Input_GetKeyState		Tests if a key or mouse button is down.
	[X]	Input_GetKey			Returns the last pressed key code.
	[X]	Input_GetChar			Returns the last pressed character.
	[X] Input_IsPadAvailable	Check if a controller pad with index is available or not.
##### Action map functions
	[X] Action_Create			Create a new action using given name.
	[X] Action_ResetBindings	Removes all bindings from action.
	[X] Action_Free				Deletes a created action.
	[X] Action_FreeAll			Deletes all actions and bindings.
	[X] Action_AddKey			Binds a keyboard key to action.
	[X] Action_AddMouseButton	Binds a mouse button to action.
	[X] Action_AddPadButton		Binds a game controller button to action.
	[X] Action_AddPadAxis		Binds a game controller axis to action.
	[X] Action_GetState			Tests if an action is happening or not based on its bindings.
	[X] Action_GetFloat			Returns the associated value [-1.0, 1.0] of an action based on its analog/digital bindings.
##### Graphics functions
	[X]	Gfx_BeginScene			Starts rendering graphics.
	[X]	Gfx_EndScene			Ends rendering and updates the screen.
	[X]	Gfx_Clear				Clears render target and z-buffer.
	[X]	Gfx_RenderLine			Renders a line.
	[X]	Gfx_RenderTriple		Renders a triple.
	[X]	Gfx_RenderQuad			Renders a quad.
	[X]	Gfx_StartBatch			Starts rendering of graphic primitives batch.
	[X]	Gfx_FinishBatch			Ends rendering of graphic primitives batch.
	[X]	Gfx_SetClipping			Sets the clipping region.
	[X]	Gfx_SetTransform		Sets global scene transformation.
	[X] Gfx_GetWidth			Returns current display width
	[X] Gfx_GetHeight			Returns current display height
	[X] Shader_Create			Create fragment shader
	[X] Shader_Free				Free shader
	[X] Gfx_SetShader			Set active program for rendering
##### Render target functions
	[X]	Target_Create			Creates a render target.
	[X]	Target_Free				Deletes a render target and associated resources.
	[X]	Target_GetTexture		Returns a render target's texture handle.
##### Texture functions
	[X]	Texture_Create			Creates an empty texture.
	[X]	Texture_Load			Loads a texture from disk or memory.
	[X]	Texture_Free			Deletes loaded texture and associated resources.
	[X]	Texture_GetWidth		Returns a texture width in pixels.
	[X]	Texture_GetHeight		Returns a texture height in pixels.
	[ ]	Texture_Lock			Locks a texture for direct access.
	[ ]	Texture_Unlock			Unlocks a texture.
##### Helper Classes
	[X] hglSprite				Sprite entities
	[X] hglAnimation			Animated sprite entities (derived from Sprite)
	[X] hglFont					Rendering bitmap font text
	[X] hglParticleSystem		Advanced 2D particle system
	[X] hglParticleManager		Takes care of all your particle systems and automates their creation, updating and removing.
	[X] hglDistortionMesh		Create effects like water, lenses, page wraps, various twists and even real-time morphing.
	[X] hglStringTable			Automates text strings management
	[ ] hglResourceManager		Automates creation of complex resource objects and their management in the memory.
	[X] hglGUI					Creating and managing user interfaces.
	[X] hglGUIObject			Abstract class used to define custom GUI controls. The default classes hgeGUIText, hgeGUIButton and hgeGUISlider are derived from it.
	[X] hglRect					Bounding box calculations and collision detection.
	[X] hglVector				2D vector calculation.
	[X] hglColor				Alias to hglColorRGB.
	[X] hglColorRGB				ARGB color calculations.
	[X] hglColorHSV				AHSV (alpha, hue, saturation, value) color calculations.


OpenGL Rendering 
===================================================================

Optimization:
-------------------------------------------------------------------
Currently I'm doing batched rendering with a local vertex buffer for
immediate operations since memory ops are much faster than using glBuffer* calls.
I'm using a 42000 vertex batch size which from testing on GTX 1070 gave the best 
performance however this could be highly device and memory bandwidth dependent.
So batch size might be modified per platform perhaps?
This has given me good enough results so far, exceeding haxe bunnymark by a far margin.

I could do more later. Some ideas:

- Non-synchronized double buffers and the trick of orphaning by passing NULL to glBindData 
  will allow me to draw one buffer (orphaned) and updating another (newly allocated) at the same time.
- Doing instancing for identical sprites (could be too specific to be useful). ES2 doesn't provide hardware
  instancing. So this is basically just trickery to minimize overhead of uniform setting and lower number
  of draw calls. Draw calls are the main bottleneck on mobile devices.
- Fully multi-threaded approach: This could be overkill and may cause less performance actually. But hell
  it'll be crazy FUN to write :D As added concurrency I could render previous frame while batching/sorting current.
	- Thread 1 (Main): renders ready batches by loading them using glBuffer*
	- Thread 2 (Batcher): receives render commands and prepares batches in temp buffers
	- Thread 3 (Sorter): sorts primitives in batches based on: Z (layer) -> Shader -> Blending -> Texture
	- Thread 4 (Dispatcher): manages process of rendering by batching, sorting then adding to actual gl render queue.

Talk about WebGL rendering optimization: https://www.youtube.com/watch?v=rfQ8rKGTVlg

Workflow when there is VAO support: 

    //initialization
    glGenVertexArrays
    glBindVertexArray

    glGenBuffers
    glBindBuffer
    glBufferData

    glVertexAttribPointer
    glEnableVertexAttribArray

    glBindVertexArray(0)

    glDeleteBuffers //you can already delete it after the VAO is unbound, since the
                //VAO still references it, keeping it alive (see comments below).

    ...

    //rendering
    glBindVertexArray
    glDrawWhatever

And when not using VAOs it would be something like that:

	//initialization
	glGenBuffers
	glBindBuffer
	glBufferData
	
	...
	
	//rendering
	glBindBuffer
	glVertexAttribPointer
	glEnableVertexAttribArray
	glDrawWhatever
	

Loading Images
===================================================================

Required image formats: JPEG, PNG (bonus for DXT)
Both PNG8 and PNG32 should be supported.

- IM Imaging Toolkit: targets scientific applications it seems.
- CImg: looks good (http://cimg.eu) in a single header and portable C++ with thread safety. 
	however? support for both jpg and png is only through a plugin that makes use of ImageMagick (or libjpg/png).
- GDAL: a bit monolithic
- ImageMagick: very capable but monolithic (suited for image editing software)
- LodePNG: seems an excellent tool for load/save PNG files specifically, however it seems it's not very fast (rumor?).
  http://lodev.org/lodepng/
- picoPNG: single header mini-lib for png loading only.
  http://lodev.org/lodepng/
- corona: old library but looks nice with zlib license http://corona.sourceforge.net/
- SOIL: Simple OpenGL Image Loader seems perfect, public domain and supports DDS too!
- Unofficial OpenGL software development kit has GL Image. The library is monolithic and will be expensive to port.

More libs: https://www.khronos.org/opengl/wiki/Image_Libraries

SOIL chosen and customized continuously. Works well and provides everything I need + no extra dependencies and simple
interface.

Action Mapping System
==================================================================

	enum hglAction_t {
		HGLA_UP = 0,	
		HGLA_HIT = 1,
		HGLA_PRESSED = 2,
		HGLA_POSITIVE = 2,
		HGLA_RELEASED = 3,
		HGLA_NEGATIVE = 4,
	
		HGLA_COUNT = 5,
	};

### Define Up

	HACTION actUp = Action_Create("Up");
	const char *actUpName = Action_GetName(actUp);
	Action_AddKey(actUp, HGLK_UP, HGLA_PRESSED);
	Action_AddKey(actUp, HGLK_W, HGLA_PRESSED);
	Action_AddPadButton(actUp, HGLP_PAD_UP, HGLA_PRESSED);
	Action_AddPadAxis(actUp, HGLP_Y_AXIS, HGLA_POSITIVE);

### Define Fire

	HACTION actFire = Action_Create("Fire");
	Action_AddKey(actFire, HGLK_CTRL, HGLA_HIT);
	Action_AddKey(actFire, HGLK_SPACE, HGLA_HIT);
	Action_AddKey(actFire, HGLK_LBUTTON, HGLA_HIT);
	Action_AddPadButton(actFire, HGLP_A, HGLA_HIT);
	Action_AddPadButton(actFire, HGLP_RT, HGLA_POSITIVE);

### Usage

	if(Action_GetState(actUp)) {
		// Move up
	}


### Clearing

	Action_ResetBindings(actUp); 	// Removes all bindings, used to allow rebinding support
	Action_Free(actUp);	 			// Removes action Up and all bindings
	Action_Free(actFire)			//
	Action_FreeAll()				// Removes everything (clean slate), used for shutdown

### Limitations:

Bindings don't support mouse location, mouse wheel, and any special inputs (ex: PS4 touch pad, touch input)
these will be added in the future maybe or will be left to do by the App through normal HGE input functions.

Sound Effects System
===================================================================

After some research I have decided to use OpenAL Soft:

- Based on OpenAL but without the hardware vendor specific stuff (software mixing)
- Supports many backends including: DirectSound, OpenAL, and OpenSL ES (for Android)
- It's generally a great alternative to OpenAL
- I may still end up using OpenSL ES on Android as an alternate sound solution if 
  OpenAL Soft proves too big of a task on Android.
- OpenAL license is LGPL -> usable for closed source commercial but as DLL only.

So now that I have the playback engine, the next logical step is decoding as OpenAL
doesn't offer that.
In HGE, sound effects system supports: WAV and MP3. I'll add Ogg to that later.

Search for a portable decoding library with support for WAV and MP3 or OGG:

- libsndfile: supports WAV and a lot of other useless formats + OGG! choice between LGPL 2.1 and 3 licenses.
	- http://www.mega-nerd.com/libsndfile/
	- Can it be ported to Android however? Yes, someone did it but for an older version.
	- Compiling on Windows might be problematic (requires ogg/vorbis/etc) but binaries are provided for 32/64.
- libaudiodecoder: supports WAV and MP3. MIT license
	- https://github.com/asantoni/libaudiodecoder/
	- Seems to focus on Windows/OSX but not linux. Uses CoreAudio and MediaFoundation to tap into the OS's mp3 decoder.
	- Any chance of porting on Linux/ARM/Android? Seems not. It uses OS specific decoders.
- libVLC: overkill
- SDL_Sound: seems great, geared towards games and supports all formats. LGPL 2.1
	- https://www.icculus.org/SDL_sound/
	- I have a feeling it will require the whole SDL2 to be included? No. It's not suitable then.
	- Seems to have been ported to MANY platforms but not Android/ARM linux.

libsndfile wins :)

Fixed issue with ogg clicks now it's working correctly, might eventually switch to direct ogg/vorbis due to perf.

Display System
==================================================================
First, I need to handle the situation when game display isn't compatible with
a fullscreen mode (example: setting game window to 200x750 or something) then
what should happen?

* HGL fires a go full screen event and allows user to decide what to do. Provide 
  a "Gfx_SetMode" function and a list of supported fullscreen display modes.
* HGL allows fullscreen only when app resolution is for a fullscreen valid mode
  so if user started with 1280x720 then fullscreen works via function Gfx_SetMode 
  otherwise it won't do anything and return false.
  User gets another function 'Gfx_SetWindowSize' to change window size in runtime 
  and a list of all supported fullscreen modes to choose from.
* HGL forces user to choose only fullscreen valid resolutions. This is the least 
  flexible but simplest to work with solution. User must use a valid mode from
  beginning so UI and everything deved correctly. Mode could be automatically chosen
  by display ratio (16:9, 16:10, etc).

For all choices, HGL must provide a list of supported display modes based on some 
criteria. Let's make that the display ratio or all.

If I focus on modern games/hardware here are a few notes:

* On portable devices, the concept of resolution is different. The device always 
runs at native screen resolution, game can only render scaled but should never 
change the actual display resolution.
* On all modern devices, LCDs always have a singular native resolution with a 
specific display ratio from 4:3 to 21:9 and anything in-between.
* Based on display technology progress, whatever is the system it should be forward 
looking accounting for any theoritical native resolution (12000x8000 for example) And
any display ratio at all since those are a moving target.

Therefore a modern approach to fullscreen may look like this:

* HGL figures out which monitor it wants to run on if multi-monitors are detected. 
  By default Monitor 1 (main)
* HGL figures out the native resolution of monitor. 
  Let's assume 1920x1080 @ 32bit
* HGL enumerates all available smaller display modes that match ratio. 
  That will give you 720x480 and 1280x720 and other 16:9 resolutions.
* User is provided with a list of all compatible modes up to native.
* User calls Gfx_ChangeMode to set 16:9 resolution. (default is always native)
* Render target scaling is done by game.
  Example: rendering 720p on a 1080p mode.
* UI layout and scaling is done by game.
* What about having a static projection resolution (say 720p) regardless of 
actual display resolution? This allows game code to not care what the display Mode
is but ratio is still significant.
* Should letter-boxing be supported? so for example if game set 1080p on a 1200p 
monitor it would still be able to render to 1080p?

Games can implement these plains of resolution:

* display mode and size of context buffers basically
* render target size will be scaled to display mode if different. 
* UI uses render target size with smart layout techniques.
* Physics resolution which is always different from display.

Conclusions:

* HGL multi-monitor detection and allowing choosing which to go to defaulting to main.
* HGL native resolution based display mode changing for fullscreen.
* HGL internal rendertarget can be set independently from actual display resolution.
  game resolution: scaled rendertarget if different from current display mode.

Regarding Pixel Format:

32-bit only is supported. This format is now the default one for all modern devices.
In Win8+ 8-bit and 16-bit are simulated.


How it looks:
-------------

	Monitor:
		EnumerateMonitors()
		SelectMainMonitor() -> All display done there
	Display Modes:
		CurrentResolution = GetSystemResolution()
		NativeResolution = GetNativeResolution()
		NativeRatio = GetNativeRatio()
		DisplayModes = EnumerateDisplayModes()
	Resolution:
		FullscreenResolution = System_SetState(HGL_RESWIDTH/HEIGHT) || NativeResolution
		FullscreenResolution.Ratio === NativeResolution.Ratio
		GameResolution = System_SetState(HGL_SCREENWIDTH/HEIGHT) || 1280x720
		DisplayScale = 100%
		if GameResolution > NativeResolution:
			DisplayScale = SetDisplayScaling(N%)
		if FullscreenResolution > NativeResolution:
			FullscreenResolution = NativeResolution
	Windowed:
		WindowSize = GameResolution * DisplayScale
	Fullscreen:
		WindowSize = FullscreenResolution
		if CurrentResolution != FullscreenResolution:
			SetDisplayMode(FullscreenResolution)
	Render:
		GameRT = RenderTarget(GameResolution)
		RenderAllTo(GameRT)
		GameRTScale = FitGameRT(WindowSize)
		Render(GameRT) to WINDOW

Advantages by device
--------------------

Desktop/Laptop: 

* Standard resolutions supported windowed or fullscreen
* Allows changing active monitor.
* Allows setting any window size.
* Allows changing resolution for current monitor.
* Automatic letter-boxing if window size ratio != display mode Ratio

NVIDIA Shield TV:

* 4k resolution supported. Internal game resolution enforced.
* Ability to switch display down to 1080p or 720p.
* Only fullscreen is supported.

CHIP/RPi:

* Detect if window size > current resolution and scale accordingly without changing 
  game size.
* Same features as desktop.

Android/iOS:

* Always fullscreen native resolution. Game resolution independent.
* Supports portrait and landscape based on game resolution.


Dependency Hell
==================================================================

I am beginning to have some dependency hell.

Because HGL is compiled as a static library the application that links to it must itself also link to all libraries
that HGL depends on! Otherwise linking HGL will fail without providing direct info to the user what to do :-S
This is not the same with HGE which is compiled as a pure DLL library allowing the application to not care about 
extra dependencies but simply supply DLLs for said dependencies.

If a DLL uses a library inside it's already linked correctly.

There is no way around it, I must convert HGL to be a fully dynamically driven library so that any dependencies
can be just extra DLLs.

So far I have these libraries as dependencies, more will come:

- OpenALSoft: OpenAL32.dll
- libsndfile: libsndfile-1.dll
- (Upcoming) SOIL: soil.dll

The best way to do this is of course to implement it like HGE does it.

This is the next high priority task as of now. Graphics will have to wait until this is done otherwise it'll get
more difficult.

Once this is done, it'll free me to use external libraries without poluting internal HGL code.

Log
==================================================================
Timer_GetDelta: general timer functionality is straight forward implemented to the HGL loop.
	- Implemented timers using steady_timer and internal nanoseconds precision.
Thread affinity set to core 1 on windows.

- Input_GetKeyState: For now keyboard only. Implemented internally (platform dependent).
  This is implemented very simply on top of gainput. It doesn't track release or hit or anything just simple states.
  Same implementation can be used for text input? otherwise gainput has a special text input mode in keyboards.

- Gfx_BeginScene/EndScene: essentially do nothing. later might be used to setup some useful states.
- Gfx_Clear: behaves like HGE


TUTORIAL 2 from HGL.

Some really good progress have been done so far.

What's upcoming:

- Gfx_RenderQuad pass 1: renders quad from a vertex object. pooling optimization in mind. uses simple blending.
- Gfx_RenderQuad pass 2: implementing full blending support and fragment shader. transformation.

:)

hglSprite interface problem, trying to figure out a work around.
HGL is a DLL but hglSprite is setup as cpp/h only with no exported interface and meant to be
directly manipulated which violates "C" export rules.

Options: 

(CHOSE THIS ONE)
1. implement an interface to helper classes. That requires a "C" exported constructor for each
helper class and possibly an abstract interface for each class? although that's probably not needed.

(BAD, will run into dependency issues eventually)
2. implement helper classes as a static library. Basically all helper classes are grouped in a
separate project to build hgl_helpers.lib which can then be linked to when needed. The issue here is
that once any of the helper classes might require an external dependency I'm back to square one 
and have to integrate it in the dll anyway.

(BAD, ties DLL to VS2015 and settings)
3. use C++ dynamic library instead of a strict "C" implementation. That way classes can be accessed 
directly without needing to worry about "C" interfaces. However it's a different approach from what
HGL is using so far which might lead to things feeling less consistent.


Fonts loading and parsing uses Resource_Load and Resource_Free.
Resource_Load adds new dependency and support for zip packs.
New dependency: zlib
I'm about to add zlib to HGL, here -> hgl_resource.cpp:35

Stuff I'm working on:

- Parsing INI direct memory mapping method, just finished parsing sections correctly with offsets stored.
  Next Steps: 
  - REMEMBER! all writes to file string buffer must mutex it.
  X Write get function for int and test it! validate everything
  X Write set function for int and test it! validate everything
  X Write get/set pair for float and string
  X Ini file support now working but no actual file operation is happening (so memory map only)
  > Create ini writer thread, which sleeps for 100ms or so between flush checks.
	X Flush triggered?
	X Lock file string buffer and copy it to own buffer, unlock
	X Perform disk I/O to update file
	X Go back to sleep
	X Repeat
  X Develop method to be able to parse font files (many "Char" keys with different values)
    X HGE's code might be usable directly since this is a helper class
	X This is a separate 'dumb' parser that has no support for modifications
	X Shares basic parsing functionality with normal Ini parser but doesn't reset ini file status

NEXT UP: > FONTS!

hglFont uses its own parser so I don't need to worry about that part.

- It uses Resource_* functions, I have to check how these are implemented. If they are portable then 
  great I could use them directly, otherwise I'll have to rewrite part or all of them.
- Font rendering uses hglSprite per letter, because of batching this should perform very well.

(DONE!) -> hglSprite implementation
(DONE) -> INI parsing
(DONE) -> HGE fonts

BunnyMark is now possible!!!
Time for some fun :D 

BunnyMark running! but very slow :(
Need profiling and an optimization pass to rendering.

GOAL: 100.000 bunnies at 60 fps smooth
Current: 50.000 bunnies at 15 fps

Simple batch buffer optimization gave me: 200000 bunnies at 60 fps
MOAR!

Milestone 4: Fixed FPS and Particle System
Possibly optimize to use particle systems hardware support (point sprites).

TUTORIAL 3: sprites, fonts and particle systems

- hglSprite and rendering
- INI file support (needed by fonts)
- hglFont: HGE fonts loading and font rendering
- Fixed fps support
- hglParticleSystem parsing of psi files and rendering effects (built on top of rendering quads for now)

Milestone 5

- Render to target support (using Begin/End Scene calls to wrap it)



- NEXT -> Graphics restore function (recovery from device lost)

From research, basically OpenGL can never lose device like D3D and 
when such an event occurs it's handled internally by the implementation.
It seems on all devices OpenGL never loses context except older Android
devices (2.x) as those have a limitation of one EGL context active only
which means if any other app tried to create a context my app would lose 
its context.
There is an event in the system to tell when that might happen, and in 
that case everything needs to be reallocated starting with the EGL context.
Textures, buffers, shaders.
There is a workaround however, by detaching rendering surface from EGL then
recreating it on resume apparently.

That being said I still need to handle resize events. Resize impacts the 
display window of course (front and backbuffer) and that's handled internally.
Default behavior seems to maintain size and align with bottom edge of display.
So far I could think of these things to be done upon resize:
- glViewport has to be reset
- Any active framebuffers will have to be recreated to match the new size but
  only when that's required. Three ways to deal with this (modes?):
	a. keep size (not a good idea I think)
	b. scale with resize (power of 2 requirement could be a problem)
	c. match screen size always
>>> Feature: handle arbitrary texture ratio in Texture_Create() so that it internally 
              gets init as pow-of-2 but used directly as WxH. Implement bOriginal 
			  option in Texture_GetWidth/Height() to handle access to actual size.
- Any view/projection matrices may have to be recalculated and reset in programs.

-> HGL_GFXRESTOREFUNC: will not be called for now. In HGE textures are restored automatically 
                       and this call is only used for renderbuffer restoration as thats game specific.

- hglAnimation and test


TUTORIAL 4: render to target

- Graphics restore function
- Render to target support (using Begin/End Scene calls to wrap it)
- hglAnimation (hglSprite pass 2)

TUTORIAL 5: Distortion Mesh and shaders

- hglDistortionMesh

- expanding on Input and font rendering

Implemented all Input_Key* functions for keyboard.
Next I was about to tackle mouse events first for Input_Key* shared events and then for mouse specific pos.
Input_GetEvent will be implemented next iteration along with game controller support.
I might either implement it myself or directly use gainput as the events processor and dispatcher.

DOING -> Implement fullscreen/windowed mode.

Advanced display system planned (below).
Basic implementation:

	- (DONE) Enumerate display modes
	- (DONE) When fullscreen is asked for check if mode is supported:
		- Yes? Set display mode to requested (if needed) and reposition/scale window.
		- No? fail in debug, stay in windowed in release.

EGL doesn't handle this. So this is a platform specific feature.

TUTORIAL 5: Distortion & Shaders

COMPLETED TUTORIAL REQUIREMENTS!

Additional things to do to complete Milestone 6:
- Implement basic support for pausing/resuming.

	A few important considerations:

	* Between pause/resume time delta should not exceed a max value. (I'll hard code that to 0.1)
	* Updating with time delta zero sounds like a game specific thing, ideally no updates are to be done and 
	no rendering. However special case must be made for networking or any external events that the game is 
	waiting for (?) this will be done in future iterations.
	* Audio should pause correctly for effects and later on streaming.
	* Pausing for first iteration will happen when: window loses focus, window minimized, window is being 
	  dragged around
	* Can I draw a simple grey rect on window to show that its disabled when it loses focus or moved around?
	  Perhaps listening to WM_PAINT as well when that happens.

- Resize mouse detection on mouse drag to eliminate continuous resize events.

Next input iteration: implement events input system, mouse wheel & full support for game controllers.

Future: TUTORIAL 6: GUI

- hglUI
- Expanding on Input: mouse, controller



TUTORIAL 7: optimize, first showable demo!

- Expanding on multiple features

TUTORIAL 8: put graphics to the test (showable demo!)

- Testing
- Optimization
- Iterating on all features

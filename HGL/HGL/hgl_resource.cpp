// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge 
#include "precomp.h"
#include "hgl_impl.h"
#ifdef HGLPLATFORM_WIN32
#include "hgl_system_win32.h"
#endif

#define ZLIB_WINAPI
#define NOCRYPT
#include <unzip.h>

// Implementation Defs

//////////////////////////////////////////////////////////// Resource system

// Loads a resource into memory from resource pack or disk file.
// If successful, returns pointer to a memory allocated for the resource and stores resource size to a 
// uint32 addressed by size parameter. Otherwise returns 0.
/* Remarks
   If filename specifies relative path, resource file is first searched within all resource packs 
   attached with Resource_AttachPack, then in the application disk folder.If filename specifies 
   absolute disk path, resource file is loaded directly from disk.
   If the function fails, you may obtain error message with System_GetErrorMessage function.Also 
   if HGE_LOGFILE system state was set, the error message is written to log.
   When the resource isn't needed longer, you must free the allocated memory with the Resource_Free call.
 */
// TODO: implement zip pack support including password encrypted functionality.
void* HGL_CALL HGL_Impl::Resource_Load(const char *filename, unsigned *size) {
	static char *res_err = "[ERROR] Can't load resource: ";
	
	std::string strName;
	std::string strZipName;
	char outZipName [MAX_RESOURCE_PATH];

	unzFile zip;
	unz_file_info file_info;
	int done;
	void *ptr;
	std::ifstream hF;
	std::ostringstream ss;

	auto resItem = resourcesList.begin();

	if (filename[0] == '\\' || filename[0] == '/' || filename[1] == ':') {
		goto _fromfile;    // skip absolute paths
	}

	// Load from pack
	strName = filename;
	HGLPlatform::_processPath(strName);

	while (resItem != resourcesList.end()) {
		zip = unzOpen(resItem->filename);
		done = unzGoToFirstFile(zip);
		while (done == UNZ_OK) {
			unzGetCurrentFileInfo(zip, &file_info, outZipName, sizeof(outZipName), NULL, 0, NULL, 0);
			strZipName = outZipName;
			HGLPlatform::_processPath(strZipName);
			if (strName == strZipName) {
				if (unzOpenCurrentFilePassword(zip, resItem->password[0] ? resItem->password : 0) != UNZ_OK) {
					unzClose(zip);
					ss << res_err << filename;
					_postError(ss.str().c_str());
					return 0;
				}

				ptr = malloc(file_info.uncompressed_size);
				if (!ptr) {
					unzCloseCurrentFile(zip);
					unzClose(zip);
					ss << res_err << filename;
					_postError(ss.str().c_str());
					return 0;
				}

				if (unzReadCurrentFile(zip, ptr, file_info.uncompressed_size) < 0) {
					unzCloseCurrentFile(zip);
					unzClose(zip);
					free(ptr);
					ss << res_err << filename;
					_postError(ss.str().c_str());
					return 0;
				}
				unzCloseCurrentFile(zip);
				unzClose(zip);
				if (size) {
					*size = file_info.uncompressed_size;
				}
				return ptr;
			}

			done = unzGoToNextFile(zip);
		}

		unzClose(zip);
		resItem++;
	}

	// Load from file
_fromfile:
	hF.open(filename, std::ios::binary);
	if (hF.fail() || hF.eof()) {
		ss << res_err << filename;
		_postError(ss.str().c_str());
		return 0;
	}
	
	hF.seekg(0, std::ios::end);
	std::streampos file_size = hF.tellg();
	hF.seekg(0, std::ios::beg);
	file_info.uncompressed_size = static_cast<uLong>(file_size);
	ptr = malloc(file_info.uncompressed_size);
	if (!ptr) {
		hF.close();
		ss << res_err << filename;
		_postError(ss.str().c_str());
		return 0;
	}
	hF.read((char*)ptr, file_size);
	auto data_read = hF.gcount();
	hF.close();
	if (data_read && size) {
		*size = file_info.uncompressed_size;
	}
	return ptr;
}

// Deletes previously loaded resource from memory.
void HGL_CALL HGL_Impl::Resource_Free(void *res) {
	if (res) {
		free(res);
	}
}

bool  HGL_CALL HGL_Impl::Resource_AttachPack(const char *filename, const char *password) {
	auto resItem = resourcesList.begin();
	unzFile zip;

	const char *szName = Resource_MakePath(filename);

	// Check to see if it already exists
	while (resItem != resourcesList.end()) {
		if (!strcmp(szName, resItem->filename)) {
			return false;
		}
		resItem++;
	}

	zip = unzOpen(szName);
	if (!zip) {
		return false;
	}
	unzClose(zip);

	resourcesList.push_back(HGL_Resource()); // add new resource
	auto& new_resource = resourcesList.back();
	strcpy_s<MAX_RESOURCE_PATH>(new_resource.filename, szName);
	if (password) {
		strcpy(new_resource.password, password);
	} else {
		new_resource.password[0] = 0;
	}

	return true;
}

void  HGL_CALL HGL_Impl::Resource_RemovePack(const char *filename) {
	const char *szName = Resource_MakePath(filename);
	auto ifind = std::find_if(resourcesList.begin(), resourcesList.end(), 
		[szName](const HGL_Resource& it){return !strcmp(it.filename, szName);});
	if(ifind != resourcesList.end()) {
		resourcesList.erase(ifind);
	}
}

void  HGL_CALL HGL_Impl::Resource_RemoveAllPacks() {
	resourcesList.clear();
}

// TODO: wchar_t this
const char* HGL_CALL HGL_Impl::Resource_MakePath(const char *filename) {
	std::string strout = HGLPlatform::_getAppPath();
	HGLPlatform::_processPath(strout);
	if(filename && strlen(filename) > 0) {
		std::string strfile{filename};
		HGLPlatform::_processPath(strfile);
		assert(strfile.length() > 2);
		if(strfile.length() > 2) {
			if(strfile[0] == '&') {
				// &TEMP& used for special environment paths
				size_t end_idx = strfile.find_first_of('&', 1);
				bool valid = false;
				if(end_idx != std::string::npos && end_idx > 1) { 
					std::string temp = strfile.substr(1, end_idx - 1);
					//temp = HGLPlatform::_getEnvironmentVariable(temp.c_str(), MAX_RESOURCE_PATH - strfile.length() + end_idx + 1);
					if(temp == "TEMP") {
						temp = HGLPlatform::_getSpecialSystemPath(HGL_SpecialSystemPath::SSP_TEMP);
						valid = true;
					} else if(temp == "SETTINGS") {
						temp = HGLPlatform::_getSpecialSystemPath(HGL_SpecialSystemPath::SSP_SETTINGS);
						valid = true;
					} else if(temp == "SAVEDATA") {
						temp = HGLPlatform::_getSpecialSystemPath(HGL_SpecialSystemPath::SSP_SAVEDATA);
						valid = true;
					} else if(temp == "HOME") {
						temp = HGLPlatform::_getSpecialSystemPath(HGL_SpecialSystemPath::SSP_HOME);
						valid = true;
					}
					if(valid) {
						// Add rest of user path to special path
						if(strfile.length() > end_idx + 1) {
							// This removes user path backslash so there won't be two backslashes (there is already one from special path)
							assert(strfile[end_idx + 1] == '\\');
							temp = temp + strfile.substr(end_idx + 2);
						}
						if(temp.length() <= MAX_RESOURCE_PATH) {
							strout = temp;
						} else {
							std::ostringstream ss;
							ss << "ERROR: Special variable resulting path is too long: " << filename;
							_postError(ss.str().c_str());
							throw std::runtime_error(ss.str());
						}
					}
				}
				if(!valid) {
					std::ostringstream ss;
					ss << "ERROR: Invalid special variable found in requested path: " << filename;
					_postError(ss.str().c_str());
					throw std::runtime_error(ss.str());
				}
			} else {
				if(strfile[0] != '.' && strfile[1] != ':') { // ':' is for absolute windows paths like executable path
					strfile = ".\\" + strfile;
				}
				strout = strfile;
			}
		}
	}
	strcpy(szPathString, strout.c_str());
	return szPathString;

	/*
	This performs the function of:
	- if filename == "": szAppPath is used which is the executable path.
	- All pathname '/' are converted to '\\' or vice-versa
	- All paths are considred relative and copied directly. Absolute paths are not supported.

	Needed platform functions:
	- _getAppPath()
	- _processPath()
	- _getEnvironmentVariable()
	- _getSpecialSystemPath()

	For Windows:
		- if filename == "": returns executable path
		- if filename starts with special path var: &VAR& platform get env var is used to substitute.
		- all other filenames are considred relative. '

	For Linux:

	For OSX:

	for Android:

	for iOS:

	*/
}

const char* HGL_CALL HGL_Impl::Resource_EnumFiles(const char *wildcard) {
	return HGLPlatform::_enumNextFile(wildcard);
}

const char* HGL_CALL HGL_Impl::Resource_EnumFolders(const char *wildcard) {
	return HGLPlatform::_enumNextFolder(wildcard);
}

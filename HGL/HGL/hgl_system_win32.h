// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// https://github.com/kvakvs/hge 
#pragma once
#include "precomp.h"

#define NOMINMAX
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Windows.h>

/////////////////////////////////////////////////////// Debug output (cerr/clog) redirected to VS output
#ifdef _MSC_VER
template<typename TChar, typename TTraits = std::char_traits<TChar>>
class OutputDebugStringBuf : public std::basic_stringbuf<TChar, TTraits> {
public:
	explicit OutputDebugStringBuf() : _buffer(256) {
		setg(nullptr, nullptr, nullptr);
		setp(_buffer.data(), _buffer.data(), _buffer.data() + _buffer.size());
	}

	~OutputDebugStringBuf() {
	}

	static_assert(std::is_same<TChar, char>::value || std::is_same<TChar, wchar_t>::value, "OutputDebugStringBuf only supports char and wchar_t types");

	int sync() try {
		MessageOutputer<TChar, TTraits>()(pbase(), pptr());
		setp(_buffer.data(), _buffer.data(), _buffer.data() + _buffer.size());
		return 0;
	}
	catch(...) {
		return -1;
	}

	int_type overflow(int_type c = TTraits::eof()) {
		auto syncRet = sync();
		if(c != TTraits::eof()) {
			_buffer[0] = static_cast<TChar>(c);
			setp(_buffer.data(), _buffer.data() + 1, _buffer.data() + _buffer.size());
		}
		return syncRet == -1 ? TTraits::eof() : 0;
	}


private:
	std::vector<TChar>      _buffer;

	template<typename TChar, typename TTraits>
	struct MessageOutputer;

	template<>
	struct MessageOutputer<char, std::char_traits<char>> {
		template<typename TIterator>
		void operator()(TIterator begin, TIterator end) const {
			std::string s(begin, end);
			OutputDebugStringA(s.c_str());
		}
	};

	template<>
	struct MessageOutputer<wchar_t, std::char_traits<wchar_t>> {
		template<typename TIterator>
		void operator()(TIterator begin, TIterator end) const {
			std::wstring s(begin, end);
			OutputDebugStringW(s.c_str());
		}
	};
};
#endif

// Forward decls
class HGL;
class HGL_Impl;

class HGLPlatform
{
public:
	/////////////////////////////////////////////////////// Constants
	static constexpr LONG_PTR HGLWINDOWS_FULLSCREEN_STYLE = WS_POPUP | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
	static constexpr LONG_PTR HGLWINDOWS_FULLSCREEN_EXSTYLE = WS_EX_APPWINDOW; 
	static constexpr LONG_PTR HGLWINDOWS_WINDOWED_STYLE = WS_BORDER | WS_CAPTION | WS_SYSMENU | WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_MINIMIZEBOX;
	static constexpr LONG_PTR HGLWINDOWS_WINDOWED_EXSTYLE = WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;

	/////////////////////////////////////////////////////// Platform
	static bool InitPlatform(HGL_Impl *_hgl);
	static void DestroyPlatform();

	/////////////////////////////////////////////////////// Threading

	// Used to set thread to run exclusively on one a specific single processor core
	static void SetThreadAffinity(std::thread& thread, int64_t mask);

	//////////////////////////////////////////////////////// Filesystem

	// Returns application path in system
	static std::string _getAppPath();
	// Performs OS specific path post-processing to make sure symbols and format are correct
	static void _processPath(std::string& path);
	// Retrieve environment variable from system
	static std::string _getEnvironmentVariable(const char* name, size_t max_length = MAX_RESOURCE_PATH);
	// Get special HGL path thats standard in system: TEMP, SETTINGS, SAVEDATA, HOME
	static std::string _getSpecialSystemPath(HGL_SpecialSystemPath ssp);
	// Get path and name of next file/folder in wildcard path
	static const char *_enumNextFile(const char *wildcard);
	static const char *_enumNextFolder(const char *wildcard);

	//////////////////////////////////////////////////////// Mouse Cursor
	static void SetMouseCursorVisible(bool is_visible);
	static void SetMousePosition(int x, int y);
	static bool IsMouseInWindow();

	/////////////////////////////////////////////////////// Display
	static bool EnumDisplayModes();
	static bool GoFullscreen(HGL_DisplayMode *display_mode);
	static bool GoWindowed();
	static void SetWindowTitle(const std::string& title);

	/////////////////////////////////////////////////////// Logging
	static void InitStdLog();
	static void LogSystemInfo(HGL* _hgl, const char *title);

	////////////////////////////////////////////////////// Window Procedure Handler
	static LRESULT CALLBACK HGLWndProc(HWND hwnd, unsigned int msg, WPARAM wParam, LPARAM lParam);
	static void _drawSuspensionRect();

	/////////////////////////////////////////////////////// Window Init
	static bool InitWindow();
	static void ShutdownWindow();

	/////////////////////////////////////////////////////////// App Loop
	static bool AppLoop();

	////////////////////////////////////////////////////////// Error Messages
	static int ShowErrorMessage(const char *message);
	static int ShowWarningMessage(const char *message);
	static int ShowInfoMessage(const char *message);

	////////////////////////////////////////////////////////// System Power
	static bool isPowerSavingMode();

	///////////////////////////////////////////////////////// Data
	static HGLNativeWindowHandle hWnd;
	static HGLNativeDisplayContext hDC;

private:
	static const wchar_t* DefaultClassName;
	static HGL_Impl* hgl;
	static bool bLaptopMode;
	static HANDLE hSearch;
	static WIN32_FIND_DATAA w32SearchData;
};

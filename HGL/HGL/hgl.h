// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge 
#pragma once

#define HGL_VERSION 0x100

#ifdef _WIN32
#define HGLPLATFORM_WIN32
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include <Windows.h>
#else
#define HGLPLATFORM_DEFAULT
#endif

// Used for utils
#include <string>
#include <algorithm>
#include <cctype>

/*
** DLL defs
*/
// A second def can be used to switch between static/dynamic, but thats not required now.
#ifdef HGLDLL
#define HGL_EXPORT  __declspec(dllexport)
#else
// Can be used to allow static linking by defining HGL_EXPORT as nothing
#define HGL_EXPORT __declspec(dllimport)
#endif

#define HGL_CALL  __stdcall

/*
* Platform independence:
*/
class HGLPlatform;
#ifdef HGLPLATFORM_WIN32
typedef HWND HGLNativeWindowHandle;
typedef HDC HGLNativeDisplayContext;
#else
typedef void* HGLNativeWindowHandle;
typedef void* HGLNativeDisplayContext;
#endif

/*
 * Resource constants
*/
#define MAX_RESOURCE_PATH _MAX_PATH

/*
** Common math constants
*/
#ifndef M_PI
#define M_PI    3.14159265358979323846f
#define M_PI_2  1.57079632679489661923f
#define M_PI_4  0.785398163397448309616f
#define M_1_PI  0.318309886183790671538f
#define M_2_PI  0.636619772367581343076f
#endif

/*
** HGL Handle types
*/
typedef void* hglHANDLE;
typedef hglHANDLE HTEXTURE;
typedef hglHANDLE HTARGET;
typedef hglHANDLE HEFFECT;
typedef hglHANDLE HMUSIC;
typedef hglHANDLE HSTREAM;
typedef hglHANDLE HCHANNEL;
typedef hglHANDLE HSHADER;
typedef hglHANDLE HACTION;

/*
** Hardware color macros
*/
#define ARGB(a,r,g,b)   ((unsigned(a)<<24) + (unsigned(r)<<16) + (unsigned(g)<<8) + unsigned(b))
#define GETA(col)       ((col)>>24)
#define GETR(col)       (((col)>>16) & 0xFF)
#define GETG(col)       (((col)>>8) & 0xFF)
#define GETB(col)       ((col) & 0xFF)
#define SETA(col,a)     (((col) & 0x00FFFFFF) + (unsigned(a)<<24))
#define SETR(col,r)     (((col) & 0xFF00FFFF) + (unsigned(r)<<16))
#define SETG(col,g)     (((col) & 0xFFFF00FF) + (unsigned(g)<<8))
#define SETB(col,b)     (((col) & 0xFFFFFF00) + unsigned(b))

#define GETAF(col)		((float)(GETA(col))/255.0f)
#define GETRF(col)		((float)(GETR(col))/255.0f)
#define GETGF(col)		((float)(GETG(col))/255.0f)
#define GETBF(col)		((float)(GETB(col))/255.0f)

/*
** Types
*/
typedef std::pair<int, int> ipair;

/*
** Forward defs
*/
enum hglKeyCode_t : unsigned;
enum hglPadCode_t : unsigned;
enum hglAction_t : unsigned;

/*
** HGL Utilities 
*/
namespace hglUtils {
	/*
	** Clamp function because it took 19 years for the C++ committe to add std::clamp -_-
	*/
	template<class T>
	inline constexpr const T& clamp(const T& value, const T& min, const T& max) {
		return (value < min ? min : max < value ? max : value);
	}
	/*
	** Trim std::string spaces from both left and right
	*/
	inline std::string trim(const std::string &str) {
		auto e_left = std::find_if_not(str.begin(), str.end(), 
			[](int c) { return std::isspace(c); });
		return std::string(
			e_left, 
			std::find_if_not(
				str.rbegin(), 
				std::string::const_reverse_iterator(e_left),
				[](int c){ return std::isspace(c); })
			.base());
	}
	/*
	** Euclid's reduction (used to calculate aspect ratio)
	*/
	inline ipair reduce(int a, int b) {
		if (a <= 1 || b <= 1) {
			return ipair(a, b);
		}
		int _a = a;
		int _b = b;
		int mod;
		int gcd = 1;
		while (true) {
			mod = _a % _b;
			if (mod > 0) {
				_a = _b;
				_b = mod;
				gcd = mod;
			} else {
				break;
			}
		}
		return ipair(a / gcd, b / gcd);
	}

	/*
	** Get display mode aspect ratio.
	*/
	inline ipair calcAspectRatio(int w, int h) {
		ipair ar = hglUtils::reduce(w, h);
		// 21:9 monitors are actually 64:27, 3440x1440 is 43:18 for some reason
		if (ar == ipair(43, 18)) {
			ar = ipair(64, 27);
		}
		// 1366x768 1360x768 are very close to 16:9 but not precisely
		if (ar == ipair(683, 384) || ar == ipair(85, 48)) {
			ar = ipair(16, 9);
		}
		// 8:5 is 16:10 reduced, will be kept as 8:5
		return ar;
	}
}


/*
** HGL Blending constants
*/
#define BLEND_COLORADD      1
#define BLEND_COLORMUL      0
#define BLEND_ALPHABLEND    2
#define BLEND_ALPHAADD      0
#define BLEND_ZWRITE        4
#define BLEND_NOZWRITE      0

/*
** HGL Game Pad Constants
*/
enum hglGamePad_t : unsigned {
	GAMEPAD_1 = 0,
	GAMEPAD_COUNT,
};

// Darken does real color multiplication, white source pixels don't change destination, while
// black source pixels make destination completely black
// Use example: http://relishgames.com/forum/index.php?p=/discussion/5799/darken-screen-plus-uneffected-hole/p1
#define BLEND_DARKEN		8
#define BLEND_BLACKEN		8 /* synonym for darken */

#define BLEND_DEFAULT       (BLEND_COLORMUL | BLEND_ALPHABLEND | BLEND_NOZWRITE)
#define BLEND_DEFAULT_Z     (BLEND_COLORMUL | BLEND_ALPHABLEND | BLEND_ZWRITE)
#define BLEND_DEFAULT_PAR	(BLEND_COLORMUL | BLEND_ALPHAADD | BLEND_NOZWRITE)

/*
** HGL runtime performance modes
*/
#define PERFORMANCE_AUTO	0	// Automatically detects if running on battery or not
#define PERFORMANCE_HIGH	1	// Force high performance and no sleep per frame
#define PERFORMANCE_LAPTOP	2	// Run in laptop mode always (sleep per frame and no unlimited fps)

/*
** Render target scaling mode in response to display resizing
*/
/*enum ERTResizeMode {
	HGL_NO_RESIZE = 0,			// Doesn't scale when display is resized
	HGL_MATCH_DISPLAY = 1,		// Always matches display (will scale up/down to match ratio)
};*/

/*
** HGL System state constants
*/
enum hglBoolState
{
	HGL_WINDOWED = 1,			// bool     run in window?      (default: false)
	HGL_ZBUFFER = 2,			// bool     use z-buffer?       (default: false)
	HGL_TEXTUREFILTER = 3,		// bool     texture filtering?  (default: true)

	HGL_USESOUND = 4,			// bool     use OpenAL Soft for sound? (default: true)

	HGL_DONTSUSPEND = 5,		// bool     focus lost:suspend? (default: false)
	HGL_HIDEMOUSE = 6,			// bool     hide system cursor? (default: true)

	HGL_SHOWSPLASH = 7,			// bool     hide system cursor? (default: true)

	HGLBOOLSTATE_FORCE_DWORD = 0x7FFFFFFF
};

enum hglFuncState
{
	HGL_FRAMEFUNC = 8,			// bool*()  frame function			(default: NULL) (you MUST set this)
	HGL_RENDERFUNC = 9,			// bool*()  render function			(default: NULL)
	HGL_FOCUSLOSTFUNC = 10,		// bool*()  focus lost function		(default: NULL)
	HGL_FOCUSGAINFUNC = 11,		// bool*()  focus gain function		(default: NULL)
	HGL_GFXRESTOREFUNC = 12,	// bool*()  exit function			(default: NULL)
	HGL_EXITFUNC = 13,			// bool*()  exit function			(default: NULL)
	HGL_RESIZEFUNC = 14,		// bool*()  resize function			(default: NULL)
	HGL_SUSPENDFUNC = 15,		// bool*()	app suspended function  (default: NULL)
	HGL_RESUMEFUNC = 16,		// bool*()  app resumed function	(default: NULL)

	HGLFUNCSTATE_FORCE_DWORD = 0x7FFFFFFF
};

enum hglHwndState
{
	HGL_HWND = 15,				// int      window handle: read only
	HGL_HWNDPARENT = 16,		// int      parent win handle   (default: 0)

	HGLHWNDSTATE_FORCE_DWORD = 0x7FFFFFFF
};

enum hglIntState
{
	HGL_SCREENWIDTH = 17,		// int      screen width        (default: 800)
	HGL_SCREENHEIGHT = 18,		// int      screen height       (default: 600)
	HGL_SCREENBPP = 19,			// int      screen bitdepth     (default: 32) (desktop bpp in windowed mode)

	HGL_SAMPLERATE = 20,		// int      sample rate         (default: 44100)
	HGL_FXVOLUME = 21,			// int      global fx volume    (default: 100)
	HGL_MUSVOLUME = 22,			// int      global music volume (default: 100)
	HGL_STREAMVOLUME = 23,		// int      global music volume (default: 100)

	HGL_FPS = 24,				// int      fixed fps           (default: HGLFPS_UNLIMITED)

	HGL_POWERSTATUS = 25,		// int      battery life percent + status
	HGL_PERFORMANCEMODE = 27,	// int		HGL_AUTO/0: HGL decides  (default)
								//          HGL_PERFORMANCE/1: force high performance mode
								//          HGL_LAPTOP/2: laptop mode

	HGLINTSTATE_FORCE_DWORD = 0x7FFFFFF
};

enum hglStringState
{
	HGL_ICON = 26,				// char*    icon resource       (default: NULL)
	HGL_TITLE = 27,				// char*    window title        (default: "HGL")

	HGL_INIFILE = 28,			// char*    ini file            (default: NULL) (meaning no file)
	HGL_LOGFILE = 29,			// char*    log file            (default: NULL) (meaning no file)

	HGLSTRINGSTATE_FORCE_DWORD = 0x7FFFFFFF
};

/*
** Callback protoype used by HGL
*/
typedef bool(*hglCallback)();

/*
** HGL_FPS system state special constants
*/
#define HGLFPS_UNLIMITED    0
#define HGLFPS_VSYNC        -1


/*
** HGL Primitive type constants
*/
#define HGLPRIM_LINES       2
#define HGLPRIM_TRIPLES     3
#define HGLPRIM_QUADS       4


/*
** HGL Vertex structure
*/
struct hglVertex
{
	hglVertex() {}
	hglVertex(float _x, float _y, float _z = 0.5f, unsigned _col = 0xffffffff, 
		float _u = 0.0f, float _v = 0.0f) 
		: x(_x), y(_y), z(_z), col(_col), tx(_u), ty(_v) {}
	void set(const float *_xyz, const float *_uv = nullptr,
		const unsigned _col = 0xffffffff) 
	{
		x = _xyz[0]; y = _xyz[1]; z = _xyz[2];
		if(_uv) {
			tx = _uv[0]; ty = _uv[1];
		} else {
			tx = 0.0f; ty = 0.0f;
		}
		col = _col;
	}
	float			x, y;       // screen position    
	float			z;			// depth
	float			tx, ty;     // texture coordinates
	unsigned		col;        // color
};


/*
** HGL Triple structure
*/
struct hglTriple
{
	hglVertex		v[3];
	HTEXTURE		tex;
	int				blend;
};


/*
** HGL Quad structure
*/
struct hglQuad
{
	// Initializes a rect
	void rect(float _x, float _y, float _w, float _h, float _tw = 1.0f, float _th = 1.0f, unsigned _col = 0xffffffff, float _z = 0.5f, float _tx = 0.0f, float _ty = 0.0f) {
		v[0] = hglVertex(_x,		_y,			_z, _col, _tx,			_ty			);
		v[1] = hglVertex(_x + _w,	_y,			_z, _col, _tx + _tw,	_ty			);
		v[2] = hglVertex(_x + _w,	_y + _h,	_z, _col, _tx + _tw,	_ty + _th	);
		v[3] = hglVertex(_x,		_y + _h,	_z, _col, _tx,			_ty + _th	);
	}
	// Initializes a fullscreen rect
	void fullscreen(float _w, float _h, float _tw = 1.0f, float _th = 1.0f, float _z = 0.0f) {
		rect(0.0f, 0.0f, _w, _h, _tw, _th, 0xffffffff, _z);
	}
	// Scrolls texture
	void scroll(float _tx, float _ty) {
		for(unsigned i = 0; i < 4; ++i) {
			v[i].tx += _tx;
			v[i].ty += _ty;
		}
	}
	hglVertex		v[4];
	HTEXTURE		tex;
	int				blend;
};

/*
** HGL Input Event structure
*/
struct hglInputEvent
{
	int     type;           // event type
	int     key;            // key code
	int     flags;          // event flags
	int     chr;            // character code
	int     wheel;          // wheel shift
	float   x;              // mouse cursor x-coordinate
	float   y;              // mouse cursor y-coordinate
};

// Interface to HGL object
class HGL
{
public:
	// Interface
	virtual void HGL_CALL Release() = 0;

	// System
	virtual bool HGL_CALL System_Initiate()	= 0;
	virtual void HGL_CALL System_Shutdown()	= 0;
	virtual bool HGL_CALL System_Start() = 0;

	virtual const char*	HGL_CALL System_GetErrorMessage() = 0;
	virtual void		HGL_CALL System_Log(const char *format, ...) = 0;
	/*virtual void		HGL_CALL System_Launch(const char *url)				= 0;
	virtual void		HGL_CALL System_Snapshot(const char *filename = 0)	= 0;*/
				 
	virtual void HGL_CALL System_SetState(hglBoolState state,	bool value)			= 0;
	virtual void HGL_CALL System_SetState(hglFuncState state,	hglCallback value)	= 0;
	virtual void HGL_CALL System_SetState(hglHwndState state,	void* value)		= 0;
	virtual void HGL_CALL System_SetState(hglIntState state,		int value)			= 0;
	virtual void HGL_CALL System_SetState(hglStringState state,	const char *value)  = 0;

	virtual bool		HGL_CALL System_GetState(hglBoolState state)		= 0;
	virtual hglCallback HGL_CALL System_GetState(hglFuncState state)		= 0;
	virtual void*		HGL_CALL System_GetState(hglHwndState state)		= 0;
	virtual int			HGL_CALL System_GetState(hglIntState state)		= 0;
	virtual const char* HGL_CALL System_GetState(hglStringState state)	= 0;

	// Resource system
	virtual void*       HGL_CALL Resource_Load(const char *filename, unsigned *size = 0) = 0;
	virtual void        HGL_CALL Resource_Free(void *res) = 0;
	virtual bool        HGL_CALL Resource_AttachPack(const char *filename, const char *password = 0) = 0;
	virtual void        HGL_CALL Resource_RemovePack(const char *filename) = 0;
	virtual void        HGL_CALL Resource_RemoveAllPacks() = 0;
	virtual const char* HGL_CALL Resource_MakePath(const char *filename = 0) = 0;
	virtual const char* HGL_CALL Resource_EnumFiles(const char *wildcard = 0) = 0;
	virtual const char* HGL_CALL Resource_EnumFolders(const char *wildcard = 0) = 0;

	// INI parsing
	virtual void        HGL_CALL Ini_SetInt(const char *section, const char *name, int value) = 0;
	virtual int         HGL_CALL Ini_GetInt(const char *section, const char *name, int def_val) = 0;
	virtual void        HGL_CALL Ini_SetFloat(const char *section, const char *name, float value) = 0;
	virtual float       HGL_CALL Ini_GetFloat(const char *section, const char *name, float def_val) = 0;
	virtual void        HGL_CALL Ini_SetString(const char *section, const char *name, const char *value) = 0;
	virtual const char* HGL_CALL Ini_GetString(const char *section, const char *name, const char *def_val) = 0;
	
	// Input
	virtual void        HGL_CALL Input_GetMousePos(float *x, float *y) = 0;
	virtual void        HGL_CALL Input_SetMousePos(float x, float y) = 0;
	virtual int         HGL_CALL Input_GetMouseWheel() = 0;
	virtual bool        HGL_CALL Input_IsMouseOver() = 0;
	virtual bool        HGL_CALL Input_KeyDown(int key) = 0;
	virtual bool        HGL_CALL Input_KeyUp(int key) = 0;
	virtual bool        HGL_CALL Input_GetKeyState(int key) = 0;
	virtual const char* HGL_CALL Input_GetKeyName(int key) = 0;
	virtual int         HGL_CALL Input_GetKey() = 0;
	virtual int         HGL_CALL Input_GetChar() = 0;
	virtual bool		HGL_CALL Input_IsPadAvailable(hglGamePad_t pad) = 0;

	// Action map
	virtual HACTION		HGL_CALL Action_Create(const char *name) = 0;
	virtual void		HGL_CALL Action_ResetBindings(HACTION action) = 0;
	virtual void		HGL_CALL Action_Free(HACTION action) = 0;
	virtual void		HGL_CALL Action_FreeAll() = 0;
	virtual hglAction_t	HGL_CALL Action_GetState(HACTION action) = 0;
	virtual float		HGL_CALL Action_GetFloat(HACTION action) = 0;
	virtual void		HGL_CALL Action_AddKey(HACTION action, hglKeyCode_t keycode)			= 0;
	virtual void		HGL_CALL Action_AddMouseButton(HACTION action, hglKeyCode_t buttoncode) = 0;
	virtual void		HGL_CALL Action_AddPadButton(HACTION action, hglPadCode_t buttoncode)	= 0;
	virtual void		HGL_CALL Action_AddPadAxis(HACTION action, hglPadCode_t axiscode)		= 0;

	// Graphics
	virtual bool        HGL_CALL Gfx_BeginScene(HTARGET target = 0) = 0;
	virtual void        HGL_CALL Gfx_EndScene() = 0;
	virtual void        HGL_CALL Gfx_Clear(unsigned color) = 0;
	virtual void        HGL_CALL Gfx_RenderLine(float x1, float y1, float x2, float y2, unsigned color = 0xFFFFFFFF, float z = 0.5f) = 0;
	virtual void        HGL_CALL Gfx_RenderTriple(const hglTriple *triple) = 0;
	virtual void        HGL_CALL Gfx_RenderQuad(const hglQuad *quad) = 0;
	virtual hglVertex*  HGL_CALL Gfx_StartBatch(int prim_type, HTEXTURE tex, int blend, int *max_prim) = 0;
	virtual void        HGL_CALL Gfx_FinishBatch(int nprim) = 0;
	virtual void        HGL_CALL Gfx_SetClipping(int x = 0, int y = 0, int w = 0, int h = 0) = 0;
	virtual void        HGL_CALL Gfx_SetTransform(float x = 0, float y = 0, float dx = 0, float dy = 0, float rot = 0, float hscale = 0, float vscale = 0) = 0;
	virtual int			HGL_CALL Gfx_GetWidth() const = 0;
	virtual int			HGL_CALL Gfx_GetHeight() const = 0;

	// Random
	virtual void        HGL_CALL Random_Seed(int seed = 0) = 0;
	virtual int         HGL_CALL Random_Int(int min, int max) = 0;
	virtual float       HGL_CALL Random_Float(float min = 0.0f, float max = 1.0f) = 0;

	// Timer
	virtual float       HGL_CALL Timer_GetTime() = 0;
	virtual float       HGL_CALL Timer_GetDelta() = 0;
	virtual double		HGL_CALL Timer_GetDeltaNS() = 0;
	virtual int         HGL_CALL Timer_GetFPS() = 0;
						
	// Sound effects 
	virtual HEFFECT		HGL_CALL Effect_Load(const char *filename, size_t size = 0) =0;
	virtual void		HGL_CALL Effect_Free(HEFFECT eff) = 0;
	virtual HCHANNEL	HGL_CALL Effect_Play(HEFFECT eff) = 0;
	virtual HCHANNEL	HGL_CALL Effect_PlayEx(HEFFECT eff, int volume = 100, int pan = 0, float pitch = 1.0f, bool loop = false) = 0;

	// Music 
	/*virtual HMUSIC      HGE_CALL    Music_Load(const char *filename, hgeU32 size = 0) = 0;
	virtual void        HGE_CALL    Music_Free(HMUSIC mus) = 0;
	virtual HCHANNEL    HGE_CALL    Music_Play(HMUSIC mus, bool loop, int volume = 100, int order = -1, int row = -1) = 0;
	virtual void        HGE_CALL    Music_SetAmplification(HMUSIC music, int ampl) = 0;
	virtual int         HGE_CALL    Music_GetAmplification(HMUSIC music) = 0;
	virtual int         HGE_CALL    Music_GetLength(HMUSIC music) = 0;
	virtual void        HGE_CALL    Music_SetPos(HMUSIC music, int order, int row) = 0;
	virtual bool        HGE_CALL    Music_GetPos(HMUSIC music, int *order, int *row) = 0;
	virtual void        HGE_CALL    Music_SetInstrVolume(HMUSIC music, int instr, int volume) = 0;
	virtual int         HGE_CALL    Music_GetInstrVolume(HMUSIC music, int instr) = 0;
	virtual void        HGE_CALL    Music_SetChannelVolume(HMUSIC music, int channel, int volume) = 0;
	virtual int         HGE_CALL    Music_GetChannelVolume(HMUSIC music, int channel) = 0;*/

	// Music Streaming
	/*virtual HSTREAM     HGE_CALL    Stream_Load(const char *filename, hgeU32 size = 0) = 0;
	virtual void        HGE_CALL    Stream_Free(HSTREAM stream) = 0;
	virtual HCHANNEL    HGE_CALL    Stream_Play(HSTREAM stream, bool loop, int volume = 100) = 0;*/

	// Channels
	virtual void        HGL_CALL Channel_SetPanning(HCHANNEL chn, int pan) = 0;
	virtual void        HGL_CALL Channel_SetVolume(HCHANNEL chn, int volume) = 0;
	virtual void        HGL_CALL Channel_SetPitch(HCHANNEL chn, float pitch) = 0;
	virtual void        HGL_CALL Channel_Pause(HCHANNEL chn) = 0;
	virtual void        HGL_CALL Channel_Resume(HCHANNEL chn) = 0;
	virtual void        HGL_CALL Channel_Stop(HCHANNEL chn) = 0;
	virtual void        HGL_CALL Channel_PauseAll() = 0;
	virtual void        HGL_CALL Channel_ResumeAll() = 0;
	virtual void        HGL_CALL Channel_StopAll() = 0;
	virtual bool        HGL_CALL Channel_IsPlaying(HCHANNEL chn) = 0;
	virtual float       HGL_CALL Channel_GetLength(HCHANNEL chn) = 0;
	virtual float       HGL_CALL Channel_GetPos(HCHANNEL chn) = 0;
	virtual void        HGL_CALL Channel_SetPos(HCHANNEL chn, float fSeconds) = 0;
	virtual void        HGL_CALL Channel_SlideTo(HCHANNEL channel, float time, int volume, int pan = -101, float pitch = -1) = 0;
	virtual bool        HGL_CALL Channel_IsSliding(HCHANNEL channel) = 0;
								 
	// Shaders					 
	virtual HSHADER		HGL_CALL Shader_Create(const char *filename) = 0;
	virtual void		HGL_CALL Shader_Free(HSHADER shader) = 0;
	virtual void		HGL_CALL Gfx_SetShader(HSHADER shader) = 0;

	// Render targets
	virtual HTARGET     HGL_CALL Target_Create(int width, int height, bool zbuffer, bool viewport = true) = 0;
	virtual void        HGL_CALL Target_Free(HTARGET target) = 0;
	virtual HTEXTURE    HGL_CALL Target_GetTexture(HTARGET target) = 0;
						
	// Textures
	virtual HTEXTURE    HGL_CALL Texture_Create(int width, int height) = 0;
	virtual HTEXTURE    HGL_CALL Texture_Load(const char *filename, unsigned size = 0, bool bMipmap = false) = 0;
	virtual void        HGL_CALL Texture_Free(HTEXTURE tex) = 0;
	virtual int			HGL_CALL Texture_GetWidth(HTEXTURE tex, bool bOriginal = false) = 0;
	virtual int			HGL_CALL Texture_GetHeight(HTEXTURE tex, bool bOriginal = false) = 0;
	virtual unsigned *	HGL_CALL Texture_Lock(HTEXTURE tex, bool bReadOnly = true, int left = 0, int top = 0, int width = 0, int height = 0) = 0;
	virtual void		HGL_CALL Texture_Unlock(HTEXTURE tex) = 0;

	friend class HGLPlatform;
};

extern "C" { 
	HGL_EXPORT HGL* hglCreate(int ver); 
}

/*
** HGL Virtual-key codes
** (From HGL's header)
*/
enum hglKeyCode_t : unsigned {
	HGLK_NO_KEY = 0x00,
	HGLM_LBUTTON = 0x01, HGLM_RBUTTON = 0x02, HGLM_MBUTTON = 0x04,
	HGLK_ESCAPE = 0x1B, HGLK_BACKSPACE = 0x08, HGLK_TAB = 0x09,
	HGLK_ENTER = 0x0D, HGLK_SPACE = 0x20, HGLK_SHIFT = 0x10,
	HGLK_CTRL = 0x11, HGLK_ALT = 0x12, HGLK_LWIN = 0x5B,
	HGLK_RWIN = 0x5C, HGLK_APPS = 0x5D, HGLK_PAUSE = 0x13,
	HGLK_CAPSLOCK = 0x14, HGLK_NUMLOCK = 0x90, HGLK_SCROLLLOCK = 0x91,
	HGLK_PGUP = 0x21, HGLK_PGDN = 0x22, HGLK_HOME = 0x24,
	HGLK_END = 0x23, HGLK_INSERT = 0x2D, HGLK_DELETE = 0x2E,
	HGLK_LEFT = 0x25, HGLK_UP = 0x26, HGLK_RIGHT = 0x27,
	HGLK_DOWN = 0x28, HGLK_0 = 0x30, HGLK_1 = 0x31,
	HGLK_2 = 0x32, HGLK_3 = 0x33, HGLK_4 = 0x34,
	HGLK_5 = 0x35, HGLK_6 = 0x36, HGLK_7 = 0x37,
	HGLK_8 = 0x38, HGLK_9 = 0x39, HGLK_A = 0x41,
	HGLK_B = 0x42, HGLK_C = 0x43, HGLK_D = 0x44,
	HGLK_E = 0x45, HGLK_F = 0x46, HGLK_G = 0x47,
	HGLK_H = 0x48, HGLK_I = 0x49, HGLK_J = 0x4A,
	HGLK_K = 0x4B, HGLK_L = 0x4C, HGLK_M = 0x4D,
	HGLK_N = 0x4E, HGLK_O = 0x4F, HGLK_P = 0x50,
	HGLK_Q = 0x51, HGLK_R = 0x52, HGLK_S = 0x53,
	HGLK_T = 0x54, HGLK_U = 0x55, HGLK_V = 0x56,
	HGLK_W = 0x57, HGLK_X = 0x58, HGLK_Y = 0x59,
	HGLK_Z = 0x5A, HGLK_GRAVE = 0xC0, HGLK_MINUS = 0xBD,
	HGLK_EQUALS = 0xBB, HGLK_BACKSLASH = 0xDC, HGLK_LBRACKET = 0xDB,
	HGLK_RBRACKET = 0xDD, HGLK_SEMICOLON = 0xBA, HGLK_APOSTROPHE = 0xDE,
	HGLK_COMMA = 0xBC, HGLK_PERIOD = 0xBE, HGLK_SLASH = 0xBF,
	HGLK_NUMPAD0 = 0x60, HGLK_NUMPAD1 = 0x61, HGLK_NUMPAD2 = 0x62,
	HGLK_NUMPAD3 = 0x63, HGLK_NUMPAD4 = 0x64, HGLK_NUMPAD5 = 0x65,
	HGLK_NUMPAD6 = 0x66, HGLK_NUMPAD7 = 0x67, HGLK_NUMPAD8 = 0x68,
	HGLK_NUMPAD9 = 0x69, HGLK_MULTIPLY = 0x6A, HGLK_DIVIDE = 0x6F,
	HGLK_ADD = 0x6B, HGLK_SUBTRACT = 0x6D, HGLK_DECIMAL = 0x6E,
	HGLK_F1 = 0x70, HGLK_F2 = 0x71, HGLK_F3 = 0x72,
	HGLK_F4 = 0x73, HGLK_F5 = 0x74, HGLK_F6 = 0x75,
	HGLK_F7 = 0x76, HGLK_F8 = 0x77, HGLK_F9 = 0x78,
	HGLK_F10 = 0x79, HGLK_F11 = 0x7A, HGLK_F12 = 0x7B,
	HGLK_COUNT = 0x7C
};

/*
** HGL controller key codes
** Codes use XBOX360 notation
*/
enum hglPadCode_t : unsigned {
	HGLP_NO_INPUT = 0,
	// Left stick
	HGLP_X_AXIS,
	HGLP_Y_AXIS,
	// Right stick
	HGLP_XR_AXIS,
	HGLP_YR_AXIS,
	// Analog triggers
	HGLP_LT,
	HGLP_RT,
	// D-Pad
	HGLP_DPAD_UP,
	HGLP_DPAD_DOWN,
	HGLP_DPAD_LEFT,
	HGLP_DPAD_RIGHT,
	// Face buttons
	HGLP_A,
	HGLP_B,
	HGLP_X,
	HGLP_Y,
	// Bumpers
	HGLP_LB,
	HGLP_RB,
	// Thumb buttons
	HGLP_RS,
	HGLP_LS,
	// Centre
	HGLP_BACK,
	HGLP_START,
	HGLP_HOME,

	HGLP_COUNT,
};

enum hglAction_t : unsigned {
	HGLA_UP = 0,
	HGLA_HIT = 1,
	HGLA_PRESSED = 2,
	HGLA_HELD = 2,
	HGLA_POSITIVE = 2,
	HGLA_RELEASED = 3,
	HGLA_NEGATIVE = 4,

	HGLA_COUNT = 5,
};



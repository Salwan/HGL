========================================================================
    STATIC LIBRARY : HGL 
========================================================================

Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
Copyright (C) 2017, Cloud Mill Games
Haaf's Game Engine 1.8
Copyright(C) 2003 - 2007, Relish Games
https://github.com/kvakvs/hge 

TODOs
=========================================================================

- Go through all external used libraries and include the correct licensing info.
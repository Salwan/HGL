// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge 
#include "precomp.h"
#include "hgl_impl.h"
#ifdef HGLPLATFORM_WIN32
#include "hgl_system_win32.h"
#endif

#include <cstdlib>
#include <ctime>

/////////////////////////////////////////////////////// Random_*
// TODO: Implement this using random from C++11

unsigned int g_seed = 0;

void HGL_CALL HGL_Impl::Random_Seed(int seed)
{
	if (!seed) {
		g_seed = static_cast<unsigned>(std::time(0));
		std::srand(g_seed);
	} else {
		g_seed = seed;
	}
}

int HGL_CALL HGL_Impl::Random_Int(int min, int max)
{
	g_seed = 214013 * g_seed + 2531011;
	return min + (g_seed ^ g_seed >> 15) % (max - min + 1);
}

float HGL_CALL HGL_Impl::Random_Float(float min, float max)
{
	g_seed = 214013 * g_seed + 2531011;
	//return min+g_seed*(1.0f/4294967295.0f)*(max-min);
	return min + (g_seed >> 16)*(1.0f / 65535.0f)*(max - min);
}

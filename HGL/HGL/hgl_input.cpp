// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// https://github.com/kvakvs/hge 
#include "precomp.h"
#include "hgl_impl.h"

#ifdef HGLPLATFORM_WIN32
#include "hgl_system_win32.h"
#endif

#include "hgl_input_tables.inl"

#pragma region "Input Internal"
////////////////////////////////////////////////// Input internal
using namespace gainput;

// TODO: implement full support for game controllers.
// TODO: Implement input system using events instead of hacking through gainput.

// Initialize all supported input devices.
bool HGL_Impl::_initInput() {
	// Setup Gainput
	spInputManager = std::make_shared<InputManager>();
	spInputManager->SetDisplaySize(iWidth, iHeight);
	diMouse		= spInputManager->CreateDevice<InputDeviceMouse>();
	diKeyboard	= spInputManager->CreateDevice<InputDeviceKeyboard>();
	for(unsigned i = GAMEPAD_1; i <= GAMEPAD_COUNT; ++i) {
		diPad[i]	= spInputManager->CreateDevice<InputDevicePad>(i);
	}

	gainputKeysBufferCount = 0;
	gainputMouseBufferCount = 0;

	// This assumes devices won't change!
	inputKeyboard = static_cast<InputDeviceKeyboard*>(spInputManager->GetDevice(diKeyboard));
	inputMouse = static_cast<InputDeviceMouse*>(spInputManager->GetDevice(diMouse));
	for(unsigned i = GAMEPAD_1; i <= GAMEPAD_COUNT; ++i) {
		inputPad[i] = static_cast<InputDevicePad*>(spInputManager->GetDevice(diPad[i]));
	}

	// Clear keyboard states
	memset(keyboardState, 0, sizeof(bool) * HGLK_COUNT);
	memset(keyboardStatePrev, 0, sizeof(bool) * HGLK_COUNT);

	// Mouse
	iMouseX = 0;
	iMouseY = 0;
	iMouseWheel = 0;

	// Action map stuff
	spActionList = std::make_shared<HGL_TActionList>();
	spInputMap = std::make_shared<InputMap>(*spInputManager.get());

	return true;
}

void HGL_Impl::_input_OnResize(int new_width, int new_height) {
	if(spInputManager) {
		spInputManager->SetDisplaySize(new_width, new_height);
	}
}

// TODO: everything keyboard, mouse, game controller
// Should/Can this be done in a thread?
void HGL_Impl::_updateInput() {
	assert(spInputManager);
	// Tick input
	spInputManager->Update();

	// Prev input state:
	// keyboardStatePrev <- keyboardState
	memcpy(keyboardStatePrev, keyboardState, sizeof(bool) * HGLK_COUNT);
	// clear keyboardState
	for (unsigned i = 0; i < gainputKeysBufferCount; ++i) {
		unsigned k = gainputKeyToHGL[gainputKeysBuffer[i].buttonId];
		keyboardState[k] = false;
	}

	// Get new keyboard/mouse state
	// TODO: Convert this to a fully event driven system
	if(inputKeyboard) {
		gainputKeysBufferCount = static_cast<unsigned>(
			inputKeyboard->GetAnyButtonDown(gainputKeysBuffer, 10));
	}
	if(inputMouse) {
		// This gives mouse X axis and Y axis continuously. 
		// Float values are 0.0-1.0 of window's W and H, emits last value when mouse is off window.
		gainputMouseBufferCount = static_cast<unsigned>(inputMouse->GetAnyButtonDown(gainputMouseBuffer, 8));
		// Mouse temp gainput button values: 200, 201, 202
		DeviceButtonSpec dbs;
		dbs.deviceId = inputMouse->GetDeviceId();
		// Reset wheel input
		//iMouseWheel = 0;
		for (unsigned i = 0; i < gainputMouseBufferCount; ++i) {
			auto bid = gainputMouseBuffer[i].buttonId;
			switch (bid) {
				case MouseAxisX: {	
					// TEMP: Clamping is a temporary fix to handle MouseX returning -1 and -2 when mouse is over border
					float imx = hglUtils::clamp<float>(inputMouse->GetFloat(bid), 0.0f, 1.0f);
					iMouseX = (int)(imx * (float)iWidth);
					assert(iMouseX >= 0);
				}
				break;
				case MouseAxisY: {
					// TEMP: Clamping is a temporary fix to handle MouseY possibly returning negative when mouse is over borders
					float imy = hglUtils::clamp<float>(inputMouse->GetFloat(bid), 0.0f, 1.0f);
					iMouseY = (int)(imy * (float)iHeight);
					assert(iMouseY >= 0);
				}
				break;
				case MouseButtonLeft:
					dbs.buttonId = 200;
					gainputKeysBuffer[gainputKeysBufferCount++] = dbs;
					break;
				case MouseButtonMiddle:
					dbs.buttonId = 201;
					gainputKeysBuffer[gainputKeysBufferCount++] = dbs;
					break;
				case MouseButtonRight:
					dbs.buttonId = 202;
					gainputKeysBuffer[gainputKeysBufferCount++] = dbs;
					break;
					// TODO: FIX ME! for some reason I'm not getting wheel events from gainput, currently
					//       I'm using WM_MOUSEWHEEL message to do this on windows.
					//       Don't forget to reset the value (commented before the for).
					// Attampted to fix it by calling HandleMessage in msgproc (hgl_system_win32.cpp) on WM_MOUSEWHEEL
					// but it didn't work, needs to be debugged got more important things now so will do later.
					/*
				case MouseButtonWheelUp:
					iMouseWheel = 1;
					break;
				case MouseButtonWheelDown:
					iMouseWheel = -1;
					break;
				default:
					System_Log("Mouse input unknown = %d", bid);
					*/
			}
		}
	}

	// New keys state
	for (unsigned i = 0; i < gainputKeysBufferCount; ++i) {
		unsigned k = gainputKeyToHGL[gainputKeysBuffer[i].buttonId];
		keyboardState[k] = true;
	}

	// Check for fullscreen shortcut
	if (Input_GetKeyState(HGLK_ALT) && Input_KeyDown(HGLK_ENTER)) {
		_toggleDisplayMode();
	}
	// Check for pause engine shortcut
	if (Input_KeyUp(HGLK_PAUSE)) {
		if (bAppSuspended) {
			_resume();
		} else {
			_suspend();
		}
	}
}

// Close all devices.
void HGL_Impl::_destroyInput() {
	spInputManager.reset();
}
#pragma endregion

#pragma region "Input Functions"
///////////////////////////////////////////////////// Input_*

// TODO: Returns current mouse cursor position.
// x Pointer to a float to store the mouse cursor X - coordinate to.
// y Pointer to a float to store the mouse cursor Y - coordinate to.
// Use Input_GetKeyState or Input_GetKey with HGLK_LBUTTON, HGLK_RBUTTON and HGLK_MBUTTON 
// virtual-key codes to obtain mouse buttons state.
void HGL_CALL HGL_Impl::Input_GetMousePos(float *x, float *y) {
	*x = static_cast<float>(iMouseX);
	*y = static_cast<float>(iMouseY);
}

// TODO: Sets current mouse cursor position.
// x X - coordinate of the new mouse cursor position.
// y Y - coordinate of the new mouse cursor position.
// You could use Input_SetMousePos to receive relative mouse movement.Just store the 
// coordinates obtained with Input_GetMousePos, then return mouse cursor to the center of 
// the screen and use the difference between current and the last coordinates as relative 
// values.
// It is not recommended to use Input_SetMousePos for visible cursor movement as this could 
// be confusing for the user.
void HGL_CALL HGL_Impl::Input_SetMousePos(float x, float y) {
	HGLPlatform::SetMousePosition(static_cast<int>(x), static_cast<int>(y));
}

// TODO: Returns the mouse wheel shift since the previous call to frame function.
// Returns the number of notches the mouse wheel was rotated through since the previous 
// call to frame function. A positive value indicates that the wheel was rotated forward, 
// away from the user; a negative value indicates that the wheel was rotated backward, toward 
// the user. If the wheel has been still, returns 0.
int HGL_CALL HGL_Impl::Input_GetMouseWheel() {
	return iMouseWheel;
}

// TODO: reimplement using events
// Tests if mouse cursor is inside HGL window or not.
// If mouse cursor is inside HGL window, returns true. Otherwise returns false.
// This function may be useful to decide if a mouse cursor should be rendered.
bool HGL_CALL HGL_Impl::Input_IsMouseOver() {
	return HGLPlatform::IsMouseInWindow();
}

// TODO: Tests if a key was pressed down during the last frame.
// key Specifies one of virtual - key codes.For more information see Key codes section.
// If the key was pressed down during the last frame, returns true. Otherwise returns false.
// Input_KeyDown handles keyboard keys and mouse buttons. 
bool HGL_CALL HGL_Impl::Input_KeyDown(int key) {
	return !keyboardStatePrev[key] && keyboardState[key];
}

// TODO: reimplement using events
// Tests if a key was released during the last frame.
// key Specifies one of virtual - key codes.For more information see Key codes section.
// If the key was released during the last frame, returns true. Otherwise returns false.
// Input_KeyUp handles keyboard keys and mouse buttons. 
// The key states are updated outside user's frame function. So don't use Input_KeyUp in 
// a loop to wait a key to be pressed or released, this will result in system hang up.
bool HGL_CALL HGL_Impl::Input_KeyUp(int key) {
	return keyboardStatePrev[key] && !keyboardState[key];
}

// Determines whether a key or mouse button is up or down at the time 
// the function is called.
// key: Specifies one of virtual - key codes.FoBBr more information see Key codes section.
bool HGL_Impl::Input_GetKeyState(int key) {
	return keyboardState[key];
}

const char* HGL_CALL HGL_Impl::Input_GetKeyName(int key) {
	return keyToName[key];
}

// TODO: Returns virtual-key code of last key pressed since previous call to frame function. 
// If no key was pressed, returns 0. For more information see Key codes section.
// Input_GetKey handles keyboard keys and mouse buttons. 
// In HGE, this returns actual char input with repeat rate and locale.
// For now I'm using KeyDown (Hit) event otherwise GetKey() works without repeat pause.
int HGL_CALL HGL_Impl::Input_GetKey() {
	/*auto out = 0;
	if(gainputKeysBufferCount > 0) {
		// Last key press is first key in state
		out = gainputKeyToHGL[gainputKeysBuffer[0].buttonId];
	}*/
	if(gainputKeysBufferCount > 0) {
		for(unsigned i = 0; i < gainputKeysBufferCount; ++i) {
			const int key = gainputKeyToHGL[(int)gainputKeysBuffer[i].buttonId];
			if (Input_KeyDown(key)) {
				return key;
			}
		}
	}
	return 0;
}

// TODO: Returns the last pressed character since previous call to frame function.
// Returns the character code. If no key was pressed or there's no corresponding character, returns 0.
// Input_GetChar translates the pressed key code to character considering the current keyboard states 
// and input locale. 
int HGL_CALL HGL_Impl::Input_GetChar() {
	// TODO: This is hacked in for now, it should use inputKeyboard->SetTextInputEnabled() and GetNextCharacter()
	// to respect locale and device state.
	// For now it just stupidly returns last found key that can be converted to a valid character.
	char out_c = 0;
	for (unsigned i = 0; i < gainputKeysBufferCount; ++i) {
		unsigned idx = static_cast<unsigned>(gainputKeysBuffer[i].buttonId);
		if (gainputKeyToChar[idx] >= ' ' && gainputKeyToChar[idx] <= '~') {
			out_c = gainputKeyToChar[idx];
		}
	}
	return out_c;
}

// Returns true if given pad id is available via InputManager
bool HGL_CALL HGL_Impl::Input_IsPadAvailable(hglGamePad_t pad) {
	assert(spInputManager && pad >= GAMEPAD_1 && pad <= GAMEPAD_COUNT);
	if(pad >= GAMEPAD_1 && pad <= GAMEPAD_COUNT && inputPad[pad]) {
		bool out = inputPad[pad]->IsAvailable() && inputPad[pad]->GetState() == gainput::InputDevice::DeviceState::DS_OK;
		return out;
	}
	return false;
}
#pragma endregion

#pragma region "Action Map"
///////////////////////////////////////////////////////////////// Action Map Action_*
HACTION	HGL_CALL HGL_Impl::Action_Create(const char *name) {
	assert(name && strlen(name) > 0);
	if(name && strlen(name) > 0) {
		HGL_Action* act = new HGL_Action();
		act->name = name;
		uNextActionId++; // First action id = 1
		act->id = uNextActionId;
		spActionList->push_back(act);
		return static_cast<HACTION>(act);
	}
	return nullptr;
}

void HGL_CALL HGL_Impl::Action_ResetBindings(HACTION action) {
	assert(action);
	if(action) {
		auto act = static_cast<HGL_Action*>(action);
		if(act->numBindings > 0) {
			spInputMap->Unmap(act->id);
		}
	}
}

void HGL_CALL HGL_Impl::Action_Free(HACTION action) {
	assert(action);
	auto ifind = std::find(spActionList->begin(), spActionList->end(), action);
	if(ifind != spActionList->end()) {
		HGL_Action* act = static_cast<HGL_Action*>(action);
		delete act;
		spActionList->erase(ifind);
	}
}

void HGL_CALL HGL_Impl::Action_FreeAll() {
	for(auto a : *spActionList.get()) {
		delete a;
	}
	spActionList->clear();
}

hglAction_t HGL_CALL HGL_Impl::Action_GetState(HACTION action) {
	assert(action);
	if(action) {
		auto act = static_cast<HGL_Action*>(action);
		if(act->numBindings) {
			bool prev = spInputMap->GetBoolPrevious(act->id);
			bool curr = spInputMap->GetBool(act->id);
			if(!prev && curr) {
				return HGLA_HIT;
			} else if(prev && curr) {
				return HGLA_PRESSED;
			} else if(prev && !curr) {
				return HGLA_RELEASED;
			}
		}
	}
	return HGLA_UP;
}

float HGL_CALL HGL_Impl::Action_GetFloat(HACTION action) {
	assert(action);
	if(action) {
		auto act = static_cast<HGL_Action*>(action);
		if(act->numBindings) {
			return spInputMap->GetFloat(act->id);
		}
	}
	return 0.0f;
}

void HGL_CALL HGL_Impl::Action_AddKey(HACTION action, hglKeyCode_t keycode) {
	assert(action && keycode > HGLM_MBUTTON);
	if(action) {
		HGL_Action* act = static_cast<HGL_Action*>(action);
		spInputMap->MapBool(act->id, diKeyboard, HGLKeyToGainput[keycode]);
		act->numBindings++;
	}
}

void HGL_CALL HGL_Impl::Action_AddMouseButton(HACTION action, hglKeyCode_t buttoncode) {
	assert(action && buttoncode >= HGLM_LBUTTON && buttoncode <= HGLM_MBUTTON);
	if(action) {
		HGL_Action* act = static_cast<HGL_Action*>(action);
		spInputMap->MapBool(act->id, diMouse, HGLKeyToGainput[buttoncode]);
		act->numBindings++;
	}
}

void HGL_CALL HGL_Impl::Action_AddPadButton(HACTION action, hglPadCode_t buttoncode) {
	assert(action && buttoncode > HGLP_NO_INPUT);
	if(action) {
		HGL_Action* act = static_cast<HGL_Action*>(action);
		spInputMap->MapBool(act->id, diPad[0], HGLPadToGainput[buttoncode]);
		act->numBindings++;
	}
}

void HGL_CALL HGL_Impl::Action_AddPadAxis(HACTION action, hglPadCode_t axiscode) {
	assert(action && axiscode > HGLP_NO_INPUT && axiscode <= HGLP_RT);
	if(action) {
		HGL_Action* act = static_cast<HGL_Action*>(action);
		act->analog = true;
		if(axiscode <= HGLP_YR_AXIS) { // Axis -1 to 1
			spInputMap->MapFloat(act->id, diPad[0], HGLPadToGainput[axiscode]);
		} else { // Trigger 0 to 1
			spInputMap->MapFloat(act->id, diPad[0], HGLPadToGainput[axiscode], 0.0f, 1.0f);
		}
		act->numBindings++;
	}
}

#pragma endregion

////////////////////////////////////////////////
// Notes:
//---------------------------------------------
// InputManager creates InputDevice
// Events are transferred based on InputMap
// Devices also offer direct access. Can use GetAnyButtonDown for full state list.
// Keyboard has text input mode: kbDev->SetTextInputEnabled() and kbDev->GetNextCharacter()
// Gainput treats all input devices the same way. They emit the same events.
// Mouse, Pad, touch and builtin (gyro/accelero/etc) all have their own GainputInputDevice header
// with specific constants per device.
//
// Current Game Controller Support: Only works with XBOX360 via XInput (no PS4, DInput) under steam this is fine
// but not in standalone.

#pragma endregion
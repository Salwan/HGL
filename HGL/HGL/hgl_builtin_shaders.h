// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// https://github.com/kvakvs/hge 
#pragma once

namespace HGL_BuiltinShaders {

	const char* defaultVertexShaderSource = R"VSDEFAULT(
		attribute vec3			aPosition;
		attribute vec2			aTexCoords;
		attribute lowp vec4		aColor;
		
		varying vec2			vTexCoords;
		varying lowp vec4			vColor;

		uniform mediump mat4	uTransformationMatrix;

		void main() {
			gl_Position = uTransformationMatrix * vec4(aPosition.xyz, 1.0);
			vTexCoords = aTexCoords;
			vColor = aColor.bgra; // This reads color as ARGB instead of ABGR!
		}
	)VSDEFAULT";

	const char* defaultFragmentShaderSource = R"FSDEFAULT(
		precision mediump float;
	
		varying vec2			vTexCoords;
		varying lowp vec4		vColor;

		uniform sampler2D		uTexture;

		void main() {
#ifdef BLEND_COLORADD
			vec4 texcolor = texture2D(uTexture, vTexCoords);
			gl_FragColor.rgb = vColor.rgb + texcolor.rgb;
			gl_FragColor.a = vColor.a * texcolor.a;
#else
			gl_FragColor = vColor * texture2D(uTexture, vTexCoords);
#endif
		}
	)FSDEFAULT";

};
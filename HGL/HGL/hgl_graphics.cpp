// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// https://github.com/kvakvs/hge 
#include "precomp.h"
#include "hgl_impl.h"
#ifdef HGLPLATFORM_WIN32
#include "hgl_system_win32.h"
#endif

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "hgl_builtin_shaders.h"

// Batch Size test: Rendering 0.5 MegaBunny -> Core i7 4770 and GTX 1070
/*	Batch Used					FPS Range
====================================================
	9000						26-27
	18000						28-30
	24000						29-31
	36000						30-31
	42000						31-32  < best perf on GTX 1070
	48000						23-24
	50000						23-24
	65536						23-24
*/

#pragma region "Implementation Definitions"
// Implementation Definitions
constexpr size_t		MAX_PRIMITIVE_VERTICES = 42000; // 42000 * 24 = 984.4 KB
constexpr size_t		MAX_LINE_PRIMITIVES		= MAX_PRIMITIVE_VERTICES / 2;	// 4500
constexpr size_t		MAX_TRIPLE_PRIMITIVES	= MAX_PRIMITIVE_VERTICES / 3;	// 3000
constexpr size_t		MAX_QUAD_PRIMITIVES		= MAX_PRIMITIVE_VERTICES / 4;	// 2250
constexpr unsigned		APOSITION_ATTRIB_LOC	= 1;
constexpr unsigned		ATEXCOORDS_ATTRIB_LOC	= 2;
constexpr unsigned		ACOLOR_ATTRIB_LOC		= 3;
constexpr const char	*ATTRIB_POS				= "aPosition";
constexpr const char	*ATTRIB_TEXCOORDS		= "aTexCoords";
constexpr const char	*ATTRIB_COLOR			= "aColor";
constexpr const char	*UNIFORM_TMATRIX		= "uTransformationMatrix";
constexpr const char	*UNIFORM_TEXTURE0		= "uTexture";
constexpr unsigned		ULOC_TMATRIX			= 0;
constexpr unsigned		ULOC_TEXTURE0			= 1;
constexpr float			COLOR_WHITE [4]			= { 1.0f, 1.0f, 1.0f, 1.0f };
constexpr float			COLOR_BLACK [4]			= { 0.0f, 0.0f, 0.0f, 0.0f };
constexpr float			IDENTITY_MATRIX [] = {
	1.0f, 0.0f, 0.0f, 0.0f,
	0.0f, 1.0f, 0.0f, 0.0f,
	0.0f, 0.0f, 1.0f, 0.0f,
	0.0f, 0.0f, 0.0f, 1.0f
};
#pragma endregion

#pragma region "Init"
//////////////////////////////////////////////// Init
bool HGL_Impl::_initOpenGLES() {
	glViewport(0, 0, iWidth, iHeight);
	glClearColor(GETRF(uClearColor), GETGF(uClearColor), GETBF(uClearColor), GETAF(uClearColor));

	// Future guards:
	assert(MAX_PRIMITIVE_VERTICES / 2 < 65535 && "OpenGL ES 2.0 is limited to 16-bit indices. You just exceeded that.");

	// Setup states
	// -- Disable culling
	glDisable(GL_CULL_FACE);
	// -- Disable depth testing
	glDisable(GL_DEPTH_TEST);
	// -- Texture (glTexParam is stored with texture at texture creation, so no to redo that)
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);

	// Initialize vertex buffers for batched rendering of primitives
	glGenBuffers(1, &vbPrimitives);
	glBindBuffer(GL_ARRAY_BUFFER, vbPrimitives);
	glBufferData(GL_ARRAY_BUFFER, MAX_PRIMITIVE_VERTICES * sizeof(hglVertex), nullptr, GL_DYNAMIC_DRAW);
	vPrimitivesBuffer.resize(MAX_PRIMITIVE_VERTICES);

	// Initialize index buffer to use with rendering quads, OpenGL ES 2 is limited to 16-bit indices
	// All indices are precalculated.
	glGenBuffers(1, &ibQuads);
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibQuads);
		std::vector<uint16_t> indices;
		indices.resize(MAX_PRIMITIVE_VERTICES / 2);	// = maxvs * 2(triperquad) / 4(vertperquad) = m / 2
		assert(indices.size() % 6 == 0 && "6 vertices = 1 quad, fix your code.");
		/* Pattern for Quads:

		0 -->-- 1
		. .     |
		.   .	V
		.	  . |
		3 --<-- 2
		*/
		uint16_t n = 0;
		for (unsigned i = 0; i < indices.size(); i += 6) {
			indices[i] = n;
			indices[i + 1] = n + 1;
			indices[i + 2] = n + 2;
			indices[i + 3] = n + 2;
			indices[i + 4] = n + 3;
			indices[i + 5] = n;
			n += 4;
		}
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, (GLsizeiptr)(sizeof(uint16_t) * indices.size()), indices.data(),
			GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	// Init default matrices
	_recalcProjection(iWidth, iHeight);
	matTransform = glm::mat4(1.0f);

	// Create default programs:
	std::string vs_defs = "";
	std::string fs_defs = "";
	{
		fs_defs = "#define BLEND_COLORMUL\n";
		_createDefaultProgram(programDefaultMul, vs_defs, fs_defs);
	}
	{
		fs_defs = "#define BLEND_COLORADD\n";
		_createDefaultProgram(programDefaultAdd, vs_defs, fs_defs, programDefaultMul.vertexShader);
	}

	// Blending
	// -- Set default
	iCurrentBlendMode = BLEND_DEFAULT;
	glEnable(GL_BLEND);
	// default = color mul and alpha blend and no depth wsrite
	// color mul -> done in shader
	// alpha blend -> done in blending (uses fragment shader alpha output)
	// no depth write -> GL state
	glBlendEquation(GL_FUNC_ADD); // only supported equation on Android
	{
		eCurrentSrcFactor = GL_SRC_ALPHA;
		eCurrentDestFactor = GL_ONE_MINUS_SRC_ALPHA;
		GLenum src_fact = eCurrentSrcFactor;
		GLenum dest_fact = eCurrentDestFactor;
		if (iCurrentBlendMode & BLEND_ALPHABLEND) {
			dest_fact = GL_ONE_MINUS_SRC_ALPHA;
		} else {
			dest_fact = GL_ONE;
		}
		if (iCurrentBlendMode & BLEND_DARKEN) {
			src_fact = GL_ZERO;
			dest_fact = GL_SRC_COLOR;
		} else {
			src_fact = GL_SRC_ALPHA;
			if (iCurrentBlendMode & BLEND_ALPHABLEND) {
				dest_fact = GL_ONE_MINUS_SRC_ALPHA;
			} else {
				src_fact = GL_ONE;
			}
		}
		glBlendFunc(src_fact, dest_fact);
		if (iCurrentBlendMode & BLEND_ZWRITE) {
			glDepthMask(GL_TRUE);
		} else {
			glDepthMask(GL_FALSE);
		}
	}
	// 
	System_Log("> Default program ready");

	return true;
}

// Deinit
void HGL_Impl::_destroyOpenGLES() {
	for (unsigned i = 0; i < vUserPrograms.size(); ++i) {
		// Delete all as it's the end of the application
		vUserPrograms[i]->deleteAll();
	}
	programDefaultAdd.deleteAll();
	programDefaultMul.deleteAll();
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glDeleteBuffers(1, &vbPrimitives);
	glDeleteBuffers(1, &ibQuads);
}

// Recalculates projection matrix
inline void HGL_Impl::_recalcProjection(int width, int height) {
	matProjection = glm::orthoLH<float>(0, static_cast<float>(width),
		static_cast<float>(height), 0, 0.0f, 1.0f);
}

// Updates transformation uniform which is = matProjection * matTransform
inline void HGL_Impl::_updateTransformationUniform() {
	if(!programDefaultAdd.uniforms.empty()) {
		glm::mat4 tmat = matProjection * matTransform;
		glUseProgram(programDefaultMul.program);
		glUniformMatrix4fv(programDefaultMul.uniforms[ULOC_TMATRIX], 1, GL_FALSE, glm::value_ptr(tmat));
		glUseProgram(programDefaultAdd.program);
		glUniformMatrix4fv(programDefaultAdd.uniforms[ULOC_TMATRIX], 1, GL_FALSE, glm::value_ptr(tmat));
	}
}
#pragma endregion

#pragma region "Gfx Functions"
/////////////////////////////////////////////////////////// Gfx Functions

// @param target Optional render target handle to use for rendering.
//        If 0 or omitted - the screen surface is used.
bool HGL_CALL HGL_Impl::Gfx_BeginScene(HTARGET target) {
	assert(!bBegunScene && "Gfx_BeginScene() called when scene already began");
	if (bBegunScene) {
		System_Log("Gfx_BeginScene() called when scene already began");
		strErrorMessage = "Gfx_BeginScene() called when scene already began";
		return false;
	}
	bBegunScene = true;
	if(target != hRenderTarget) {
		// Reset transformation if render target is changed
		Gfx_SetTransform();
	}
	if (target) {
		hRenderTarget = target;
		auto fbo = static_cast<HGL_RenderTarget*>(target);
		glBindFramebuffer(GL_FRAMEBUFFER, fbo->frameBuffer);
		assert(_checkGLError("Gfx_BeginScene", "Bind Framebuffer"));
		if (fbo->eViewport == HGL_VIEWPORT_DEFAULT) {
			Gfx_SetClipping(0, 0, fbo->uWidth, fbo->uHeight);
		}
	} else {
		hRenderTarget = 0;
	}
	return true;
}

void HGL_CALL HGL_Impl::Gfx_EndScene() {
	if (!bBegunScene) {
		System_Log("[WARNING] Gfx_EndScene() called without BeginScene.");
	}
	assert(bBegunScene && "Gfx_EndScene() called without BeginScene.");
	_renderPrimitives();

	// Reset framebuffer
	if (hRenderTarget) {
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		Gfx_SetClipping(0, 0, iWidth, iHeight);
		hRenderTarget = 0;
	}

	// Reset things
	uPrimitiveCount = 0;
	bBegunScene = false;
}

// Gfx_Clear function must be called between the Gfx_BeginScene and Gfx_EndScene calls.
void HGL_CALL HGL_Impl::Gfx_Clear(unsigned color) {
	assert(bBegunScene && "Gfx_Clear() called without BeginScene.");
	if (!bBegunScene) {
		System_Log("[ERROR] Gfx_Clear() called without BeginScene.");
		strErrorMessage = "Gfx_BeginScene() called when scene already began";
		return;
	}
	if (color != uClearColor) {
		glClearColor(GETRF(color), GETGF(color), GETBF(color), GETAF(color));
	}
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}


// Sets clipping region for all graphics being rendered further.
// If all the parameters are 0 or omitted, the clipping region will be reset to the whole 
// render target. When the render target is switched, the clipping region is also reset to 
// the whole render target. 
void HGL_CALL HGL_Impl::Gfx_SetClipping(int x, int y, int w, int h) {
	// Flush current batch
	if(bBegunScene && uPrimitiveCount > 0) {
		_renderPrimitives();
	}
	if(x + y + w + h == 0) {
		glViewport(0, 0, iWidth, iHeight);
	} else {
		glViewport(x, y, w, h);
	}
}

// Sets global transformation for all graphics being rendered further.
// Transformations are applied in this order: first scaling, then rotation and finally displacement. 
// If all the parameters are 0 or omitted, global transformation will be reset.When the render target 
// is switched, global transformation is also reset.
// Gfx_SetTransform flushes all accumulated graphic primitives.
/* \param x Center point X - coordinate.
 * \param y Center point Y - coordinate.
 * \param dx Center point X displacement.
 * \param dy Center point Y displacement.
 * \param rot Rotation angle in radians.
 * \param hscale Horizontal scaling factor.
 * \param vscale Vertical scaling factor.
*/
void HGL_CALL HGL_Impl::Gfx_SetTransform(float x, float y, float dx, float dy, float rot, float hscale, float vscale) {
	if(bBegunScene && uPrimitiveCount > 0) {
		_renderPrimitives();
	}
	glm::mat4 mt = glm::mat4();
	if(x + y + dx + dy + rot + hscale != 0.0f || vscale != 0.0f) {
		// -- Loop through all default programs and reset projection
		// TODO: How to handle non-default programs? (user created). Easiest option is to allow app to access
		//       new projection matrix and then set things manually in Resize function.
		mt = glm::translate(mt, glm::vec3(-x, -y, 0.0f));
		if(hscale + vscale != 0.0f) {
			mt = glm::scale(mt, glm::vec3(hscale, vscale, 1.0f));
		}
		if(rot != 0.0f) {
			mt = glm::rotate(mt, rot, glm::vec3(0.0f, 0.0f, 1.0f));
		}
		mt = glm::translate(mt, glm::vec3(x + dx, y + dy, 0.0f));
	}
	if(matTransform != mt) {
		matTransform = mt;
		// Update transformation matrix in all shaders
		_updateTransformationUniform();
	}
}

///////////////////////// Gfx_Render functions

/*
Gfx_RenderLine omits the last point of the line, so you can draw joint lines
without any additional coordinates adjustments. The color parameter includes
alpha value. See the Hardware color format section for details. If Z-buffer
isn't used, the Z-order value is ignored.
*/
void  HGL_CALL HGL_Impl::Gfx_RenderLine(float x1, float y1, float x2, float y2, unsigned color, float z) {
	if (!bBegunScene) {
		System_Log("[ERROR] Gfx_RenderLine() called without Gfx_BeginScene!");
		return;
	}
	assert(bBegunScene && "Gfx_RenderLine() called without Gfx_BeginScene!");
	// Write vertices / render perhaps
	bool batch_full = (uPrimitiveCount + 1) * 2 >= MAX_PRIMITIVE_VERTICES ? true : false;
	if (eCurrentPrimitiveType == HGL_PRIM_LINE) {
		if (batch_full) {
			_renderPrimitives();
		}
	} else {
		// primitive type changed, render our current buffer
		_renderPrimitives();
		eCurrentPrimitiveType = HGL_PRIM_LINE;
		_setBlending(BLEND_DEFAULT);	// reset blending to default
	}
	hglVertex line[2];
	line[0].x = x1; line[0].y = y1; line[0].z = z; line[0].tx = 0.0f; line[0].ty = 0.0f; line[0].col = color;
	line[1].x = x2; line[1].y = y2; line[1].z = z; line[1].tx = 0.0f; line[1].ty = 0.0f; line[1].col = color;

	// Write vertices to primitives buffer
	memcpy(&vPrimitivesBuffer[uPrimitiveCount * 2], line, sizeof(hglVertex) * 2);

	uPrimitiveCount += 1;
}

/*
Triple is the basic HGE graphic primitive. See Triple structure section for
more information about triples. If tex field of triple is 0, white color is
used as texture data. Also note that each of 3 triple's vertices can have it's
own color, alpha and Z-order. Triples are automatically adjusted to allow
perfect rendering of joint quads without any additional screen and texture
coordinates tweaking.
*/
void  HGL_CALL HGL_Impl::Gfx_RenderTriple(const hglTriple *triple) {
	assert(bBegunScene && "Gfx_RenderTriple() called without Gfx_BeginScene!");
	if (!bBegunScene) {
		System_Log("[ERROR] Gfx_RenderTriple() called without Gfx_BeginScene!");
		return;
	}
	// Write vertices / render when needed
	bool batch_full = (uPrimitiveCount + 1) * 3 >= MAX_PRIMITIVE_VERTICES ? true : false;
	if (eCurrentPrimitiveType == HGL_PRIM_TRIPLE &&
		triple->tex == hCurrentTexture &&
		triple->blend == iCurrentBlendMode) {
		if (batch_full) {
			_renderPrimitives();
		}
	} else {
		_renderPrimitives();
		eCurrentPrimitiveType = HGL_PRIM_TRIPLE;
		hCurrentTexture = triple->tex;
		_setBlending(triple->blend);
	}

	// Write vertices to primitives buffer
	memcpy(&vPrimitivesBuffer[uPrimitiveCount * 3], triple->v, sizeof(hglVertex) * 3);

	uPrimitiveCount += 1;
}

/*
Quad is the basic HGE graphic primitive. See Quad structure section for more
information about quads. If tex field of quad is 0, white color is used as
texture data. Also note that each of 4 quad's vertices can have it's own color,
alpha and Z-order. Quads are automatically adjusted to allow perfect rendering
of joint quads without any additional screen and texture coordinates tweaking.
*/
void HGL_CALL HGL_Impl::Gfx_RenderQuad(const hglQuad *quad) {
	if (!bBegunScene) {
		System_Log("[ERROR] Gfx_RenderQuad() called without Gfx_BeginScene!");
		return;
	}
	assert(bBegunScene && "Gfx_RenderQuad() called without Gfx_BeginScene!");

	bool batch_full = (uPrimitiveCount + 1) * 4 >= MAX_PRIMITIVE_VERTICES ? true : false;
	if (eCurrentPrimitiveType == HGL_PRIM_QUAD &&
		quad->tex == hCurrentTexture &&
		quad->blend == iCurrentBlendMode) {
		if (batch_full) {
			_renderPrimitives();
		}
	} else {
		_renderPrimitives();
		eCurrentPrimitiveType = HGL_PRIM_QUAD;
		hCurrentTexture = quad->tex;
		_setBlending(quad->blend);
	}

	// Write vertices to primitives buffer
	memcpy(&vPrimitivesBuffer[uPrimitiveCount * 4], quad->v, sizeof(hglVertex) * 4);
	// This dropped cost from ~50% of runtime to 1% but only in Debug/Profiling, otherwise it's actually slower!
	//static hglVertex* primitives_buffer_data = vPrimitivesBuffer.data();
	//memcpy(primitives_buffer_data + (uPrimitiveCount * 4), quad->v, sizeof(hglVertex) * 4);

	uPrimitiveCount += 1;
}

// TODO: Make StartBatch/FinishBatch use own primitives buffer and state info so it doesn't 
//       disrupt normal rendering at all.
hglVertex* HGL_CALL HGL_Impl::Gfx_StartBatch(int prim_type, HTEXTURE tex, int blend, int *max_prim) {
	assert(bBegunScene && "Gfx_StartBatch() called without Gfx_BeginScene!");
	assert(!bBegunBatch && "Gfx_StartBatch() called before finishing previous batch.");
	if (!bBegunScene) {
		System_Log("[ERROR] Gfx_StartBatch() called without Gfx_BeginScene!");
		return nullptr;
	}

	// Flush rendering batch
	_renderPrimitives();

	// If successful, returns pointer to the vertex buffer to be filled with vertices and stores 
	// the maximum allowed number of primitives into max_prim. Otherwise returns 0.
	eCurrentPrimitiveType = static_cast<HGL_PrimType>(prim_type);
	hCurrentTexture = tex;
	iCurrentBlendMode = blend;
	*max_prim = MAX_PRIMITIVE_VERTICES / prim_type;
	bBegunBatch = true;

	return vPrimitivesBuffer.data();
}

void HGL_CALL HGL_Impl::Gfx_FinishBatch(int nprim) {
	assert(bBegunScene && "Gfx_FinishBatch() called without Gfx_BeginScene!");
	assert(bBegunBatch && "Gfx_FinishBatch() called without Gfx_BeginBatch!");
	assert(nprim <= MAX_PRIMITIVE_VERTICES / eCurrentPrimitiveType);
	if (nprim > 0) {
		uPrimitiveCount = nprim;
		_renderPrimitives();
	}
	bBegunBatch = false;
}

#pragma endregion

#pragma region "Shaders Functions"

// TODO: Only Fragment shaders are supported. Do I required Vertex shader support?
HSHADER	HGL_CALL HGL_Impl::Shader_Create(const char *filename) {
	assert(filename && strlen(filename) > 0 && "Invalid shader filename");
	using namespace std;
	string desc;
	{
		ostringstream ss;
		ss << "Custom shader \"" << filename << "\"";
		desc = ss.str();
	}
	// Open and Load shader code
	string shader_source;
	ifstream ifs(filename, ios_base::in);
	if (ifs.good() && ifs.is_open()) {
		// File exists, load
		stringstream ss;
		ss << ifs.rdbuf();
		shader_source = ss.str();
		assert(!shader_source.empty() && "Failed to load shader file");
		ifs.close();
	} else {
		ostringstream str;
		str << "Error: Shader_Create() failed to open/load shader code file: " << filename;
		_postError(str.str().c_str());
		assert(false && "Failed to open shader file");
		return nullptr;
	}
	// Compile and link everything
	HGL_ShaderProgram* sr = new HGL_ShaderProgram();
	std::string empty_str("");
	bool result = _createNewProgram(desc.c_str(), *sr, empty_str, empty_str, 
		programDefaultMul.vertexShader, -1, nullptr, shader_source.c_str());
	if (result) {
		vUserPrograms.push_back(sr);
		return static_cast<HSHADER>(sr);
	} else {
		delete sr;
		return nullptr;
	}
}

void HGL_CALL HGL_Impl::Shader_Free(HSHADER shader) {
	assert(shader);
	if (shader) {
		HGL_ShaderProgram* sr = static_cast<HGL_ShaderProgram*>(shader);
		// Delete program and fragment shader but not vertex shader
		glDeleteProgram(sr->program);
		glDeleteShader(sr->fragmentShader);
		// Delete actual object
		auto ifind = std::find(vUserPrograms.begin(), vUserPrograms.end(), sr);
		if (ifind != vUserPrograms.end()) {
			vUserPrograms.erase(ifind);
		}
	}
}

// Set custom fragment shader or null to use builtin fragment shaders.
void HGL_CALL HGL_Impl::Gfx_SetShader(HSHADER shader) {
	// Verify shader is legit
	if (shader) {
		HGL_ShaderProgram *sr = static_cast<HGL_ShaderProgram*>(shader);
		auto ifind = std::find(vUserPrograms.begin(), vUserPrograms.end(), 
			sr);
		assert(ifind != vUserPrograms.end() && sr->program && sr->fragmentShader);
		if(ifind == vUserPrograms.end() || !sr->program || !sr->fragmentShader) {
			_postError("Error: Gfx_SetShader given invalid program object. Using default instead.");
			pCurrentUserShader = nullptr;
			return;
		}
	}
	// Use default, if program is different render all first.
	if(bBegunScene && uPrimitiveCount > 0 && shader != pCurrentUserShader) {
		_renderPrimitives();
	}
	pCurrentUserShader = shader;
}

int	HGL_CALL HGL_Impl::Gfx_GetWidth() const {
	return iWidth;
}

int	HGL_CALL HGL_Impl::Gfx_GetHeight() const {
	return iHeight;
}

#pragma endregion

#pragma region "Handlers"
/////////////////////////////////////////////////////////////// Handlers
bool HGL_Impl::_checkGLError(const char* functionLastCalled, const char *extra_error_title) {
	GLenum last_error = glGetError();
	if(last_error != GL_NO_ERROR) {
		std::ostringstream errmsg;
		std::ostringstream errtitle;
		const char *glerrmsg = nullptr;
		for (unsigned i = 0; i < GLErrors.size(); i+=2) {
			// Careful here GLErrors items will have a different size in x86 and x64.
			unsigned err_code = static_cast<unsigned>((uint64_t)(GLErrors[i]) & 0xFFFFFFFFLL);
			if (err_code == last_error) { 
				glerrmsg = GLErrors[i + 1];
				break;
			}
		}
		errtitle << (extra_error_title? extra_error_title : "Error") << ": " << functionLastCalled;
		errmsg << functionLastCalled << "() failed (0x" << std::hex << last_error;
		if (glerrmsg) {
			errmsg << " = " << glerrmsg;
		}
		errmsg << "): " << (extra_error_title? extra_error_title : ".");
		System_Log(errmsg.str().c_str());
		strErrorMessage = errmsg.str();
		MessageBoxA(nullptr, errmsg.str().c_str(), errtitle.str().c_str(), MB_OK | MB_ICONEXCLAMATION);
		return false;
	}
	return true;
}

void HGL_Impl::_handleShaderCompileError(GLuint shader, const char *shader_desc) {
	if(shader) {
		int info_log_len, chars_written;
		glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &info_log_len);
		std::vector<char> info_log;
		info_log.resize(info_log_len);
		glGetShaderInfoLog(shader, info_log_len, &chars_written, info_log.data());
		std::ostringstream errtitle;
		std::ostringstream errmsg;
		errtitle << "Compilation Failed: " << shader_desc;
		errmsg << "Failed to compile shader: " << shader_desc << "\nReason: "<< (info_log_len > 1? info_log.data() : "(No Information)");
		System_Log(errmsg.str().c_str());
		strErrorMessage = errmsg.str();
		MessageBoxA(nullptr, errmsg.str().c_str(), errtitle.str().c_str(), MB_OK | MB_ICONEXCLAMATION);
	}
}

void HGL_Impl::_handleProgramLinkError(GLuint program, const char *program_desc) {
	if(program) {
		int info_log_len, chars_written;
		glGetProgramiv(program, GL_INFO_LOG_LENGTH, &info_log_len);
		std::vector<char> info_log;
		info_log.resize(info_log_len);
		glGetShaderInfoLog(program, info_log_len, &chars_written, info_log.data());
		std::ostringstream errtitle;
		std::ostringstream errmsg;
		errtitle << "Linking Failed: " << program_desc;
		errmsg << "Failed to link program: " << program_desc << "\nReason: " << (info_log_len > 1 ? info_log.data() : "(No Information)");
		System_Log(errmsg.str().c_str());
		strErrorMessage = errmsg.str();
		MessageBoxA(nullptr, errmsg.str().c_str(), errtitle.str().c_str(), MB_OK | MB_ICONEXCLAMATION);
	}
}

// TODO: right now WM_RESIZE messages come continuesly as you scale the window. I need to implement a
//       way to detect dragging to resize, pausing the game until resizing is finished then resuming.
//       This way resizing doesn't disrupt gameplay.
void HGL_Impl::_graphics_OnResize(int new_width, int new_height) {
	System_Log("Window Resized to: %dx%d", new_width, new_height);

	// Reset viewport
	Gfx_SetClipping(0, 0, new_width, new_height);

	// Reset view/projection matrices in all programs
	_recalcProjection(new_width, new_height);
	_updateTransformationUniform();

	// Process framebuffers
	// TODO: any active framebuffers will have to be recreated to match the new size but
	//	only when that's required. Three ways to deal with this (modes?):
	//		a.keep size (not a good idea I think)
	//		b.scale with resize(power of 2 requirement could be a problem)
	//		c.match screen size always
	//	>>> Feature: handle arbitrary texture ratio in Texture_Create() so that it internally
	//	gets init as pow-of-2 but used directly as WxH. Implement bOriginal option in 
	//  Texture_GetWidth / Height() to handle access to actual size.
}
#pragma endregion

#pragma region "Shaders internal functions"
/////////////////////////////////////////////////////////// Shaders
bool HGL_Impl::_compileShader(GLuint &shader_out, const char *shader_source, GLenum shader_type, 
							  const char *desc) {
	assert(shader_source && strlen(shader_source) > 10 && 
		(shader_type == GL_FRAGMENT_SHADER || shader_type == GL_VERTEX_SHADER) && "Something is given wrong");
	GLint status;
	GLuint so = glCreateShader(shader_type);
	glShaderSource(so, 1, &shader_source, NULL);
	glCompileShader(so);
	glGetShaderiv(so, GL_COMPILE_STATUS, &status);
	if (status != GL_TRUE) {
		_handleShaderCompileError(so, desc);
		std::ostringstream ss;
		if(desc) {
			ss << "_initOpenGLGLES() Failed to compile " << desc;
		} else {
			ss << "_compileShader() failed while compiling " << 
				(shader_type == GL_FRAGMENT_SHADER? "Fragment Shader" : "Vertex Shader");
		}
		throw std::runtime_error(ss.str());
		return false;
	}
	assert(status == GL_TRUE && "Failed to compile shader");
	shader_out = so;
	return true;
}

bool HGL_Impl::_createNewProgram(const char *program_desc, HGL_ShaderProgram &program_out,
								 std::string& vs_defs, std::string& fs_defs,
								 int compiled_vs_shader, int compiled_fs_shader,
								 const char* vs_source, const char *fs_source) {
	if (compiled_vs_shader != -1) {
		assert(vs_defs.empty() && "You are passing defs to customize vertex shader with an already compiled shader");
		if (!vs_defs.empty()) {
			_postError("ERROR: _createNewProgram() You are passing defs to customize vertex shader with an already compiled shader");
			return false;
		}
	} else {
		assert(vs_source && "You are not passing a compiled vertex shader and no vertex shader source to compile");
		if (!vs_source) {
			_postError("ERROR: _createNewProgram() You are not passing a compiled vertex shader and no vertex shader source to compile");
			return false;
		}
	}
	if (compiled_fs_shader != -1) {
		assert(fs_defs.empty() && "You are passing defs to customize fragment shader with an already compiled shader");
		if (!fs_defs.empty()) {
			_postError("ERROR: _createNewProgram() You are passing defs to customize fragment shader with an already compiled shader");
			return false;
		}
	} else {
		assert(fs_source && "You are not passing a compiled fragment shader and no fragment shader source to compile");
		if (!fs_source) {
			_postError("ERROR: _createNewProgram() You are not passing a compiled fragment shader and no fragment shader source to compile");
			return false;
		}
	}
	System_Log("Creating %s..", program_desc);
	if (!vs_defs.empty()) {
		System_Log("Vertex Shader defines = %s", vs_defs.c_str());
	}
	if (!fs_defs.empty()) {
		System_Log("Fragment Shader defines = %s", fs_defs.c_str());
	}
	GLint status;
	bool result;
	// -- Vertex Shader
	if (compiled_vs_shader == -1) {
		std::string vstr;
		if (vs_defs.empty()) {
			vstr = vs_source;
		} else {
			vstr = vs_defs + vs_source;
		}
		const char *vsc = vstr.c_str();
		std::string pd = program_desc;
		pd += ": Vertex Shader";
		result = _compileShader(program_out.vertexShader, vsc, GL_VERTEX_SHADER, pd.c_str());
		assert(result);
		System_Log("> Vertex Shader ready");
	} else {
		program_out.vertexShader = compiled_vs_shader;
	}
	// -- Fragment Shader
	if (compiled_fs_shader == -1) {
		std::string fstr;
		if (fs_defs.empty()) {
			fstr = fs_source;
		} else {
			fstr = fs_defs + fs_source;
		}
		const char *fsc = fstr.c_str();
		std::string pd = program_desc;
		pd += ": Fragment Shader";
		result = _compileShader(program_out.fragmentShader, fsc, GL_FRAGMENT_SHADER, pd.c_str());
		assert(result);
		System_Log("> Fragment Shader ready");
	} else {
		program_out.fragmentShader = compiled_fs_shader;
	}
	// -- Program (at this point, both VS and FS are guaranteed to be compiled successfully)
	program_out.program = glCreateProgram();
	glAttachShader(program_out.program, program_out.vertexShader);
	glAttachShader(program_out.program, program_out.fragmentShader);

	// -- Shader specific code
	// ---- aPosition
	glBindAttribLocation(program_out.program, APOSITION_ATTRIB_LOC, ATTRIB_POS);
	result = _checkGLError("glBindAttribLocation()", "Binding Attrib: aPosition");
	assert(result);
	if (!result) {
		throw std::runtime_error("glBindAttribLocation() failed to bind aPosition attrib");
		//return false;
	}
	// ---- aTexCoords
	glBindAttribLocation(program_out.program, ATEXCOORDS_ATTRIB_LOC, ATTRIB_TEXCOORDS);
	result = _checkGLError("glBindAttribLocation()", "Binding attrib: aTexCoords");
	assert(result);
	if (!result) {
		throw std::runtime_error("glBindAttribLocation() failed to bind aTexCoords attrib");
		//return false;
	}
	// ---- aColor
	glBindAttribLocation(program_out.program, ACOLOR_ATTRIB_LOC, ATTRIB_COLOR);
	result = _checkGLError("glBindAttribLocation()", "Binding attrib: aColor");
	assert(result);
	if (!result) {
		throw std::runtime_error("glBindAttribLocation() failed to bind aColor attrib");
		//return false;
	}
	System_Log("> Program init and binding complete, linking..");

	// -- Link program
	glLinkProgram(program_out.program);
	glGetProgramiv(program_out.program, GL_LINK_STATUS, &status);
	if (status != TRUE) {
		_handleProgramLinkError(program_out.program, program_desc);
		throw std::runtime_error("_createNewProgram() Failed to link program");
		//return false;
	}
	glUseProgram(program_out.program);
	System_Log("> Program ready\n");

	// -- Uniforms
	program_out.uniforms.resize(2);
	int uloc = -1;
	// ---- transformation matrix
	uloc = glGetUniformLocation(program_out.program, UNIFORM_TMATRIX);
	assert(uloc >= 0 && "_createNewProgram() failed to get uniform location for uTransformationMatrix (required)");
	if (uloc < 0) {
		std::string err = "_createNewProgram() failed to get uniform location for uTransformationMatrix (required)";
		System_Log(err.c_str());
		strErrorMessage = err;
		throw std::runtime_error(err.c_str());
		//return false;
	}
	program_out.uniforms[ULOC_TMATRIX] = uloc;
	// Set to screen projection:
	_recalcProjection(iWidth, iHeight);
	glUniformMatrix4fv(program_out.uniforms[ULOC_TMATRIX], 1, GL_FALSE, glm::value_ptr(matProjection * matTransform));
	// ---- texture 0
	uloc = glGetUniformLocation(program_out.program, UNIFORM_TEXTURE0);
	assert(uloc >= 0 && "_createNewProgram() failed to get uniform location for uTexture (required)");
	if (uloc < 0) {
		std::string err = "_createNewProgram() failed to get uniform location for uTexture (required)";
		System_Log(err.c_str());
		strErrorMessage = err;
		throw std::runtime_error(err.c_str());
		//return false;
	}
	program_out.uniforms[ULOC_TEXTURE0] = uloc;
	// Set to texture 0
	glUniform1i(program_out.uniforms[ULOC_TEXTURE0], 0);
	return true;
}

bool HGL_Impl::_createDefaultProgram(HGL_ShaderProgram &program, std::string& vs_defs, std::string& fs_defs,
									 int compiled_vs_shader, int compiled_fs_shader) {
	return _createNewProgram("Default Program", program, vs_defs, fs_defs, compiled_vs_shader, compiled_fs_shader, 
		HGL_BuiltinShaders::defaultVertexShaderSource, HGL_BuiltinShaders::defaultFragmentShaderSource);
}


#pragma endregion

#pragma region "Rendering"
//////////////////////////////////////////////////////////////////// Rendering
void HGL_Impl::_setBlending(int new_blend) {
	if(new_blend != iCurrentBlendMode) {
		GLenum src_fact = eCurrentSrcFactor;
		GLenum dest_fact = eCurrentDestFactor;
		if (new_blend & BLEND_DARKEN) {
			if (new_blend & BLEND_COLORADD) {
				// D3DTOP_BLENDCURRENTALPHA
				// Linearly blend this texture stage, using the alpha taken from the previous texture stage
				// TODO: This is tested but no idea if it's working correctly or not.
				src_fact = GL_DST_ALPHA;
				dest_fact = GL_ONE;
			} else {
				src_fact = GL_ZERO;
				dest_fact = GL_SRC_COLOR;
			}
		} else {
			if (new_blend & BLEND_ALPHABLEND) {
				src_fact = GL_SRC_ALPHA;
				dest_fact = GL_ONE_MINUS_SRC_ALPHA;
			} else {
				src_fact = GL_SRC_ALPHA;
				dest_fact = GL_DST_ALPHA;
			}
		}
		glBlendFunc(src_fact, dest_fact);
		if((new_blend & BLEND_ZWRITE) != (iCurrentBlendMode & BLEND_ZWRITE)) {
			if(new_blend & BLEND_ZWRITE) {
				glDepthMask(GL_TRUE);
			} else {
				glDepthMask(GL_FALSE);
			}
		}
		iCurrentBlendMode = new_blend;
		eCurrentSrcFactor = src_fact;
		eCurrentDestFactor = dest_fact;
	}
}

void HGL_Impl::_renderPrimitives() {
	glBindBuffer(GL_ARRAY_BUFFER, vbPrimitives);
	if(uPrimitiveCount > 0) {
		bool use_whitetexture = false; // Zero texture for additive, White texture for multiplicative (to retain vertex colors)
		// Program
		HGL_ShaderProgram* active_program;
		if(!pCurrentUserShader) {
			if(iCurrentBlendMode & BLEND_COLORADD) {
				active_program = &programDefaultAdd;
			} else {
				active_program = &programDefaultMul;
				use_whitetexture = true;
			}
		} else {
			active_program = static_cast<HGL_ShaderProgram*>(pCurrentUserShader);
		}
		glUseProgram(active_program->program);
		// Textures
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, _getGLTexture(hCurrentTexture, use_whitetexture));
		glUniform1i(active_program->uniforms[ULOC_TEXTURE0], 0);
		// Setting up vertex attrib array
		glEnableVertexAttribArray(APOSITION_ATTRIB_LOC);
		glVertexAttribPointer(APOSITION_ATTRIB_LOC, 3, GL_FLOAT, false, sizeof(hglVertex), 0);
		glEnableVertexAttribArray(ATEXCOORDS_ATTRIB_LOC);
		glVertexAttribPointer(ATEXCOORDS_ATTRIB_LOC, 2, GL_FLOAT, false, sizeof(hglVertex),
			(void*)(sizeof(float) * 3));
		glEnableVertexAttribArray(ACOLOR_ATTRIB_LOC);
		glVertexAttribPointer(ACOLOR_ATTRIB_LOC, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(hglVertex),
			(void*)(sizeof(float) * (3 + 2)));
		// Rendering
		if(eCurrentPrimitiveType == HGL_PRIM_LINE) {
			glBufferSubData(GL_ARRAY_BUFFER, (GLintptr)0, (GLsizeiptr)sizeof(hglVertex) * 2 * uPrimitiveCount,
				vPrimitivesBuffer.data());
			glDrawArrays(GL_LINES, 0, uPrimitiveCount * 2);
		} else if(eCurrentPrimitiveType == HGL_PRIM_TRIPLE) {
			glBufferSubData(GL_ARRAY_BUFFER, (GLintptr)0, (GLsizeiptr)sizeof(hglVertex) * 3 * uPrimitiveCount,
				vPrimitivesBuffer.data());
			glDrawArrays(GL_TRIANGLES, 0, uPrimitiveCount * 3);
		} else if(eCurrentPrimitiveType == HGL_PRIM_QUAD) {
			// The glBuffer* call is the current rendering bottleneck:
			//glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr)sizeof(hglVertex) * 4 * uPrimitiveCount,
			//	NULL, GL_STREAM_DRAW);
			//glBufferSubData(GL_ARRAY_BUFFER, (GLintptr)0, (GLsizeiptr)sizeof(hglVertex) * 4 * uPrimitiveCount,
			//	vPrimitivesBuffer.data());
			glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr)sizeof(hglVertex)*4*uPrimitiveCount,
				vPrimitivesBuffer.data(), GL_DYNAMIC_DRAW);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibQuads);
			glDrawElements(GL_TRIANGLES, uPrimitiveCount * 6, GL_UNSIGNED_SHORT, (void*)0);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		} else {
			assert(false && "wtf?");
		}
	}
	uPrimitiveCount = 0;
}
#pragma endregion

// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// https://github.com/kvakvs/hge 
#include "precomp.h"
#include "hgl_impl.h"

#ifdef HGLPLATFORM_WIN32
#include "hgl_system_win32.h"
#endif

// Destructor
#pragma region "Destructor"
HGL_Impl::~HGL_Impl() {
	if(bSystemInitiated) {
		System_Shutdown();
	}
}
#pragma endregion

// TODO: Implement HGL Resize callback event.
// TODO: Implement HGL Focus Lost callback event.
// TODO: Implement HGL Focus gained callback event.
// TODO: Implement HGL Exit callback event.
// TODO: Implement mouse hiding/showing via boolean state HGL_HIDEMOUSE
// TODO: Implement HGL_ICON state to customize application icon.
// TODO: Implement fullscreen support. (requires HGL_RESIZE support)
// TODO: Implement HGL_DONTSUSPEND to continue runtime when app loses focus.
// TODO: Implement HGL_HWND and HGL_HWNDPARENT. Setting hwnd parent turns on child mode.
// TODO: Implement HGL_POWERSTATUS as read only of power source and battery level. (already done in code)

////////////////////////////// System
#pragma region "System"
bool HGL_Impl::System_Initiate() {
	assert(!bSystemInitiated);
	bool result;
	// -- Init Platform
	result = _initPlatform();
	if (!result) {
		strErrorMessage = "System_Initiate() failed: _initPlatform() failed";
		return false;
	}
	// -- Init Log
	// by default:
	// - Windows/Visual Studio	-> Debug: Uses visual studio output
	//							-> Release: Uses default log file
	result = _initLog();
	if(!result) {
		strErrorMessage = "System_Initiate() failed: _initLog() failed";
		return false;
	}
	// -- Init Display
	result = _initDisplay();
	if (!result) {
		strErrorMessage = "System_Initiate() failed: _initDisplay(), no fullscreen support.";
	}
	// -- Init Timers
	result = _initTimers();
	if (!result) {
		strErrorMessage = "System_Initiate() failed: _initTimers() failed";
		return false;
	}
	// -- Init windowing
	result = _initWindow();
	if(!result) {
		strErrorMessage = "System_Initiate() failed: _initWindow() failed";
		return false;
	}
	// -- Init input
	result = _initInput();
	if (!result) {
		strErrorMessage = "System_Initiate() failed: _initInput() failed";
		return false;
	}
	// -- Init writer thread
	result = _initWriterThread();
	if (!result) {
		strErrorMessage = "System_Inititate() failed: _initWriterThread() failed";
		return false;
	}
	// -- Init Resource System
	result = _initResourceSystem();
	if (!result) {
		strErrorMessage = "System_Initiaite() failed: _initResourceSystem() failed";
		return false;
	}
	// -- Init EGL
	result = _initEGL();
	if(!result) {
		std::ostringstream ss;
		ss << "System_Initiate() failed: _initEGL() failed: " << eglGetError();
		strErrorMessage = ss.str();
		return false;
	}
	// -- Init OpenGL ES
	result = _initOpenGLES();
	if (!result) {
		strErrorMessage = "_initOpenGLES() failed";
		return false;
	}
	// -- Init Audio
	result = _initAudio();
	if(!result) {
		strErrorMessage = "_initAudio() failed, audio unavailble";
		bAudioAvailable = false;
	}
	// -- Init Textures
	result = _initTextures();
	if (!result) {
		strErrorMessage = "_initTextures() failed";
		return false;
	}
	bSystemInitiated = true;
	System_Log("\n[[[[[**** APPLICATION BEGINS ****]]]]]\n");
	return result;
}
void HGL_Impl::System_Shutdown() {
	// TODO: Restore Video Mode if fullscreen
	// TODO: Free all allocated resources
	assert(bSystemInitiated);
	System_Log("\n[[[[[**** APPLICATION ENDS ****]]]]]\n");
	// Destroy Audio
	_destroyAudio();
	// Destroy Input
	_destroyInput();
	// Destroy Window
	System_Log("Shutting down Window..");
	// Destroy Timers
	_destroyTimers();
	// Destroy Writer thread
	_destroyWriterThread();
	// Destroy Resource Manager
	_destroyResourceSystem();
	// Destroy Render Targets
	_destroyRenderTargets();
	// Destroy Textures
	_destroyTextures();
	// Destroy OpenGL ES
	_destroyOpenGLES();
	// Destroy EGL
	_destroyEGL();
	// Destroy Window
	HGLPlatform::ShutdownWindow();
	// Destroy Display
	_destroyDisplay();
	// Destroy platform
	_destroyPlatform();
	//
	bSystemInitiated = false;
}

// HGL game loop
bool HGL_Impl::System_Start() {
	assert(bSystemInitiated && "HGL used but System is not initiated");
	if(!cbFrameFunc) {
		strErrorMessage = "System_Start failed: no FrameFunc callback defined!";
		return false;
	}
#ifdef _DEBUG
	_performTesting();
#endif
	// TODO: game should pause entirely when it loses focus.
	//		 easiest way to pause: just wait for reactivate!
	while(!bAppQuit) {
		// Platform loop stuff (message handling)
		if(!HGLPlatform::AppLoop()) {
			break;
		}

		// Input and timer always run even if paused

		_updateInput();
		_updateTimer();

		if(!bAppSuspended) {
			// Frame Function
			if(cbFrameFunc) {
				if(cbFrameFunc()) {
					bAppQuit = true;
				}
			}

			// Render Function
			if(cbRenderFunc) {
				cbRenderFunc();
			}

			//---------------
			// FPS processing
			//---------------
			// Explanation Table:
			// FPS Mode				Performance Mode	FPS Sleep?		Zero Sleep?
			//---------------------------------------------------------------------
			// VSYNC				AUTO and LAPTOP		No				Yes
			// VSYNC				HIGH				No				No
			// UNLIMITED			AUTO and LAPTOP		No				Yes
			// UNLIMITED			HIGH				No				No
			// > 1					AUTO and LAPTOP		Yes				Yes
			// > 1					HIGH				Yes				No

			// iFPS decides which fps to run
			// iPerformanceMode is a special case of unlimited fps to force limited fps on laptops
			bool limit_fps = false;
			bool zero_sleep = false;
			if (iFPS == HGLFPS_VSYNC || iFPS == HGLFPS_UNLIMITED) {
				// When FPS=unlimited & Performance=high, no zero sleep and no limitation
				// And when FPS == UNLIMITED && Performance = Laptop, zero sleep
				if (iPerformanceMode != PERFORMANCE_HIGH) {
					zero_sleep = true;
				}
			} else {
				// Performance mode ignored as limitation overrides it
				limit_fps = true;
			}
			if (limit_fps) {			
				// The above always causes one frame to have a delta of near zero and another
				// to have 1 big delta causing visible jerkyness.
			
				// Limiting fps is performed on accumulated time deltas up to 1.0 second.
				// This means that if sleep in one frame slept only N-1 ms, it will sleep
				// N+1 next frame and so on.
				// This looks accurate and stable enough to be usable.
				static double actual_accum = 0.0;
				static double fps_accum = 0.0;
				double actual_delta = Timer_GetDeltaNS();
				double fps_delta = 1.0 / static_cast<double>(iFPS);
				actual_accum += actual_delta;
				fps_accum += fps_delta;
				if(actual_accum < fps_accum) { 
					unsigned sleep_period =
						static_cast<unsigned>((fps_accum - actual_accum) * 1000.0);
					std::this_thread::sleep_for(std::chrono::milliseconds(sleep_period));
				} else if (PERFORMANCE_HIGH) {
					std::this_thread::sleep_for(std::chrono::milliseconds(0));
				}
				if (actual_accum >= 1.0) {
					actual_accum -= 1.0;
					fps_accum -= 1.0;
				}
			
			} else if (zero_sleep) {
				std::this_thread::sleep_for(std::chrono::milliseconds(0));
			}
		} else {
			std::this_thread::sleep_for(std::chrono::milliseconds(25)); // ~30 Hz when paused
		}

		// Update timer in timer thread (potentially runs with eglSwapBuffers)
		_tickTimer();

		// Swap to display
		if(!bAppSuspended) {
			EGLBoolean result = eglSwapBuffers(eglDisplay, eglSurface);
			if(!result) {
				EGLint err = eglGetError();
				if(err != EGL_SUCCESS) {
					System_Log("eglSwapBuffers failed: %x", err);
					std::ostringstream ss;
					ss << "eglSwapBuffers failed: " << err;
					HGLPlatform::ShowErrorMessage(ss.str().c_str());
					return false;
				}
			}
		}

		// TODO: handle child mode
		// TODO: FIXME! gainput doesn't send WM_MOUSEWHEEL so I'm handling it in WM_MOUSEWHEEL for now.
		//       reseting this value should be in hgl_input.cpp when gainput works.
		iMouseWheel = 0;
	}
	return true;
}
#pragma endregion

////////////////////////////// System -> Set State
#pragma region "System -> Set State"
void HGL_Impl::System_SetState(hglBoolState state, bool value) {
	switch(state) {
		case hglBoolState::HGL_WINDOWED:
			// If mode changed:
			if (bWindowed != value) {
				if (bWindowed) {
					_goWindowed();
				} else {
					_goFullscreen();
				}
			}
			bWindowed = value;
			break;
		case hglBoolState::HGL_USESOUND:
			bAudio = value;
			// TODO: unmute
			break;
		case hglBoolState::HGL_HIDEMOUSE:
			bMouseCursor = !value;
			HGLPlatform::SetMouseCursorVisible(bMouseCursor);
			break;
		// TODO: all other bool states and their gets
		default:
			;
	}
}

void HGL_Impl::System_SetState(hglFuncState state, hglCallback value) {
	switch(state) {
		case hglFuncState::HGL_FRAMEFUNC:
			cbFrameFunc = value;
			break;
		case hglFuncState::HGL_RENDERFUNC:
			cbRenderFunc = value;
			break;
		case hglFuncState::HGL_GFXRESTOREFUNC:
			cbRestoreFunc = value;
			break;
		case hglFuncState::HGL_RESIZEFUNC:
			cbResizeFunc = value;
			break;
		case hglFuncState::HGL_FOCUSGAINFUNC:
			cbFocusGainFunc = value;
			break;
		case hglFuncState::HGL_FOCUSLOSTFUNC:
			cbFocusLostFunc = value;
			break;
		case hglFuncState::HGL_EXITFUNC:
			cbExitFunc = value;
			break;
		case hglFuncState::HGL_SUSPENDFUNC:
			cbSuspendFunc = value;
			break;
		case hglFuncState::HGL_RESUMEFUNC:
			cbResumeFunc = value;
			break;
		default:
			;
	}
}

void HGL_Impl::System_SetState(hglHwndState, void*) {
	// TODO: all hwnd states and their gets
	/*switch(state) {
		default:
			;
	}*/
}

void HGL_Impl::System_SetState(hglIntState state, int value) {
	switch(state) {
		case HGL_SCREENWIDTH:
			iWidth = value;
			iConfigWidth = value;
			break;
		case HGL_SCREENHEIGHT:
			iHeight = value;
			iConfigHeight = value;
			break;
		case HGL_SCREENBPP:
			iDepth = value;
			break;
		case HGL_FPS:
			assert(iFPS >= -1);
			iFPS = value;
			// SetState could be called before system initiated. iFPS will be used when
			// EGL is initialized in this case.
			if(bSystemInitiated) {
				if(iFPS == HGLFPS_UNLIMITED) {
					eglSwapInterval(eglDisplay, 0);
				} else if (iFPS == HGLFPS_VSYNC) {
					eglSwapInterval(eglDisplay, 1);
				} else {
					// manually hold fps
					eglSwapInterval(eglDisplay, 0);
				}
			}
			break;
		case HGL_PERFORMANCEMODE:
			iPerformanceMode = value;
			break;
		// TODO: all int states and their gets
		default:
			;
	}
}

void HGL_Impl::System_SetState(hglStringState state, const char *value) {
	switch(state) {
		case hglStringState::HGL_TITLE:
			strTitle = value;
			HGLPlatform::SetWindowTitle(strTitle);
			break;
		case hglStringState::HGL_LOGFILE:
			strLogFile = value;
			if(!strLogFile.empty()) {
				FILE *hf;
				errno_t result = fopen_s(&hf, strLogFile.c_str(), "w");
				if(result || !hf) {
					strLogFile = "";
				} else {
					fclose(hf);
				}
			}
			break;
		case hglStringState::HGL_INIFILE: {
				strIniFile = value;
				bool result = _initIniFile(strIniFile.c_str());
				if (!result) {
					System_Log("Failed to parse Ini file %s, reason: %s", value, strErrorMessage.c_str());
				}
				break;
			}
		// TODO: all other string states and their gets
		default:
			;
	}
}
#pragma endregion

////////////////////////////// System -> Get State
#pragma region "System -> Get State"
bool HGL_Impl::System_GetState(hglBoolState state) {
	switch(state) {
		case hglBoolState::HGL_WINDOWED:
			return bWindowed;
		case hglBoolState::HGL_USESOUND:
			return bAudio;
		case hglBoolState::HGL_HIDEMOUSE:
			return !bMouseCursor;
		default:
			;
	}
	return false;
}
hglCallback HGL_Impl::System_GetState(hglFuncState state) {
	switch(state) {
		case hglFuncState::HGL_FRAMEFUNC:
			return cbFrameFunc;
		case hglFuncState::HGL_RENDERFUNC:
			return cbRenderFunc;
		case hglFuncState::HGL_GFXRESTOREFUNC:
			return cbRestoreFunc;
		case hglFuncState::HGL_RESIZEFUNC:
			return cbResizeFunc;
		case hglFuncState::HGL_FOCUSLOSTFUNC:
			return cbFocusLostFunc;
		case hglFuncState::HGL_FOCUSGAINFUNC:
			return cbFocusGainFunc;
		case hglFuncState::HGL_EXITFUNC:
			return cbExitFunc;
		case hglFuncState::HGL_SUSPENDFUNC:
			return cbSuspendFunc;
		case hglFuncState::HGL_RESUMEFUNC:
			return cbResumeFunc;
		default:
			;
	}
	return nullptr;
}

void* HGL_Impl::System_GetState(hglHwndState) {
	/*switch(state) {
		default:
			;
	}*/
	return nullptr;
}

int	HGL_Impl::System_GetState(hglIntState state) {
	switch(state) {
		case HGL_SCREENWIDTH:
			return iWidth;
		case HGL_SCREENHEIGHT:
			return iHeight;
		case HGL_SCREENBPP:
			return iDepth;
		case HGL_FPS:
			return iFPS;
		case HGL_PERFORMANCEMODE:
			return iPerformanceMode;
		default:
			;
	}
	return 0;
}

const char* HGL_Impl::System_GetState(hglStringState state) {
	switch(state) {
		case HGL_TITLE:
			return strTitle.c_str();
		case HGL_LOGFILE:
			return strLogFile.c_str();
		case HGL_INIFILE:
			return strIniFile.c_str();
		default:
			;
	}
	return "";
}
#pragma endregion

////////////////////////////////////// System -> Utils
#pragma region "System -> Utils"
const char* HGL_Impl::System_GetErrorMessage() {
	return strErrorMessage.c_str();
}

void HGL_Impl::System_Log(const char *format, ...) {
	va_list args;
	va_start(args, format);
	System_Log(format, args);
	va_end(args);
}

void HGL_Impl::System_Log(const char *format, va_list args) {
	FILE *hf = nullptr;
	if(strLogFile.empty()) {
		return;
	}
	errno_t result = fopen_s(&hf, strLogFile.c_str(), "a");
	if(result || !hf) {
		return;
	}
	if(hf) {
		vfprintf(hf, format, args);
		fprintf(hf, "\n");
		fclose(hf);
	}
}



void HGL_Impl::_postError(const char *error_msg)
{
	System_Log(error_msg);
	strErrorMessage = error_msg;
}
#pragma endregion

//////////////////////////////////////////// Init
#pragma region "Init"

bool HGL_Impl::_initLog() {
	// By setting: LOGFILE you'll initialize a log file anytime.
	// This on the other hand will set: clog/cerr to VS debug window when in VS.
	HGLPlatform::InitStdLog();
	System_Log("HGL Started..\n");
	System_Log("HGL version: %X.%X", HGL_VERSION >> 8, HGL_VERSION & 0xff);
	HGLPlatform::LogSystemInfo(this, strTitle.c_str());
	return true;
}

bool HGL_Impl::_initWindow() {
	bool result = HGLPlatform::InitWindow();
	return result;
}

#pragma endregion

///////////////////////////////////////////// Init -> Platform
#pragma region "Init -> Platform"

bool HGL_Impl::_initPlatform() {
	return HGLPlatform::InitPlatform(this);
}

void HGL_Impl::_destroyPlatform() {
	HGLPlatform::DestroyPlatform();
}
#pragma endregion

///////////////////////////////////////////// Init -> Display
#pragma region "Init -> Display"

bool HGL_Impl::_initDisplay() {
	return HGLPlatform::EnumDisplayModes();
}

void HGL_Impl::_destroyDisplay() {
	// Restore display mode if changed
	// Clear all memory used by display modes
	if (systemDisplayMode.platformMode) {
		delete systemDisplayMode.platformMode;
	}
	for (unsigned i = 0; i < vDisplayModes.size(); ++i) {
		if (vDisplayModes[i]) {
			if(vDisplayModes[i]->platformMode) {
				delete vDisplayModes[i]->platformMode;
			}
			delete vDisplayModes[i];
		}
	}
	vDisplayModes.clear();
}
#pragma endregion

///////////////////////////////////////////// Init -> EGL
#pragma region "Init -> EGL"

bool HGL_Impl::_initEGL() {
	bool result = _createEGLDisplay();
	if (!result) {
		std::cerr << "createEGLDisplay() failed" << std::endl;
		return false;
	}
	result = _chooseEGLConfig();
	if (!result) {
		std::cerr << "chooseEGLConfig() failed" << std::endl;
		return false;
	}
	result = _createEGLSurface();
	if (!result) {
		std::clog << "createEGLSurface() failed" << std::endl;
		return false;
	}
	result = _setupEGLContext();
	if (!result) {
		std::clog << "setupEGLContext() failed" << std::endl;
		return false;
	}
	return result;
}

void HGL_Impl::_destroyEGL() {
	System_Log("Shutting down EGL..");
	if(eglDisplay) {
		eglMakeCurrent(eglDisplay, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
		eglTerminate(eglDisplay);
	}
}

bool HGL_Impl::_createEGLDisplay() {
	// EGL Get Display
	eglDisplay = eglGetDisplay(HGLPlatform::hDC);
	if(eglDisplay == EGL_NO_DISPLAY) {
		std::cerr << "eglGetDisplay() found no display! retrying with default.." << std::endl;
		eglDisplay = eglGetDisplay(EGL_DEFAULT_DISPLAY);
		if(eglDisplay == EGL_NO_DISPLAY) {
			std::cerr << "eglGetDisplay() returned no display" << std::endl;
			return false;
		}
	}
	// EGL Initialize
	EGLBoolean result = eglInitialize(eglDisplay, &iEGLVerMaj, &iEGLVerMin);
	if(result == EGL_FALSE) {
		std::cerr << "eglInitialize() failed" << std::endl;
		return false;
	}
	System_Log("EGL display created successfully");
	return true;
}

bool HGL_Impl::_chooseEGLConfig() {
	constexpr EGLint config_attribs[] = {
		EGL_SURFACE_TYPE,			EGL_WINDOW_BIT,
		EGL_RENDERABLE_TYPE,		EGL_OPENGL_ES2_BIT,
		EGL_CONFORMANT,				EGL_OPENGL_ES2_BIT,	// Allow ES2 context specifically
		EGL_ALPHA_SIZE,				1,					// Largest alpha preferred (should be 8)
		EGL_BIND_TO_TEXTURE_RGBA,	EGL_TRUE,			// Ability to bind RGBA required (default: dont care)
		EGL_COLOR_BUFFER_TYPE,		EGL_RGB_BUFFER,		// RGB or Luminance
		EGL_RED_SIZE,				1,
		EGL_GREEN_SIZE,				1,
		EGL_BLUE_SIZE,				1,
		EGL_LUMINANCE_SIZE,			0,
		EGL_DEPTH_SIZE,				24,					// Smallest non-zero depth preferred (>=24)
		EGL_STENCIL_SIZE,			1,					// Smallest non-zero stencil preferred (should = 8)
		EGL_NONE
	};
	EGLint configs_count = 0;
	EGLBoolean result = eglChooseConfig(eglDisplay, config_attribs, &eglConfig, 1, &configs_count);
	if(result == EGL_FALSE) {
		std::cerr << "eglChooseConfig() failed" << std::endl;
		return false;
	}
	System_Log("EGL config ready");
	{
		int pixel_size; // R+G+B+A
		int transparent_type;
		int alpha_size;
		int depth_size;
		int stencil_size;
		result = eglGetConfigAttrib(eglDisplay, eglConfig, EGL_BUFFER_SIZE, &pixel_size);
		assert(result && "Failed to retrieve EGL config buffer size");
		result = eglGetConfigAttrib(eglDisplay, eglConfig, EGL_TRANSPARENT_TYPE, &transparent_type);
		assert(result && "Failed to retrieve EGL config transparent type");
		result = eglGetConfigAttrib(eglDisplay, eglConfig, EGL_ALPHA_SIZE, &alpha_size);
		assert(result && "Failed to retrieve EGL config alpha size");
		result = eglGetConfigAttrib(eglDisplay, eglConfig, EGL_DEPTH_SIZE, &depth_size);
		assert(result && "Failed to retrieve EGL config depth size");
		result = eglGetConfigAttrib(eglDisplay, eglConfig, EGL_STENCIL_SIZE, &stencil_size);
		assert(result && "Failed to retrieve EGL config stencil size");
		System_Log("    Pixel Size = %d\n    Transparent Type = %s", pixel_size,
			transparent_type == GL_FALSE? "False" : "True");
		System_Log("    Alpha Size = %d\n    Depth Size = %d\n    Stencil Size = %d", alpha_size,
			depth_size, stencil_size);
	}
	return true;
}

bool HGL_Impl::_createEGLSurface() {
	eglSurface = eglCreateWindowSurface(eglDisplay, eglConfig, HGLPlatform::hWnd, NULL);
	if(eglSurface == EGL_NO_SURFACE) {
		std::cout << "Failed to create surface" << std::endl;
		// attempt creating a non-window surface
		eglGetError();
		eglSurface = eglCreateWindowSurface(eglDisplay, eglConfig, NULL, NULL);
		if(eglSurface == EGL_NO_SURFACE) {
			std::cerr << "Could not create EGL surface: " << eglGetError() << std::endl;
			return false;
		}
		System_Log("Non-window surface created (native window passed as null)");
	} else {
		System_Log("EGL surface ready");
	}
	EGLint last_err = eglGetError();
	if(last_err != EGL_SUCCESS) {
		std::cerr << "Something went wrong while creating window surface: " << last_err << std::endl;
		return false;
	}
	return true;
}

bool HGL_Impl::_setupEGLContext() {
	// Bind API
	EGLBoolean result = eglBindAPI(EGL_OPENGL_ES_API);
	if(result == EGL_FALSE) {
		std::cerr << "eglBindAPI() failed, error code = " << eglGetError() << std::endl;
		return false;
	} else if(result == EGL_BAD_PARAMETER) {
		std::cerr << "eglBindAPI() returned BAD_PARAMETER" << std::endl;
		return false;
	} else {
		System_Log("OpenGL ES API bound");
	}
	// Create Context
	constexpr EGLint context_attribs[] = {
		EGL_CONTEXT_CLIENT_VERSION, 2,
		EGL_NONE
	};
	eglContext = eglCreateContext(eglDisplay, eglConfig, NULL, context_attribs);
	if(eglContext == EGL_NO_CONTEXT) {
		std::cerr << "eglCreateContext() failed" << std::endl;
		return false;
	}
	// Make Current Context
	System_Log("EGL context ready\n");
	result = eglMakeCurrent(eglDisplay, eglSurface, eglSurface, eglContext);
	if(result != EGL_TRUE) {
		std::cerr << "eglMakeCurrent() failed, EGL error: " << eglGetError() << std::endl;
		return false;
	}
	// Setup default swap interval (controls refresh rate vsync)
	// Default: as fast as possible == HGLFPS_UNLIMITED
	if(iFPS != HGLFPS_VSYNC) {
		eglSwapInterval(eglDisplay, 0);
	} else {
		eglSwapInterval(eglDisplay, 1);
	}
	// Log some useful info
	System_Log("EGL Version   = %s", eglQueryString(eglDisplay, EGL_VERSION));
	System_Log("EGL Vendor    = %s", eglQueryString(eglDisplay, EGL_VENDOR));
	System_Log("EGL Client APIs: \n  %s", eglQueryString(eglDisplay, EGL_CLIENT_APIS));
	std::string ext;
	std::stringstream ext_stream;
	ext_stream << eglQueryString(eglDisplay, EGL_EXTENSIONS);
	System_Log("EGL Extensions:");
	while(ext_stream >> ext) {
		System_Log("  %s", ext.c_str());
	}
	return true;
}
#pragma endregion

//////////////////////////////////////////// Init -> OALS
#pragma region "Init -> OpenGLSoft"

bool HGL_Impl::_initAudio() {
	// OpenALSoft is the library of choice for audio playback on all devices
	bool result = _createOALSDevice();
	if (!result) {
		std::cerr << "_createOALSDevice() failed" << std::endl;
		return false;
	}
	result = _setupOALSContext();
	if (!result) {
		std::cerr << "_setupOALSContext() failed" << std::endl;
		if (alDevice) {
			alcCloseDevice(alDevice);
		}
		return false;
	}
	result = _setupOALSListener();
	if (!result) {
		std::cerr << "_setupOALSListener() failed (shouldn't)" << std::endl;
		alcMakeContextCurrent(NULL);
		if (alContext) {
			alcDestroyContext(alContext);
		}
		if (alDevice) {
			alcCloseDevice(alDevice);
		}
		return false;
	}

	// Any additional non-OpenALSoft initialization and setup
	_initExtraAudio();

	return true;
}

bool HGL_Impl::_createOALSDevice() {
	System_Log("\nAudio Started");
	System_Log("Audio system: OpenALSoft");

	// Enumeration
	ALboolean enumeration = alcIsExtensionPresent(NULL, "ALC_ENUMERATION_EXT");
	if(enumeration == AL_FALSE) {
		System_Log("ALC enumeration not supported");
		const char *default_device = alcGetString(NULL, ALC_DEFAULT_DEVICE_SPECIFIER);
		if(default_device) {
			System_Log("Default audio device: %s", default_device);
		} else {
			return false;
		}
	} else {
		System_Log("ALC enumeration is supported");
		const char *devices = alcGetString(NULL, ALC_DEVICE_SPECIFIER);
		if(devices) {
			std::vector<std::string> device_list;
			// When enumeration is not supported only one device (default) is here
			// Devices have \0 separater and list ends with \0\0
			while(devices[0] != '\0') {
				size_t len = strlen(devices);
				if(len > 0) {
					device_list.push_back(std::string(devices));
					devices += len + 1;
				}
			}
			System_Log("OpenALSoft found %d compatible audio devices:", device_list.size());
			for(unsigned i = 0; i < device_list.size(); ++i) {
				System_Log("  - Device #%d: %s", i, device_list[i].c_str());
			}
		}
	}

	// Open and init a device with default settings
	alDevice = alcOpenDevice(NULL);
	if(!alDevice) {
		std::clog << "initAL() Could not open a device" << std::endl;
		System_Log("Error initializing OpenALSoft..");
		ALenum err = alGetError();
		if(err != AL_NO_ERROR) {
			System_Log("OpenALSoft error: %s", alGetString(err));
		}
		return false;
	}

	System_Log("OpenALSoft device created successfully");

	alcGetIntegerv(alDevice, ALC_MAJOR_VERSION, 1, &iALVerMaj);
	alcGetIntegerv(alDevice, ALC_MINOR_VERSION, 1, &iALVerMin);
	System_Log("OpenALSoft version = %d.%d", iALVerMaj, iALVerMin);

	alcGetIntegerv(alDevice, ALC_FREQUENCY, 1, &iALSampleRate);
	System_Log("Device sample rate  = %d", iALSampleRate);

	return true;
}

bool HGL_Impl::_setupOALSContext() {
	alContext = alcCreateContext(alDevice, NULL);
	if(!alContext) {
		std::clog << "initAL() Could not create audio device context" << std::endl;
		System_Log("Error create OpenAL context..");
		ALenum err = alGetError();
		if(err != AL_NO_ERROR) {
			System_Log("OpenALSoft error: %s", alGetString(err));
		}
		return false;
	}
	ALCboolean result = alcMakeContextCurrent(alContext);
	if(!result) {
		std::cerr << "initAL() Could not activate context" << std::endl;
		System_Log("Error activating OpenAL device context..");
		ALenum err = alGetError();
		if(err != AL_NO_ERROR) {
			System_Log("OpenALSoft error: %s", alGetString(err));
		}
		if(alContext != NULL) {
			alcDestroyContext(alContext);
		}
		alcCloseDevice(alDevice);
		return false;
	}
	System_Log("OpenALSoft context activated");
	return true;
}

bool HGL_Impl::_setupOALSListener() {
	// Setting listener 3D information to origin stationary
	alListener3f(AL_VELOCITY, 0.0f, 0.0f, 0.0f);
	alListener3f(AL_POSITION, 0.0f, 0.0f, 0.0f);
	constexpr float orientation[] = {0.0f, 0.0f, -1.0f}; // Note: AL_ORIENTATION doesn't support 3f!
	alListenerfv(AL_ORIENTATION, orientation);
	System_Log("OpenALSoft listener initialized");
	return true;
}

bool HGL_Impl::_initExtraAudio() {
	return true;
}
#pragma endregion

//////////////////////////////////////////////// Pausing/Resuming
#pragma region "Pausing/Resuming"

void HGL_Impl::_suspend() {
	if(!bAppSuspended) {
		bAppSuspended = true;
		System_Log("[Runtime] Engine Suspended");
		// Suspend all audio
		_audio_Suspend();
		// Call user suspend function
		if(cbSuspendFunc) {
			cbSuspendFunc();
		}
		// Restore mouse cursor if hidden
		if(!bMouseCursor) {
			HGLPlatform::SetMouseCursorVisible(true);
		}
	}
}

void HGL_Impl::_resume() {
	if(bAppSuspended) {
		bAppSuspended = false;
		System_Log("[Runtime] Engine Resumed");
		// Hide mouse cursor if it was previously hidden by app
		if(!bMouseCursor) {
			HGLPlatform::SetMouseCursorVisible(false);
		}
		// Resume all audio
		_audio_Resume();
		// Call user resume function
		if(cbResumeFunc) {
			cbResumeFunc();
		}
	}
}

void HGL_Impl::_focusGain() {
	if(bAppSuspended) {
		// TODO: audio and networking may need to do something here to handle reactivation
		_resume();
		if(cbFocusGainFunc) {
			cbFocusGainFunc();
		}
	}
}

void HGL_Impl::_focusLost() {
	if(!bAppSuspended) {
		// TODO: audio and networking may need to do something here to handle deactivation
		_suspend();
		if(cbFocusLostFunc) {
			cbFocusLostFunc();
		}
	}
}

#pragma endregion

//////////////////////////////////////////////// Events Handling
#pragma region "Events Handling"

void HGL_Impl::_handleResize(int new_width, int new_height) {
	if(bSystemInitiated) {
		_graphics_OnResize(new_width, new_height);
		_input_OnResize(new_width, new_height);
	}
	iWidth = new_width;
	iHeight = new_height;
}

void HGL_Impl::_handleFocusChange(bool activating) {
	assert(bAppSuspended == activating);
	if(activating) {
		// Focus gain
		_focusGain();
	} else {
		// Focus lost
		_focusLost();
	}
}

#pragma endregion

//////////////////////////////////////////////// Display Mode
#pragma region "Display Modes"

// TODO: what happens if screen width/height were changed after a game started and went fullscreen?
//       ideally this is when game changes settings on the fly so it should be handled by HGL.

bool HGL_Impl::_toggleDisplayMode() {
	bool result;
	if (bWindowed) {
		result = _goFullscreen();
		bWindowed = !result;
	} else {
		result = _goWindowed();
		bWindowed = result;
	}
	return result;
}

bool HGL_Impl::_goFullscreen() {
	// Check fullscreen resolution support.
	bool result = _resolutionSupportsFullscreen(iWidth, iHeight);
	assert(result);
	if(result) {
		// Set new display mode via HGLPlatform
		HGL_DisplayMode *selected = _selectDisplayModeByRefreshRate(iWidth, iHeight, 60);
		assert(selected);
		if (!selected) {
			_postError("Error: Could not go fullscreen, reason: no appropriate refresh rate was found for this mode");
			return false;
		}
		// If native resolution matches then use that instead (so no display mode change is needed)
		if(selected->width == systemDisplayMode.width && selected->height == systemDisplayMode.height) {
			selected = &systemDisplayMode;
		}
		HGLPlatform::GoFullscreen(selected);
	}
	if(!result) {
		strErrorMessage = "Could not go fullscreen as resolution is not supported by device";
	}
	return result;
}

bool HGL_Impl::_goWindowed() {
	if (bWindowCreated && !bWindowed) {
		return HGLPlatform::GoWindowed();
	}
	return false;
}

bool HGL_Impl::_resolutionSupportsFullscreen(int width, int height) {
	for (unsigned i = 0; i < vDisplayModes.size(); ++i) {
		if(width == vDisplayModes[i]->width && height == vDisplayModes[i]->height) {
			return true;
		}
	}
	return false;
}

HGL_DisplayMode* HGL_Impl::_selectDisplayModeByRefreshRate(int width, int height, int preferred_refresh_rate,
										   int min_refresh_rate) {
	HGL_DisplayMode *outdm = nullptr;
	for (unsigned i = 0; i < vDisplayModes.size(); ++i) {
		if (width == vDisplayModes[i]->width && height == vDisplayModes[i]->height) {
			if (vDisplayModes[i]->refreshRate == preferred_refresh_rate) {
				return vDisplayModes[i];
			} else if (vDisplayModes[i]->refreshRate >= min_refresh_rate) {
				// This will select the highest available mode until preferred refresh rate
				outdm = vDisplayModes[i];
			}
		}
	}
	return outdm;
}

#pragma endregion

//////////////////////////////////////////////// Resource System
#pragma region "Resource System"

bool HGL_Impl::_initResourceSystem() {
	return true;
}

void HGL_Impl::_destroyResourceSystem() {
	// TODO: clear all loaded HGL_Resource resources in resourcesList
}

#pragma endregion

//////////////////////////////////////////////// Testing
#pragma region "Testing"

void HGL_Impl::_performTesting() {

}

#pragma endregion

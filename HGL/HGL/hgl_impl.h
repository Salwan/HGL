// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// https://github.com/kvakvs/hge 
#pragma once
#include "hgl.h"
#include "hgl_errors.h"

#if defined(HGLPLATFORM_WIN32) && defined(_MSC_VER)
// Disable warning: Deprecation.
// This warning is always emitted when using GetVersionEx() and when using stdio 
// non-secure functions. Secure stdio is part of MS's VS implementation and not standard yet.
#pragma warning(disable : 4996)
// Disable warning: symbol will be dynamically initialized (implementation limitation)
// VS2015.1 has a bug that leads it to fire sometimes when an initializer_list is used.
#pragma warning(disable : 4592)
// Disable warning: names struct in union not in standard, it's now standard in C++11
// This is for GLM
#pragma warning(disable : 4201)
// Disable warning: while I want to use them, unfortunately *_s safe io functions like
// strcpy_s are not standard.
#pragma warning(disable : 4996)
// Disable warning: unreferenced formal parameter.
// Just to save my sanity. There will be hundreds of this warning until HGL is feature complete.
#pragma warning(disable : 4100)
#endif

#include <glm/glm.hpp>

// Rendering
struct HGL_ShaderProgram {
	HGL_ShaderProgram() : vertexShader(0), fragmentShader(0), program(0) {}
	~HGL_ShaderProgram() {}
	void deleteAll() {
		if (program != 0) glDeleteProgram(program);
		if (vertexShader != 0) glDeleteShader(vertexShader);
		if (fragmentShader != 0) glDeleteShader(fragmentShader);
	}
	GLuint	vertexShader;
	GLuint	fragmentShader;
	GLuint	program;
	std::vector<int> uniforms;
};
// Value represents num of vertices per primitive
enum HGL_PrimType : unsigned {
	HGL_PRIM_NONE = 0,
	HGL_PRIM_LINE = 2,
	HGL_PRIM_TRIPLE = 3,
	HGL_PRIM_QUAD = 4,
};

// Timer data
struct HGL_TimerThread {
	HGL_TimerThread() : bTickTimerThread(false),
		bEndTimerThread(false), duration_ns(0) {}
	std::thread thread;
	std::mutex mutex;
	std::condition_variable cvar;
	bool bTickTimerThread;
	bool bEndTimerThread;
	std::atomic_llong duration_ns;
};
struct HGL_SystemTimer {
	HGL_SystemTimer() : duration(0.0), timeAccum(0.0), frameCounter(0), FPS(60) {}
	double duration;
	double timeAccum;
	int frameCounter;
	int FPS;
private: // Disable move assignment
	HGL_SystemTimer& operator=(HGL_SystemTimer&&) {}
};

// Sound effects
struct HGL_SoundEffect {
	HGL_SoundEffect(ALuint buffer, ALuint *sources) : alBuffer(buffer),
		fGain{0.0f, 0.0f, 0.0f, 0.0f}, iSourceWalker(0) {
		memcpy(alSources, sources, sizeof(ALuint) * 4);
	}
	ALuint alBuffer;	// AL Buffer
	ALuint alSources[4];	// AL Source (will be up to 4 sources when multi-sound playing is implemented)
	float fGain[4];		// Gain for channel
	int iSourceWalker;		// goes from 0 to 3 when all channels are playing (otherwise first channel will
							// always be chosen which might sound weird? this is more elegant.
};
typedef std::list<HGL_SoundEffect*> HGL_SoundEffectsList;
typedef std::vector<ALuint> HGL_SuspendedSources; // used for engine suspend/resume

// Textures
struct HGL_Texture {
	HGL_Texture(GLuint gl_texture, const char* filename, unsigned width, unsigned height, unsigned channels,
		unsigned gl_width = 0, unsigned gl_height = 0)
		: texture(gl_texture), strSource(filename), uWidth(width), uHeight(height), uChannels(channels),
		uGLWidth(gl_width), uGLHeight(gl_height) {
		if(gl_width == 0) uGLWidth = width;
		if(gl_height == 0) uGLHeight = height;
	}
	GLuint texture;		// GL Texture buffer
	std::string strSource;	// Source is filename for normal loading and null for loading from memory.
	unsigned uWidth;
	unsigned uHeight;
	unsigned uChannels;		// Each channel is an 8-bit component.
	unsigned uGLWidth;		// Actual OpenGL Texture width/height that's always pow-of-2
	unsigned uGLHeight;
};
typedef std::list<HGL_Texture*> HGL_TexturesList;

// Render target
enum HGL_RTViewportType {
	HGL_VIEWPORT_DEFAULT = 0,		// enables glViewport(0,0,fbo.w,fbo.h)
	HGL_VIEWPORT_NOTSET	 = 1,		// enables no viewport (no glViewport call)
};
struct HGL_RenderTarget {
	HGL_RenderTarget() : texture(0), frameBuffer(0), depthTarget(0), 
		bDepth(false), eViewport(HGL_VIEWPORT_DEFAULT) {}
	HTEXTURE texture;
	GLuint frameBuffer;
	GLuint depthTarget;
	unsigned uWidth;
	unsigned uHeight;
	bool bDepth;
	HGL_RTViewportType eViewport;
};
typedef std::list<HGL_RenderTarget*> HGL_RenderTargetsList;

// Ini parsing
enum HGL_IniLineType {
	ILT_EMPTY = 0,
	ILT_COMMENT = 1,
	ILT_KEYVALUE = 2,
};
struct HGL_IniLine {
	HGL_IniLine() : type(ILT_EMPTY), data("") {}
	HGL_IniLine(HGL_IniLineType _type, std::string _data)
		: type(_type), data(_data) {}
	HGL_IniLineType type;
	std::string data;
};
typedef std::vector<HGL_IniLine> HGL_TIniLines;
typedef std::map<std::string, size_t> HGL_TIniKeyLines;
struct HGL_IniSection {
	HGL_IniSection() {}
	HGL_TIniLines			lines;
	HGL_TIniKeyLines		keyLines;
};
typedef std::map<std::string, HGL_IniSection> HGL_TIniSections;
struct HGL_IniFile {
	HGL_IniFile() {}
	HGL_TIniSections		sections;
};

// File writer thread
// Writes files to disc when prompted. Assumes ownership of given buffer.
// TODO: Support for resource packs.
struct HGL_WriterThread {
	HGL_WriterThread() : bFlush(false), bEndThread(false) {}
	std::thread				thread;
	std::mutex				queueMutex;
	std::condition_variable cvarRun;
	std::atomic_bool		bFlush;
	std::atomic_bool		bEndThread;
};
struct HGL_FileRecord {
	HGL_FileRecord() : filename(""), data(nullptr), size(0) {}
	HGL_FileRecord(std::string _filename,  void *_data, size_t _size) 
		: filename(_filename), data(_data), size(_size) {}
	std::string		filename;
	void			*data;
	size_t			size;
};
// Queue is in hgl_fileio.cpp so that its validity doesn't depend on HGL's object
typedef std::deque<HGL_FileRecord> HGL_TFileWriteQueue;

// Display
// All modes stored are 32-bit modes. platformMode is OS specific data for display mode.
// All modes must have a frequency > 30 (this should include 59.9 and 60 as well as 75, 90, 120, etc)
// in Windows platformMode is the DEVMODE of the display mode.
struct HGL_DisplayMode {
	HGL_DisplayMode() : platformMode(nullptr) {}
	int width;
	int height;
	int refreshRate;
	std::pair<int, int> aspectRatio;
	void* platformMode;
};

// Action Map
struct HGL_Action {
	HGL_Action() : name(""), id(0), analog(false), numBindings(0) { }
	unsigned			id;
	std::string			name;
	bool				analog; // has analog input
	unsigned			numBindings;
};
typedef std::vector<HGL_Action*> HGL_TActionList;

// Resource System
struct HGL_Resource {
	HGL_Resource() { filename[0] = '\0'; password[0] = '\0'; }
	char                filename[MAX_RESOURCE_PATH];
	char                password[64];
};
typedef std::list<HGL_Resource> HGL_TResourcesList;
enum class HGL_SpecialSystemPath {
	SSP_TEMP,		
	SSP_HOME,		
	SSP_SETTINGS,	
	SSP_SAVEDATA,	// Standard save data location (Windows Vista+: %USERPROFILE%\Saved Games, 
};
constexpr unsigned MAX_ENVVAR_DATA = 1024;
// User home directory: Windws = %USERPROFILE%, Linux = $HOME or '~', OSX = '~'
// User application settings: Windows = %APPDATA%\{DeveloperName}\{AppName}, Linux = ~/.config/AppName, OSX = ~/Library/Application Support/AppName
// Temp: Windows = %TEMP% (LocalRoaming), Linux = $TEMP (/tmp), OSX = /tmp
// SaveData: Windows = %USERPROFILE%\Saved Games, Linux = ~/.AppName

class HGL_Impl : public HGL
{
public:
	explicit HGL_Impl() {
		cbFrameFunc = nullptr;
		cbRenderFunc = nullptr;
		cbRestoreFunc = nullptr;
		cbResizeFunc = nullptr;
		cbFocusLostFunc = nullptr;
		cbFocusGainFunc = nullptr;
		cbExitFunc = nullptr;

		strTitle = "HGL";
		strErrorMessage = "No Error";

		iWidth = 1280;
		iHeight = 720;
		iDepth = 32;
		iFPS = HGLFPS_UNLIMITED;
		iPerformanceMode = PERFORMANCE_AUTO;
		iWindowedPosX = 0;
		iWindowedPosY = 0;
		iConfigWidth = 0;
		iConfigHeight = 0;

		bWindowed = true;
		bWindowCreated = false;
		bDisplayModeChanged = false;
		bAudio = true;
		bMouseCursor = false;

		bSystemInitiated = false;
		bAppQuit = false;
		bAppSuspended = false;

		eglDisplay = nullptr;
		eglSurface = nullptr;
		eglConfig = nullptr;
		eglContext = nullptr;
		iEGLVerMaj = 0;
		iEGLVerMin = 0;

		bAudioAvailable = true;
		alDevice = nullptr;
		alContext = nullptr;
		iALVerMaj = 0;
		iALVerMin = 0;
		iALSampleRate = 0;

		spSoundEffectsList.reset(new HGL_SoundEffectsList);
		spSuspendedAudioSources.reset(new HGL_SuspendedSources);
		spTexturesList.reset(new HGL_TexturesList);
		spRenderTargetsList.reset(new HGL_RenderTargetsList);
		spInputManager.reset();
		spActionList.reset();
		uNextActionId = 0;

		gainputKeysBufferCount = 0;

		uClearColor = 0;
		bBegunScene = false;
		vbPrimitives = 0;
		uPrimitiveCount = 0;
		eCurrentPrimitiveType = HGL_PRIM_NONE;	// line by default
		hCurrentTexture = nullptr;
		iCurrentBlendMode = BLEND_DEFAULT;
		eCurrentSrcFactor = GL_SRC_ALPHA;
		eCurrentDestFactor = GL_ONE_MINUS_SRC_ALPHA;
		hRenderTarget = 0;
		hWhiteTexture = 0;
		pCurrentUserShader = nullptr;

		szPathString[0] = 0;
	}
	virtual ~HGL_Impl();

	// Interface
	virtual void		HGL_CALL Release() {}

	// System
	virtual bool		HGL_CALL System_Initiate();
	virtual void		HGL_CALL System_Shutdown();
	virtual bool		HGL_CALL System_Start();

	// System -> Utils
	virtual const char*	HGL_CALL System_GetErrorMessage();
	virtual void		HGL_CALL System_Log(const char *format, ...);
	virtual void		HGL_CALL System_Log(const char *format, va_list args);

	// System -> Set State
	virtual void		HGL_CALL System_SetState(hglBoolState state, bool value);
	virtual void		HGL_CALL System_SetState(hglFuncState state, hglCallback value);
	virtual void		HGL_CALL System_SetState(hglHwndState state, void* value);
	virtual void		HGL_CALL System_SetState(hglIntState state, int value);
	virtual void		HGL_CALL System_SetState(hglStringState state, const char *value);

	// System -> Get State
	virtual bool		HGL_CALL System_GetState(hglBoolState state);
	virtual hglCallback HGL_CALL System_GetState(hglFuncState state);
	virtual void*		HGL_CALL System_GetState(hglHwndState state);
	virtual int			HGL_CALL System_GetState(hglIntState state);
	virtual const char* HGL_CALL System_GetState(hglStringState state);

	// Resource system
	virtual void*       HGL_CALL Resource_Load(const char *filename, unsigned *size = 0);
	virtual void        HGL_CALL Resource_Free(void *res);
	virtual bool        HGL_CALL Resource_AttachPack(const char *filename, const char *password = 0);
	virtual void        HGL_CALL Resource_RemovePack(const char *filename);
	virtual void        HGL_CALL Resource_RemoveAllPacks();
	virtual const char* HGL_CALL Resource_MakePath(const char *filename = 0);
	virtual const char* HGL_CALL Resource_EnumFiles(const char *wildcard = 0);
	virtual const char* HGL_CALL Resource_EnumFolders(const char *wildcard = 0);

	// INI parsing
	virtual void        HGL_CALL Ini_SetInt(const char *section, const char *name, int value);
	virtual int         HGL_CALL Ini_GetInt(const char *section, const char *name, int def_val);
	virtual void        HGL_CALL Ini_SetFloat(const char *section, const char *name, float value);
	virtual float       HGL_CALL Ini_GetFloat(const char *section, const char *name, float def_val);
	virtual void        HGL_CALL Ini_SetString(const char *section, const char *name, const char *value);
	virtual const char* HGL_CALL Ini_GetString(const char *section, const char *name, const char *def_val);
	
	// Input
	virtual void        HGL_CALL Input_GetMousePos(float *x, float *y);
	virtual void        HGL_CALL Input_SetMousePos(float x, float y);
	virtual int         HGL_CALL Input_GetMouseWheel();
	virtual bool        HGL_CALL Input_IsMouseOver();
	virtual bool        HGL_CALL Input_KeyDown(int key);
	virtual bool        HGL_CALL Input_KeyUp(int key);
	virtual bool        HGL_CALL Input_GetKeyState(int key);
	virtual const char* HGL_CALL Input_GetKeyName(int key);
	virtual int         HGL_CALL Input_GetKey();
	virtual int         HGL_CALL Input_GetChar();
	virtual bool		HGL_CALL Input_IsPadAvailable(hglGamePad_t pad); 

	// Action map
	virtual HACTION		HGL_CALL Action_Create(const char *name);
	virtual void		HGL_CALL Action_ResetBindings(HACTION action);
	virtual void		HGL_CALL Action_Free(HACTION action);
	virtual void		HGL_CALL Action_FreeAll();
	virtual hglAction_t	HGL_CALL Action_GetState(HACTION action);
	virtual float		HGL_CALL Action_GetFloat(HACTION action);
	virtual void		HGL_CALL Action_AddKey(HACTION action, hglKeyCode_t keycode);
	virtual void		HGL_CALL Action_AddMouseButton(HACTION action, hglKeyCode_t buttoncode);
	virtual void		HGL_CALL Action_AddPadButton(HACTION action, hglPadCode_t buttoncode);
	virtual void		HGL_CALL Action_AddPadAxis(HACTION action, hglPadCode_t axiscode);

	// Graphics
	virtual bool        HGL_CALL Gfx_BeginScene(HTARGET target = 0);
	virtual void        HGL_CALL Gfx_EndScene();
	virtual void        HGL_CALL Gfx_Clear(unsigned color);
	virtual void        HGL_CALL Gfx_RenderLine(float x1, float y1, float x2, float y2, unsigned color = 0xFFFFFFFF, float z = 0.5f);
	virtual void        HGL_CALL Gfx_RenderTriple(const hglTriple *triple);
	virtual void        HGL_CALL Gfx_RenderQuad(const hglQuad *quad);
	virtual hglVertex*  HGL_CALL Gfx_StartBatch(int prim_type, HTEXTURE tex, int blend, int *max_prim);
	virtual void        HGL_CALL Gfx_FinishBatch(int nprim);
	virtual void        HGL_CALL Gfx_SetClipping(int x = 0, int y = 0, int w = 0, int h = 0);
	virtual void        HGL_CALL Gfx_SetTransform(float x = 0, float y = 0, float dx = 0, float dy = 0, float rot = 0, float hscale = 0, float vscale = 0);
	virtual int			HGL_CALL Gfx_GetWidth() const;
	virtual int			HGL_CALL Gfx_GetHeight() const;

	// Random
	virtual void        HGL_CALL Random_Seed(int seed = 0);
	virtual int         HGL_CALL Random_Int(int min, int max);
	virtual float       HGL_CALL Random_Float(float min = 0.0f, float max = 1.0f);

	// Timer
	virtual float       HGL_CALL Timer_GetTime();
	virtual float       HGL_CALL Timer_GetDelta();
	virtual double		HGL_CALL Timer_GetDeltaNS();
	virtual int         HGL_CALL Timer_GetFPS();

	// Channels
	virtual void        HGL_CALL Channel_SetPanning(HCHANNEL chn, int pan);
	virtual void        HGL_CALL Channel_SetVolume(HCHANNEL chn, int volume);
	virtual void        HGL_CALL Channel_SetPitch(HCHANNEL chn, float pitch);
	virtual void        HGL_CALL Channel_Pause(HCHANNEL chn);
	virtual void        HGL_CALL Channel_Resume(HCHANNEL chn);
	virtual void        HGL_CALL Channel_Stop(HCHANNEL chn);
	virtual void        HGL_CALL Channel_PauseAll();
	virtual void        HGL_CALL Channel_ResumeAll();
	virtual void        HGL_CALL Channel_StopAll();
	virtual bool        HGL_CALL Channel_IsPlaying(HCHANNEL chn);
	virtual float       HGL_CALL Channel_GetLength(HCHANNEL chn);
	virtual float       HGL_CALL Channel_GetPos(HCHANNEL chn);
	virtual void        HGL_CALL Channel_SetPos(HCHANNEL chn, float fSeconds);
	virtual void        HGL_CALL Channel_SlideTo(HCHANNEL channel, float time, int volume, int pan = -101, float pitch = -1);
	virtual bool        HGL_CALL Channel_IsSliding(HCHANNEL channel);

	// Sound effects
	virtual HEFFECT		HGL_CALL Effect_Load(const char *filename, size_t size = 0);
	virtual void		HGL_CALL Effect_Free(HEFFECT eff);
	virtual HCHANNEL	HGL_CALL Effect_Play(HEFFECT eff);
	virtual HCHANNEL	HGL_CALL Effect_PlayEx(HEFFECT eff, int volume = 100, int pan = 0, float pitch = 1.0f, bool loop = false);

	// Shaders
	virtual HSHADER		HGL_CALL Shader_Create(const char *filename);
	virtual void		HGL_CALL Shader_Free(HSHADER shader);
	virtual void		HGL_CALL Gfx_SetShader(HSHADER shader);

	// Render targets
	virtual HTARGET     HGL_CALL Target_Create(int width, int height, bool zbuffer, bool viewport = true);
	virtual void        HGL_CALL Target_Free(HTARGET target);
	virtual HTEXTURE    HGL_CALL Target_GetTexture(HTARGET target);

	// Textures
	virtual HTEXTURE    HGL_CALL Texture_Create(int width, int height);
	virtual HTEXTURE    HGL_CALL Texture_Load(const char *filename, unsigned size = 0, bool bMipmap = false);
	virtual void        HGL_CALL Texture_Free(HTEXTURE tex);
	virtual int			HGL_CALL Texture_GetWidth(HTEXTURE tex, bool bOriginal = false);
	virtual int			HGL_CALL Texture_GetHeight(HTEXTURE tex, bool bOriginal = false);
	virtual unsigned *	HGL_CALL Texture_Lock(HTEXTURE tex, bool bReadOnly = true, int left = 0, int top = 0, int width = 0, int height = 0);
	virtual void		HGL_CALL Texture_Unlock(HTEXTURE tex);

protected:
	// Rendering
	// -- Default attribute layout struct
	//    vec3 aPosition
	//    vec2 aTexCoords
	//    vec4 aColor
	unsigned				uClearColor;
	bool					bBegunScene;
	bool					bBegunBatch;
	std::vector<hglVertex>	vPrimitivesBuffer;	// Buffer for vertex batch (to avoid calling glBuffer* every quad)
	GLuint					vbPrimitives;		// Primitive Vertex Buffers
	GLuint					ibQuads;			// Index buffer for quads
	unsigned				uPrimitiveCount;
	HGL_ShaderProgram		programDefaultMul;
	HGL_ShaderProgram		programDefaultAdd;
	std::vector<HGL_ShaderProgram*> vUserPrograms;
	HSHADER					pCurrentUserShader;
	HGL_PrimType			eCurrentPrimitiveType;
	HTEXTURE				hCurrentTexture;
	int						iCurrentBlendMode;
	GLenum					eCurrentSrcFactor;
	GLenum					eCurrentDestFactor;
	HTARGET					hRenderTarget;
	glm::mat4				matProjection;
	glm::mat4				matTransform;

	// Sound effects
	std::shared_ptr<HGL_SoundEffectsList> spSoundEffectsList;
	std::shared_ptr<HGL_SuspendedSources> spSuspendedAudioSources;

	// Textures
	std::shared_ptr<HGL_TexturesList> spTexturesList;
	HTEXTURE hWhiteTexture;

	// Render targets
	std::shared_ptr<HGL_RenderTargetsList> spRenderTargetsList;
	
	// Callbacks

	// Pointer to a user-defined frame function that takes no parameters and returns bool. This function 
	// will be called each frame, put your game logic here. When you want the application to terminate, 
	// return true. Otherwise return false. If this state is not set, System_Start function call will fail. 
	// Although it can be changed at any time later.
	hglCallback cbFrameFunc;

	// Pointer to a user-defined function that will be called when the application window needs to be 
	// repainted. Put your rendering code here. The function takes no parameters and should always return 
	// false. This state may be set and changed at any time you want.
	hglCallback cbRenderFunc;

	// Pointer to a user-defined function that will be called when video memory contents is lost (eg.
	// video mode switching). All textures are restored automatically and you only need this function
	// if you use render targets. The function takes no parameters. If successful, it should return true, 
	// otherwise it should return false. This state may be set and changed at any time you want.
	hglCallback cbRestoreFunc;

	// Pointer to a user-defined function that will be called when window is resized either directly by
	// user or fullscreen/windowed. The function takes no parameters and should return false. The state
	// may be set and changed at any time you want. New width and height can be retrieved via HGL.
	hglCallback cbResizeFunc;

	// Pointer to a user-defined function that will be called when the application loses focus (only when 
	// user switches to another application, not when the main window is destroyed) or before video mode 
	// change. The function takes no parameters and should always return false. This state may be set and 
	// changed at any time you want.
	hglCallback cbFocusLostFunc;

	// Pointer to a user-defined function that will be called when the application gains focus (only when 
	// user switches back from another application, not when the main window is created). The function 
	// takes no parameters and should always return false. This state may be set and changed at any time 
	// you want.
	hglCallback cbFocusGainFunc;

	// Pointer to a user-defined function that will be called when user attempts to close the application. 
	// If you want to allow termination, this function should return true. If it returns false the 
	// application will continue to run. This state may be set and changed at any time you want.
	hglCallback cbExitFunc;

	// Pointer to a user-defined function that will be called when application window is busy doing something
	// and cannot show the game temporarily, this function should return true. Suspend will internally update
	// timer, input and a few vital systems running at lower Hz so as to not impact performance.
	hglCallback cbSuspendFunc;

	// Pointer to a user-defined function that will be called when application window runtime is resumed 
	// after suspension, this indicates that the game can now continue running.
	hglCallback cbResumeFunc;

	// Strings
	std::string strTitle;
	std::string strLogFile;
	std::string strIniFile;
	std::string strErrorMessage;

	// Application
	bool bWindowed;
	bool bWindowCreated;
	bool bDisplayModeChanged;
	bool bAudio;
	bool bAppQuit;
	bool bSystemInitiated;
	bool bMouseCursor;
	bool bAppSuspended;
	int iWidth;
	int iHeight;
	int iDepth;
	int iFPS;
	int iPerformanceMode;
	// This are mainly used for fullscreen <-> windowed switching
	int iWindowedPosX;
	int iWindowedPosY;
	// Because WM_SIZE sets iWidth/iHeight now it might change window size when going windowed/fullscreen
	// from the original mode to something weird like 1920x1174 because of taskbar/window decoration. So 
	// the correct window width/height for the app is stored here to use when going windowed.
	int iConfigWidth;
	int iConfigHeight;

	// Input data
	gainput::DeviceButtonSpec gainputKeysBuffer[16];
	gainput::DeviceButtonSpec gainputMouseBuffer[8];
	//gainput::DeviceButtonSpec gainputPadBuffer[16];
	unsigned gainputKeysBufferCount;
	unsigned gainputMouseBufferCount;
	//unsigned gainputPadBufferCount;
	bool keyboardState [HGLK_COUNT];			// Current key states
	bool keyboardStatePrev[HGLK_COUNT];			// Prev key states
	std::shared_ptr<gainput::InputManager>	spInputManager;
	std::shared_ptr<gainput::InputMap>		spInputMap;
	gainput::DeviceId diKeyboard;
	gainput::DeviceId diMouse;
	gainput::DeviceId diPad [GAMEPAD_COUNT + 1];
	gainput::InputDeviceKeyboard* inputKeyboard;
	gainput::InputDeviceMouse* inputMouse;
	gainput::InputDevicePad* inputPad [GAMEPAD_COUNT + 1];
	int iMouseX;
	int iMouseY;
	int iMouseWheel;
	std::shared_ptr<HGL_TActionList>	spActionList;
	unsigned							uNextActionId;

	// Timer data
	std::chrono::steady_clock::time_point tpInitTime;
	HGL_TimerThread sTimerThread;
	HGL_SystemTimer sActiveTimer;

	// Ini parsing
	std::shared_ptr<HGL_IniFile>	pIniFile;

	// Writer thread
	HGL_WriterThread sWriterThread;
	HGL_TFileWriteQueue deqFileWrite;

	// Resource System
	HGL_TResourcesList resourcesList;
	char szPathString [MAX_RESOURCE_PATH];

	// EGL
	EGLDisplay eglDisplay;
	EGLSurface eglSurface;
	EGLConfig eglConfig;
	EGLContext eglContext;
	EGLint iEGLVerMaj;
	EGLint iEGLVerMin;

	// Display
	std::vector<HGL_DisplayMode*> vDisplayModes;
	HGL_DisplayMode systemDisplayMode;  // Used to store system mode

	// OpenAL Soft
	bool bAudioAvailable;
	ALCdevice *alDevice;
	ALCcontext *alContext;
	ALint iALVerMaj;
	ALint iALVerMin;
	ALint iALSampleRate;

	// Init methods
	bool _initPlatform();
	bool _initTimers();
	bool _initLog();
	bool _initDisplay();
	bool _initWindow();
	bool _initInput();
	bool _initEGL();
	bool _initOpenGLES();
	bool _initAudio();
	bool _initTextures();
	bool _initIniFile(const char *filename);
	bool _initWriterThread();
	bool _initResourceSystem();
	void _postError(const char* error_msg);
	void _handleResize(int new_width, int new_height);
	void _handleFocusChange(bool activating);

	// EGL
	bool _createEGLDisplay();
	bool _chooseEGLConfig();
	bool _createEGLSurface();
	bool _setupEGLContext();

	// Audio
	bool _createOALSDevice();
	bool _setupOALSContext();
	bool _setupOALSListener();
	bool _initExtraAudio();
	void _audio_Suspend();
	void _audio_Resume();
	void _audio_Stop();

	// Engine
	void _suspend();
	void _resume();
	void _focusGain();
	void _focusLost();

	// Testing: this is to do any tests on new system required, runs in debug only
	void _performTesting();

	// Display mode methods
	bool _toggleDisplayMode();
	bool _goFullscreen();
	bool _goWindowed();
	bool _resolutionSupportsFullscreen(int width, int height);
	HGL_DisplayMode* _selectDisplayModeByRefreshRate(int width, int height, int preferred_refresh_rate = 60,
		int min_refresh_rate = -1);

	// Timer methods
	static void _mainTimer(HGL_Impl* hgl_impl);
	void _tickTimer();
	void _updateTimer();
	void _updateInput();

	// Input methods
	void _input_OnResize(int new_width, int new_height);

	// Graphics methods
	void _graphics_OnResize(int new_width, int new_height);
	bool _checkGLError(const char *functionLastCalled, const char *extra_error_title = "");
	void _handleShaderCompileError(GLuint shader, const char *shader_desc = "");
	void _handleProgramLinkError(GLuint program, const char *program_desc = "");
	void _renderPrimitives();
	void _setBlending(int new_blend);
	GLuint _getGLTexture(HTEXTURE tex, bool null_is_white = false);
	inline void _recalcProjection(int width, int height);
	inline void _updateTransformationUniform();
	// Mega function to create new programs. It can compile/link existing or new shaders
	// and set up all necessary stuff for attributes and uniforms.
	bool HGL_Impl::_createNewProgram(
		const char *program_desc, HGL_ShaderProgram &program_out,
		std::string& vs_defs, std::string& fs_defs, 
		int compiled_vs_shader = -1, int compiled_fs_shader = -1, 
		const char* vs_source = nullptr, const char *fs_source = nullptr);
	// Function to simplify creating built-in programs (deprecated)
	bool _createDefaultProgram(HGL_ShaderProgram &program, std::string& vs_defs, std::string& fs_defs,
		int compiled_vs_shader = -1, int compiled_fs_shader = -1);
	bool _compileShader(GLuint &shader_out, const char *shader_source, GLenum shader_type, 
		const char *desc = nullptr);

	// INI methods
	bool _parseIniFile(const char *filename, std::ifstream& in_stream);
	void _malformedIniFile(unsigned line, const char *file, const char *desc = nullptr);
	bool _validateKey(const char **section, const char **name);
	HGL_TIniSections::iterator _getOrCreateSection(const char *section);
	template<typename T>
	HGL_TIniKeyLines::iterator _getOrCreateKey(HGL_TIniSections::iterator& itsection, 
		const char *name, T def_val, bool &added_new_key);

	// Writer thread methods
	static void _mainWriterThread(HGL_Impl* hgl_impl);
	void _writeFile(const char *filename, void *data, size_t size);

	// Deinitializers
	void _destroyAudio();
	void _destroyInput();
	void _destroyRenderTargets();
	void _destroyTextures();
	void _destroyOpenGLES();
	void _destroyEGL();
	void _destroyDisplay();
	void _destroyTimers();
	void _destroyWriterThread();
	void _destroyResourceSystem();
	void _destroyPlatform();

private:
	HGL_Impl(const HGL_Impl& other) = default;
	HGL_Impl(HGL_Impl&& other) = default;
	HGL_Impl& operator=(const HGL_Impl& other) = default;
	HGL_Impl& operator=(HGL_Impl&& other) = default;

public:
	friend class HGLPlatform;

};
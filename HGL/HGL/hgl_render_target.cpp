// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge 
#include "precomp.h"
#include "hgl_impl.h"

//////////////////////////////////////////////////////////////////// Render Targets destructor
void HGL_Impl::_destroyRenderTargets() {
	for (auto rt : *spRenderTargetsList.get()) {
		Target_Free(rt);
	}
}

//////////////////////////////////////////////////////////////////// Target_*
// TODO: Clear framebuffers and memory in HGL destructor.
// Notes:
// - FBO can have texture attachments or render target attachments
// - Texture attachments imply render to texture, render targets imply offscreen
//   rendering. Render targets can store stencil and depth, textures can't.
// - Changing FBOs is slow. Texture/Renderbuffer attachments switching is fast.
// - glFramebufferRenderbuffer to switch renderbuffers. 
// - glFramebufferTexture2D to switch 2D textures.
// - OpenGL ES 2 doesn't require RGBA8888 by default, most modern devices provide it
//   as a transparent extension so I'll use it by default. (otherwise RGB565 or RGBA4444)
// - Renderbuffers aren't required to be pow-of-2. Texture 2D npot is supported on most
//   renderers so enforcing that is not strictly required (I'm doing it anyway).

HTARGET HGL_CALL HGL_Impl::Target_Create(int width, int height, bool zbuffer, bool viewport) {
	GLuint fbno = 0;
	GLuint rtex = 0;
	GLuint depthrt = 0;
	HTEXTURE texture = Texture_Create(width, height);
	if (texture) {
		rtex = _getGLTexture(texture);
		assert(rtex);
		if (rtex) {
			// Gen and bind framebuffer
			glGenFramebuffers(1, &fbno);
			_checkGLError("Target_Create", "Gen framebuffer");
			glBindFramebuffer(GL_FRAMEBUFFER, fbno);
			_checkGLError("Target_Create", "Bind framebuffer");

			HGL_RenderTarget *prt = new HGL_RenderTarget();
			prt->bDepth = zbuffer;
			if (prt->bDepth) {
				prt->depthTarget = depthrt;
			}
			prt->texture = texture;
			prt->frameBuffer = fbno;
			prt->uWidth = width;
			prt->uHeight = height;
			prt->eViewport = viewport? HGL_VIEWPORT_DEFAULT : HGL_VIEWPORT_NOTSET;
			spRenderTargetsList->push_back(prt);

			// Depth attachment
			if (zbuffer) {
				glGenRenderbuffers(1, &depthrt);
				_checkGLError("Target_Create", "Gen render buffers");
				glBindRenderbuffer(GL_RENDERBUFFER, depthrt);
				_checkGLError("Target_Create", "Bind render buffer");
				glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT16,
					width, height);
				_checkGLError("Target_Create", "Set renderbuffer storage");
				glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
					GL_RENDERBUFFER, depthrt);
				_checkGLError("Target_Create", "Attach renderbuffer");
				glBindRenderbuffer(GL_RENDERBUFFER, 0);
			}

			// Texture attachment
			glBindTexture(GL_TEXTURE_2D, rtex);
			_checkGLError("Target_Create", "Bind texture");
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
				rtex, 0);
			_checkGLError("Target_Create", "Attach texture 2D");

			// Verify framebuffer
			GLenum fb_status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
			if (fb_status != GL_FRAMEBUFFER_COMPLETE) {
				std::ostringstream ss;
				ss << "Framebuffer incomplete: ";
				switch (fb_status) {
					case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
						ss << "GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT";
						break;
					case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS:
						ss << "GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS";
						break;
					case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
						ss << "GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT";
						break;
					case GL_FRAMEBUFFER_UNSUPPORTED:
						ss << "GL_FRAMEBUFFER_UNSUPPORTED";
						break;
				}
				_postError(ss.str().c_str());
			}
			assert(fb_status == GL_FRAMEBUFFER_COMPLETE && "Framebuffer wasn't initialized correctly");

			// Done 
			glBindFramebuffer(GL_FRAMEBUFFER, 0);
			glBindTexture(GL_TEXTURE_2D, 0);
			if(prt->bDepth) {
				glBindRenderbuffer(GL_RENDERBUFFER, 0);
			}
			return static_cast<HTARGET>(prt);
		} else {
			strErrorMessage = "Could not retrieve GL texture for render target";
		}
	} else {
		strErrorMessage = "Could not create render target texture.";
	}
	// If it got here, this is a failure
	System_Log(strErrorMessage.c_str());
	return 0;
}

void HGL_CALL HGL_Impl::Target_Free(HTARGET target) {
	auto ifind = std::find(spRenderTargetsList->begin(), spRenderTargetsList->end(), 
		static_cast<HGL_RenderTarget*>(target));
	if (ifind == spRenderTargetsList->end()) {
		strErrorMessage = "Target_Free called on unknown render target.";
		System_Log(strErrorMessage.c_str());
	} else {
		// First unbind for safety:
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, 0, 0);
		glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, 0);
		// Delete: texture target, depth rendertarget, and framebuffer
		auto fbo = (*ifind);
		Texture_Free(fbo->texture);
		if (fbo->bDepth) {
			glDeleteRenderbuffers(1, &fbo->depthTarget);
		}
		glDeleteFramebuffers(1, &fbo->frameBuffer);
	}
}

// Returns a render target's texture handle.
HTEXTURE HGL_CALL HGL_Impl::Target_GetTexture(HTARGET target) {
	auto ifind = std::find(spRenderTargetsList->begin(), spRenderTargetsList->end(),
		static_cast<HGL_RenderTarget*>(target));
	if (ifind == spRenderTargetsList->end()) {
		strErrorMessage = "Target_GetTexturee called on unknown render target.";
		System_Log(strErrorMessage.c_str());
		return 0;
	} else {
		return (*ifind)->texture;
	}
}

/*
You may use the returned texture for rendering to screen or other render targets.

DON'T delete the textures obtained with Target_GetTexture call! The results are unpredictable. 
A render target's texture is deleted automatically during render target deletion.

The texture handle, returned by Target_GetTexture may change when contents of video memory is 
lost (eg. mode switching). So, obtain the handle every time you need it. If you want to use 
cached handles you have to update them in your HGE_GFXRESTOREFUNC function.

Render targets' content can be lost due to events like video mode switching. If you render a 
texture each frame this is not a problem. But if you render it once, at startup - you must 
rerender it upon signal from HGE. To do this - write a rendering function and attach it by 
setting HGE_GFXRESTOREFUNC system state.
*/


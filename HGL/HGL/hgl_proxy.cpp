// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// https://github.com/kvakvs/hge 
#include "precomp.h"

#define HGLDLL
#include "hgl.h"
#include "hgl_impl.h"

static int iRef = 0;
static HGL_Impl* hgl_impl = nullptr;

class HGL_Proxy : public HGL
{
public:
	// Interface
	virtual void HGL_CALL Release() {
		iRef--;
		if(iRef <= 0 && hgl_impl) {
			delete hgl_impl;
			hgl_impl = nullptr;
		}
	}

	// System
	virtual bool HGL_CALL System_Initiate() { return hgl_impl->System_Initiate(); }
	virtual void HGL_CALL System_Shutdown() { return hgl_impl->System_Shutdown(); }
	virtual bool HGL_CALL System_Start() { return hgl_impl->System_Start(); }

	// System: Utils
	virtual const char*	HGL_CALL System_GetErrorMessage() { return hgl_impl->System_GetErrorMessage(); }
	virtual void		HGL_CALL System_Log(const char *format, ...) {
		va_list args;
		va_start(args, format);
		hgl_impl->System_Log(format, args);
		va_end(args);
	}

	// System: Set State
	virtual void HGL_CALL System_SetState(hglBoolState state, bool value) { return hgl_impl->System_SetState(state, value); }
	virtual void HGL_CALL System_SetState(hglFuncState state, hglCallback value) { return hgl_impl->System_SetState(state, value); }
	virtual void HGL_CALL System_SetState(hglHwndState state, void* value) { return hgl_impl->System_SetState(state, value); }
	virtual void HGL_CALL System_SetState(hglIntState state, int value) { return hgl_impl->System_SetState(state, value); }
	virtual void HGL_CALL System_SetState(hglStringState state, const char *value) { return hgl_impl->System_SetState(state, value); }

	// System: Get State
	virtual bool		HGL_CALL System_GetState(hglBoolState state) { return hgl_impl->System_GetState(state); }
	virtual hglCallback HGL_CALL System_GetState(hglFuncState state) { return hgl_impl->System_GetState(state); }
	virtual void*		HGL_CALL System_GetState(hglHwndState state) { return hgl_impl->System_GetState(state); }
	virtual int			HGL_CALL System_GetState(hglIntState state)	{ return hgl_impl->System_GetState(state); }
	virtual const char* HGL_CALL System_GetState(hglStringState state) { return hgl_impl->System_GetState(state); }


	// Resource system
	virtual void*       HGL_CALL Resource_Load(const char *filename, unsigned *size = 0)				{ return hgl_impl->Resource_Load(filename, size); }
	virtual void        HGL_CALL Resource_Free(void *res)												{ return hgl_impl->Resource_Free(res); }
	virtual bool        HGL_CALL Resource_AttachPack(const char *filename, const char *password = 0)	{ return hgl_impl->Resource_AttachPack(filename, password); }
	virtual void        HGL_CALL Resource_RemovePack(const char *filename)								{ return hgl_impl->Resource_RemovePack(filename); }
	virtual void        HGL_CALL Resource_RemoveAllPacks()												{ return hgl_impl->Resource_RemoveAllPacks(); }
	virtual const char* HGL_CALL Resource_MakePath(const char *filename = 0)							{ return hgl_impl->Resource_MakePath(filename); }
	virtual const char* HGL_CALL Resource_EnumFiles(const char *wildcard = 0)							{ return hgl_impl->Resource_EnumFiles(wildcard); }
	virtual const char* HGL_CALL Resource_EnumFolders(const char *wildcard = 0)							{ return hgl_impl->Resource_EnumFolders(wildcard); }
	
	// INI parsing
	virtual void        HGL_CALL Ini_SetInt(const char *section, const char *name, int value)				{ return hgl_impl->Ini_SetInt(section, name, value); }
	virtual int         HGL_CALL Ini_GetInt(const char *section, const char *name, int def_val)				{ return hgl_impl->Ini_GetInt(section, name, def_val); }
	virtual void        HGL_CALL Ini_SetFloat(const char *section, const char *name, float value)			{ return hgl_impl->Ini_SetFloat(section, name, value); }
	virtual float       HGL_CALL Ini_GetFloat(const char *section, const char *name, float def_val)			{ return hgl_impl->Ini_GetFloat(section, name, def_val); }
	virtual void        HGL_CALL Ini_SetString(const char *section, const char *name, const char *value)	{ return hgl_impl->Ini_SetString(section, name, value); }
	virtual const char* HGL_CALL Ini_GetString(const char *section, const char *name, const char *def_val)	{ return hgl_impl->Ini_GetString(section, name, def_val); }

	// Input
	virtual void        HGL_CALL Input_GetMousePos(float *x, float *y) { return hgl_impl->Input_GetMousePos(x, y); }
	virtual void        HGL_CALL Input_SetMousePos(float x, float y) { return hgl_impl->Input_SetMousePos(x, y); }
	virtual int         HGL_CALL Input_GetMouseWheel() { return hgl_impl->Input_GetMouseWheel(); }
	virtual bool        HGL_CALL Input_IsMouseOver() { return hgl_impl->Input_IsMouseOver(); }
	virtual bool        HGL_CALL Input_KeyDown(int key) { return hgl_impl->Input_KeyDown(key); }
	virtual bool        HGL_CALL Input_KeyUp(int key) { return hgl_impl->Input_KeyUp(key); }
	virtual bool        HGL_CALL Input_GetKeyState(int key) { return hgl_impl->Input_GetKeyState(key); }
	virtual const char* HGL_CALL Input_GetKeyName(int key) { return hgl_impl->Input_GetKeyName(key); }
	virtual int         HGL_CALL Input_GetKey() { return hgl_impl->Input_GetKey(); }
	virtual int         HGL_CALL Input_GetChar() { return hgl_impl->Input_GetChar(); }
	virtual bool		HGL_CALL Input_IsPadAvailable(hglGamePad_t pad) { return hgl_impl->Input_IsPadAvailable(pad); }

	// Action map
	virtual HACTION		HGL_CALL Action_Create(const char *name) { return hgl_impl->Action_Create(name); }
	virtual void		HGL_CALL Action_ResetBindings(HACTION action) { return hgl_impl->Action_ResetBindings(action); }
	virtual void		HGL_CALL Action_Free(HACTION action) { return hgl_impl->Action_Free(action); }
	virtual void		HGL_CALL Action_FreeAll() { return hgl_impl->Action_FreeAll(); }
	virtual hglAction_t	HGL_CALL Action_GetState(HACTION action) { return hgl_impl->Action_GetState(action); }
	virtual float		HGL_CALL Action_GetFloat(HACTION action) { return hgl_impl->Action_GetFloat(action); }
	virtual void		HGL_CALL Action_AddKey(HACTION action, hglKeyCode_t keycode) { return hgl_impl->Action_AddKey(action, keycode); }
	virtual void		HGL_CALL Action_AddMouseButton(HACTION action, hglKeyCode_t buttoncode) { return hgl_impl->Action_AddMouseButton(action, buttoncode); }
	virtual void		HGL_CALL Action_AddPadButton(HACTION action, hglPadCode_t buttoncode) { return hgl_impl->Action_AddPadButton(action, buttoncode); }
	virtual void		HGL_CALL Action_AddPadAxis(HACTION action, hglPadCode_t axiscode) { return hgl_impl->Action_AddPadAxis(action, axiscode); }


	// Graphics
	virtual bool        HGL_CALL Gfx_BeginScene(HTARGET target = 0) { return hgl_impl->Gfx_BeginScene(target); }
	virtual void        HGL_CALL Gfx_EndScene() { return hgl_impl->Gfx_EndScene(); }
	virtual void        HGL_CALL Gfx_Clear(unsigned color) { return hgl_impl->Gfx_Clear(color); }
	virtual void        HGL_CALL Gfx_RenderLine(float x1, float y1, float x2, float y2, unsigned color = 0xFFFFFFFF, float z = 0.5f) { return hgl_impl->Gfx_RenderLine(x1, y1, x2, y2, color, z); }
	virtual void        HGL_CALL Gfx_RenderTriple(const hglTriple *triple) { return hgl_impl->Gfx_RenderTriple(triple); }
	virtual void        HGL_CALL Gfx_RenderQuad(const hglQuad *quad) { return hgl_impl->Gfx_RenderQuad(quad); }
	virtual hglVertex*  HGL_CALL Gfx_StartBatch(int prim_type, HTEXTURE tex, int blend, int *max_prim) { return hgl_impl->Gfx_StartBatch(prim_type, tex, blend, max_prim); }
	virtual void        HGL_CALL Gfx_FinishBatch(int nprim) { return hgl_impl->Gfx_FinishBatch(nprim); }
	virtual void        HGL_CALL Gfx_SetClipping(int x = 0, int y = 0, int w = 0, int h = 0) { return hgl_impl->Gfx_SetClipping(x, y, w, h); }
	virtual void        HGL_CALL Gfx_SetTransform(float x = 0, float y = 0, float dx = 0, float dy = 0, float rot = 0, float hscale = 0, float vscale = 0) { return hgl_impl->Gfx_SetTransform(x, y, dx, dy, rot, hscale, vscale); }
	virtual int			HGL_CALL Gfx_GetWidth() const { return hgl_impl->Gfx_GetWidth(); }
	virtual int			HGL_CALL Gfx_GetHeight() const { return hgl_impl->Gfx_GetHeight(); }

	// Sound effects
	virtual HEFFECT		HGL_CALL Effect_Load(const char *filename, size_t size = 0) { return hgl_impl->Effect_Load(filename, size); }
	virtual void		HGL_CALL Effect_Free(HEFFECT eff) { return hgl_impl->Effect_Free(eff); }
	virtual HCHANNEL	HGL_CALL Effect_Play(HEFFECT eff) { return hgl_impl->Effect_Play(eff); }
	virtual HCHANNEL	HGL_CALL Effect_PlayEx(HEFFECT eff, int volume = 100, int pan = 0, float pitch = 1.0f, bool loop = false) { return hgl_impl->Effect_PlayEx(eff, volume, pan, pitch, loop); }
	
	// Channels
	virtual void        HGL_CALL Channel_SetPanning(HCHANNEL chn, int pan) { return hgl_impl->Channel_SetPanning(chn, pan); }
	virtual void        HGL_CALL Channel_SetVolume(HCHANNEL chn, int volume) { return hgl_impl->Channel_SetVolume(chn, volume); }
	virtual void        HGL_CALL Channel_SetPitch(HCHANNEL chn, float pitch) { return hgl_impl->Channel_SetPitch(chn, pitch); }
	virtual void        HGL_CALL Channel_Pause(HCHANNEL chn) { return hgl_impl->Channel_Pause(chn); }
	virtual void        HGL_CALL Channel_Resume(HCHANNEL chn) { return hgl_impl->Channel_Resume(chn); }
	virtual void        HGL_CALL Channel_Stop(HCHANNEL chn) { return hgl_impl->Channel_Stop(chn); }
	virtual void        HGL_CALL Channel_PauseAll() { return hgl_impl->Channel_PauseAll(); }
	virtual void        HGL_CALL Channel_ResumeAll() { return hgl_impl->Channel_ResumeAll(); }
	virtual void        HGL_CALL Channel_StopAll() { return hgl_impl->Channel_StopAll(); }
	virtual bool        HGL_CALL Channel_IsPlaying(HCHANNEL chn) { return hgl_impl->Channel_IsPlaying(chn); }
	virtual float       HGL_CALL Channel_GetLength(HCHANNEL chn) { return hgl_impl->Channel_GetLength(chn); }
	virtual float       HGL_CALL Channel_GetPos(HCHANNEL chn) { return hgl_impl->Channel_GetPos(chn); }
	virtual void        HGL_CALL Channel_SetPos(HCHANNEL chn, float fSeconds) { return hgl_impl->Channel_SetPos(chn, fSeconds); }
	virtual void        HGL_CALL Channel_SlideTo(HCHANNEL channel, float time, int volume, int pan = -101, float pitch = -1) { return hgl_impl->Channel_SlideTo(channel, time, volume, pan, pitch); }
	virtual bool        HGL_CALL Channel_IsSliding(HCHANNEL channel) { return hgl_impl->Channel_IsSliding(channel); }

	// Shaders
	virtual HSHADER		HGL_CALL Shader_Create(const char *filename) { return hgl_impl->Shader_Create(filename); }
	virtual void		HGL_CALL Shader_Free(HSHADER shader) { return hgl_impl->Shader_Free(shader); }
	virtual void		HGL_CALL Gfx_SetShader(HSHADER shader) { return hgl_impl->Gfx_SetShader(shader); }

	// Random
	virtual void        HGL_CALL Random_Seed(int seed = 0) { return hgl_impl->Random_Seed(seed); }
	virtual int         HGL_CALL Random_Int(int min, int max) { return hgl_impl->Random_Int(min, max); }
	virtual float       HGL_CALL Random_Float(float min = 0.0f, float max = 1.0f) { return hgl_impl->Random_Float(min, max); }

	// Timer
	virtual float       HGL_CALL Timer_GetTime() { return hgl_impl->Timer_GetTime(); }
	virtual float       HGL_CALL Timer_GetDelta() { return hgl_impl->Timer_GetDelta(); }
	virtual double		HGL_CALL Timer_GetDeltaNS() { return hgl_impl->Timer_GetDeltaNS(); }
	virtual int         HGL_CALL Timer_GetFPS() { return hgl_impl->Timer_GetFPS(); }

	// Render targets
	virtual HTARGET     HGL_CALL Target_Create(int width, int height, bool zbuffer, bool viewport = true) { return hgl_impl->Target_Create(width, height, zbuffer, viewport); }
	virtual void        HGL_CALL Target_Free(HTARGET target) { return hgl_impl->Target_Free(target); }
	virtual HTEXTURE    HGL_CALL Target_GetTexture(HTARGET target) { return hgl_impl->Target_GetTexture(target); }

	// Textures
	virtual HTEXTURE    HGL_CALL Texture_Create(int width, int height) { return hgl_impl->Texture_Create(width, height); }
	virtual HTEXTURE    HGL_CALL Texture_Load(const char *filename, unsigned size = 0, bool bMipmap = false) { return hgl_impl->Texture_Load(filename, size, bMipmap); }
	virtual void        HGL_CALL Texture_Free(HTEXTURE tex) { return hgl_impl->Texture_Free(tex); }
	virtual int			HGL_CALL Texture_GetWidth(HTEXTURE tex, bool bOriginal = false) { return hgl_impl->Texture_GetWidth(tex, bOriginal); }
	virtual int			HGL_CALL Texture_GetHeight(HTEXTURE tex, bool bOriginal = false) { return hgl_impl->Texture_GetHeight(tex, bOriginal); }
	virtual unsigned *	HGL_CALL Texture_Lock(HTEXTURE tex, bool bReadOnly = true, int left = 0, int top = 0, int width = 0, int height = 0) { return hgl_impl->Texture_Lock(tex, bReadOnly, left, top, width, height); }
	virtual void		HGL_CALL Texture_Unlock(HTEXTURE tex) { return hgl_impl->Texture_Unlock(tex); }

} hgl_proxy;

extern "C" {
	HGL_EXPORT HGL* hglCreate(int version)
	{
		if(version == HGL_VERSION) {
			iRef++;
			if(!hgl_impl) {
				hgl_impl = new HGL_Impl();
			}
			return &hgl_proxy;
		} else {
			return 0;
		}
	}
}

precision mediump float;
	
varying vec2			vTexCoords;
varying lowp vec4		vColor;

uniform sampler2D		uTexture;

void main() {
	vec4 Color;
	Color = (1.0 - texture2D(uTexture, vTexCoords)) + vColor;
	Color.a = 1.0;
	gl_FragColor = Color;
}
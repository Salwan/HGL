precision mediump float;
	
varying vec2			vTexCoords;
varying lowp vec4		vColor;

uniform sampler2D		uTexture;

void main() {
	vec4 Color;
	Color.a = 1.0;
	Color.rgb = vec3(0.5);
	Color -= texture2D(uTexture, vTexCoords.xy-0.001)*2.0;
	Color += texture2D(uTexture, vTexCoords.xy+0.001)*2.0;
	Color.r = 1.0 - ((Color.r + Color.g + Color.b)/3.0);
	Color.g = Color.r;
	Color.b = Color.r;
	gl_FragColor = Color;
}
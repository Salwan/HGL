precision mediump float;

varying vec2			vTexCoords;
varying lowp vec4		vColor;

uniform sampler2D		uTexture;

void main() {
	vec4 Color;
	Color =  texture2D( uTexture, vTexCoords);
	for(float i = -0.015; i <= 0.015; i+=0.003) {
		Color += texture2D( uTexture, vTexCoords+i);
	}
	Color = (Color / 10.0) + vColor;
	gl_FragColor = Color;
}

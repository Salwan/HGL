//// Sal's HGL-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge 

// Copy the files "particles.png", "menu.wav",
// "font1.fnt", "font1.png" and "trail.psi" from
// the folder "precompiled" to the folder with
// executable file. Also copy hgl.dll and bass.dll
// to the same folder.


#include <hgl.h>
#include <hgl_font.h>
#include <Hgl_distort.h>

#include <math.h>


// Pointer to the HGL interface.
// Helper classes require this to work.
HGL *hgl = 0;

HTEXTURE            tex;
HSHADER             shad1;
HSHADER             shad2;
HSHADER             shad3;
HSHADER             currShader = NULL;
HEFFECT				testSound = NULL;

// Pointers to the HGL objects we will use
hglDistortionMesh*  dis;
hglFont*            fnt;

// Some "gameplay" variables
const int nRows = 16;
const int nCols = 16;
const float cellw = 512.0f / (nCols - 1);
const float cellh = 512.0f / (nRows - 1);

const float meshx = 144;
const float meshy = 44;

bool useShader = false;

bool FrameFunc()
{
	float dt = hgl->Timer_GetDelta();
	static float t = 0.0f;
	static int trans = 0;

	int i, j, col;
	float r, a, dx, dy;

	t += dt;

	// Process keys
	switch (hgl->Input_GetKey()) {
	case HGLK_ESCAPE:
		return true;

	case HGLK_SPACE:
		if (++trans > 2) trans = 0;
		dis->Clear(0xFF000000);
		useShader = false;
		break;

	case HGLK_1:
		currShader = NULL;
		break;

	case HGLK_2:
		currShader = shad1;
		break;

	case HGLK_3:
		currShader = shad2;
		break;

	case HGLK_4:
		currShader = shad3;
		break;
	}
	
	// Calculate new displacements and coloring for one of the three effects
	switch (trans) {
	case 0: for (i = 1;i<nRows - 1;i++)
		for (j = 1;j<nCols - 1;j++) {
			dis->SetDisplacement(j, i, cosf(t * 10 + (i + j) / 2) * 5, sinf(t * 10 + (i + j) / 2) * 5, HGLDISP_NODE);
		}
			break;

	case 1: for (i = 0;i<nRows;i++)
		for (j = 1;j<nCols - 1;j++) {
			dis->SetDisplacement(j, i, cosf(t * 5 + j / 2) * 15, 0, HGLDISP_NODE);
			col = int((cosf(t * 5 + (i + j) / 2) + 1) * 35);
			dis->SetColor(j, i, 0xFF << 24 | col << 16 | col << 8 | col);
		}
			break;

	case 2: for (i = 0;i<nRows;i++)
		for (j = 0;j<nCols;j++) {
			r = sqrtf(powf(j - (float)nCols / 2, 2) + powf(i - (float)nRows / 2, 2));
			a = r*cosf(t * 2)*0.1f;
			dx = sinf(a)*(i*cellh - 256) + cosf(a)*(j*cellw - 256);
			dy = cosf(a)*(i*cellh - 256) - sinf(a)*(j*cellw - 256);
			dis->SetDisplacement(j, i, dx, dy, HGLDISP_CENTER);
			col = int((cos(r + t * 4) + 1) * 40);
			dis->SetColor(j, i, 0xFF << 24 | col << 16 | (col / 2) << 8);
		}
			break;
	}
	return false;
}


bool RenderFunc()
{
	// Render graphics
	hgl->Gfx_BeginScene();
	hgl->Gfx_Clear(0x202050);
	hgl->Gfx_SetShader(currShader);
	dis->Render(meshx, meshy);
	hgl->Gfx_SetShader(NULL);
	float mx, my;
	hgl->Input_GetMousePos(&mx, &my);
	fnt->printf(5, 5, HGLTEXT_LEFT, "dt:%.4f\nFPS:%d\n\nPress SPACE,1,2,3,4\nChar: %d, Mouse: %d, %d, L%d", 
		hgl->Timer_GetDelta(), hgl->Timer_GetFPS(), hgl->Input_GetKey(), (int)mx, (int)my,
		(hgl->Input_GetKeyState(HGLM_LBUTTON)? 1 : 0));
	hgl->Gfx_EndScene();

	return false;
}


int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	hgl = hglCreate(HGL_VERSION);

	hgl->System_SetState(HGL_LOGFILE, "hgl_tut05.log");
	hgl->System_SetState(HGL_FRAMEFUNC, FrameFunc);
	hgl->System_SetState(HGL_RENDERFUNC, RenderFunc);
	hgl->System_SetState(HGL_TITLE, "HGL Tutorial 05 - Using distortion mesh");
	hgl->System_SetState(HGL_WINDOWED, true);
	hgl->System_SetState(HGL_SCREENWIDTH, 1280);
	hgl->System_SetState(HGL_SCREENHEIGHT, 720);
	hgl->System_SetState(HGL_SCREENBPP, 32);
	hgl->System_SetState(HGL_USESOUND, false);
	hgl->System_SetState(HGL_HIDEMOUSE, false);
	hgl->System_SetState(HGL_PERFORMANCEMODE, PERFORMANCE_HIGH);

	if (hgl->System_Initiate()) {

		// Load sound and texture
		tex = hgl->Texture_Load("assets/texture2.jpg");
		if (!tex) {
			// If one of the data files is not found, display
			// an error message and shutdown.
			MessageBoxA(NULL, "Can't load TEXTURE.JPG", "Error", MB_OK | MB_ICONERROR | MB_APPLMODAL);
			hgl->System_Shutdown();
			hgl->Release();
			return 0;
		}

		shad1 = hgl->Shader_Create("assets/shader1.fsh");
		shad2 = hgl->Shader_Create("assets/shader2.fsh");
		shad3 = hgl->Shader_Create("assets/shader3.fsh");

		// Create a distortion mesh
		dis = new hglDistortionMesh(nCols, nRows);
		dis->SetTexture(tex);
		dis->SetTextureRect(0, 0, 512, 512);
		dis->SetBlendMode(BLEND_COLORADD | BLEND_ALPHABLEND | BLEND_ZWRITE);
		dis->Clear(0xFF000000);

		// Load a font
		fnt = new hglFont("assets/font1.fnt");

		// Load and play test sound
		testSound = hgl->Effect_Load("assets/test.ogg");
		hgl->Effect_PlayEx(testSound);

		// Let's rock now!
		hgl->System_Start();

		// Delete created objects and free loaded resources
		delete fnt;
		delete dis;
		hgl->Shader_Free(shad1);
		hgl->Shader_Free(shad2);
		hgl->Shader_Free(shad3);
		hgl->Texture_Free(tex);
		hgl->Effect_Free(testSound);
	}

	// Clean up and shutdown
	hgl->System_Shutdown();
	hgl->Release();
	return 0;
}

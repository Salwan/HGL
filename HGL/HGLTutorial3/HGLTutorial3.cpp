//// Sal's HGE-compatible mini engine using EGL, OpenGL ES 2
// Copyright (C) 2017, Cloud Mill Games
// Haaf's Game Engine 1.8
// Copyright (C) 2003 - 2007, Relish Games
// https://github.com/kvakvs/hge 


// Copy the files "particles.png", "menu.wav",
// "font1.fnt", "font1.png" and "trail.psi" from
// the folder "precompiled" to the folder with
// executable file. Also copy hge.dll and bass.dll
// to the same folder.

#include <fstream>
#include <sstream>

#include <hgl.h>
#include <hgl_sprite.h>
#include <hgl_font.h>
#include <hgl_particle.h>

// Pointer to the HGL interface.
// Helper classes require this to work.
HGL *hgl = 0;


// Pointers to the HGE objects we will use
hglSprite*			spr;
hglSprite*			spt [2];
hglFont*			fnt;
hglParticleSystem*	par [2];

// Handles for HGL resourcces
HTEXTURE			tex;
HEFFECT				snd;

// Some "gameplay" variables
float x = 100.0f, y = 100.0f;
float dx = 0.0f, dy = 0.0f;

const float speed = 90;
const float friction = 0.98f;

// Play sound effect
void boom() {
	int pan = int((x - 400) / 4);
	float pitch = (dx*dx + dy*dy)*0.0005f + 0.2f;
	hgl->Effect_PlayEx(snd, 100, pan, pitch);
}

bool FrameFunc()
{
	float dt = hgl->Timer_GetDelta();

	// Process keys
	if(hgl->Input_GetKeyState(HGLK_ESCAPE)) return true;
	if(hgl->Input_GetKeyState(HGLK_LEFT)) dx -= speed*dt;
	if(hgl->Input_GetKeyState(HGLK_RIGHT)) dx += speed*dt;
	if(hgl->Input_GetKeyState(HGLK_UP)) dy -= speed*dt;
	if(hgl->Input_GetKeyState(HGLK_DOWN)) dy += speed*dt;

	// Do some movement calculations and collision detection	
	dx *= friction; dy *= friction; x += dx; y += dy;
	if(x>784) { x = 784 - (x - 784); dx = -dx; boom(); }
	if(x<16) { x = 16 + 16 - x; dx = -dx; boom(); }
	if(y>584) { y = 584 - (y - 584); dy = -dy; boom(); }
	if(y<16) { y = 16 + 16 - y; dy = -dy; boom(); }

	// Update particle system
	par[0]->info.nEmission = (int)(dx*dx + dy*dy) * 2;
	par[0]->MoveTo(x, y);
	par[0]->Update(dt);
	par[1]->Update(dt);

	return false;
}


bool RenderFunc()
{
	// Render graphics
	hgl->Gfx_BeginScene();
	hgl->Gfx_Clear(0);

	par[0]->Render();
	par[1]->Render();

	spr->Render(x, y);

	{
		// Max and min time deltas (to see variation in framerate when iFPS > 1)
		static float delta_accum = 0.0f;
		static float max_delta = 0.0f;
		static float min_delta = 1.0f;
		static float last_max_delta = 0.0f;
		static float last_min_delta = 0.0f;
		const float dt = hgl->Timer_GetDelta();
		delta_accum += dt;
		if (delta_accum > 1.0f) {
			delta_accum -= 1.0f;
			last_max_delta = max_delta;
			last_min_delta = min_delta;
			max_delta = 0.0f;
			min_delta = 1.0f;
		}
		max_delta = dt > max_delta? dt : max_delta;
		min_delta = dt < min_delta? dt : min_delta;
		fnt->printf(5, 5, HGLTEXT_LEFT, "dt:%.3f (%.3f to %.3f)\nFPS:%d", 
			dt, last_min_delta, last_max_delta, hgl->Timer_GetFPS());
	}

	hgl->Gfx_EndScene();

	return false;
}


int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	hgl = hglCreate(HGL_VERSION);

	hgl->System_SetState(HGL_LOGFILE, "hgl_tut03.log");
	hgl->System_SetState(HGL_FRAMEFUNC, FrameFunc);
	hgl->System_SetState(HGL_RENDERFUNC, RenderFunc);
	hgl->System_SetState(HGL_TITLE, "HGL Tutorial 03 - Using helper classes");
	hgl->System_SetState(HGL_FPS, 100);
	hgl->System_SetState(HGL_WINDOWED, true);
	hgl->System_SetState(HGL_SCREENWIDTH, 800);
	hgl->System_SetState(HGL_SCREENHEIGHT, 600);
	hgl->System_SetState(HGL_SCREENBPP, 32);

	if(hgl->System_Initiate()) {

		// Load sound and texture
		snd = hgl->Effect_Load("assets/menu.wav");
		tex = hgl->Texture_Load("assets/particles.png");
		if(!snd || !tex) {
			// If one of the data files is not found, display
			// an error message and shutdown.
			MessageBoxA(NULL, 
				"Can't load one of the following files:\nMENU.WAV, PARTICLES.PNG, FONT1.FNT, FONT1.PNG, TRAIL.PSI", 
				"Error", MB_OK | MB_ICONERROR | MB_APPLMODAL);
			hgl->System_Shutdown();
			hgl->Release();
			return 0;
		}

		// Create and set up a sprite
		spr = new hglSprite(tex, 96, 64, 32, 32);
		spr->SetColor(0xFFFFA000);
		spr->SetHotSpot(16, 16);

		// Load a font
		fnt = new hglFont("assets/font1.fnt");
		// Create and set up a particle system
		spt[0] = new hglSprite(tex, 32, 32, 32, 32);
		spt[0]->SetBlendMode(BLEND_DEFAULT_PAR);
		spt[0]->SetHotSpot(16, 16);

		spt[1] = new hglSprite(tex, 0, 32, 32, 32);
		spt[1]->SetBlendMode(BLEND_DEFAULT_PAR);
		spt[1]->SetHotSpot(16, 16);

		par[0] = new hglParticleSystem("assets/trail.psi", spt[0]);
		par[0]->Fire();

		par[1] = new hglParticleSystem("assets/particle9.psi", spt[1]);
		par[1]->FireAt(400, 300);

		// Let's rock now!
		hgl->System_Start();

		// Delete created objects and free loaded resources
		delete par[0];
		delete par[1];
		delete fnt;
		delete spt[0];
		delete spt[1];
		delete spr;
		hgl->Texture_Free(tex);
		hgl->Effect_Free(snd);
	}

	// Clean up and shutdown
	hgl->System_Shutdown();
	hgl->Release();
	return 0;
}
